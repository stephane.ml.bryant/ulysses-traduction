La fille blonde de chez Thornton tapissait l’intérieur du panier d’osier de fibres bruissantes. Braise Boylan lui passa la bouteille emmaillotée dans du papier de soie puis un petit bocal.

– Mettez ça d’abord, voulez-vous, dit-il.

– Oui, monsieur, dit la fille blonde. Et les fruits sur le dessus.

– Cela fera l’affaire, tiercé gagnant, dit Braise Boylan.

Elle disposa les opulentes poires avec soin, alternant têtes et queues, et entre elle des pêches aux joues rougissantes.

Braise Boylan allait et venait dans ses souliers fauves, neufs, arpentant la boutique qui fleurait le fruit, prenant les fruits en main, fruits frais juteux ridés et les tomates rouges et rebondies, reniflant les odeurs.

H. E. L. Y’ S défilèrent devant lui sous leurs blancs hauts-de-forme et dépassèrent l’allée de Tanger, avançant d’un pas lourd vers leur objectif.

Il se détourna brusquement d’un panier de fraise, tira une montre en or de son gousset et la tint au bout de sa chaîne.

– Pouvez-vous les envoyer par tram ? Tout de suite ?

La silhouette d’un dos sombre, sous la galerie des Marchands, passait en revue des livres sur la carriole d’un bouquiniste.

– Certainement, monsieur. Est-ce en ville ?

– Oh, oui, dit Braise Boylan. À dix minutes.

La fille blonde lui tendit une fiche et un crayon.

– Voulez-vous écrire l’adresse, monsieur ?

Braise Boylan écrivit au comptoir et repoussa la fiche vers elle.

– Envoyez-le de suite, voulez-vous ? dit-il. C’est pour un invalide.

– Oui, monsieur. Je m’en occupe, monsieur.

Braise Boylan fit sonner de gaies piécettes dans la poche de son pantalon.

– Quels sont les dégâts ? demanda-t-il.

Les fins doigts de la fille blonde partirent reconnaître les fruits.

Braise Boylan regardait dans l’échancrure de sa blouse. Une jeune poularde. Il prit un œillet rouge du long vase en verre.

– Pour moi ? demanda-t-il galamment.

La file blonde lui jeta un regard oblique, se redressa néanmoins, il a sa cravate un peu tordue, rougit.

– Oui, monsieur, dit-elle.

Se penchant intentionnellement elle reprit le compte des poires opulentes et pêches rougissantes.

Braise Boylan scrutait l’échancrure avec une complaisance accrue, tout sourire, la tige de la fleur rouge entre les dents.

– Puis-je dire un mot à votre téléphone, mamzelle ? demanda-t-il, fripon.

***

