Du cadran en solaire vers la porte James marchait M. Kernan, enchanté de la commande qu’il venait de placer pour Pulbrook et Robertson, le port fier le long de la rue James, à la parade devant les bureaux de Shackleton. Comme je te l’ai embobiné. Comment allez-vous, M. Crimmins ? À merveille, monsieur. J’avais peur que vous ne soyez dans votre autre établissement, à Pimlico. Comment vont les affaires ? On survit. Un temps magnifique. Oui, vraiment. Bon pour le pays. Ces paysans sont toujours à grogner. Je vous prendrai un doigt de votre meilleur gin, M. Crimmins. Un petit gin, monsieur. Oui, monsieur. Effroyable cette explosion du General Slocum. Terrible, terrible ! Un millier de victimes. Et des scènes déchirantes. Des hommes qui piétinent femmes et enfants. Une telle brutalité. Qu’est ce qui se dit de la cause ? Combustion spontanée. Des révélations scandaleuses. Pas un canot de sauvetage qui flotte et les tuyaux d’incendie tous crevés. Ce que je ne peux comprendre c’est comment les inspecteurs ont pu permettre qu’un tel bateau… Voilà qui est parler M. Crimmins. Vous savez pourquoi ? Pots-de-vin ? Est-ce un fait établi ? Pas l’ombre d’un doute. Eh bien, voyez-moi ça. Et on dit que l’Amérique est le pays de la liberté. Moi qui pensais qu’on était mal lotis ici.

Je lui ai souri. *L’amérique,* je lui ai glissé, comme ça, en douceur. *Qu’est-ce que c’est ? Le rebut de tous les pays, dont le nôtre. N’est-ce pas ainsi ?* Voilà un fait établi.

La vénalité, mon cher monsieur. Eh bien, cela va de soi, là où l’argent coule il y a toujours quelqu’un pour se baisser.

L’ai vu qui zieutait ma redingote. L’habit, ça joue. Rien ne vaut une allure classy. Ça les fait basculer.

— Hello, Simon, dit le père Cowley. Comment cela va-t-il ?

— Hello, Bob, mon vieux, répondit M. Dedalus, s’arrêtant.

M. Kernan s’arrêta et bomba le torse devant la glace inclinée de Peter Kennedy, coiffeur. Classe redingote, pas à dire. De chez Scott sur la rue Dawson. Vaut bien le demi-souverain que j’en ai donné à Neary. Pas moins de trois guinées de confection, sûr. Me va comme un gant. D’un bourge du club de la rue Kildare, probablement. John Mulligan, le directeur de la banque Hibernian, m’a jeté un sacré coup d’œil hier sur le pont Carlisle comme s’il se souvenait de moi.

Ahem ! Faut incarner un personnage avec ces gars-là. Chevalier du commerce. Gentilhomme. Et maintenant, M. Crimmins, voudrez-vous bien nous honorer à nouveau de votre clientèle, monsieur. La coupe égaie mais n’égare pas, comme dit le proverbe.

North wall et quai John Robertson, entre coques et chaînes d’ancrage, naviguant vers l’ouest, porté par un esquif, un rebut froissé, secoué par l’écume du ferry, Élie arrive.

M. Kernan lança un dernier regard à son image. Haut en couleur, évidemment. Moustache poivre et sel. Un officier de retour des Indes. Il portait crânement son corps trapu, les pieds guêtrés, carrant les épaules, en avant ! Est-ce le frère de Ned Lambert de l’autre côté, Sam ? Hein ? Oui. Il lui ressemble bougrement. Non. Le pare-brise de cette automobile en plein soleil. Comme un flash. Bougrement ressemblant.

Ahem ! l’essence brûlante du genièvre lui réchauffait l’intérieur, attisait sa respiration. Une bonne goutte de gin, bougre. Les basques de sa redingote clignotaient dans la lumière crue du soleil au rythme de son pas épais.

Par là on a pendu Emmet, pendu, écartelé, débité. La corde noire et graisseuse. Les chiens qui lapaient le sang des pavés quand la femme du lord lieutenant passa dans son fiacre.

Une sombre époque, bougre. Allons, allons. Finie et enterrée. De fameux soiffards aussi. Tournaient à quatre bouteilles.

Voyons voir. Est-il enterré à saint Michan ? Ou plutôt non, enterrement de minuit à Glasnevin. Le corps qu’on amène par une porte secrète dans le mur. Dignam y est maintenant. Parti sans crier gare. Allons, allons. Autant tourner par là. Faisons un détour.

M. Kernan tourna et descendit la pente de la rue Watling au coin de la salle de la salle d’attente de Guinness. Devant les magasins de la Compagnie des Distillateurs de Dublin, un bahut sans client ni cocher, les reines nouées à la roue. Bougrement dangereux. Encore un pedzouille de Tipperary qui met en danger la vie des citoyens. Cheval emballé.

Denis Breen et ses volumes, las d’avoir attendu une heure dans les bureaux de John Henry Menton, marchait au-devant de sa femme sur le pont O’Connell, en direction des bureaux de MM. Collis et Ward.

M. Kernan approchait de la rue de l’Isle.

L’époque des troubles. Faut que je demande à Ned Lambert de me prêter ces réminiscences de sir Jonah Barrington. Aujourd’hui avec la distance tout ça parait comme une sorte d’arrangement rétrospectif. Tripot chez Daly. Point de tricheurs à l’époque. Un de ces gars s’est retrouvé la main clouée à la table par une dague. Quelque-part par là lord Edward Fitzgerald a échappé au major Sirr. Les étables derrière la maison Moira.

Bougrement bon ce gin.

Un superbe jeune gentleman. Bon sang ne saurait mentir. Ce scélérat, ce messire de pacotille, avec ces gants violets l’a livré. Sûr qu’ils étaient du mauvais côté. En ces jours de ténèbres ils s’élevèrent. Superbe poème : Ingram. C’étaient des gentilshommes. Ben Dollard chante cette ballade d’une manière émouvante. Interprétation magistrale.

*Au siège de Ross mon père tomba.*

Une cavalcade au petit trot passa le long du quai Pembroke, les éclaireurs tressautant, sautant sur leur, sur leurs selles. Redingotes. Ombrelles pastel.

M. Kernan s’empressa, vite essoufflé.

Son excellence ! Pas de bol ! Manqué d’un rien. Bougre ! Quel dommage !

***




