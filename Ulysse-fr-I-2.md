– Toi, Cochrane, quelle cité l’a envoyé chercher ?

– Tarente, monsieur

– Très bien. Et ?

– ll y a eu une bataille, monsieur.

– Très bien. Où ?

Le visage vide du garçon interrogea la fenêtre vide.

Filé par les filles de la mémoire. Et pourtant cela a eu lieu, d’une certaine manière sinon de la manière dont la mémoire l’a tissé. Rengaine de l’impatience, donc, sourd battement des ailes de l’outrance, Blake. J’entends s’effondrer l’espace entier, le verre brisé et la maçonnerie qui croule, suspendu à une dernière flamme exsangue. Que nous en reste-t-il ?

– J’ai oublié où, monsieur. 279 avant Jésus-Christ.

– Asculum, compléta Stephen, vérifiant d’un coup d’œil le nom et la date sur le livre meurtri d’horreur.

– Oui, monsieur. Et il a dit : *une autre victoire comme celle-là et nous sommes perdus*.

Cette rengaine le monde s’en était souvenu. Insipide béquille de l’esprit. Du haut d’une colline surmontant une plaine plantée de cadavres, un général parle à ses officiers, appuyé sur sa lance. N’importe-quel général à n’importe-quels officiers. Ils prêtent l’oreille.

– Toi, Armstrong, dit Stephen. Quelle a été la fin de Pyrrhus ?

– La fin de Pyrrhus, monsieur ?

– Je sais, monsieur. Demandez-moi, monsieur, fit Comyn.

– Attends. Toi, Armstrong. Sais-tu quelque chose de Pyrrhus ?

Un sachet de chaussons aux figues était accommodé dans la sacoche d’Armstrong. De temps en temps, il en roulait un sur sa paume et l’avalait sans bruit. Des miettes adhéraient à la surface de ses lèvres. Une haleine sucrée d’enfant. Des gens prospères, fiers de leur aîné dans la marine. Route de Vico, Daley.

– Pyrrhus, monsieur ? Pyrrhus, un pier.

Tous rirent. Rire perché et malicieux, sans joie. Armstrong se tourna pour regarder ses camarades autour de lui, le profil d’une gaîté sotte. Et ils riront plus fort encore, conscients qu’ils sont de mon absence d’autorité et des honoraires que paient leurs papas.

– Dis-moi maintenant, fit Stephen en enfonçant un coin de son livre dans l’épaule du garçon, qu’est-ce qu’un pier.

– Un pier, monsieur, répondit Armstrong. Une chose qui sort de l’eau. Une sorte de pont. Le pier de Kingstown, monsieur.

Quelques-uns rirent à nouveau : sans joie, mais avec intention. Deux, sur le banc du fond, chuchotaient. Oui. Ils savaient : sans avoir appris ni avoir été, jamais, innocents. Tous. Il les dévisagea avec envie : Edith, Ethel, Gery, Lily. Leurs pareilles : l’haleine tout aussi sucrée de thé et de confiture, les bracelets qui gloussent quand elles se débattent.

– Le pier de Kingston, dit Stephen. Oui, un pont désappointé.

Ce mot jeta le trouble dans leurs yeux.

– Comment ça, monsieur ? demanda Comyn. Un pont, ça traverse une rivière.

Pour le recueil de Haines. Personne ici pour entendre. Cette nuit adroitement, entre deux verres et à bâtons rompus, un trait pour percer l’armure polie de son esprit. Et après ? Un bouffon à la cour de son maître, encouragé, méprisé, et qui en recueille une louange indulgente. Pourquoi avaient-ils choisi tous ce rôle ? Pas seulement pour cette douce caresse. Pour eux aussi l’histoire était un conte comme un autre, trop rabâché, et leur pays le bric-à-brac d’un prêteur-sur-gage.

Si Pyrrhus n’était pas tombé sous les tuiles d’une mégère, ou Jules César sous les poignards. La pensée ne peut les évacuer. Le temps les a marqués au fer. Enchaînés ils partagent leur cachot avec les possibilités infinies qu’ils ont oblitérées. Mais peuvent-elles avoir été possibles si elles n’ont jamais eu lieu ? Ou seul est possible ce qui finit par arriver ? Tisse, tisseur de vent.

– Racontez-nous une histoire, monsieur.

– Oh, oui monsieur. Une histoire de fantômes.

– Où en sommes-nous là-dedans ? demanda Stephen, ouvrant un deuxième livre.

– *Ne pleurez plus*, répondit Comyn.

– Continue, Talbot.

– Mais l’histoire, monsieur ?

– Plus tard, dit Stephen. Continue, Talbot.

Un garçon brun ouvrit un livre et le cala prestement sur son cartable, comme sur un rempart. Il récitait les vers par à-coup en louchant sur le texte :

>– *Ne pleurez plus, dolents bergers, ne pleurez plus  
Car Lycidas, qui vous endeuille, n’est pas perdu  
Quoique sous le sol des eaux il soit reclus…*

Cela doit donc être un mouvement, une actualisation du possible en tant que possible. La formule d’Aristote prenait corps entre les rimes ânonnées et flottait sur le silence studieux de la bibliothèque Sainte-Geneviève où il avait lu, à l’abri du péché parisien, nuit après nuit. À son côté un siamois délicat compulsait un manuel de stratégie. Cerveaux repus et repaissant aux alentours : épinglés sous les liseuses, les antennes palpitant faiblement : et dans l’obscurité de mon esprit un paresseux du monde souterrain, réfractaire, éludant la lumière, roule ses plissures squameuses de dragon. La pensée est la pensée de la pensée. Clarté tranquille. L’âme est d’une certaine manière tout ce qui est : l’âme est la forme des formes. Tranquillité soudaine, vaste, incandescente : forme des formes.

Talbot répétait :

>– *Par la grâce de celui qui marcha sur les flots  
Par la grâce…*

– Tourne la page, fit Stephen sans élever la voix. Je ne vois rien.

– Quoi, monsieur ? demanda simplement Talbot en se penchant en avant.

Ses mains tournèrent la page. Il s’adossa et continua, les mots lui revenant. De celui qui marche sur les flots. Ici aussi sur ces cœurs veules son ombre s’étend, et sur le cœur et les lèvres du railleur et sur les miens. Elle s’étend sur leurs visages avides, eux qui lui ont offert une pièce du tribut. À César ce qui est à César, à Dieu ce qui est à Dieu. Un long regard, des yeux sombres, une phrase lourde d’énigmes qui doit être tissée et tissée sur les métiers de l’église. Hélas.

>*Devinez ma devinette, devinez  
Mon père m’a donné des graines à planter.*

Talbot glissa son livre fermé dans son cartable.

– Ai-je tout entendu ? demanda Stephen.

– Oui, monsieur. Hockey à dix heures, monsieur.

– Demi-journée, monsieur. C’est jeudi.

– Qui peut répondre à une devinette ? demanda Stephen.

Ils rangèrent leurs livres : tambour des crayons, bruissement des pages. Ils sanglaient et bouclaient leurs cartables dans une gaie bousculade, et caquetaient en chœur :

– Un devinette, monsieur ? demandez-moi, monsieur.

– Ah, demandez-moi, monsieur ?

– Une difficile, monsieur.

– Voilà la devinette, dit Stephen :

>*Le coq chantait,  
Le ciel était bleu :  
Les cloches dans les cieux  
Onze fois sonnaient.  
L’heure pour cette pauvre âme  
De s’en aller aux cieux.*  

Qu’est-ce que c’est ?

– Quoi, monsieur ?

– Encore, monsieur. On n’a pas entendu.

Leurs yeux s’agrandissaient pendant qu’il répétait. Après un silence, Cochrane fit :

– Qu’est-ce que c’est, monsieur ? Nous abandonnons.

Stephen, un picotement dans la gorge, répondit :

– Le renard qui enterre sa grand-mère sous un buisson de houx.

Il se leva et poussa un éclat de rire nerveux, et dans l’écho de leurs cris perçait la gêne.

Une crosse heurta la porte et une voix appela du couloir :

– Hockey !

Ils se dispersèrent, filant le long des bancs, sautant par-dessus. Et déjà, ils n’étaient plus là ; de la remise on entendait le choc des crosses, le tumulte des bottes et des voix.

Sargent, qui seul s’était attardé s’avança lentement, un cahier ouvert. La tignasse épaisse, le cou décharné trahissait son impréparation, et ses yeux apeurés imploraient au travers de lunettes embuées. Sur sa joue blême et anémiée s’étalait une tache d’encre en forme de datte, encore fraîche et humide comme le lit d’un escargot.

Il tendit son cahier. Le mot ADDITION était écrit en titre ; en dessous, des chiffres qui s’inclinaient ; au pied une signature tortueuse, aux boucles encrées, et un pâté. Cyril Sargent : son nom et son sceau.

– Monsieur Deasy m’a dit de tout recommencer, dit-il, et de vous les montrer, monsieur.

Stephen effleura les bords du cahier. Futilité.

– Est-ce que tu comprends comment les faire maintenant ? demanda-t-il.

– De onze à quinze, répondit Sargent. Monsieur Deasy m’a dit de les recopier du tableau, monsieur.

– Peux-tu les faire tout seul ? insista Stephen.

– Non, monsieur.

Laid et futile : cou maigre, tignasse et tache d’encre, le lit d’un escargot. Et pourtant quelqu’un l’avait aimé, l’avait porté dans ses bras et dans son cœur. Sans elle, la course du monde l’aurait laissé piétiné, un escargot, invertébré, écrabouillé. Elle avait aimé ce sang dilué et faible, issu du sien. Cela était-il donc réel ? La seule chose sûre en ce monde ? Le corps prostré de sa mère : dans un zèle sacré enfourché par le fougueux Colomban. Elle n’était plus : le squelette tremblant d’une brindille consumé par le feu, une odeur de palissandre et de cendres mouillées. Elle l’avait sauvé d’être piétiné, et s’en était allée, ayant à peine été. Une pauvre âme montée au ciel : et dans la lande sous les étoiles acquiesçant, un renard, le relent rouge de la rapine sur sa fourrure, les yeux brillants et sans merci raclait la terre, écoutait, raclait la terre, écoutait, et raclait et raclait.

Assis à son côté, Stephen résolvait le problème. Il prouve par l’algèbre que le fantôme de Shakespeare est le grand-père de Hamlet. Sargent observait, méfiant, lunettes de guingois. Les crosses de hockey crépitèrent dans la remise. Choc creux d’un palet, appels du terrain.

Sur la page les symboles dansaient une cérémonie grave et muette, arlequins mauresques aux chapeaux désuets de carrés et de cubes. Donnez-vous les mains, traversez, saluez votre partenaire : donc : des djinns, nés de la fantaisie des Maures. Disparus eux aussi, Averroès et Moïse Maimonides, hommes sombres de constitution et d’allure, qui dans leurs miroirs moqueurs capturaient l’âme obscure du monde, ténèbres irradiant la lumière et que la lumière ne pouvait comprendre.

– Tu comprends maintenant ? Peux-tu faire le second tout seul ?

– Oui, monsieur.

Sargent recopiait par jets longs et tremblants. Toujours en attente d’une parole d’aide sa main déplaçait fidèlement les symboles instables, une ombre de honte palpitant sous sa peau terne. *Amor matris* : génitif subjectif et objectif. De son sang pauvre, de son lait clair et passé elle l’avait nourri, et dissimulé ses langes des yeux d’autrui.

J’étais comme lui, ces épaules fuyantes, cette absence de grâce. Mon enfance se penche à mes côtés. Trop éloignée pour que j’y mette la main, pas même une fois pour l’effleurer. La mienne est lointaine, la sienne secrète comme nos yeux. Des secrets, silencieux, immuables, règnent dans les sombres palaces de nos cœurs à tout deux : des secrets lassés de leur tyrannie : des tyrans, désireux qu’on les détrône.

L’addition était terminée.

– C’est très simple, fit Stephen en se levant.

– Oui, monsieur. Merci, répondit Sargent.

Il sécha l’encre avec un fin papier buvard et rapporta le cahier à son pupitre.

--Va donc prendre ta crosse et retrouver les autres, dit Stephen en accompagnant à la porte la forme disgracieuse du garçon.

– Oui, monsieur.

Son nom retentit dans le couloir, depuis le terrain de jeu.

– Sargent !

– Cours, dit Stephen, monsieur Deasy t’appelle.

Il se tenait sous le porche et regardait le retardataire s’empresser vers le terrain piteux d’où s’élevaient des voix aiguës en conflit. Ils étaient répartis en équipes et monsieur Deasy s’en éloignait en enjambant les touffes d’herbe de ses guêtres. Quand il eut atteint le bâtiment de l’école les voix, qui s’affrontaient à nouveau, l’appelèrent. Il tourna sa moustache blanche et coléreuse.

– Qu’est-ce que c’est encore ? clamait-il sans écouter.

– Cochrane et Halliday sont du même côté, monsieur, dit Stephen.

– Voulez-vous m’attendre dans mon étude un instant, dit M. Deasy, le temps que je restaure l’ordre ici.

Et tandis qu’il retraversait d’un pas irrité le terrain sa voix de vieillard grondait sévèrement :

– Qu’est-ce qui se passe ? Qu’est-ce que c’est encore ?

Leurs voix perçantes l’assaillaient de toutes parts : leurs formes amassées l’encerclèrent, et le soleil criard décolorait l’ambre de ses cheveux mal teints.

Un air rassi et enfumé flottait sur l’étude, mélangé à l’odeur du cuir défraîchi et érodé des sièges. Comme le premier jour quand il a marchandé avec moi ici. Comme il était au commencement, maintenant. Sur le buffet, le présentoir de monnaies Stuart, vil trésor de tourbière : et toujours. Et bien au chaud dans leur écrin à cuiller de peluche pourpre, passés, les douze apôtres qui prêchèrent aux Gentils : pour les siècles des siècles.

Des pas hâtifs sur le perron, le couloir. Soufflant sa moustache clairsemée M. Deasy s’arrêta à la table.

– D’abord, notre petit arrangement financier, dit-il.

Il sortit de sa veste un portefeuille fermé par une lanière de cuir, l’ouvrit d’un geste et en sortit deux billets, dont un recollé au milieu, et les déposa soigneusement sur la table.

– Deux, fit-il, bouclant le portefeuille avant de le remettre à sa place.

Et maintenant sa chambre forte pour l’or. La main incommodée de Stephen détaillait les conques amassées sur le froid mortier de pierre : bulots, cauris, cyprées tigres : et ceci, enroulé comme le turban d’un émir, et ceci, la coquille Saint-Jacques. Le magot d’un vieux pèlerin, un trésor mort, des coquilles vides.

Un souverain tomba, brillant, neuf, sur le velours du tapis de table.

– Trois, fit M. Deasy, jouant des doigts avec sa petite boite à monnaie. C’est une chose très pratique. Voyez. Ça c’est pour les souverains. Ça c’est pour les shillings. Six-pences, demi-couronnes. Et là les couronnes. Voyez.

Il fit sauter deux couronnes et deux shillings.

– Trois et douze, dit-il. Je pense que vous trouverez que c’est correct.

– Merci, monsieur, dit Stephen, ramassant hâtif et gêné l’argent et mettant le tout dans la poche de son pantalon.

– Il n’y a pas de quoi, dit M. Deasy. Vous l’avez gagné.

La main de Stephen, libérée, retourna aux conques. Des symboles de beauté et de puissance, aussi bien. Cette bosse dans ma poche : des symboles souillés par l’avarice et la misère.

– Ne transportez pas ça comme ça, dit M. Deasy. Vous allez les sortir quelque part et les perdre. Achetez donc un de ces appareils. Vous verrez, c’est très pratique.

Réponds quelque-chose.

– Le mien serait souvent vide, dit Stephen.
Même lieu, même heure, même sagesse : et moi de même. Trois fois déjà. Trois nœuds autour de mon cou. Et alors ? Je peux les rompre à l’instant si je le veux. 

– Parce que vous n’épargnez pas, dit M.Daisy, le doigt levé. Vous ignorez encore la valeur de l’argent. L’argent, c’est le pouvoir. Quand vous aurez vécu aussi longtemps que moi. Je sais, je sais. Si jeunesse savait. Mais que dit Shakespeare ? *l’argent, et rien d’autre, fait ta bourse*.

– Iago, murmura Stephen.

Son regard laissa les conques oiseuses pour celui, fixe, du vieil homme.

– Lui savait ce qu’était l’argent, continua M. Deasy. Il en gagnait. Un poète, certes, mais aussi un Anglais. Quel est le point d’honneur des Anglais, le savez-vous ? Connaissez-vous les mots les plus fiers qui sortiront jamais de la bouche d’un Anglais ?

Le maître des mers. Ses yeux froid-marins sur la baie vide : la faute à l’histoire, à ce qu’il semble : à mon propos, sur mes paroles, sans haine.

– Que sur son empire, récita Stephen, le soleil ne se couche jamais.

– Bah ! s’exclama M. Deasy. Ce n’est pas anglais. C’est un celte, et français, qui a dit cela. Il tapotait de la boite l’ongle de son pouce.

– Je vais vous dire, annonça-t-il solennellement, quel est son plus grand titre de gloire. *J’ai payé mon dû*.

Brave homme, brave homme.

– *j’ai payé mon dû. Je n’ai jamais emprunté un sou de ma vie*. Est-ce que vous pouvez ressentir cela. *je ne dois rien*. Pouvez-vous le dire ?

Mulligan neuf livres, trois paires de chaussettes, un falzar, des cravates. Curran, dix guinées. McCann, une guinée. Fred Ryan, deux shillings. Temples, deux déjeuners. Russell, une guinée, Cousin, dix shillings, Bob Reynolds, une demi-guinée, Kohler trois guinées, Mme MacKernan, cinq semaines de pension. La bosse n’en viendra pas à bout.

– Pour le moment, non, répondit Stephen.

M. Deasy, riant d’aise et de contentement, rangea sa boite à monnaie.

– Je le savais bien, fit-il joyeusement. Mais un jour viendra : il vous faudra bien le ressentir. Nous sommes un peuple généreux mais nous devons aussi être justes.

– Je crains ces grands mots, dit Stephen, qui nous rendent si malheureux.

Le regard de M. Deasy s’arrêta gravement sur la forme massive d’un homme en kilt écossais, au-dessus de la cheminée : Albert Edward, prince de Galles.

– Vous pensez que je suis un vieux schnock, et un vieux tory, fit sa voix pensive. J’ai vu trois générations depuis O’Connell. Je me souviens de la famine de 46. Savez-vous que les loges orangistes militèrent pour l’abrogation de l’union vingt ans avant O’Connell, avant que les prélats de votre communion ne le dénoncent comme démagogue ? Vous autres fénians oubliez certaines choses.

Glorieux, pieux, immortel souvenir. La loge du Diamant à Armagh la splendide pavoisée de cadavres papistes. En armes, rauques et masqués les planteurs prêtent serment. Le nord noir, protestant, et la vraie bible. Rebelles, à bas.

Stephen esquissa un geste.

– Moi aussi j’ai du sang rebelle, continuait M. Daisy. Du côté de la quenouille. Mais je suis un descendant de Sir John Blackwood, qui a voté l’union. Nous sommes tous des Irlandais, et tous des enfants du roi.

– Hélas, dit Stephen.

– *Per vias rectas*, affirma M. Deasy, était sa devise. Il l’a voté, et a enfourché ses bottes et son cheval jusqu’à Dublin, depuis les Ards du Bas, pour le faire.

>*Trotte, trotte, petit canasson  
Sur la route rocailleuse de Dublin.*

Un rude hobereau à cheval, les bottes tirées et astiquées. Beau temps, Sir John ! Beau temps votre honneur ! … Temps ! … Temps ! Deux bottes pendouillent en mesure sur la route de Dublin. Trotte, Trotte. Trotte petit canasson.

– Cela me fait penser, dit M. Deasy. Vous pouvez me rendre un service, monsieur Dedalus, auprès de vos littérateurs d’amis. J’ai une carte pour les journaux. Asseyez-vous un instant. Je dois en recopier la fin.

Il tira sa chaise par deux fois sous le bureau, près de la fenêtre, et relut à voix haute quelques mots de la feuille qui sortait de la machine à écrire.

– Asseyez-vous. Excusez-moi, lança-t-il par-dessus l’épaule, *Les prescriptions du bon sens*. J’en ai pour une minute.

le manuscrit sous le coude, le regard broussailleux posé dessus, il marmonnait, et commença à actionner pesamment les touches raides du clavier, soufflant quand il revissait le tambour pour effacer une erreur.

Stephen s’assit sans bruit devant la présence princière. Des murs une assemblée de chevaux disparus rendaient hommage depuis leurs cadres, les têtes hautes et dociles : le Repulse de lord Hastings, le Ceylon du duc de Beaufort, *Prix de Paris, 1866*. De fins cavaliers les montaient, guettant un signe. Il voyait leur vitesse, misait sur les couleurs du roi, et criait à l’unisson des foules disparues.

– Point, dicta M. Deasy à son clavier. Mais la prompte circulation de cette question essentielle…
Là Cranly m’avait mené, en quête d’une fortune rapide, courant après le bon cheval entre les breaks éclaboussés de boue, parmi les braillements des bookies à leurs postes et les relents de cantine, dans un bourbier bigarré. Fair Rebel ! Fair Rebel ! Le favori au pair : dix contre un pour les autres. Dépassant les joueurs de dés et de bonneteau nous courrions après les sabots, casquettes et casaques en course, dépassée la femme à la tête de steak, la rombière d’un boucher, qui plongeait son museau assoiffé dans un quartier d’orange.

Des cris perçants, la stridence d’un sifflet, s’élevèrent du terrain de jeu.

Encore : un but. Je suis parmi eux, entre leurs corps en lutte dans la mêlée, la joute de la vie. Tu veux dire ce petit cagneux là, le chéri de sa maman qui a l’air un peu mal fichu ? Joutes. Le temps heurté rebondit, heurt après heurt. Joutes, bourbier, tumulte des batailles, l’écume froide des morts, le choc des piques enguirlandées des boyaux encore sanguinolents des hommes.

– Voilà, fit M. Deasy en se levant.

Il s’avança vers la table, rassemblant ses feuillets. Stephen se leva.

– J’ai réduit la question à l’essentiel, dit M. Deasy. Il s’agit de la fièvre aphteuse. Jetez un coup d’œil. Il n’y a pas trente-six solutions.

Puis-je faire intrusion dans vos estimables colonnes. La doctrine du *Laisser-faire* qui si souvent dans notre histoire. Notre commerce de bétail. Le sort de toutes nos industries. La clique de Liverpool qui a saboté le projet du port de Galway. Conflagration européenne. Approvisionnements en céréales au travers du détroit de la Manche. L’impassibilité plus-que-parfaite du ministère de l’Agriculture. Qu’on me pardonne cette allusion aux classiques. Cassandre. Une femme qui ne valait que sa réputation. Pour en arriver à la question.

– Je ne mâche pas mes mots, n’est-ce pas ? demanda M. Deasy pendant que Stephen poursuivait sa lecture.

Fièvre aphteuse. Connu comme la préparation de Koch. Sérum et virus. Pourcentage de chevaux inoculés. Peste bovine. Les chevaux de l’empereur à Murzsteg, Basse-Autriche. Chirurgiens vétérinaires. M. Henry Blackwood Price. Offre courtoise d’un essai impartial. Prescriptions du bon sens. Question essentielle. Prendre le taureau par les cornes, tous les sens du terme. Vous remerciant de l’hospitalité de vos colonnes.

– Je veux que cela soit imprimé et lu, dit M. Deasy. Vous verrez qu’à la prochaine épidémie ils mettront un embargo sur le bétail irlandais. Alors que c’est guérissable. On la guérit. Mon cousin, Blackwood Price, m’écrit qu’en Autriche les vétérinaires bovins la traitent régulièrement. Ils offrent de venir ici. J’essaie de gagner des appuis au ministère. Et maintenant j’essaie l’opinion publique. Mais je suis assailli de difficultés… des intrigues… des coteries de…

Son index s’éleva sénile et battit l’air, avant que sa voix n’enchaîne :

– Retenez ça, M. Dedalus, fit-il. L’Angleterre est aux mains des juifs. Aux plus hauts rangs : sa finance, sa presse. Et ils sont les signes de la décadence d’une nation. Où qu’ils s’assemblent, ils drainent la force vitale d’une nation. Voilà des années que je vois ça venir. Aussi vrai que je vous vois devant moi, les marchands juifs sont déjà à pied d’œuvre, minant et détruisant. La vieille Angleterre se meurt.

Il s’éloigna abruptement, ses yeux recouvrant vie, bleus, à croiser une plage de jour. Il fit volte-face, deux fois.

– Se meurt, répéta-t-il, si elle n’est pas déjà morte.

>*L’appel de la gueuse qui vend sa chair  
Sera le suaire de l’Angleterre.*

Ses yeux s’ouvraient à une vision, agrandis et sévères, par-delà le rayon de soleil où il s’était figé.

– Est marchand, dit Stephen, celui qui achète à bas prix et vend cher, qu’il soit juif ou gentil, n’est-ce pas ça ?

– Ils ont péché contre la lumière, dit gravement M. Deasy. Et vous pouvez voir les ténèbres dans leurs yeux. Et c’est pourquoi ils errent encore sur cette terre aujourd’hui.

Sur les marches de la Bourse de Paris des hommes à la peau dorée chiffraient les cours de leurs doigts bagués. Oies qui caquettent. Ils s’attroupaient, bruyants et frustes aux alentours du temple, leurs têtes lourdes de combines et de gauches hauts-de-forme. Empruntés : ces vêtements, ces mots, ces gestes. Leurs yeux bombés et traînants démentaient ces mots, l’empressement révérencieux des gestes, mais savaient les rancœurs accumulées, savaient la vanité de l’effort. Vaine la patience qui amasse et préserve. Le temps éparpillerait tout à coup sûr. Un trésor amassé sur le bord d’une route : pillé et dispersé. Leurs yeux savaient les années d’errance et, patients, savaient la souillure de leur souche.

– Qui ne l’a fait ? dit Stephen.

– Que voulez-vous dire ? demanda M. Deasy.

Il s’avança d’un pas, debout devant la table. Sa mâchoire s’ouvrit de travers, béante et incertaine. Est-ce là la sagesse de l’âge ? Il attend que je parle.

– L’histoire, fit Stephen, est un cauchemar dont j’essaie de m’éveiller.

Du terrain de jeu les cris des garçons. Un sifflet strident : but. Et si ce cauchemar te renvoyait un coup de latte ?

– Les voies du Créateur ne sont pas les nôtres, dit M. Deasy. Toute l’histoire humaine tend vers un grand but, la manifestation de Dieu.

D’un geste du pouce Stephen indiqua le fenêtre, et dit :

– Voilà Dieu.

Hourra ! Ay ! Rrhuii !

– Pardon ? demanda M. Deasy.

– Un cri dans une rue, répondit Stephen, haussant les épaules.

M. Deasy baissa le regard et pinça longuement ses narines entre ses doigts. Il les libéra et releva les yeux.

– Je suis plus heureux que vous ne l’êtes, dit-il. Nous avons commis bien des erreurs, et bien des péchés. Une femme apporta le péché en ce monde. À cause d’une femme qui ne valait pas mieux que sa réputation, Hélène la fugitive, femme de Ménélas, les Grecs guerroyèrent dix ans devant Troie. Une femme infidèle attira la première sur nos côtes des étrangers, la femme de MacMurrough et son galant, O’ Rourke, prince de Breifne. Une femme a causé la perte de Parnell. Bien des erreurs, bien des échecs, mais non LE péché. Je suis un lutteur, qui arrive à la fin de son combat. Mais je me battrai pour ce qui est juste jusqu’à la fin.

>*Car Ulster se battra  
Et Ulster prévaudra.*

Stephen rassembla les feuillets à la verticale.

– Eh bien, monsieur, commença-t-il…

– J’anticipe, dit M. Deasy, que vous ne resterez pas très longtemps ici à ce poste. Vous n’êtes pas fait pour enseigner, je pense. Mais j’ai peut-être tort.

-- Pour apprendre, plutôt, fit Stephen.

Et ici qu’apprendras-tu de plus ?

M. Deasy secoua la tête.

– Qui sait ? continua-t-il. Pour apprendre il faut être humble. Mais la vie est le plus grand des professeurs.

Stephen fit à nouveau bruisser les feuillets.

– En ce qui concerne ceci, recommença-t-il.

– Oui, dit M. Deasy. Vous en avez deux exemplaires. Vous pouvez les faire publier dès maintenant.

*Le Télégraphe. Le foyer irlandais.*

– Je vais essayer, répondit Stephen, et je vous le ferai savoir demain. Je connais vaguement deux rédacteurs.

– Cela ira, dit sèchement M. Deasy. J’ai écrit la nuit dernière à Monsieur Field, le député. L’association des éleveurs bovins se réunit aujourd’hui à l’hôtel des Armes de la cité. Je lui ai demandé d’y déposer la lettre avant la réunion. Voyez si cela peut paraître dans vos deux journaux. Quels sont-ils ?

– le *Télégraphe du soir*…

– Cela ira, dit M. Deasy. Il n’y a pas de temps à perdre. Il faut maintenant que je réponde à la lettre de mon cousin.

– Bonne journée, monsieur, fit Stephen, en mettant les feuillets dans sa poche. Merci.

– Mais de rien, répondit M. Deasy qui fouillait les papiers sur son bureau. J’apprécie ces joutes avec vous, même à mon âge.

– Bonne journée, monsieur, répéta Stephen, s’inclinant devant le dos courbé.

Il sortit sur le porche et suivit le chemin de gravier sous les arbres, écoutant les cris des enfants et les chocs des crosses qui venaient du terrain de jeu. Les lions couchés sur les colonnes, tandis qu’il dépassait le portail : terreurs édentées. Et pourtant je vais l’assister dans son combat. Mulligan m’affublera d’un nouveau surnom : le barde kiffe-les-bœufs.

– Monsieur Dedalus !

Sur mes traces. Pas une autre lettre, j’espère.

– Rien qu’un instant.

– Oui, monsieur, dit Stephen, se retournant vers l’entrée.

M. Deasy s’arrêta, à bout de souffle, haletant.

– Je voulais juste vous dire, fit-il. L’Irlande, dit-on, a l’honneur d’être le seul pays qui n’a jamais persécuté les juifs. Le saviez-vous ? Non. Et savez-vous pourquoi ?

Il fronça sévèrement ses sourcils dans l’air vif.

– Pourquoi, monsieur ? demanda Stephen avec un début de sourire.

– Parce qu’elle ne les a jamais laissés entrer, dit M. Deasy solennellement.

Une quinte de rire secoua sa gorge, roulant avec elle une chaîne de glaire. Il se retourna aussitôt, toussant, riant, agitant ses bras dans l’air.

– Elle ne les a jamais laissés entrer, s’esclaffa-t-il une autre fois en écrasant de ses guêtres le gravier du chemin. Voilà pourquoi.

Sur ses sages épaules, au travers des feuilles en damier, le soleil tirait paillettes et monnaies dansantes.
