L’inéluctable modalité du visible : pensé au travers de mes yeux : au minimum, sinon plus. Signatures de toutes choses que je suis appelé à lire ici, dépôts et algues marins, la marée qui s’approche, cette botte rouillée. Vert-de-morve, bleu-argent, rouille : signes colorés. Les limites du diaphane. Mais il ajoute : dans les corps. Donc il les percevait corps avant que colorés. Comment ? En s’y cognant le chandelier, certes. Ne va pas trop vite. C’était un chauve, et un millionnaire, *Maestro di color que sanno*. Limite du diaphane dans. Pourquoi dans ? Diaphane, a-diaphane. Si tu peux passer tes cinq doigts au travers c’est une grille, pas une porte. Ferme tes yeux et vois.

Stephen ferma les yeux pour entendre ses bottes broyer bruyamment les algues et les coquillages. Tu les traverses, dans tous les sens. Oui, une foulée à la fois. Un très court espace de temps au cours de très courts temps d’espace. Cinq, six : le *nacheinander*. Précisément : et c’est là l’inéluctable modalité de l’audible. Ouvre les yeux. Non. Bon dieu ! Et si je tombais d’une falaise qui surplombe son assise, au travers du *nebeneinander*, inéluctablement ! Je me débrouille bien dans le noir. Mon sabre de bois à mon côté. Tâte avec : c’est ce qu’ils font. Mes deux pieds dans ses bottes terminent ses jambes,  *nebeneinander*. Ça sonne plein : façonné par le marteau de *los Demiurgos*. Suis-je en chemin pour l’éternité sur la grève de Sandymount ? Crac, croc, cric, cric. Monnaies de mer sauvage. Master Deasy les masterise toutes.

>*Viendras-tu à sandymount,  
Madeleine la jument ?*

Le rythme prend, tu vois. J’entends. Un tétramètre d’iambes acatelectiques en marche. Au galop, plutôt : *Deleine la jument*.

Ouvre les yeux maintenant. Je vais le faire. Un instant. Est-ce que tout a disparu ? Et si en les ouvrant je me retrouve dans le noir a-diaphane, à jamais. *Basta !* Je verrai si je peux voir.

Vois maintenant. Tout était là, tout ce temps sans toi : maintenant et toujours, pour les siècles des siècles.

Elles descendirent prudemment les marches de la terrasse de chez Leahy, *frauenzimmer* : et les pieds en canard, s’enfonçant dans le sable vaseux, suivant mollement la pente vers le bas du rivage. Comme moi, comme Algy, au-devant de notre puissante mère. Numéro un balançait, balourde, son sac de sage-femme, et le parasol de l’autre crevait la plage. Échappées du quartier des Liberties pour la journée. Mme Florence MacCabe, veuve de feu Patk MacCabe, profonds regrets, de la rue Bride. Une de ses consœurs m’a dragué à la vie, tout couinant. Qu’a-t-elle dans son sac ? Une fausse-couche, le cordon ombilical à la traîne, fourrée dans de la laine rougeâtre. Les cordons de tous qui remontent : le câble entortelacé de toute chair. D’où les moines mystiques. Serez-vous comme les dieux ? Vérifiez votre *omphalos*. Allo ! Ici Kinch. Connectez-moi à Edenville. Aleph, alpha : zéro, zéro, un.

Épouse et assistante d’Adam Kadmon : Heva, l’Eve nue. Elle n’avait pas de nombril. Contemple. Ballon immaculé, protubérance sans faille, un bouclier de vélin tendu, non, motte blanche de blé, aurorale et immortelle, érigée, d’éternité en éternité. Matrice du péché.

Dans la matrice, obscurité-péché, je demeurais aussi, fait mais non engendré. Par eux, l’homme avec ma voix et mes yeux et une femme fantôme à l’haleine de cendre. Ils se serraient et se déprenaient, suivant la volonté de l’accoupleur. Qui m’a voulu avant même le temps, et qui maintenant ne pourra jamais plus me vouer au néant. Une *lex eterna* ne Le quitte pas. Est-ce donc là la substance divine dont Père et Fils sont consubstantiels ? Où est notre pauvre Arius pour offrir ses conclusions ? Parti guerroyer, sa vie contre la contransmagnajudeobangtantialité. Hérésiarque né sous une mauvaise étoile, il expira dans une salle d’eau grecque : euthanasie. La mitre perlée et la crosse, calé sur son trône, veuf de sa chaire veuve, l’omophorion bien raide et le postérieur englué.

L’air mordant lâchait ses tourbillons acérés sur lui. Ils arrivent. Vagues. Les étalons des mers à la blanche crinière, à la foulée puissante, la bride éclatante au vent, les coursiers de Manannan. 

Faut pas que j’oublie sa lettre à la presse. Et après ? Le Ship, douze heures trente. À propos, n’y va pas trop fort avec cet argent comme un jeune imbécile.

Oui, il le faut.

Son pas ralentit. M’y voilà. Aller chez tante Sarah, ou pas ? La voix de mon père consubstantiel. As-tu aperçu ton artiste de frère, Stephen, dernièrement ? Non ? Sûr, il n’est pas à la terrasse du Strasburg avec sa tante Sally ? Il ne pourrait pas frayer un peu plus haut que ça, hein ? Et et et et et dis-nous, Stephen, comment va l’oncle Sy ? Ah, Dieu m’est témoin, à quoi me suis-je marié ! Des ploucs échappés du grenier à foin. Le petit comptable ivrogne et son frère, le souffleur de cornet. Des gondoliers bien dignes de révérence ! Et ce bigleux de Walter qui donne du Monsieur à son père, rien que ça ! Oui, monsieur, non monsieur. Jésus a pleuré : et ce n’est pas étonnant, Christ !

Je tire la sonnette asthmatique de leur cottage aux volets tirés : et j’attends. Ils me prennent pour un collecteur, et guettent d’un point en surplomb.

– C’est Stephen, monsieur.

– Fais-le entrer. Fais entrer Stephen.

On tire un verrou et Walter m’accueille.

– Nous pensions que c’était quelqu’un d’autre.

Sur oreillers et sous couvertures, de son large lit tonton Richie étend par-dessus les tertres jumeaux de ses genoux son avant-bras robuste. Torse propre. Il a lavé la moitié du haut.

– Le bonjour, neveu.

Il écarte le plateau où il dresse les comptes pour maître Goff et maître Shapland Tandy, classe conciliations, enquêtes courantes et actes de *Duces tecum*. Un cadre en bois de tourbière au-dessus de sa tête chauve : le *Requiescat* de Wilde. Il siffle entre les doigts, et Walter revient.

– Oui, monsieur ?

– du malt pour Richie et Stephen, dis-le à la mère. Où est-elle ?

– Elle baigne Crissie, monsieur.

La petite copine de lit de papa. Pomme d’amour.

– Mais, oncle Richie…

– Appelle-moi Richie. Merde à ton eau minérale. Ça rend mou. Whousky !

– Oncle Richie, vraiment…

– Assieds-toi, ou sur la tête de… je t’assoie par terre.

Walter louche en vain vers une chaise absente.

– Il n’a pas où s’asseoir, monsieur.

– Il n’a nulle part où se poser… espèce de cruche. Amène notre fauteuil Chippendale. Tu veux manger un morceau ? Fais pas ta Marie-Antoinette ici. Du gras de bacon frit avec un hareng ? Sûr ? Et bien tant mieux. On n’a rien à manger dans cette maison, à part des pilules pour le mal de dos.

*All’erta !*

Il sifflote quelques mesures de l’*Aria di sortita* de Ferrando. Le plus beau de tout l’opéra, Stephen. Écoute.

Il reprend, mélodieux, tout en nuance, le souffle puissant et les poings battant le tambour sur ses genoux capitonnés.

Le vent est plus doux ici.

Règnes de pourriture. Ma maison, la sienne. Toutes. Tu disais aux noblillons de Clongowes que tu avais un oncle juge et un autre général dans l’armée. Fuis-les, Stephen. La beauté n’est pas là. Ni dans l’alcôve languide de la bibliothèque de Marsh où tu lisais les prophéties étiolées de Joachim Abbas. Pour qui ? La racaille aux cent têtes du cloître de la cathédrale. Un autre les a fuis, qui haïssait sa propre espèce, détalant vers les bois de la folie, la crinière écumant sous la lune, ses yeux arrondis des étoiles. Houyhnhnm, tête-à-naseaux. Visages ovales et chevalins : Temple, Buck Mulligan, Foxy Campbell, Menton-en-galoche. Abbas père, – doyen enragé, quel outrage a mis le feu à leurs cervelles ? Paf ! *Descende, calve, ut ne amplius decalveris*. Une auréole de cheveux gris sur sa tête anathème, vois-le-moi qui se dévisse jusqu’à l’abside (*Descende !*) agrippé à un encensoir, avec des yeux de basilic. Descends, chauve-teste ! Un chœur en réponse bruisse de menace, des cornes de l’autel, et reprend en écho le latin que braient les curetons ballottant lourdement sous leurs aubes, tonsurés, oints et châtrés, gras de la graisse des rognons de froment.

Et au même instant, peut-être, au coin de la rue, un prêtre accomplit l’élévation. Drelin-drelin ! Et deux rues plus loin un autre la range dans un ciboire. Drelin-drelan ! Et dans une chapelle mariale un autre boulotte l’hostie entière. Drelin-drelin. En bas, en haut, en avant, en arrière. Dom Occam y a pensé, l’invincible docteur. Un de ces matins anglais brumeux, l’hypostase du feu-follet lui a chatouillé la cervelle. Comme il abaissait son hostie et s’agenouillait il entendit par deux fois sa deuxième clochette s’accorder avec la première cloche dans le transept (il lève la sienne) et, se relevant, entendit (maintenant je lève la mienne) leurs deux cloches (il s’agenouille) vibrer en diphtongue.

Cousin Stephen, tu ne seras jamais un saint. Île aux saints. Tu étais terriblement dévot, n’est-ce pas ? Tu priais la Sainte Vierge de ne pas avoir un nez rouge. Tu priais le diable que la veuve potelée qui passait en face sur l’avenue Serpentine retroussât un peu plus ses jupons, loin du pavé humide. *O, certo !* Vends ton âme pour ça, va-s-y, pour des chiffons bariolés épinglés à une squaw. Conte-s-en moi plus, conte ! encore !! La plateforme du tramway, ligne Howth, seul, et hurlant à la pluie : des femmes nues ! DES FEMMES NUES ! Ça te va, ça ?

Qu’est-ce que tu en dis ? Ne les a-t-on inventées pour ça ?

Et toutes ces nuits, à lire deux pages par livre, sept livres à la fois ? J’étais jeune. Tu te saluais devant la glace, tu allais avide au-devant des applaudissements, quelle tête… Un hourra pour le foutu crétin ! Viva ! Personne ne l’a vu : ne le dis à personne. Et ces livres que tu allais écrire, avec des lettres pour seul titre. Avez-vous lu son F ? Certes, mais je préfère le Q. Oui, mais W est splendide. Ah, oui, W. Tu te souviens de tes épiphanies écrites sur des feuilles vertes et ovales, profondément profondes – en-cas-décès-envoyez-exemplaires-à-toutes-grandes-bibliothèques-monde, y compris Alexandrie ? Quelqu’un les lirait là d’ici à quelques millénaires, un mahamanvantara. Très Pico Della Mirandola. Tout autant qu’une baleine ou un chameau. Qui, qui ne lisant ces pages étranges du temps jadis ne se sent tel qu’il ne fait qu’un avec qui jadis…

Le sable épais avait disparu sous ses pieds. Ses bottes foulaient à nouveau une litière humide et crissante, conques-couteaux, galets qui couinent, ce qui sur les galets innombrables se vient briser, bois criblés de vers, Armada perdue. Les aplats de sable torves guettaient ses semelles pour les aspirer, exhalant une haleine d’égout, une poche d’algues se consumant du feu marin sous un amoncellement de cendres humaines. Il les longeait avec prudence. Une bouteille de bière brune se dressait, enlisée à demi, dans la pâte sablée. Une sentinelle : île de l’atroce soif. Cerceaux brisés au bord de l’eau ; sur le rivage un labyrinthe de filets sombres et retors ; plus loin les portes du fond griffonnées à la craie et plus haut encore une corde à linge et deux chemises crucifiées. Ringsend : wigwams de pilotes hâlés et de patrons de barques. Conques humaines.

Il s’arrêta. J’ai dépassé la maison de tante Sarah. N’y vais-je donc pas ? Il ne semble pas. Personne en vue. Il s’orienta vers le nord-est et traversa le sable plus ferme en direction du Pigeonnier.

>*– Qui vous a mis dans cette fichue position ?  
– C’est le pigeon, Joseph.*

Patrice, en permission, lapait du lait avec moi au bar MacMahon. Fils de l’oie sauvage, Kevin Egan de Paris. Mon père, un oiseau – il lapait le doux *Lait chaud* avec une langue rose et juvénile et sa tête de lapin potelé. Lape, *lapin*. Il espère gagner les *gros lots*. À propos de la nature des femmes, il a lu Michelet. Mais il faut qu’il me passe *La vie de Jésus* de M. Leo Taxil. L’avait prêté à son ami.

– *C’est tordant, vous savez. Moi, je suis socialiste. Je ne crois pas en l’existence de Dieu. Faut pas le dire a mon père.  
– Il croit ?  
– Mon père, oui.*

*Schluss*. Il lape.

Mon chapeau quartier-latin. Bon Dieu, il faudra bien qu’on habille le personnage. Je veux des gants bordeaux. Vous étiez étudiant, n’est-ce pas ? Et de quoi, au nom de l’autre diable ? Pécéhenne. P.C.N., vous savez, *Physiques, Chimiques et Naturelles*. Aaah. À manger ta pitance de *mou en civet*, marmites garnies de l’Égypte, parmi les chauffeurs qui rotent et jouent des coudes. Répète du ton le plus naturel : quand j’habitais Paris ; *Boul’ Mich’* : j’avais l’habitude de. Oui, l’habitude d’emporter des tickets poinçonnés pour avoir un alibi s’ils vous arrêtaient pour un meurtre quelconque. La justice. La nuit du dix-sept février 1904, le prisonnier a été vu par deux témoins. C’est l’autre qui l’a fait : l’autre moi. Chapeau, cravate, imperméable, nez. *Lui, c’est moi*. Il semble que vous ayez eu du bon temps.

Fière allure. De qui essayais-tu d’imiter la démarche ? Laisse tomber : un dépossédé. Le mandat de ma mère en main, huit shillings, et la porte de la poste que le concierge te ferme au nez. Rage de dent de famine. *Encore deux minutes*. Regarde l’horloge. Doit obtenir. *Fermé*. Vendu ! Bute-le à la, pam, carabine, en pièces, repeins les murs lambeaux homme boutons cuivre. Lambeaux par khrrrrklak tout clack en place. Pas de mal ? Non, ce n’est rien. Serre main. Voyez ce que je veux dire ? Oh, ça va bien. Serre une serre est une serre. Oh, c’est bien tout va bien.

Tu allais faire des merveilles, hein ? Missionnaire en Europe à la suite du fougueux Colomban. Fiacre et Scot le cul sur l’escabeau en renversèrent leurs choppes au ciel, le latin hilare : *Euge ! Euge !* Tu affectais un mauvais anglais en traînant ta valise, trois pence le porteur, sur le ponton poisseux de Newhaven. *Comment ?* Fameux butin que tu rapportais ; *Le Tutu*, cinq numéros de *Pantalon blanc et culotte rouge*, en lambeaux ; un télégramme français, bleu – mais quelle curiosité !

–  Mère mourante reviens père.

La tante pense que tu as tué ta mère. C’est pour ça qu’elle ne veut.

>*À la santé de tante Mulligan  
Et je dirais bien pourquoi, tu penses  
Elle a toujours préservé la décence  
Dans la famille Hannigan.*  

Ses pieds battaient maintenant les ridules sablonneuses, paradant soudain d’un rythme fier devant les blocs rocheux du mur sud. Il les regardait fièrement : ces piles de crânes de mammouths en pierre. Lumière dorée sur la mer, le sable, les blocs. Le soleil est là, les arbres graciles, les maisons citrons.

Paris s’ébroue au réveil, le soleil écru sur ses rues citrons. Pulpe humide du pétrin, l’absinthe vert-grenouille, son encens du matin, courtise l’air. Belluomo quitte le lit de la femme de l’amant de sa femme, la mère de famille s’active sous son fichu, une soucoupe d’acide acétique à la main. Chez Rodot Yvonne et Madeleine rebattissent des fondations leur beauté écroulée tout en broyant la chair des *chaussons* de leurs dents en or, la bouche jaune du *pus* du *flanc breton*. Passent des visages d’hommes parisiens, ceux qui bien contents les contentent, conquistadores crêpés.

Torpeurs de midi. Kevin Egans roule des cigarettes de poudre à canon entre ses doigts maculés d’encre d’imprimerie, sirotant sa fée verte tout comme Patrice imbibait sa blanche. Autour de nous les goinfres pellettent les flageolets dans leur goulot. *Un demi setier !* De la bouilloire patinée, un jet de vapeur de café. Sur un signe de lui, elle me sert. *Il est irlandais. hollandais ? Non fromage. Deux Irlandais, nous, Irlande, vous savez ah, oui !* Elle pensait que vous vouliez du fromage *hollandais*. votre postprandial, vous connaissez ce mot ? Postprandial. J’ai connu un type à Barcelone, drôle de type, il appelait ça son postprandial. Eh bien : *Slainte !* Autour des tables communes l’enchevêtrement des haleines avinées et des gosiers grinchant. Son haleine flotte entre nos plats tachés de sauce, la fée verte projette son dard entre ses lèvres. Irlande, Dalcassiens, espoirs, conspirations, Arthur Griffith aujourd’hui, A. E., Pimandre, bon berger des hommes. Viens sous le joug à mes côtés, nos crimes notre cause commune. Tu es le fils de ton père. Je reconnais la voix. Sa chemise de futaine, fleurettes rouge-sang et pompons hispaniques, tremble sous ses secrets. M. Drumont, journaliste illustre, Drumont, tu sais comment il a appelé la reine Victoria ? *Vieille ogresse* aux *dents jaunes*. Maud Gonne, femme superbe, *la patrie*, M. Millevoye, Felix Faure, tu sais comment il est mort ? Hommes licencieux. La froeken, *bonne à tout faire*, qui frotte les nudités masculines aux thermes d’Upsala. *Moi faire*, dit-elle, *Tous les messieurs*. Pas ce *monsieur*, ai-je répondu. Coutume des plus licencieuses. Bain une chose des plus intimes. Je ne laisserais pas mon frère, pas même mon propre frère, chose des plus lascives. Yeux verts, je vous vois. Dard, je te sens. Peuple lascif.

La mèche bleue se consume mortellement entre ses doigts et se libère. Des brins de tabac épars prennent feu : une flamme, une fumée âcre illuminent notre coin. Pommettes saillantes sous son chapeau de conspirateur. Comment le premier chef s’est échappé, version authentique. Déguisé en jeune mariée, mon gars, voile, fleurs d’oranger, et en voiture sur la route de Malahide. Tel quel, parole. Dirigeants perdus, dirigeants trahis, folles évasions. Déguisements, dernière chance, parti, pas là.

Amoureux éconduit. J’étais jeune et bien gaillard, à l’époque, laissez-moi vous dire. Je vous montrerai un portrait un de ces jours. Parole. Amoureux, pour l’amour d’elle il rodait avec le colonel Richard Burke, dauphin désigné de son clan, sous les murs de Clerkenwell et vit, tapis dans un coin, une flamme vengeresse les projeter dans le brouillard. Verre brisé et maçonnerie qui croule. Il se cache dans le gai Paw-riii, Egan de Paris, personne recherchée. De personne, sinon moi. Son chemin de croix quotidien, la casse miteuse d’imprimeur, ses trois tavernes, le terrier sur Montmartre où il passe ses courtes nuits, rue de la Goutte d’Or, tapissé de visages disparus, criblés de mouche. Sans amour, sans terre, sans femme. Elle mène sa petite vie pépère sans son proscrit, madame, rue Git-le-Cœur, un canari et deux mâles en pension. Joues de pêche, jupe zébrée, fringante comme une jeunette. Éconduit et aspirant toujours. Dites à Pat que vous m’avez vu, voulez-vous ? J’ai voulu lui trouver du travail à ce pauvre Pat, à une époque. *Mon fils*, soldat de France. Je lui ai appris à chanter : *Les gars de Kilkenny sont forts comme des lions*. Vous connaissez cette vieille antienne ? Je l’ai apprise à Patrice. Le vieux Kilkenny : Saint-Canice, le château de Strongbow sur la Nore. Ça commence comme ça. Oh, Oh. Il me prend, Napper Tandy, par la main.

>*Oh, oh, les gars  
de Kilkenny*

Main décharnée qui s’échoue sur la mienne. Eux ont oublié Kevin Egan, lui qui ne les oublie pas. Se souvenir de toi, Ô Sion.

Il s’était rapproché de l’eau et le sable humide claquait sous ses bottes. L’air neuf l’accueillait, pinçant nerfs à vif, vent d’air à vif, graines de lumière. Allons, je ne vais pas marcher jusqu’au bateau-phare de Kish, non plus ? Il s’arrêta court. Ses pieds s’envasaient peu à peu dans le sol mou. Demi-tour.

Il fit volte-face et scruta la côte sud, ses pieds s’affaissant dans de nouvelles niches, lentement. La voûte froide de la tour attend. Des meurtrières les traits de lumière balaient le cadran de dalles sans jamais s’interrompre, ni se presser, comme mes pieds s’enfoncent, et rampent vers le crépuscule. Crépuscule bleu, nuit tombée, nuit noire. Tapis dans l’ombre de la voûte elles attendent, leurs chaises éconduites, ma valise obélisque, autour de la table et des plats abandonnés. Qui nettoiera ça ? Il a la clé. Je ne dormirai pas là cette nuit. Une porte close, une tour silencieuse, une tombe –  pour leurs corps aveugles, le sahib à la panthère et son chien d’arrêt. Appel : pas de réponse. Il dégagea ses pieds de la succion du sable et repartit par la digue de pierre. Prends tout, garde tout. Mon âme marche à mes côtés, forme des formes. Ainsi, quand la lune fait sa ronde, j’arpente le sentier qui surplombe la roche, de sable argenté, et j’écoute l’appel des flots d’Elseneur.

Les flots me suivent. Je les vois d’ici, ils me gagnent. Reviens donc par la route de Poolbeg jusqu’à la grève. Il escalada marisques, algues brunes et visqueuses, et s’assit sur une roche plane, coinçant son bâton dans une crevasse.

La charogne boursouflée d’un chien s’étalait affablement sur le varech. Et devant lui le plat-bord d’un bateau, coulé dans le sable. *Un coche ensablé*, Louis Veuillot appelait la prose de Gautier. Ces sables lourds sont un langage que la marée et le vent ont déposé ici. Et là, les piles de pierres de bâtisseurs défunts, un terrier à belettes. Y cacher de l’or. Pourquoi pas ? Tu en as. Sables et pierres. Lourds de passé. Sire rustaud et ses jouets. Gare à tes oreilles. Z’est moi le zacré zéant, ze roule les foutus blocs, faut des os pour mon gué. Fi-Fo-Fum. Ze zens le zang d’un fenian.

Un point se détacha de l’étendue sablonneuse et la parcourait : un chien, vivant. Bon Dieu, il ne va pas m’attaquer ? Respecte sa liberté. Tu ne seras ni le maître d’autrui ni son esclave. J’ai mon bâton. Ne bouge plus. Plus loin, remontant le rivage sur fond d’écume, des silhouettes, deux. Les deux Maries. Elles l’ont déposé à l’abri entre les joncs. Coucou, qui est là ? Je vous vois. Non, le chien. Il court vers elles. Qui ?

Les galères des Lochlanns ont ici accosté en quête de proies, proues et éperons sanglants glissant sur l’écume d’étain en fusion. Vikings danois, torques de tomahawks étincelant sur leurs poitrails, du temps où Malachi portait le collier d’or. Un banc de baleines échouées sous un soleil de plomb, qui expirent en gigotant dans les haut-fonds. Et des orifices de la cité affamée une horde de nains en justaucorps, mes gens, se ruent armés de couteaux d’écorcheurs et dépècent et débitent la chair grasse et verte. Famine, peste, massacres. Leur sang est en moi, leurs appétits me portent. J’étais parmi eux sur la Liffey gelée, moi, un renégat, entre les feux crépitant de résine. Je ne parlais à personne : personne ne me parlait.

L’aboiement filait sur lui, s’arrêta, s’éloigna. Chien de mon ennemi. Je restais là, pâle et silencieux, soumis, aux hurlements. *Terribilia meditans*. Un pourpoint primevère, de la fortune le valet, souriait de mon effroi. C’est pour ça que tu te consumes, leurs aboie-plaudissements ? Des prétendants : vis leur vie. Le frère du Bruce, Thomas Fitzgerald, chevalier à la soierie, Perkin Warbeck, faux héritier d’York, sa culotte de soie d’un ivoire empourpré, la merveille d’un jour, et Lambert Simnel, avec sa suite de soubrettes et de cantiniers, le garçon de cuisine couronné. Tous des enfants du roi. Le paradis des imposteurs, alors et aujourd’hui. Il a sauvé des hommes de la noyade, et tu trembles aux jappements d’un chiot. Mais les courtisans qui raillaient Guido à Or san Michele habitaient leur propre maison. Maisons de … Nous n’avons rien à faire de vos monstruosités médiévales. Ferais-tu ce qu’il a fait ? S’il y avait un bateau tout près, une bouée de sauvetage. *naturlich*, placés là à dessein. Le ferais-tu ou pas ? L’homme qui s’est noyé il y a neuf jours au rocher de la Vierge. Ils l’attendent toujours. La vérité, crache, enfin. Je voudrais le vouloir. J’essaierais. Je ne suis pas un bon nageur. Eau froide, molle. Quand j’y mets la tête dans la cuvette à Clongowes. Peux pas voir ! Qui est derrière moi ? Sors, vite, vite ! Vois-tu la marée qui monte rapidement de toutes parts, qui rapidement nappe les aplats de sables, comme d’un chocolat de conques ? Si j’avais pied. Je veux qu’il conserve sa vie, et moi la mienne. Un homme qui se noie. Ses yeux d’hommes hurlent vers moi dans l’horreur de la mort. Et moi … Avec lui par le fond. Je n’ai pas pu la sauver. Les eaux : mort amère : perdue.

Une femme et un homme. Je vois son jupon. Retroussée, je parie.

Leur chien trottinait sur un banc de sable en train de fondre, reniflant de toutes parts. A-t-il perdu quelque chose dans une autre vie ? Et soudain il fila comme un lièvre, les oreilles rejetées en arrière, pourchassant l’ombre d’un goéland qui volait ras. Le sifflet perçant de l’homme le rattrapa par les oreilles. Volte-face, il saute, se rapproche, les jarrets clignotent. Sur champ tanné un cerf, cadencé, au naturel, sans attirail. Sur la frange dentelée du flot il s’arrêta, raide sur ses appuis avant, les oreilles pointées vers la mer. Le museau dressé aboyait au ronflement des vagues, troupeaux de morses qui serpentent vers ses pattes, ondoyant, déployant une à une leurs nombreuses crêtes, à la neuvième se brisant, éclaboussant, de loin, de plus loin encore, des vagues et des vagues.

Pêcheurs de moules. Ils s’enfoncèrent un peu dans l’eau, se courbèrent, immergeant leurs sacs puis, les retirant, pataugèrent vers le rivage. Le chien les rejoignit en jappant, leur sautait dessus, retombait, se dressait à nouveau dans une adoration muette d’ours. Il les suivit, ignoré, alors qu’ils remontaient sur le sable sec, langue de loup, rouge et pantelante tombant comme un haillon entre ses crocs. Son corps moucheté les dépassa, il partit d’un trot de veau. La carcasse l’attendait. Il s’arrêta, reniflant, tourna autour, un frère, flaira de plus près, tourna encore, reniflant hâtivement, comme font les chiens, le pelage détrempé du chien mort. Crane de chien, flair de chien, les yeux fixés à terre, et en route vers un seul grand but. Ah, pauvre cabot ! Voilà le caveau du pauvre cabot.

–  Loqueteux ! Lâche ça, espèce de bâtard !

Le cri le ramena tête-basse auprès de son maître qui d’un brusque coup de son pied nu l’envoya valser, indemne, par-delà une langue de sable. Il revint obliquement. Pas vu, pas pris. Le long du môle, il traînassait, folâtrant, reniflait un rocher, pissait dessus la patte levée. Il trotta vers l’avant et, levant à nouveau sa patte arrière, gicla sur un rocher sans se donner la peine de le flairer. Les plaisirs simples des pauvres. Ses pattes arrières dispersèrent soudain le sable ; puis des pattes avant il grattait puis creusait. Quelque chose qu’il y a enterré, sa grand-mère. Il s’enfouissait dans le sable, grattant, creusant, s’arrêta pour humer l’air, racla à nouveau le sable de ses griffes furieuses, s’arrêtant bientôt, un pard, une panthère, fruit de l’adultère, vautour sur sa charogne.

Après qu’il m’a réveillé la nuit dernière, même rêve, ou un autre ? Attends. Un porche. Rue des gueuses. Souviens-toi. Haroun al Al Rachid. J’y suis presque. L’homme me guidait, m’a parlé. Je n’avais pas peur. Le melon qu’il tenait, près de mon visage. Sourit : odeur onctueuse, fruitée. Dit : c’est la règle. Entre. Viens. Étalements de tapis rouge. Tu verras qui.

Sac à l’épaule, ils peinent, les rouges égyptiens. Ses pieds à lui s’échappaient bleuis du pantalon retroussé pour frapper le sable collant, un cache-nez brique terne étranglait son col velu. À pas de femme, elle suivait : le ruffian et la catin errante. Rapines sur le dos. Ses pieds à elle, nus, incrustés de sable et de débris de conques. Sur son visage écorché par le vent, des cheveux s’attardaient. Derrière son seigneur et maître, avanti à Romeville. Quand la nuit cache de son corps les défauts elle racole, d’une arcade que les chiens ont souillée, sous un châle brun. Son proxo en régale deux du Royal Dublins chez O’Loughlin à Blackpitts. Baise-la, chevauche, en argot rum des truands, car, Ô, ma jument, ma baiseuse ! Une blancheur de succube sous ses haillons rances. Passage Fumbally cette nuit-là : l’odeur des tanneries.

>*Et, au resveil, quant le ventre luy bruit,  
Monte sur moy, que ne gaste son fruit.  
Soubz elles geins, plus qu’un aiz me fait plat;  
De paillarder tout elle me destruit*,  

De la délectation morose, d’Aquin la barrique appelle ça,  *Frate poscospino*. L’Adam d’avant la chute chevauchait sans luxure. Qu’il brame, laisse-le : *monte sur moy*. Langage pas un grain pire que le sien. Paroles de moines, grains de rosaire bredouillés sous la ceinture : paroles de truands, dures pépites qui papotent dans leurs poches.

Les voilà qui passent.

Lorgne mon chapeau Hamlet. Et si j’étais soudain nu, là, assis ? Je ne le suis pas. Au travers les sables du monde, poursuivis de l’épée flamboyante du soleil, vers l’Ouest, en marche vers les terres du soir. Elle charrie, schleppe, traîne, drague, trascine sa charge. Une marée la suit, à la lune attelée. Marées aux myriades d’îles en elle, sang qui n’est pas le mien, *oinopa ponton*, mer vineuse et sombre. Voici la servante de la lune. Dans son sommeil le signe humide l’appelle à son heure, la fait lever. Lit de noce, lit de conception, lit de mort, bougie spectrale. *Omnis caro ad te veniet*. Il vient, pâle vampire, ses yeux perçant la tempête, ses voiles chiroptères ensanglantant la mer, bouche sur le baiser de sa bouche.

Attends. Choppe ça tu veux bien ? Mes tablettes. Bouche sur son baiser.

Non. Faut qu’il soit deux. Glue-les bien. Bouche sur le baiser de sa bouche.

Ses lèvres d’une moue muèrent l’air en moue aux lèvres désincarnées : bouche sur son ventre. Antre, tombe où tout entre. Sa bouche modulait le souffle exhalé sans parole : oo-ii-aa : planètes cataractiques qui rugissent, globes fulgurants rugissant loin-si-loin-au-loin-si-loin. Papier. Les billets de banques, merde. La lettre du vieux Deasy. Voilà. Vous remerciant de l’hospitalité, déchire la partie vierge. Tournant le dos au soleil il se pencha jusqu’à atteindre une table de roche et griffonna quelques mots. Ça fait deux fois que j’oublie de piquer des fiches sur le comptoir de la bibliothèque.

Son ombre s’arrêtait sur les rochers pendant que, courbé, il terminait. Pourquoi pas sans fin jusqu’à la plus lointaine étoile ? Obscurs sont-ils sous cette lumière, obscurité qui brille sous la clarté, Delta de Cassiopée, mondes. Et moi assis avec son bâton d’oracle, sandales d’emprunt : de jour auprès de la mer livide, ignoré ; de nuit, violette, rodant sous le règne d’astres primaux. Je projette mon ombre bornée, forme humaine inéluctable, et je la rappelle. Sans fin, m’appartiendrait-elle, forme de ma forme ? Qui prend garde à moi ici ? Qui lira jamais ces mots que j’écris ? Signes sur champ blanc. Quelque-part, à quelqu’un, de ta voix la plus flûtée. Le bon évêque de Cloyne sortit le voile du temple de sous sa calotte : voile d’espace orné d’emblèmes colorés éclos sur son champ. Tiens bon. Colorés sur un aplat : c’est ça. Je vois à plat, puis pense distance, proche, loin, à plat je vois, à l’est, en arrière. Ah, vois donc ! Ça retombe soudain, figé sur stéréoscope. Un clic et hop. Vous trouvez mes mots obscurs ? Mais l’obscurité n’est-elle pas dans nos âmes ? Plus flûté. Nos âmes, blessées et honteuses de nos péchés, se cramponnent à nous toujours plus, une femme s’accrochant à son amant, d’autant plus d’autant plus.

Elle se fie à moi : la main douce, les yeux aux longs cils. Mais où diable est-ce que je l’emmène, par-delà le voile ? Vers l’inéluctable modalité de l’inéluctable visualité. Elle, elle, elle. Quelle elle ? La vierge devant la vitrine de la librairie Hodges Figgis, lundi, qui cherchait des yeux un des livres alphabet que tu devais écrire. Tu l’as bien reluquée. Le poignet qui traverse le lacet tressé de son ombrelle. Elle vit à Leeson park avec son chagrin et des breloques, une femme de lettre. Causes-en à une autre, Stevie : une allumeuse. Tu paries qu’elle porte ces jarretelles à damner un jésus avec des bas jaunes, reprisés à la grosse laine. Parle de chaussons-aux-pommes, *piuttosto*. Où as-tu la tête ?

Touche-moi. Doux yeux. La main si douce douce douce. Je suis si seul ici. Oh touche-moi sans attendre, tout de suite. Quel est ce mot que tous les hommes connaissent ? Je suis sage ici et seul. Et triste. Touche-moi, touche-moi.

Il s’étira de tout son long sur les roches saillantes, fourrant papier griffonné et crayon dans une poche son chapeau. Son chapeau rabattu sur les yeux. C’est le geste de Kevin Egan, je l’ai repris, quand il pique sa sieste, le repos du Sabbat. *Et vidit deus. Et erant valde bona*. Allo ! *Bonjour*. Bienvenu comme fleur en mai. Sous l’aile du chapeau et derrière ses cils de paon gazouillant il regardait le soleil au midi. Me voilà pris dans ce brasier. L’heure de Pan, l’apogée du faune. Parmi les plantes serpentines et lourdes de sève épaisse, parmi les fruits d’où sourd le lait, là où sur l’eau fauve les feuilles se déploient. La douleur est loin.

*Et ne te détourne plus pour ruminer.*

Il ruminait, le regard sur la pointe carrée de ses bottes, les rebuts du cerf, *Nebeneinander*. Il compta les crevasses du cuir bosselé où le pied d’un autre avait fait son nid chaud. Pied qui bat le sol lors du tripudium, pied que je n’aime point. Mais tu étais bien aux anges quand le soulier d’Esther Osvalt t’a entré : une fille que je connaissais à Paris. *Tiens, quel petit pied !* Un ami à toute épreuve, une âme sœur : amour à la Wilde qui n’ose dire son nom. Son bras : le bras de Cranly. Il va me quitter, maintenant. À qui la faute ? Tel que je suis. Tel que je suis. Tout entier ou rien du tout.

Par de longs lacets l’eau montait en force du lac de Cork, recouvrait les lagons de sable vert-dorés, fluait, montait encore. Mon bâton sera emporté par les flots. Attendre je dois. Non, ils passeront bien, ils passent, abrasant les rochers bas, en tourbillons ils passent. Mieux vaut en finir vite. Prête l’oreille : siiissou, whrsssn, whresiiiss, ouuuss. Le souffle véhément des eaux parmi les serpents de mers, les chevaux cabrés, les rochers. Dans les coupelles des rochers il s’épanche : flic, flac, floc : prisonnier des barils. Et, épandu, cesse son discours. Il flue et bruisse, il flue large, étang d’écume flottante, fleur qui éclot.

Sous la marée enflée, il voyait les algues se tortiller, lever et balancer avec langueur des bras réticents, trousser leurs jupons, au murmure de l’eau balancer et dévoiler leurs fronts d’argent effarouchés. Jour après jour : Nuit après nuit : élevées, submergées, et laissées choir. Dieu, comme elles sont lasses ; et soupirent à chaque murmure. Saint Ambroise l’a entendu, le soupir des feuilles et des vagues, qui attendent et attendent que leur temps advienne, *diebus ac noctibus iniurias patiens ingemiscit*. Assemblées sans raison, libérées en vain, flottant de l’avant, rebroussant chemin : la lune à son métier. Lasse elle aussi sous les yeux des amants, ces hommes lascifs, une femme nue qui rayonne sur sa cour, elle tire le filet des eaux.

Cinq brasses au large. À cinq pleines brasses ton père gît. À une heure, a-t-il dit. Trouvé un noyé. Marée haute à la barre de Dublin. Poussant devant soi une écume mal assortie de détritus, nageoires de poissons, coquillages idiots. Un cadavre blanc de sel qui émerge du ressac, plouf replouf le marsouin avance vers la terre. Le voilà. Harponne-le vite. Tire. Aussi profond qu’il ait coulé sous le sol des eaux. On l’a. Va-s-y doucement maintenant.

Poche de gaz-cadavre qui macère dans la saumure infâme. Le fretin frémit, gras du morceau spongieux, et file par les fentes de la braguette. Dieu se fait homme se fait poisson se fait oie se fait édredon. Mortes exhalaisons, j’inhale bien vivant, je foule la poussière morte, je dévore les abats pisseux de tout ce qui est mort. Hissé raide par-dessus le plat-bord il exhale aux cieux la puanteur de son tombeau vert, son nez aux trous lépreux ronflant au soleil.

Mer-amorphose moi ça, yeux bruns bleus-de-sel. La mort en mer, la plus douce des morts qui s’offrent à l’homme. Bon père Océan. PRIX DE PARIS : attention aux contrefaçons. Faites un essai impartial. Nous nous sommes tant amusés.

Allons. J’ai soif. Ça se couvre. Pas de nuages noirs, à ce que je vois. Orage. Il tombe étincelant, l’éclair fier de l’intellect, *Lucifer, dico, qui nescit occasum*. Mon chapeau de pèlerin, bâton et ses-miennes chausses-sandales. Où ? Vers les terres du soir. Le soir trouvera le soir.

Il prit son bâton par la garde et s’escrimait mollement, badinant sur place. Oui, le soir se retrouvera en moi, sans moi. Les jours font leur propre fin. Au fait, le suivant c’est quoi mardi sera le jour le plus long. De toute cette heureuse nouvelle année, mère, le ba-ba-dou-ba-ba. Tennyson du vert-gazon, gentleman poète. *Gia*. Pour la vieille ogresse aux dents jaunes. Et Monsieur Drumont, gentleman journaliste. *Gia*. J’ai de si mauvaises dents. Pourquoi, on se demande. Tâte. Celle-là aussi est foutue. Coquilles. Devrais aller au dentiste, je me demande, avec cet argent ? Celle-là. Ci. Kinch l’édenté, le surhomme. Et pourquoi, je me le demande, ou est-ce que ça veut dire quelque chose, peut être ?

Mon mouchoir. Il l’a jeté. Je me souviens. Et je ne l’ai pas ramassé ?

Ses mains tâtaient en vain ses poches. Et non. Autant en acheter un.

Il déposa soigneusement la morve sèche qu’il avait extrait de ses narines sur le rebord d’un rocher. Et ce qu’il en reste que regarde qui veut.

Derrière. Il y a peut-être quelqu’un.

Il tourna la tête sur l’épaule, *rere regardant*. Déplaçant l’air, les gréements d’un trois-mâts, les voiles cordées sur les flèches, remontant vers son port, glissant en silence, vaisseau silencieux.


