Le supérieur, le très révérend John Conmee, S.J. remit sa montre lisse dans sa poche intérieure tout en descendant les marches du presbytère. Trois heures moins cinq. Juste le temps qu’il faut pour aller à pied à Artane. Comment s’appelait ce garçon déjà ? Dignam. Oui. *Vere dignum et iustum est*. C’était frère Swan qu’il fallait voir. La lettre de M. Cunningham. Oui. Lui rendre ce service, si possible. Bon catholique, pragmatique : utile en temps de mission.

Un marin unijambiste avançait par saccades chaloupées, paresseuses, porté par ses béquilles, grognant un air. Il pila devant le couvent des sœurs de la charité et tendit sa casquette à visière, en appelant au bon cœur du très révérend John Conmee, S.J. Au soleil, le père Conmee le gratifia d’une bénédiction, car sa bourse contenait, il le savait bien, une couronne d’argent orpheline.

Le père Conmee traversa pour gagner le square Mountjoy. Il gratifia d’une pensée, sans trop s’y attarder, les soldats et les marins dont les jambes avaient été emportées par des boulets, qui finissaient leur vie dans quelque hospice, et pondéra les paroles du cardinal Wolfey : *si j’avais servi mon Dieu comme j’ai servi mon roi Il ne m’aurait pas abandonné pendant mes vieux jours*. Il marchait le long des arbres, sous l’ombre du feuillage papillotante de soleil : et à sa rencontre vint l’épouse de M. David Sheehy, M.P.

– Mais très bien, mon père. Et vous, mon père ?

Le père Conmee allait mais merveilleusement bien. Il irait à Buxton pour sa cure, probablement. Et ses garçons à elle, comment cela se passait pour eux au Belvédère ? Vraiment ? Le père Conmee était mais très heureux de l’entendre. Et M. Sheehy ? Encore à Londres. La chambre siégeait encore, assurément. Un temps magnifique, délicieux vraiment. Oui, il était très probable que le père Bernard Vaughan vînt prêcher à nouveau. Mais oui, un très grand succès. Une merveille d’homme vraiment.

Le père Conmee était très heureux de voir l’épouse de M. David Sheehy, M.P. en si bonne santé et la pria de le rappeler au bon souvenir de M. David Sheehy, M.P. Oui, certainement, il passerait leur rendre visite.

– Bonne après-midi, Mme Sheehy.

Le père Conmee retira son chapeau de soie et gratifia d’un sourire, prenant congé, les perles de jais de la mantille qui brillaient comme de l’encre. Et il sourit une nouvelle fois, alors qu’il s’éloignait. Il s’était brossé les dents, il le savait bien, avec de la pâte de noix d’arec.

Le père Conmee marchait, et souriait tout en marchant car il pensait aux yeux drolatiques du père Bernard Vaughan et à son accent cockney :

– Pilate ! Pourquoi tu r’tiens pas c’te meute hurlante ?

Un grand zèle, cependant. Mais vraiment. Et qui vraiment faisait beaucoup de bien à sa manière. Sans l’ombre d’un doute. Il aimait l’Irlande, disait-il, et il aimait les Irlandais. De bonne famille avec ça, qui l’eût cru ? Des Gallois, si je ne me trompe ?

Oh, avant d’oublier. La lettre pour le père provincial.

Le père Conmee arrêta trois petits écoliers au coin du square Mountjoy. Oui, ils étaient du Belvédère. La petite maison. Aha. Et étaient-ils sages à l’école. Oh. Voilà qui était très bien. Et comment s’appelait-il ? Jack Sohan. Et celui-ci ? Ger. Gallaher. Et l’autre petit bonhomme ? Il s’appelait Brunny Lynam. Oh, voilà qui était un bien joli nom.

Le père Conmee sortit une lettre de son sein, la donna à Maistre Brunny Lynam et indiqua du doigt la boîte rouge au coin de la rue Fitzgibbon.

– Mais fais attention de pas t’y poster aussi, petit homme, dit-il.

Les six yeux des garçons se posèrent sur le père Conmee et rirent :

– Oh, monsieur.

– Eh bien, montre-moi si tu sais poster une lettre, dit le père Conmee.

Maistre Brunny Lynam traversa la route en courant et mit la lettre du père Conmee au père provincial dans la fente de la boite-aux-lettres rouge. Le père Conmee sourit, hocha du chef, sourit, et continua son chemin longeant l’Est du square Mountjoy.

M. Denis J Maginni, professeur de dance &c., haut-de-forme, redingote ardoise, revers soie, culotte lavande moulante, gants canaris, souliers vernis, maintien grave, descendit respectueusement du trottoir pour dépasser dame Maxwell au coin du cours Dignam.

N’était-ce pas Mme M’Guinness ?

Mme M’Guinness, imposante chevelure argentée, inclina son mât en guise de salut depuis le trottoir opposé où elle faisait voile. Et le père Conmee sourit et salua. Comment allait-elle donc ?

Quelle prestance. Quelque-chose, Marie reine d’Écosse. Et dire que c’était une prêteuse sur gages ! Et pourtant ! Un tel… Comment pourrait-il dire… un port si princier.

Le père Conmee descendit la rue Charles-le-Grand et jeta un œil sur le temple fermé de l’église libre. Le Révérend T. R. Greene B. A. prendra (D. V.) la parole. L’habilité l’appelait-on. Il se sentait habilité à dire quelques mots. Mais il fallait rester charitable. L’invincible ignorance. Ils agissaient selon leurs lumières.

Le père Conmee tourna au coin et suivit le périphérique nord. C’était surprenant qu’il n’y eût pas une ligne de tram sur une voie de cette importance. Certainement, il en aurait fallu une.

Une bande d’écoliers sac-aux-dos traversaient en provenance de la rue Richmond. Tous soulevèrent des casquettes dépenaillées. Le père Conmee les salua à plusieurs reprises, indulgent. Pupilles des frères chrétiens.

Le père Conmee sentit une odeur d’encens, sur sa droite, en avançant. L’église Saint-Joseph, allée Portland. Pour femmes d’âge et de vertu certaines. Le père Conmee souleva son chapeau pour le Saint Sacrement. Vertueuses : mais à l’occasion peu commodes.

À l’approche de la maison Aldborough, le père Conmee eut une pensée pour ce gentilhomme trop prodigue. Et aujourd’hui c’était des bureaux ou autre.

Le père Conmee prit la route du North Strand et reçut le salut de M. William Gallagher qui se tenait sur le seuil de son échoppe. Le père Conmee salua M. Gallagher et perçut les odeurs qui émanaient des quartiers de bacon et des amples mottes de beurre. Il passa le bureau de tabac de Grogan dont les présentoirs inclinés annonçaient une horrible catastrophe à New York. En Amérique ces choses arrivaient sans arrêt. Malheureuses gens, mourir comme cela, sans préparation. Un acte de contrition parfait, cependant.

Le père Conmee longea le pub de Daniel Bergin dont la devanture soutenait deux hommes désœuvrés. Ils le saluèrent et furent salués.

Le père Conmee dépassa l’entreprise de pompes funèbres de H. J. O’Neill où Corny Kelleher faisait des additions sur le registre en mâchouillant un brin de paille. Un agent de police qui faisait les cent pas le salua et le père Conmee salua l’agent de police. Chez Youksletter, le charcutier, le père Conmee contempla les boudins de porc, blancs et noirs et rouges, enroulés soigneusement sur l’étal.

Amarrée sous les arbres des allées Charleville, le père Conmee vit une péniche de tourbe, un cheval de halage la tête pendante, un batelier à bord qui sous un chapeau de paille sale fumait en contemplant au-dessus de sa tête une branche de peuplier. C’était idyllique : et le père Conmee songea à la providence du Créateur qui avait placé la tourbe dans les marais afin que les hommes pussent l’en extraire et l’amener aux bourgs et hameaux pour alimenter les foyers des humbles.

Au pont Newcomen le très révérend John Conmee, S.J. de l’église Saint-François-Xavier, des hauts de la rue Gardiner, monta dans un tram qui sortait.

D’un tram qui rentrait descendit le révérend Nicholas Dudley C.C de l’église Saint-Agathe, du nord de la rue William, au pont Newcomen.

Au pont Newcomen le père Conmee monta dans un tram qui partait car il lui était désagréable de faire à pied le passage sans charme qui longeait l’île Vaseuse.

Le père Conmee s’assit dans un coin du wagon, un ticket bleu planté avec soin dans l’embouchure d’un gant de chevreau replet, alors que quatre shillings, une pièce de six pence et cinq pennies glissaient de son autre pomme replète et gantée vers son porte-monnaie. À la hauteur de l’église au lierre il se fit la réflexion que le contrôleur passait le plus souvent après qu’on eut l’étourderie de jeter son ticket. La solennité des occupants du wagon sembla excessive au père Conmee pour un trajet si court et bon-marché. Le père Conmee appréciait un décorum plus enjoué.

C’était une journée paisible. Le monsieur à lunettes en face du père Conmee avait fini ses explications et baissa les yeux. Sa femme, supposa le père Conmee. Un imperceptible bâillement entrouvrit la bouche de la femme du monsieur à lunettes. Elle éleva un poing menu et ganté, bailla si doucement, tapotant de son poing menu et ganté sa bouche entrouverte sur un sourire imperceptible et doux.

Le père Conmee perçut son parfum dans le wagon. Il perçut aussi que le vieil homme gauche qui la flanquait de l’autre côté était assis sur le rebord du siège.

Le père Conmee du balustre de l’autel plaça non sans difficulté l’hostie dans la bouche du vieil homme gauche qui branlait la tête.

Au pont Annesley le tram fit un arrêt et, quand il était sur le point de repartir, une vieille femme se leva soudainement de son siège pour descendre. Le conducteur tira sur la courroie de transmission pour arrêter le tram. Elle passa avec son panier et un filet à provision et le père Conmee vit le conducteur l’aider à descendre elle et son panier et son filet ; et le père Conmee pensa que, comme elle avait presque dépassé le zonage de son ticket, c’était une de ces bonnes âmes qu’on devait toujours bénir par deux fois mon enfant, qu’elles avaient été absoutes, priez pour moi. Mais elles avaient tant de soucis dans la vie, tant de préoccupations, pauvres créatures.

Placardé sur une palissade, lèvres épaisses et négroïdes, M. Eugène Stratton grimaçait à l’intention du père Conmee.

Le père Conmee pensa aux âmes des hommes noirs bruns et jaunes et à son sermon sur saint Pierre Claver S.J. et la mission africaine et la propagation de la foi et aux millions d’âmes noires brunes et jaunes qui n’avaient pas reçu le baptême de l’eau quand leur dernière heure survenait comme un voleur dans la nuit. Ce livre du jésuite belge, *Le Nombre des Élus* était, il semblait au père Conmee, un plaidoyer raisonnable. C’étaient là des millions d’âmes humaines créées par Dieu à Son Image auxquelles la foi n’avait pas (D. V.) été révélée. Mais c’étaient là les âmes de Dieu, créées par Dieu. Il semblait dommage au père Conmee qu’elles fussent toutes perdues, un gaspillage, si l’on pouvait s’exprimer ainsi.

À l’arrêt route de Howth le père Conmee descendit, reçut le salut du conducteur et salua en retour.

La route de Malahide était calme. Voilà qui plaisait au père Conmee, et la route et le nom. Les cloches de l’église carillonnaient sur Malahide la gaie. Lord Talbot de Malahide, par droit héréditaire lord amiral de Malahide et des mers avoisinantes. Puis ce fut l’appel aux armes et elle fut vierge, épouse et veuve en un seul jour. C’était le bon vieux temps, le temps de la loyauté et des bourgs joyeux, la baronnie au temps jadis.

Le père Conmee, tout en marchant, pensa à son petit livret *La baronnie au temps jadis* et au livre qu’on pourrait écrire sur les maisons jésuites et à Mary Rochfort, fille de lord Molesworth, première comtesse du Belvédère.

Une dame languide, la jeunesse passée, longeait seule les rives du lough Ennel, Mary, première comtesse du Belvédère, marchant alanguie dans l’air du soir, sans tressaillir au plongeon d’une loutre. Qui aurait pu connaitre la vérité ? Ni le lord jaloux du Belvédère ni son confesseur, avait-elle consommé pleinement l’adultère, *eiaculatio seminis inter vas naturale mulieris*, avec le frère de son époux ? Elle confesserait à moitié si elle n’avait pas péché en entier comme le faisaient les femmes. Seul Dieu savait et elle et lui, le frère de son époux.

Le père Conmee pensa à cette incontinence tyrannique, quoique nécessaire sur cette terre à la race de l’homme, et aux voies de Dieu qui ne sont pas les nôtres.

Don John Conmee marchait et évoluait aux temps jadis. Il y dispensait son humanité et en recevait les honneurs. Il chargeait en sa conscience les secrets de la confession et il souriait à de nobles visages souriants dans un salon au parquet miroitant de cire d’abeille, aux plafonds marquetés de grappes de fruits mûrs. Et les mains des promis étaient unies, noble à noble, paume sur paume, par Don John Conmee.

C’était une journée charmante.

Le portillon à claire-voie d’un champ montra au père Conmee des longueurs de choux qui lui faisaient la révérence de leurs amples jupons. Le ciel lui montrait un troupeau de petits nuages blancs poussés lentement par le vent. *Moutonner*, disent les Français. Une tournure juste et familière.

Le père Conmee, tout en lisant son office, regardait un troupeau de nuages moutonner au-dessus de Rathcoffey. Les chaumes du champ de Clongowes lui chatouillait les chevilles sous leurs fines chaussettes. Il marchait là, lisant dans l’air du soir, et entendit les cris des rangées de jeunes garçons qui jouaient, cris jeunes dans l’air calme du soir. Il était leur recteur : et son règne était doux.

Le père Conmee ôta ses gants et sortit son bréviaire relié de rouge. Un signet en ivoire lui indiquait la page.

La None. J’aurais dû la lire avant le déjeuner. Mais lady Maxwell était venue.

Le père Conmee lut en secret *Pater* et *Ave* et fit un signe de croix sur sa poitrine. *Deus in adiutorium*.

Il marchait calmement et lisait en silence la None, marcha et lut jusqu’au *Beati immaculati: Principium verborum tuorum veritas : in eternum omnia indicia iustitiæ tuæ.*

Un jeune homme congestionné apparut d’une brèche de la haie, puis suivit une jeune femme avec dans la main des marguerites sauvages qui approuvaient de la tête. Le jeune homme retira brusquement sa casquette : la jeune femme se pencha brusquement et avec une lente précision, enleva de sa jupe légère une brindille qui s’accrochait.

Le père Conmee les bénit tout deux gravement et tourna une mince page de son bréviaire. *Sin : Principes persecuti sunt me gratis : et a verbis tuis formidavit cor meum*.

 
***


