M. Bloom feuilletait distraitement les pages des *Terribles révélations de Maria Monk*, puis passa au Chef-d'œuvre d’Aristote. Bâclées, rien qui va, ces impressions. Ces gravures : des enfants roulés en boule dans des utérus rouge sang comme les foies de vaches à l’abattage. Des tonnes comme ça à cet instant même partout dans le monde. Qui cognent du crâne pour trouver la sortie. Un enfant par minute quelque-part. Mme Purefoy.

Il laissa les deux livres de côté et regarda le troisième : *Contes du ghetto* de Léopold von Sacher Masoch.

— Celui-là je l’ai eu, dit-il, le repoussant de même. Le bouquiniste laissa tomber deux volumes sur le comptoir.

— Deux bons ces deux-là, dit-il.

Les oignons de son haleine, fuyant sa bouche édentée traversèrent le comptoir. Il se pencha pour mettre en pile les autres livres, les pressa contre son veston déboutonné et les emporta derrière un rideau miteux.

Sur le pont O’Connell de nombreuses personnes observèrent maintien, grave, et de l’habillement, gai, de M. Denis J Maginni, professeur de danse &c.

M. Bloom, resté seul, regarda les titres. *Blondes despotes* de James Lovebirch. Connais le genre. L’ai eu ? Oui. 

Il l’ouvrit. Me semblait bien.

Une voix de femme derrière le rideau miteux. Écoutons : l’homme.

Non, elle n’aimerait pas beaucoup ça. Lui ai dégotté une fois.

Il lut l’autre titre : *Les douceurs du Péché*. Plus son genre. Voyons.

il lut là où son doigt était tombé.

— *Et tous les dollars que son mari lui donnait étaient dépensés en toilettes extravagantes et en fanfreluches hors-de-prix. Pour lui ! Pour Raoul !*

Oui. Ça. Ici. Voyons.

— *Elle colla sa bouche à la sienne en un baiser voluptueux et lascif, pendant qu’il cherchait à pleines mains ses rondeurs opulentes sous le déshabillé.*

Oui. Prends-le. La fin.

— *Vous êtes en retard, dit-il, la voix rauque, l’œil chargé de soupçon.*

*La splendide créature rejeta son manteau de zibeline, découvrant ses épaules de reine, un embonpoint croissant. Un sourire imperceptible flottait sur ses lèvres parfaites alors qu’elle se tournait calmement vers lui.*

M. Bloom lut une seconde fois : *La splendide créature*.

Une douce chaleur l’envahissait, amollissait sa chair. Chair qui se livre dans un froissement d’habits : yeux qui, blancs, chavirent. Ses narines s’arquèrent, prêtes à fondre sur la proie. Onguents qui fondent sur la gorge (*pour lui ! pour Raoul !*). La sueur des aisselles, cette odeur d’oignon. Limon aquatique et gluant (*son embonpoint croissant*). Sens ! Serre ! Broyée ! Déjections soufrées des lions !

Jeune ! Jeune !

Une femme âgée, la jeunesse passée, sortit du bâtiment des cours de la chancellerie, du banc du roi, de l’échiquier et des affaires courantes, ayant assisté à la cours du Lord Chancelier à l’affaire de démence Potterton, dans la division amirauté des citations, motion *ex parte*, des propriétaires du Lady Cairns *vs.* les propriétaire de la barque Mona, dans la cours d’appel en révision du jugement du cas Harvey *vs.* la Ocean Accident & Guarantee Corporation.

Des quintes grasses ébranlèrent l’air de la librairie, gonflant le rideau miteux. La tête grise, ignorant le peigne, du bouquiniste en sortit, suivie de son visage mal rasé et rubicond, toussant. Il se racla grossièrement la gorge, cracha une glaire sur le sol. Il écrasa de sa botte ce qu’il avait craché, essuya sa semelle dessus, se pencha, exhibant une couronne de peau, peuplée de rares cheveux.

M. Bloom la contempla.

Raffermissant sa respiration agitée, il dit :

— Je vais prendre celui-là.

Le bouquiniste leva des yeux troubles et chassieux.

— *Les douceurs du Péché*, dit-il, le tapotant du doigt. Ça c’est un bon.

***