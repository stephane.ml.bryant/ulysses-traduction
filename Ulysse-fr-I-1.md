
[comment]: # (revoir les verbes énonciateurs)
[comment]: # (ne pas ajouter trop de ponctuation de lisibilité dans les passage 'stream of consciousness' quand il n'y en a pas dans l'original)
[comment]: # (reprendre les unités monétaires anglaises)
# I

Buck Mulligan, le rond, sortit de l’escalier, soutenant un bol de savon où reposaient en croix miroir et rasoir. Une robe de chambre jaune, dénouée, flottait derrière lui dans l’air doux du matin. Il éleva le bol et psalmodia :

 – *INTROIBO AD ALTARE DEI.*

Puis, figé, il scruta le sombre colimaçon d’escaliers et gueula :

 – Monte, Kinch ! Monte, trouillard de jésuite !

Solennel il s’avança et grimpa sur le créneau ; se retourna et bénit gravement, à trois reprises la tour, la campagne environnante et les montagnes au loin. Apercevant alors Stephen Dedalus, il se pencha en sa direction et esquissa des croix rapides qu’il accompagnait de gargouillis gutturaux et de hochements de tête. Stephen Dedalus, mécontent et endormi, appuya ses bras sur la dernière marche et posa un regard froid sur la longue tête de cheval, marmonnante, remuante, qui le bénissait, et sur le poil mal rasé, clair et poivré comme du chêne pâle.

Buck Mulligan jeta un coup d’œil par-dessous le miroir, recouvrit vivement le bol.

– Retour aux baraquements ! fit-il sombrement.

Puis il ajouta d’un ton de prédicateur :

– Car ceci, mon très cher, est l’authentique Christuine : corps et âme et sang et ôme. Musique lente, siou-plaît. Fermez vos yeux les sieurs. Un instant – petit problème de globules blancs. Silence, tous.

Il regarda obliquement vers le haut, siffla longuement, s’immobilisa un moment dans une ferveur extatique, ses dents blanches et égales brillant çà et là d’éclats dorés. Chrysostomos. Deux sifflets stridents percèrent le calme en réponse.

– Merci, mon vieux, s’exclama-t-il. Ça devrait le faire. Coupe le courant, s’il te plaît.

Il sauta du créneau et regarda gravement son spectateur, refermant sur ses jambes les plis épars de sa robe de chambre. Le visage rond et modelé, les bajoues ovales et boudeuses évoquaient un prélat, mécène du Moyen Âge. Un sourire amusé perça doucement ses lèvres :

– Quelle dérision ! fit-il gaiement en le pointant du doigt. Ton nom absurde, un Antique Grec !

Et il se dirigea mi-hilare vers le parapet. Stephen Dedalus monta d’une marche, le suivit sans envie, s’assit à mi-chemin sur le bord du créneau, puis l’observa alors qu’il redressait le miroir sur le parapet, trempait le blaireau dans le bol et se badigeonnait les joues et le cou.

La voix joyeuse de Buck Mulligan continua.

– Mon nom est tout aussi absurde : Malachi Mulligan, deux dactyles. Mais ça sonne hellénique, n’est-ce pas ? Cadencé et solaire comme le cerf. Nous devons aller à Athènes. Viendras-tu si je peux soutirer vingt billets à la tante ?

Il posa le blaireau, et poussa en riant :

– Viendra-t-il, le jeunot jésuite !?

Puis il se concentra sur son rasoir.

– Dis-moi, Mulligan, fit Stephen froidement.

– Oui, mon chou ?

– Combien de temps Haines va-t-il rester dans cette tour ?

Buck Mulligan présenta sa joue droite rasée par-dessus l’épaule.

– Bon Dieu, qu’il est sinistre, laissa-t-il échapper. Le lourd saxon. Il pense que tu n’es pas un honnête homme. Bon dieu, ces foutus anglais. Gavés d’argent et d’indigestion. Il vient d’Oxford vois-tu ? Tu sais, Dedalus, toi tu as le style Oxford. Il n’arrive pas à te saisir. Ah, le nom que je t’ai trouvé : Kinch, le fil du couteau, c’est bien le mieux.

Il se rasait la joue avec précaution.

– Il a déliré toute la nuit sur une panthère noire, fit Stephen. Où est sa carabine ?
– Affligeant et lunatique ! répondit Mulligan. Ça t’a terrifié ?
– Oui, j’étais terrifié, dit Stephen avec force, et une peur visible. Dans le noir avec un homme que je ne connais pas qui geint, délire, et veut descendre une panthère noire. Tu as sauvé des hommes de la noyade. Moi non, je ne suis pas un héros. S’il reste je me barre.

Buck Mulligan regarda d’un œil mauvais la mousse sur son rasoir. Il sauta de son perchoir et fouilla précipitamment les poches de son pantalon.

– Urgence ! grogna-t-il.

Il se dirigea vers le créneau, plongea la main dans la poche de chemise de Stephen et fit :

– Prêtez-nous un bout de votre torche-nez pour essuyer mon rasoir.

Stephen le laissa tirer et exhiber, du bout des doigts, le mouchoir froissé et sale. Buck Mulligan essuya la lame soigneusement. Puis, étudiant le linge, commenta :

– le torche-nez du barde ! Une nouvelle couleur artistique pour les poètes irlandais : vert-de-morve. On en a presque le goût en bouche, non ?

Il grimpa à nouveau sur le parapet et contempla la baie de Dublin. Ses cheveux brun clair ondulaient légèrement.

– Bon dieu ! dit-il doucement. La mer, c’est bien ce que dit Algy : une mère, énorme et douce ? La mer vert-de-morve. La mer à vous faire plisser la prostate. EPI OINPA PONTON. Ah, Dedalus, les Grecs ! Il faut que je t’enseigne. Il faut que tu les lises dans le texte. THALATTA ! THALATTA ! Elle est notre mère, si grande, si douce. Viens voir.
Stephen se mit sur pied, marcha jusqu’au parapet. Il se pencha et regarda l’eau, et le bateau du courrier qui tournait la digue de Kingstown.

– Notre mère toute puissante ! ajouta Buck Mulligan.

Il tourna vivement ses yeux gris et curieux, de la mer vers le visage de Stephen.

– La tante pense que tu as tué ta mère, dit-il. C’est pour ça qu’elle ne veut pas que je traîne avec toi.

– Quelqu’un l’a bien tuée, fit Stephen sombrement.

– Tu aurais pu t’agenouiller, merde, Kinch, quand ta mère t’a demandé, dit Buck Mulligan. Je suis tout aussi hyperboréen que toi. Mais quand je pense à ta mère qui te supplie, dans son dernier souffle, de t’agenouiller, de prier pour elle. Et tu as refusé. Tu as quelque chose de sinistre …

Il ne continua pas, et se badigeonna l’autre joue de savon. Ses lèvres s’ouvrirent d’un sourire indulgent.

– Mais un charmant arlequin ! murmura-t-il pour lui-même. Kinch, le plus charmant des arlequins !

Il se rasait posément, soigneusement, en silence, avec sérieux.

Stephen, un coude reposant sur le granit dur, appuya la main sur son front et contempla l’ourlet élimé, noir et brillant de sa manche. Une douleur, qui n’était pas encore la douleur de l’amour, le taraudait. Elle était venue à lui en silence, dans un rêve, après sa mort ; son corps abîmé, enveloppé lâchement dans un linge mortuaire brun, dégageait une odeur de cire et de palissandre ; son souffle, qui s’était posé sur lui, muet, plein de reproche, une odeur fine de cendres mouillées. Il voyait, par-delà la manche effrangée, cette mer que la voix bien nourrie dans son dos saluait comme une grande et douce mère. La baie, l’horizon formaient un anneau qui contenait la masse verte, terne, de liquide. Une bassine de porcelaine blanche avait été posée au chevet de son lit de mort, pour contenir la bile verte et collante qu’elle avait exprimé de son foie à force de vomissements, grognant, geignant.

Buck Mulligan essuya à nouveau son rasoir.

– Ah, pauvre cabot ! dit-il d’une voix généreuse. Je dois te passer une chemise et quelques mouchoirs. Le falze seconde-main, ça tient ?

– Ils ne me vont pas trop mal, répondit Stephen.

Buck Mulligan s’attaquait au pli soutenant la lèvre inférieure.

– Quelle dérision, lança-t-il amusé. Seconde-jambe, devrait-on dire. Dieu sait quel clodo vérolé les a laissés. J’en ai une jolie paire, avec une raie bien nette, gris. Tu seras épatant, dedans. Je ne blague pas, Kinch. Tu as une sacrée allure quand tu t’habilles.

– Merci, dit Stephen. Je ne les porterai pas s’ils sont gris.
– Il ne les portera pas, confia Buck Mulligan à son reflet dans le miroir. L’étiquette, c’est l’étiquette. Il tue sa mère, mais ne peut pas porter de pantalons gris.

Il referma son rasoir avec soin, et tapotait ses joues lisses.

Stephen détourna son regard de la mer et le posa sur le visage rond, aux yeux bleus grisés et mobiles.

– Ce type qui était sur le Bateau avec moi, la nuit dernière, dit Buck Mulligan, dit que tu as une P.G.A. Il est parti à Dottyville avec Connolly Norman. Paralysie Générale des Aliénés !

Il balaya l’air sur un demi-cercle, flashant le miroir dans le soleil qui irradiait maintenant le large, pour annoncer la nouvelle à la ronde. Ses lèvres rasées s’ourlèrent dans un rire, luisant les pointes de ses dents blanches. Le rire secoua tout son tronc bien fait.

– Regarde-toi, dit-il, l’affreux barde !

Stephen se pencha et contempla le miroir qu’il lui tendait, curieusement fendu. Pointe touffue. Comme il, les autres me voient. Qui m’a choisi une tête pareille ? Bon à épouiller, le cabot. C’est ce que je vois, aussi.

– Je l’ai piqué dans la chambre de la bonniche, fit Buck Mulligan. Bien fait pour elle. La tante a toujours en réserve des servantes fades pour Malachi. Ne le soumet pas à la tentation. Et son nom est Ursula.

Riant à nouveau, il ôta le miroir de la vue de Stephen, qui scrutait encore.

– La rage de Caliban à ne pas reconnaître sa tête dans le miroir, dit-il. Si Wilde était en vie pour voir ça !

Stephen se rétracta tout en pointant du doigt, avec amertume :

– Un symbole de l’art irlandais. Le miroir fêlé d’une servante.

Buck Mulligan prit d’un geste Stephen sous le bras, et l’emmena ainsi faire le tour du rempart, rasoir et miroir claquetant dans la poche où il les avait fourrés.

– Ce n’est pas bien juste de te taquiner ainsi Kinch, pas vrai ? fit-il, bonhomme. Dieu sait que tu as plus de moelle que tous.

Bonne parade. Il craint le scalpel de mon art, et moi le sien. La plume, d’acier froid.

–  Le miroir fêlé d’une servante ! Répète ça au bovin du dessous, tu en toucheras un billet. Il pue le fric et pense que tu n’es pas un honnête homme. Son daron a fait son blé en vendant des laxatifs aux Zoulous ou une foutue arnaque du genre. Bon Dieu, Kinch, si seulement on pouvait travailler ensemble, on pourrait faire quelque chose pour cette île. L’helléniser.

Le bras de Cranly. Son bras.

– Et dire que tu dois mendier devant ces salauds. Je suis le seul qui sait ce que tu es. Pourquoi n’as-tu pas plus confiance en moi ? Quelle dent as-tu contre moi ? C’est Haines ? S’il fait un bruit je ferai venir Seymour et on en fera un bon bizuth, pire que ce qu’on a fait à Clive Kempthorpe.

Voix prospères résonnant, juvéniles, dans les quartiers de Clive Kempthorpe. Visages-pâles : ils se tiennent les côtes hilares, se tapent l’épaule. Ah, j’en crève ! Annoncez-lui la chose avec ménagement, Aubrey ! J’en mourrai ! Les lambeaux de sa chemise fouettent l’air, il chancelle, sautille autour de la table, le pantalon aux chevilles, et Ades du collège Magdalen le poursuit de ses longs ciseaux. Figure de veau, apeurée, barbouillée de confiture. Je ne veux pas qu’on me défroque ! N’allez pas jouer les gros bœufs avec moi !

Cris qui tombent de la fenêtre ouverte, ébranlent le soir sur le court carré. Un jardinier sourd, en tablier, masqué du faciès de Matthew Arnolds, pousse sa tondeuse sur la pelouse sombre, regardant de près la danse contrainte des touffes d’herbe folle.

À nous… Nouveau paganisme… Omphalos.

– Laisse-le rester, fit Stephen. Il ne fait pas de mal, sauf la nuit.

– Qu’est-ce que c’est dans ce cas ? demanda Buck Mulligan impatient. Crache le morceau. Je suis bien franc avec toi. Qu’as-tu contre moi ?

Ils s’arrêtèrent, face à la pointe émoussée du Bray Head qui reposait sur l’eau comme le groin d’une baleine endormie. Stephen dégagea lentement son bras.

– Tu souhaites que je te le dise ? demanda-t-il.

– Certes, qu’est-ce que c’est ? répondit Buck Mulligan. Je ne me souviens de rien.

Il regardait Stephen en parlant. Une brise passa sur son front, gonflant doucement le désordre de ses mèches blondes, et fit scintiller des pointes d’anxiété dans ses yeux.

Stephen, que déprimait déjà le son de sa propre voix, avança :

– Te souviens-tu de la première fois que je suis venu chez toi après la mort de ma mère ?

Buck Mulligan répliqua d’un froncement les sourcils et fit :

– Quoi ? Où ? Je ne peux pas me souvenir de tout. Je me souviens seulement d’idées et de sensations ? Pourquoi ? Qu’a-t-il bien pu se passer, par Dieu ?

– Tu faisais du thé, continua Stephen, et tu es sorti sur le palier pour chercher plus d’eau chaude. Ta mère et une visite sont sortis du salon. Ils ont demandé qui était dans ta chambre.

– Oui ? Dit Buck Mulligan. Qu’est-ce que j’ai dit ? J’oublie.

– Tu as dit, répondit Stephen, *Ah, c’est juste Dedalus dont la mère est morte comme une bête*.

Les joues de Buck Mulligan s’empourprèrent, il parut plus jeune et plus avenant.

– J’ai dit ça ? demanda-t-il. Eh bien ? Où est le mal ?

Il contrôla nerveusement son embarras :

– Et qu’est-ce que la mort, la mort de ta mère, la tienne, la mienne ? Tu n’as vu que ta mère mourir. Moi j’en vois tous les jours à Mater et à Richmond, et les tripes à l’air dans la salle de dissection. C’est une chose bestiale et rien d’autre. Cela n’a aucune importance. Tu n’as pas daigné t’agenouiller au chevet de ta mère quand elle l’a demandé. Pourquoi ? Parce que tu as cette maudite souche de jésuite en toi – sauf qu’elle pousse à l’envers. À mes yeux, tout cela n’est que dérision, dérision bestiale. Ses lobes cérébraux ne fonctionnent pas. Elle appelle le docteur Dom Bucéphale et cueille des boutons-d’or sur son couvre-lit. Contente-la jusqu’au final. Tu as dénié son dernier souhait, et tu me fais la tête parce que je ne chiale pas comme les pleureurs muets qu’affrète le croque-mort Lalouette. Absurde ! Et si je l’ai dit. Je ne cherchais pas à offenser la mémoire de ta mère.

Le flot de paroles l’avait rétabli dans son audace. Stephen, parant et protégeant ainsi les plaies béantes qu’avaient en son cœur laissées les mots, répliqua d’un ton glacial :

– Je ne pensais pas à l’offense faite à ma mère.

– Et à quoi, alors ? demanda Buck Mulligan.

– À l’offense qui m’a été faite, répondit Stephen.

Buck Mulligan fit volte-face.

– Quel être impossible ! s’exclama-t-il,

et d’un pas vif il s’éloigna, longeant le rempart. Stephen restait à son poste, les yeux sur la mer calme en direction du cap. Mer et cap se brouillèrent. Le sang battait dans ses yeux, voilait son regard ; il sentit ses joues s’enfiévrer.

Une voix, de l’intérieur de la tour, appela fortement :

– Es-tu là-haut, Mulligan ?

– J’arrive, répondit Buck Mulligan.

Il se tourna vers Stephen et lança :

– Regarde bien la mer. Que lui importe les offenses ? Laisse béton Loyola, Kinch, et redescend. Le saxon de garde veut son bacon du matin.

Il arrêta sa descente encore un instant, la tête émergeant de l’escalier, au ras du toit :

– Ne gamberge pas là-dessus toute la journée. Je suis un inconséquent. Arrête de broyer du noir.

Sa tête disparut, mais le bourdonnement de sa voix décroissante retentissait encore dans l’escalier :

> *Et ne te détourne pas pour ruminer  
de l’amour l’amer mystère  
car Fergus commande aux chars airains.*
 
Des ombres boisées et silencieuses ondoyaient dans la paix matinale, par-delà la trappe de l’escalier, vers la mer qu’il contemplait. Du rivage vers le large le miroir d’eau blanchissait sous les piques fugaces de ballerines empressées. Gorge blanche de la mer obscure. Les pointes tressées deux à deux. Une main pinçant les cordes de la harpe, alliant leurs accords en-tressés. Mots blancs d’écume, accouplés, chatoyant sur la marée obscure.

Un nuage épais recouvrait peu à peu le soleil, plongeant la baie dans une ombre verdâtre. Elle s’étalait derrière lui, une bassine d’eaux bilieuses. La chanson de Fergus : Je la chantais seul, dans la maison, soutenant les longs accords sombres. Sa porte était ouverte : elle voulait m’écouter. D’effroi et de pitié rendu silencieux, j’allai à son chevet. Elle pleurait dans son triste lit. Ce sont ces mots, Stephen : de l’amour l’amer mystère.

Où, maintenant ?

Ses secrets : vieux éventails en plume, carnets de danse à gland, poudrés de musc, une parure de perles d’ambre dans son tiroir fermé à clé. Une cage d’oiseau se balançait dans la fenêtre ensoleillée de sa maison quand elle était petite. Elle avait entendu le vieux Royce chanter dans la pantomime de *Turko le Terrible* et rit avec les autres quand il entonnait :

>*Je suis le gars  
qui peut jouir  
de l’invisibilité*

Gaîté spectrale, pliée, rangée : parfumée au musc.

> *Et ne te détourne pas pour ruminer*

Rangée, dans la mémoire de la nature avec ses jouets d’enfant. Sous l’assaut des souvenirs, il ruminait. Le verre d’eau qu’elle prenait du robinet de la cuisine, après la communion. Une pomme évidée, remplie de sucre roux rôtissait pour elle sur le feu de la cuisinière, un soir d’automne où il faisait sombre. Ses ongles ronds, rougis du sang des poux qu’elle extirpait des chemises d’enfants.

En rêve, en silence, elle était venue à lui ; son corps, abîmé dans un linceul trop grand, dégageait une odeur de cire et de palissandre ; son souffle, posé sur lui chargé de mots secrets muets, une odeur fine de cendres mouillées.

Ses yeux vitreux, d’outre-tombe fixés sur mon âme, pour la faire vaciller, plier. Sur moi seul fixés. La bougie spectrale sur son agonie. Lumière spectrale sur son visage tordu. Sa respiration lourde et rauque, râle de terreur, et tous priaient à genoux. Ses yeux sur moi pour me faire plier. *Liliata rutilantium te confessorum turma circumdet : iubilantium te virginem chorus excipiat*.

Goule ! Mâcheuse de cadavre !

Non, mère ! Laisse-moi et laisse-moi vivre.

– Ohé Kinch !

La voix de Buck Mulligan résonna dans la tour. Elle se rapprochait de l’escalier, hélant toujours. Stephen, qui tremblait encore du cri de son âme, perçut la lumière douce du soleil, et dans l’air derrière lui les paroles amicales.

– Dedalus, reviens, comme un bon épagneul. Le petit-déjeuner est prêt. Haines s’excuse de nous avoir réveillé cette nuit. Tout va bien.

– J’arrive, fit Stephen en se tournant.

– Descends, pour l’amour de Dieu, pour l’amour de moi, pour l’amour de nous.

Sa tête disparut, réapparut :

– Je le lui ai sorti, ton symbole de l’art irlandais. Très fort, il dit. Tire-s-en un billet, tu veux ? une thune, je veux dire.

– On me paie ce matin, dit Stephen.

– Le butin de l’école ? Combien ? Quatre billets ? Prêtes-en nous un.

– Si tu le veux.

– Quatre magnifiques souverains, s’écria ravi Buck Mulligan. De quoi nous faire une gloire de beuverie, à en épater les druides druidiques. Quatre tout-puissants souverains.

Les bras au ciel, il dévala de tout son poids l’escalier de pierre en chantant dans un cockney désaccordé :

>*Ah, qu’est-ce qu’on sera heureux  
À boire bières, vins et spiritueux  
Le jour du couronnement,  
Du couronnement !  
Ah, qu’est-ce qu’on sera heureux  
Le jour du couronnement !*  

Doux soleil s’égayant sur la mer. Le bol de rasage, de nickel étincelant, abandonné sur le parapet. Je devrais le descendre. Pourquoi ? Ou le laisser là, toute la journée, amitié abandonnée ?

Il s’avança, le tint dans ses mains, absorbant sa fraîcheur, aspirant l’humeur froide du savon baveux sous le blaireau. C’est ainsi que je portais l’encensoir à Clongowes. Je suis un autre aujourd’hui, et pourtant le même. Toujours un servant. Le serviteur d’un servant.

Dans la pièce commune, voûtée et obscure, la robe de chambre de Buck Mulligan passait et repassait devant l’âtre, cachant et révélant tour à tour le halo jaune. Deux traits de jour tombaient des meurtrières sur le sol dallé : à l’intersection des rayons un nuage de charbon et des fumées de graisse grillée s’élevaient en volutes.

– On va s’asphyxier, cria Buck Mulligan. Haines, ouvre la porte tu veux bien ?

Stephen posa le bol sur le buffet. Une longue silhouette s’éleva du hamac où elle reposait, se dirigea vers la porte d’entrée et en ouvrit les battants intérieurs.

– Tu as la clé ? fit sa voix.

– Dedalus l’a, répondit Buck Mulligan. Doux gisant, j’étouffe !

– Il hurla, sans quitter le feu des yeux :

– Kinch !

– Elle est sur la serrure, fit Stephen en s’avançant.

La clé racla par deux fois, la porte s’entrouvrit : entrent, bienvenus ! lumière et air pur. Haines restait sur le seuil, tourné vers l’extérieur. Stephen hala sa valise jusqu’à la table, la posa sur la tranche et s’assit à attendre. Buck Mulligan tira la friture dans un plat à son côté, puis amena le plat et une grosse théière jusqu’à la table, les posa lourdement et soupira d’aise, enfin.

– Je fonds, remarqua-t-il, comme disait la bougie quand… Mais chut ! Plus un mot là-dessus ! Kinch, réveille-toi ! Pain, beurre, miel. Haines, viens donc. La bouffe est prête. Bénissez-nous, Ô Seigneur, ainsi que vos dons ici-bas. Où est le sucre ? Ah, jizz, il n’y a pas de lait.

Stephen rapporta la miche, le pot de miel et le beurrier du buffet. Buck Mulligan s’attabla, pris d’une rogne soudaine.

– C’est quoi ce bordel ? fit-il. Je lui ai dit d’arriver sur les huit heures.

– Nous pouvons le boire noir, dit Stephen, assoiffé. Il y a un citron dans le buffet.

– Toi et tes airs parisiens, tu peux aller te… Je veux du lait de Sandycove.

Haines passa le pas de la porte et, calmement :

– La femme arrive avec le lait.

– Que les bénédictions du Seigneur soient sur toi ! s’écria Buck Mulligan, bondissant de sa chaise. Assieds-toi. Verse le thé. Le sucre est dans la poche. Voilà, j’en peux plus de triturer ces foutus œufs.

Il trancha la friture à même le plat et la flanqua dans les trois assiettes, en récitant :

>– *In nomine patris et filii et spiritus sancti.*

Haine s’assit et versa le thé.

– Je vous donne deux morceaux à chacun. Mais dis, Mulligan, il est plutôt fort, ton thé, non ?

Buck Mulligan, qui allait taillant d’épaisses tranches de pain, prit une voix mielleuse de vieillarde :

– Quand je fas du thé je fas du thé, comme disait la mère Grogan. Et quand je fas de l’eau je fas de l’eau.

– Bigre ! Ça c’est du thé, dit Haines

Buck Mulligan continua, tout tranche et tout miel :

– *C’est comme ça, mamzelle Cahill*, dit-elle. *Fichtre, m’ame*, répondit mamzelle Cahill, *Dieu vous accorde de ne pas faire les deux dans le même pot*.

Il avança, empalée sur son couteau, une épaisse tranche de pain à chacun des commensaux.

– En voilà du folklore pour ton bouquin, Haines, dit-il avec le plus grand sérieux. Cinq lignes de texte, dix pages de notes sur les natifs de Dundrum et leurs dieux aquatiques. Aux presses des sœurs mal-lunées, l’année du grand vent.

Puis, tourné vers Stephen, d’une voix de fausset, perplexe, le sourcil haussé :

– Te souviens-tu, mon frère, si les pots de thé et de chambre de la mère Grogan apparaissent dans le Mabinogion, ou les Upanishad ?

– J’en doute, répondit Stephen d’un ton grave.

– Vous en doutez ? continua Buck Mulligan de la même voix. Vos raisons, je vous prie ?

– Je me figure, fit Stephen sans arrêter de manger, que cela n’est ni dans le Mabinogion, ni ailleurs. La mère Grogan était, on subodore, une parente de Mary Ann.

Le visage de Buck Mulligan jubilait.

– Charmant, dit-il d’une voix hautaine et doucereuse, découvrant ses dents blanches et guignant plaisamment des yeux. Ainsi, vous le pensez ? Trop charmant !

Toute son expression changea d’un trait, et il brama d’une voix rauque et éraillée, attaquant à nouveau la miche au couteau :

>– *Car Mary Ann la vieille peau  
Ça ne lui fait ni froid ni chaud  
Quand ses jupons elle retrousse…*

Il s’empiffra de friture, et mastiqua en fredonnant.
Une forme assombrit l’entrée.

– Voilà le lait, monsieur !

– Entrez m’ame, fit Mulligan. Kinch, attrape la cruche.

Une vieille femme s’avança et s’arrêta aux côtés de Stephen.

– C’est une belle matinée, monsieur, dit-elle. Grâce à Dieu.

– À qui ? demanda Mulligan, lui jetant un coup d’œil. Ah, pour sûr !

Stephen s’étira en arrière et prit le pot à lait du buffet.

– Les insulaires, commenta Mulligan désinvolte à Haines, mentionnent fréquemment le collecteur de prépuces.

– Combien, monsieur ? demanda la vieille femme.

– Un quart, répondit Stephen.

Il la regardait alors qu’elle versait dans le godet, puis dans la cruche un lait blanc et crémeux, pas le sien. Vieux tétons fripés. Elle versa un nouveau godet, et une lichette pour bonne mesure. Elle avait surgi du monde de l’aube, vieille et secrète, une messagère peut-être. Elle vantait les mérites du lait, à le verser. Accroupie au point du jour sous une vache patiente, dans le pré opulent, une sorcière sur son tabouret, ses doigts ridés et agiles sur les mamelles qui giclent. Elles meuglaient pour l’accueillir, à la reconnaître, vaches soyeuses de rosée. Soie de taure, pauvre vieillarde, ces noms qu’on donnait à l’Irlande d’antan. Une vieille chouette errante, enveloppe terrestre d’une immortelle au service de son conquérant et du gai félon, leur cocue commune, une messagère du matin secret. Pour servir ou tancer, il ne saurait dire : mais il dédaignait de mendier ses faveurs.

– C’est bien vrai, ma’me, fit Buck Mulligan en versant le lait dans les tasses.

– Goûtez-le, monsieur, dit-elle.

Il s’exécuta.

– Si on pouvait se sustenter toujours aussi sainement, déclama-t-il à son intention, nous n’aurions pas tant de dents pourries et d’estomacs aigris dans le pays. Nous vivons dans une tourbière, nous mangeons des cochonneries, et les rues sont recouvertes de poussière, de crottin de cheval et du crachat des tuberculeux.

– Êtes-vous un étudiant en médecine, monsieur ? demanda la vieille.

– Je le suis, ma’me, répondit Buck Mulligan.

– Voyez-vous ça, fit elle.

Stephen écoutait dans un silence méprisant. Elle courbe sa vieille tête devant qui lui parle fort, son rebouteux, son homme-médecine : et elle m’ignore. Devant la voix qui la confesse et oint pour le tombeau tout ce qu’il y a d’elle sauf ses entrailles impures, faite de la chair de l’homme mais non à la semblance de Dieu : la proie du serpent. Et maintenant, devant cette voix forte qui commande son silence : le regard vacillant et surpris.

– Comprenez-vous ce qu’il dit ? lui demanda Stephen.

– Est-ce là du français que vous parlez, monsieur ? fit la vieille femme à Haines.

Haines lui adressa un plus long discours, avec assurance.

– De l’irlandais, intervint Buck Mulligan. Vous avez un fond gaélique ?

– Je pensais bien, à l’oreille, que c’était de l’irlandais, fit-elle. Venez-vous de l’Ouest, monsieur ?

– Je suis anglais, répondit Haines.

– Il est anglais, commenta Buck Mulligan, et il pense que nous devrions parler irlandais en Irlande.

– Pour sûr qu’on devrait, dit la vieille femme, et j’ai bien honte de ne pas le parler moi-même. Les gens qui savent, à ce que j’ai entendu, disent que c’est un grand langage.

– Grand, ce n’est pas le mot, ajouta Buck Mulligan. Une pure merveille. Une resucée de thé, Kinch. En voulez-vous, ma’me ?

– Non merci, monsieur, dit la vieille femme, enfilant l’anse de son bidon sur son avant-bras et s’apprêtant à partir.

Haines lui dit alors :

– Avez-vous votre facture ? Nous ferions mieux de la payer, non, Mulligan ?

Stephen remplit les trois tasses.

– Une facture, monsieur ? fit-elle hésitante. Eh bien, ce sont sept matins une pinte à deux pence sept fois deux un shilling et deux pence et ces trois matins un quart à quatre pence encore un shilling. Un shilling et un et deux ça fait deux et deux, monsieur.

Buck Mulligan poussa un soupir, s’enfourna un croûton généreusement beurré des deux côtés dans la bouche, étendit les jambes et se mit à fouiller ses poches de pantalon.

– Paie, et de bon gré, lui dit Haines en souriant.

 Stephen versa une troisième tasse, un reste de thé colorant à peine le lait gras et épais. Buck Mulligan sortit un florin, le fit tourner entre ses doigts et s’écria :

– Un miracle !

Il le fit passer le long de la table, en direction de la vieille femme, et dit :

– N’exigez plus rien de moi, ma douce. Tout ce que je peux vous donner, je le donne.

Stephen mit la pièce dans la main réticente.

– Nous vous devrons deux pence, ajouta-t-il.

– Il y a le temps, monsieur, dit-elle, prenant la pièce, il y a le temps. Bonne journée, monsieur.

Elle fit une révérence et s’en fut, accompagnée des tendres accents de Buck Mulligan :

>– *Cœur de mon cœur, s’il y avait plus,
Plus serait déposé à vos pieds*

Il se tourna vers Dedalus et dit : sérieux, Dedalus, je suis à sec. File chercher ton butin à l’école et rapporte-nous de l’argent. Aujourd’hui les bardes doivent boire et festoyer. L’Irlande compte que chaque homme aujourd’hui fasse son devoir.

– Cela me fait penser, fit Haines, que je dois rendre visite à votre librairie nationale aujourd’hui.

– Mais d’abord, la baignade, dit Mulligan.

Puis, se tournant à nouveau vers Stephen, il lui demanda platement :

– Le jour de tes ablutions mensuelles est-il arrivé, Kinch ?

Et, à Haines :

– Ce barde malpropre met un point d’honneur à ne se baigner qu’une fois par mois.

– Toute l’Irlande est baignée par le Gulfstream, décréta Stephen en regardant un filet de miel s’épancher sur sa tranche de pain.

Haines, qui était dans un coin et nouait avec prestance une écharpe sur le col déboutonné de son polo, commenta :

– J’ai l’intention de faire un recueil de vos dires, si vous m’y autorisez.

À mon intention. Ils se lavent, se baignent, se frottent. Contrition de l’esprit. Conscience. Mais la tache ne part pas.

– Et celui sur le miroir fêlé d’une servante, le symbole de l’art irlandais, c’est carrément bon.

Buck Mulligan fit du pied à Stephen sous la table, et appuya avec chaleur :

– Attends de l’avoir entendu sur Hamlet, Haines.

– Vraiment, c’est sincère, continua Haines à l’intention de Stephen. J’y pensais justement quand cette pauvre vieille chose est entrée.

– Est-ce que j’en tirerais de l’argent ? Demanda Stephen.

Haines rit, et tout en prenant son chapeau de feutre gris sur le crochet du hamac, répondit :

– Je ne sais pas, certainement.

Il fit quelques pas sur le seuil. Buck Mulligan se pencha vers Stephen et lui glissa brutalement :

– Les pieds dans le plat. Pourquoi lui as-tu dit ça ?

– Quoi ? fit Stephen. Il s’agit d’obtenir de l’argent. De qui ? De la laitière ou de lui. C’est pile ou face, il me semble.

– Je le chauffe bien, continua Buck Mulligan, et tu t’amènes avec tes airs à la con et tes sarcasmes glauques de jésuite.

– J’ai peu d’espoir, répondit Stephen, d’un côté ou de l’autre.

Buck Mulligan soupira tragiquement, posa sa main sur le bras de Stephen :

– Et de mon côté, Kinch.

Mais sa voix changea et il ajouta :

– Pour te dire la vérité vraie, je pense que tu as raison. Qu’ils aillent se faire foutre, s’ils ne sont pas bons à ça. Pourquoi ne la joues-tu pas comme moi ? Qu’ils aillent se faire foutre. Sortons de ce boxon.

Il se leva, dénoua gravement sa robe de chambre et s’en dévêtit, commentant d’un air de résignation :

– Mulligan est dépouillé de ses vêtements.

Il vida ses poches sur la table.

– Voilà ton torche-morve.

Il ajustait son col rigide, sa cravate rebelle tout en leur parlant, les rabrouant, ainsi que sa chaîne de montre qui pendouillait. Ses mains plongèrent dans la malle et y fourrageaient alors qu’il réclamait un mouchoir propre. Bon Dieu, il faudra bien qu’on habille le personnage. Je veux des gants bordeaux et des bottines vertes. Contradiction ? Je me contredirais ? Fort bien, je me contredis. Malachi, le mercuriel. Un missile noir et bosselé vola des mains bavardes.

– Et voilà ton chapeau quartier-latin, fit-il.

Stephen le ramassa en s’en coiffa. Haines les appela du seuil :

– Vous venez, vous autres ?

– Je suis prêt répondit Buck Mulligan, qui s’avança vers la porte. Viens, Kinch. Tu as mangé tout ce qu’on a laissé, j’imagine. L’allure et la voix graves et résignées, il passa le pas de la porte, et lança d’un ton presque douloureux :

– Il sortit, et trouva là Mèrement.

Stephen attrapa son bâton posé contre le mur, les suivit au-dehors et, pendant qu’ils descendaient l’échelle, tira à lui la lourde porte de fer et la verrouilla. Il mit l’énorme clé dans sa poche intérieure.

Au pied de l’échelle, Buck Mulligan demanda :

– Tu as pris la clé ?

Je l’ai, dit Stephen, et il prit les devants.

Il marchait. Derrière lui Buck Mulligan battait de sa lourde serviette de bain les pousses éminentes de fougères ou d’herbes.

– À bas, sire ! Comment osez-vous, sire !

Haines demanda :

– Paies-tu un loyer pour cette tour ?

– Douze billets, répondit Mulligan.

– Au ministère de la Guerre, ajouta Stephen par-dessus son épaule.

Ils s’arrêtèrent pendant que Haines inspectait la tour. Il lâcha :

– Plutôt lugubre, en hiver, je dois dire. Des Martello, vous appelez ça ?

– Billy Pitt les a fait construire, expliqua Buck Mulligan, quand les Français tenaient la mer. Mais la nôtre est l’*Omphalos*.

– Quelle est votre idée, sur Hamlet ? demanda Haines à Stephen.

– Non, non, cria d’angoisse Buck Mulligan. Je ne suis pas à la hauteur de Thomas d’Aquin, ni de ses cinquante-cinq raisons de l’existence de Dieu. Attendez que j’aie imbibé quelques pintes.

Il se tourna vers Stephen et, tirant avec soin sur les pointes de son gilet primevère, lui lança :

– Tu n’y arriverais pas en dessous de trois pintes, hein, Kinch ?

– Cela a tant attendu, dit Stephen mollement, que cela peut encore attendre.

– Vous piquez ma curiosité, dit aimablement Haines. Est-ce une sorte de paradoxe ?

– Pouah ! fit Buck Mulligan. Nous avons dépassé Wilde et les paradoxes. C’est tout simple. Il prouve par l’algèbre que le petit-fils de Hamlet est le grand-père de Shakespeare, et qu’il est lui-même le fantôme de son propre père.

– Hein ? dit Haines, qui esquissa un geste vers Stephen. Lui lui-même ?

Buck Mulligan balança sa serviette sur son cou à la manière d’une étole, et, pouffa dans l’oreille de Stephen :

– Oh, ombre de Kinch l’ancien ! Japhet en quête d’un père !

– Nous sommes toujours fatigués le matin, dit Stephen à Haines, et c’est plutôt long à expliquer.

Buck Mulligan, qui avait repris la marcha, leva les bras au ciel :

– Seule la sainte pinte peut délier la langue de Dedalus.

– Je voulais dire, expliqua Haines à Stephen en emboîtant le pas, cette tour, ces falaises, cela m’évoque Elseneur, d’une certaine manière. *Qui déborde son assise et surplombe la mer*, n’est-ce pas ?

Buck Mulligan se tourna un court instant vers Stephen mais resta coi. Et dans cet instant d’intense silence Stephen vit sa propre image, endeuillé-poussiéreux-bon-marché, entre leurs costumes clairs.

– C’est un conte merveilleux, dit Haines, les arrêtant à nouveau.

Yeux, pâles comme la mer que le vent a fraîchie, plus pâles encore, fermes, prudents.
Le maître des mers, il regardait le midi par-delà la baie vide que seuls troublaient, sur l’horizon brillant le mince filet du courrier et, tournant les Muglins, une voile.

– J’en ai lu quelque part une interprétation théologique, reprit-il, absorbé. Le Père et le Fils. Le Fils s’efforçant d’obtenir sa rédemption du Père.

En réponse, le visage de Buck Mulligan s’ouvrit de part en part d’un sourire insouciant. Il les regarda, la bouche bien dessinée, béante, joviale, les yeux, dont il avait retiré toute malice, guignant d’une folle gaîté. Il hochait la tête comme une marionnette, les bords de son panama suivaient la mesure, et d’une voix de bête heureuse il entonna :

>– *Un type plus bizarre, où trouvez-vous ça ?  
Ma mère est une juive et mon père un oiseau  
Avec le charpentier, c’est pas de tout repos  
Alors vivent les douze et en avant l’trépas.*  

Il leva le majeur, en avertissement :

>– *et si quelqu’un dit que je n’suis pas divin,  
il boira pas à l’œil quand je ferai du vin  
que de l’eau et encore pas très claire,  
c’qui sort quand du vin à l’eau j’m’affaire.*

Il tira sur le bâton de Stephen en guise d’adieu, se précipita vers un escarpement, battant des mains comme des nageoires, ou plutôt des ailes, prêt à l’essor, et continua :

>– *adieu, oui, adieu ! mes dires, qu’on les grave  
des morts, j’en suis rev’nu, dites-le à Mathieu.  
j’ai ça dans le sang, le vol je l’ai grave,  
le vent dans les olives… Adieu, oui, adieu !*

Il cabriolait pour eux, au bord du vide, une douzaine de mètres, battant ses ailes palmées, sautillant, son chapeau mercuriel palpitant dans la brise fraîche qui ramenait vers eux ses pépiements d’oiseau.

Haines, qui avait ri avec réserve, dit à Stephen en marchant à ses côtés :

– Je suppose qu’on ne devrait pas rire. C’est plutôt blasphématoire. Je ne suis pas un croyant, personnellement je veux dire. Mais sa gaîté rend tout cela presque inoffensif, n’est-ce pas ? Comment l’a-t-il appelé ? L’histoire du charpentier ?

– La ballade de Jésus le blagueur, répondit Stephen.

– Ah, remarqua Haines, vous l’avez déjà entendue ?

– Trois fois par jour, après les repas, fit Stephen sèchement.

– Vous n’êtes pas croyant, n’est-ce pas ? demanda Haines. Je veux dire, un croyant au sens strict. Création du néant, les miracles, un Dieu personnel.

– Le mot n’a qu’un seul sens, il me semble, dit Stephen.

Haines s’arrêta et sortit un étui à cigarette d’argent poli, orné d’une gemme verte. Il l’ouvrit du pouce et le présenta.

– Merci, dit Stephen en prenant une cigarette.

Haines se servit et fit claquer le couvercle. Il le remit dans sa poche latérale, prit dans la poche de son gilet un briquet nickelé, en fit sauter la capote, alluma sa cigarette, puis avança la mèche enflammée, protégée par l’autre main, à Stephen.

– Oui, bien sûr, fit-il, alors qu’ils reprenaient la marche. Soit on croit, soit on ne croit pas, n’est-ce pas. Pour ma part je n’ai jamais pu digérer cette notion de Dieu personnel. Ce n’est pas quelque chose que vous défendez, je suppose ?

– Vous avez en moi, répondit Stephen acculé et de mauvais gré, un affreux spécimen de libre pensée.

Il avançait dans un silence réticent, traînant de côté son bâton. La pointe suivait le sentier et crissait par moment sur ses talons. Mon fiel, qui me suit et me hèle, Steeeeeeeeeeeephen ! Un sillon tortueux le long du chemin. Ils le fouleront cette nuit, quand ils reviendront dans le noir. Il veut la clé. Elle est à moi. J’ai payé le loyer. Et maintenant je mange son pain amer. Donne-lui la clé. Aussi. Tout. Il la demandera. Je l’ai vu dans ses yeux.

Après tout, commença Haines…

Stephen se retourna et vit que le regard froid qui l’avait jaugé n’était pas totalement dénué de sympathie.

– Après tout, je pense bien que vous êtes capable de vous libérer. Vous êtes votre propre maître, il me semble.

– Je sers deux maîtres, dit Stephen, l’un anglais et l’autre italien.

– Italien ? fit Haines.

Une reine démente. Vieille et jalouse. À genoux, devant moi.

– Et un troisième, ajouta Stephen, il y en a un qui me veut pour des gâches.

– Italien ? répéta Haines. Que voulez-vous dire ?

– L’empire britannique, répondit Stephen dont le teint se colorait, et la sainte église catholique apostolique et romaine.

Haines détacha de sa lèvre inférieure quelques fibres de tabac avant de parler :

– Je peux tout à fait le comprendre, dit-il avec calme. Un Irlandais se doit de penser cela, j’avancerais même. Nous avons le sentiment, en Angleterre, de vous avoir traité assez injustement. La faute à l’histoire, à ce qu’il semble.

Sonnèrent à toute volée sur la mémoire de Stephen les cloches de bronze du triomphe : Gros titres, fiers oriflammes : *et unam sanctam catholicam et apostolicam ecclesiam*. L’insensible poussée, les inflexions du rite et du dogme, comme la distillation de ses propres pensées, une alchimie d’étoiles. Symbole des apôtres pendant la messe en l’honneur du pape Marcel, les chants s’unirent, d’une seule voix forte tonnant l’affirmation : et derrière leur chant l’ange vigilant de l’église militante venait désarmer et menacer ses hérésiarques. Une horde d’hérésies en fuite, les mitres en désordre : Photius et sa portée de railleurs, Mulligan l’un d’eux ; Arius, toujours à combattre la consubstantialité du Fils et du Père ; et Valentin, celui qui snobait le corps terrestre du Christ ; et Sabellius, subtil africain, à soutenir que le Père était lui-même son propre Fils. Paroles de Mulligan, parlées il y a peu en moquerie de l’étranger. Vaine moquerie. Sûrement, le néant attend ceux qui tissent du vent : une menace de ces anges combattants de l’église, hôte de Michel, et qui toujours la défendent à l’heure du conflit de leurs lances et leurs boucliers, un désarmement, une défaite.

Oyez, oyez ! Longs applaudissements. *Zut ! Nom de dieu !*
– Bien sûr, je suis britannique, continua la voix de Haines, et je me sens tel. Je ne veux pas non plus voir mon pays tomber dans les mains de juifs allemands. C’est notre problème national, j’en ai peur, aujourd’hui.

Deux hommes observaient, debout au bord de la falaise : homme d’affaires, marin.

– Elle fait voile vers port Bullock.

Le marin hocha la tête en direction du nord de la baie, un peu méprisant :

– Il y a cinq brasses là-bas. Il se fera emporter par là quand la marée arrivera, d’ici à une heure. C’est le neuvième jour, aujourd’hui.

L’homme qui s’est noyé. Une voile qui raie la baie vierge, dans l’attente qu’une boursouflure remonte des fonds et se roule pour offrir sa face bouffie et blanche de sel au soleil.
Me voici.

Ils descendirent le sentier tortueux jusqu’à la crique. Buck Mulligan se dressait sur une pierre, en manche de chemise, sa cravate déclipsée ondoyant sur l’épaule. Un jeune homme s’accrochait à un éperon rocheux et remuait lentement, à la manière d’une grenouille, ses jambes vertes dans la gelée sombre de l’eau.

– Le frère t’accompagne, Malachi ?

– Descendu à Westmeath. Avec les Bannons.

– Il est encore là-bas ? J’ai reçu une carte de Bannon. Dit qu’il s’y est trouvé une friandise, toute fraîche. La fille-photo l’appelle-t-il.

– Un instantané, alors ? Courte exposition.

Buck Mulligan s’assit à dénouer ses bottines. Un homme âgé surgit à proximité de l’éperon rocheux, le visage rouge, soufflant. Il grimpait de pierre en pierre. L’eau scintillait sur son front et sa couronne de cheveux gris, l’eau ravinait sa poitrine puis sa panse, et giclait en rythme du pagne noir qui couvrait lâchement ses reins.

Buck Mulligan s’écarta pour le laisser passer et, jetant un coup d’œil à Haines et Stephen se signa pieusement de l’ongle du pouce, front et lèvres et poitrine.

– Seymour est de retour, dit le jeune homme, agrippant à nouveau l’éperon. Plaque la médecine et s’engage dans l’armée.

– Non, tu te fous de moi ! fit Buck Mulligan.

– Part dès la semaine prochaine pour potasser. Tu connais cette rousse de Carlisle, Lily ?

– Oui.

– Ils étaient tout câlins hier sur la jetée. Le père est pou-rri-to de fric.

– Elle est en cloque ?

– Demande ça à Seymour.

– Seymour, un putain d’officier ! dit Buck Mulligan.

Il hochait la tête, absorbé, tout en baissant ses pantalons, puis se leva et annonça platement :

– Les rousses, ça fraie comme les chèvres.

Il s’arrêta, paniqué, se palpant le torse sous la chemise ouverte.

– Ma douzième côte a disparu, cria-t-il. Je suis le *Ubermensch*. Moi et Kinch l’édenté, les surhommes.

Il s’extirpa de la chemise et la lança sur le tas que formaient ses vêtements derrière lui.

– Tu entres, Malachi ?

– Oui. Fais-moi une place dans le lit.

Le jeune homme se propulsa à reculons au travers l’eau et gagna le milieu de la crique en deux brasses longues et précises. Haines s’assit sur un caillou, la cigarette aux lèvres.

– Tu ne viens pas ? demanda Buck Mulligan.

– Un peu plus tard. Pas sur le déjeuner.

Stephen se détourna.

– J’y vais, Mulligan, dit-il.

– Donne-nous cette clé, fit Buck Mulligan, pour lester ma chemise.

Stephen lui passa la clé. Buck Mulligan la déposa sur ses vêtements.

– Et deux pence, dit-il, pour une pinte. Lance-les là.

Stephen jeta les deux pence sur le tas. Habillage, déshabillage. Buck Mulligan, raide et droit, les mains jointes sur le devant, récita solennellement :

– Celui qui vole le pauvre prête au Seigneur. Ainsi parlait Zarathoustra.

Le corps rondelet plongea.

– Nous nous reverrons, dit Haines en se tournant, souriant devant l’Irlande ensauvagée, vers Stephen qui remontait le sentier.

Corne de taureau, sabot de cheval, sourire de saxon.

– Au Ship, cria Buck Mulligan. Douze heures trente.

– Bien, dit Stephen.

Il remontait les lacets sentier.

>*Liliata rutilantium.  
Turma circumdet.  
Iubilantium te virginum.*

L’auréole grise du prêtre, dans une niche où il s’habillait discrètement. Je ne dormirai pas ici ce soir. Quant à la maison, je ne puis.

Une voix douce l’appela longuement de la mer. Au tournant, il agita la main. L’appel retentit à nouveau. Une tête lisse et brune, celle d’un phoque, au large, ronde.

Usurpateur.
