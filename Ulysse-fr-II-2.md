M. Bloom marchait sobrement le long des camions alignés sur le quai Sir John Rogerson. Il passa le passage des Moulins à Vent, Leask le broyeur de lin, les bureaux des Postes et Télégraphes. Aurais pu donner cette adresse, en fait. Et voilà la maison des marins. Il tourna le dos aux bruits matinaux qui s’élevaient du quai et s’engagea dans la rue Lime. Au niveau des cottages Brady un garçon des tanneries traînassait, la chaîne de son seau d’abats en main, un mégot mâchouillé aux lèvres. Une fille plus petite, le front barré de cicatrices d’eczéma, posa un œil sur lui, la main sans vie sur un cerceau cabossé, de barrique. Lui dire que s’il fume il ne grandira pas. Oh, laisse-le vivre ! Qu’elle n’est pas si rose, sa vie. À attendre à la porte du bistrot pour ramener pa’ à la maison. Reviens chez ‘man, pa’. Heure creuse : doit pas y avoir grand monde dedans. Il traversa la rue Townsend, passa devant la façade sévère du Bethel. El, oui : la maison de : Aleph, Beth. Et devant chez Nichols, le croque-mort. C’est à onze heures. J’ai le temps. Parie que c’est Corny Kelleher qui a dégoté le job pour O’Neill. Chante les yeux fermés. Corny. L’ai rencontrée au square. Un soir. Quelle foire. Mouchard de police. Nom et adresse elle m’a donnés, mirliton ton ton ton. Sûrement lui qui l’a dégoté. Enterrez-le pas cher dans un comment-vous-dites. Mirliton ton ton ton, mirliton ton ton ton.

Dans l’allée Westland, il s’arrêta devant la vitrine de la Compagnie des Thés de Belfast et d’Orient et se mit à lire les étiquettes sur le papier brun des paquets : mélange premier choix, qualité supérieur, thé des familles. Plutôt chaud. Thé. Faut que j’en tire de Tom Kernan. Impossible de lui demander à l’enterrement, par contre. Laissant ses yeux lire mollement il se découvrit le chef, respirant au passage son huile capillaire, et envoya sa main droite passer sur son front et ses cheveux dans un ralenti gracieux. Quelle chaleur ce matin. Ses yeux sous leurs paupières tombées allèrent chercher le minuscule nœud du bandeau intérieur en cuir de son chap. prem. choix. Y est bien. Sa main droite plongea dans la coupe du chapeau. Ses doigts trouvèrent vite une carte sous le bandeau et la transportèrent à sa poche de veston.

Si chaud. Sa main droite passa à nouveau, plus lentement encore, sur son front et ses cheveux. Puis il remit son chapeau, soulagé : et reprit sa lecture : mélange premier choix, composé des meilleures variétés de Ceylan. L’extrême orient. Ça doit être un chouette endroit : le jardin du monde, de grandes feuille paresseuses, flotter dessus à la dérive, des cactus, les prés en fleur, les lianes-serpents comme ils appellent ça. Me demande si c’est comme ça. Ces cinghalais qui bullent au soleil, Dolce Farniente, ils n’en fichent pas une de la journée. Dorment six mois sur douze. Trop chaud, pas d’embrouille. Influence du climat. Léthargie. Fleurs d’oisiveté. Vivent d’air, surtout. Azotes. Serres des jardins botaniques. Plantes fragiles. Nénuphars. Les pétales sans force pour. La maladie du sommeil dans l’air. Marcher sur des feuilles de rose. Imagine d’être là à digérer des tripes ou des pieds de veau. Où était ce type dans la photo quelque-part ? Ah oui, dans la mer morte, à faire la planche avec son bouquin sous un parasol. À cause du poids de l’eau, non, le poids du corps dans l’eau est égal au poids de quoi ? Ou c’est le volume qui est égal au poids ? C’est une loi un truc comme ça. Vance au lycée, qui faisait craquer ses jointures en prêchant. Le programme de lycée. Programme de craques. Qu’est-ce que le poids quand on dit poids ? Trente-deux par seconde par seconde. La chute des corps. La terre. C’est la force de la gravité de la terre c’est le poids.

Il se détourna et traversa la rue d’un pas nonchalant. Comment marchait-elle, avec ses saucisses ? Un peu comme ça. Sans marquer d’arrêt il sortit *L’homme libre* plié de sa poche de veston, le déplia, l’enroula dans le sens de la longueur comme une badine, puis en frappait la jambe de son pantalon à chaque enjambée, languide. L’air de rien : je passais juste. Par seconde par seconde. Par seconde chaque seconde ça veut dire. Du bord du trottoir il lança un regard acéré sur le bureau de poste, au travers de la porte. Boîte aux lettres. Déposer ici. Personne. Entrons.

Il tendit la carte au travers de la grille métallique.

– Y a-t-il des lettres pour moi ? demanda-t-il.

Pendant que la postière cherchait dans un casier il contempla l’affiche de recrutement, des soldats à la parade, toutes armes : et tenait sa badine sous le nez, respirant le papier journal fraîchement imprimé. Pas de réponse, sans doute. Suis allé trop loin l’autre fois.

L’employée lui rendit sa carte, et une lettre, au travers de la grille. Il la remercia et regarda hâtivement l’enveloppe tapée à la machine.

M. Henry Flower,
Boîte Postale Westland Row,
City.

Répondu, au moins. Il glissa carte et lettre dans sa poche de veston, passa en revue les soldats à la parade. Où est le régiment du vieux Tweedy ? Réprouvé. Là : bonnet à poil et plumet. Non, c’est un grenadier. Manchette à pointe. Le voilà : Royal Dublin Fusiliers. Tuniques Rouges. Tape-à-l’œil. Ça explique que les femmes leur courent après. L’uniforme. Plus facile de les engager les entraîner. La lettre de Maud Gonne, qu’on les interdise dans la rue O’Connell la nuit : la honte de notre capitale irlandaise. La feuille de Griffith qui enfonce le clou : une armée minée par les maladies vénériennes : règne sur les mers et sous la bière. Z’ont pas l’air bien finis : comme hypnotisés. Droit devant. Marquez le pas. Table-hable. Lit-hit. Les gens du Roi. On le voit jamais en pompier ou en poulet. Un maçon : pour sûr.

Il sortit en flânant de la poste et tourna à droite. Parler : comme si ça pouvait arranger les choses. Sa main glissa dans sa poche et son index trouva son chemin sous le revers de l’enveloppe, et le déchira par à-coup. Les femmes y seront sensibles : non je ne crois pas. Ses doigts firent sortir la lettre et froissèrent l’enveloppe dans sa poche. Il y a un truc d’épinglé : un photo peut-être. Mèche ? Non.

M’Coy. M’en défaire, vite. Me ferait perdre mon chemin. Déteste les gens quand tu.

– Salut Bloom. Où vas-tu comme ça ?

– Salut, M’Coy. Je me balade.

– Sain de corps ?

– Au poil. Et toi ?

– On survit, dit M’Coy.

Les yeux sur la cravate et le costume noirs, il baissa la voix pour demander, déférent :

– Il n’y a… rien de fâcheux j’espère ? Je vois que tu…

– Oh, non, fit M. Bloom. Ce pauvre Dignam, tu sais. C’est l’enterrement aujourd’hui.

– C’est vrai, le pauvre. C’est ainsi. À quelle heure ?

Une photo, non. Un badge peut-être.

– On… onze, répondit M. Bloom.

– Il faut que j’essaie d’y aller, dit M’Coy. Onze heures, c’est ça ? Je l’ai appris hier au soir. Qui me l’a dit ? Holohan. Tu connais La Retenue ?

– Je le connais.

M. Bloom regardait de l’autre côté de la chaussée, un cabriolet qui s’était garé devant la porte du Grosvenor. Le portier hissa la valise entre les deux sièges. Elle attendait, immobile, pendant que l’homme, mari, frère, lui ressemble, cherchait de la monnaie dans ses poches. Plutôt stylé ce manteau, avec ce col roulé, plutôt chaud par un temps pareil, on dirait une couverture. Immobile et désinvolte, les mains dans les poches apparentes. Comme cette altière créature au match de polo. Les femmes, toujours l’esprit de caste, jusqu’à ce qu’on touche au bon endroit. Porte bien et agit bien. Réserve, sur le point de tomber. L’honorable Mme et Brutus est un homme honorable. La posséder, une fois, et voilà la raideur envolée.

– J’étais avec Bob Doran, il est parti pour une de ses cuites périodiques, et comment il s’appelle, Bantam Lyons. On était pas loin, chez Conway.

Doran Lyons, chez Conway. Levait une main gantée à ses cheveux. Arrive La Retenue. Pas complètement à sec. Redressant la tête il regarda au loin, sous ses paupières abaissées, il voyait la peau fauve et brillante luire sous le jour cru, l’entrelacs des tresses. Je vois clair, aujourd’hui. L’humidité ambiante fait voir loin, peut être. Papotant de ci de ça. Main de dame. De quel côté va-t-elle monter ?

– Et il a dit : C’est bien triste, ce qui est arrivé à notre ami, pauvre Paddy ! Quel Paddy ? J’ai fait. Ce pauvre petit Paddy Dignam, il a dit.

Partie de campagne : Broadstone, sans doute. Bottines brunes, montantes, lacets qui pendillent. Pied bien tourné. Qu’est-ce qu’il a à farfouiller pour la monnaie ? Me voit la regarder. Toujours un œil pour le type suivant. Plan B. Deux cordes à son arc.

– Pourquoi ? je lui ai dit. Qu’est-ce qu’il a ? j’ai dit.

Fière : Riche : bas de soie.

– Oui, dit M. Bloom

D’un léger mouvement de côté il dépassa la tête parlante de M’Coy. Monte dans la minute.

– Qu’est-ce qu’il a ? j’ai dit. Il est mort, il a fait. Et, juré, il a vidé sa pinte. C’est Paddy Dignam ? j’ai dit. J’en croyais pas mes oreilles. J’étais avec lui vendredi dernier, ou jeudi c’était, à L’Arche. Oui, il a dit. Parti. Il est mort lundi, pauvre gars.

Alerte ! Alerte ! Éclair-de-soie-riche-bas-blanc. Alerte !

Une voiture de tramway vira lourdement en klaxonnant devant lui.

Raté. La peste de ton groin, beuglard. Sentiment de mise à l’écart. Le Paradis et la Péri. Se passe toujours comme ça. Juste au moment. La fille du porche, rue Eustace, lundi c’était qui ajustait sa jarretelle. Et sa copine qui s’interpose. Esprit de Corps. Et alors, vous voulez ma photo ?

– Oui, oui, laissa échapper M. Bloom sur un morne soupir. Encore un qui s’en va.

– Un des meilleurs, fit M’Coy.

Le tram passa. Ils roulaient déjà vers le Pont de la Boucle, sa main richement gantée sur l’accoudoir d’acier. Girouette, volette : la dentelle de sa voilette. Girouette, gire.

– La femme va bien, je suppose ? demanda M’Coy, changeant de ton.

– Mais oui, dit M.Bloom. On ne peut mieux. Merci.

Distrait, il déroula sa badine en papier journal et distrait, il lut :

>*Mais qu’est un foyer  
Sans La Terrine du Prunier ?  
Inachevé  
Alors qu’avec, quel pied.*

– Ma bourgeoise vient de recevoir un engagement. Enfin, ce n’est pas encore conclu.

Encore le coup de la valise. Au fait serait-ce un problème. Déjà donné, merci.

M. Bloom leva affable et sans empressement ses larges paupières.

– Ma femme aussi, dit-il. Elle va chanter dans un truc chicos au Ulster Hall de Belfast, le vingt-cinq.

– Vraiment ? fit M’Coy. Enchanté de l’apprendre, mon vieux. Qui monte ça ?

Mme Marion Bloom. Pas encore montée. La reine était dans sa chambre, qui mangeait du pain bis. Pas de livre. Sept cartes à jouer le long de sa cuisse, noircies. Dame brune et homme blond. Boule de fourrure : le chat. Lambeau d’enveloppe.

>*Pour  
Un  
Peu  
D’amour  
Un peu d’amour…*

– C’est un genre de tournée, vois-tu, dit M. Bloom pensivement. Un peu d’amouur. Il y a un comité de formé. Partage des frais et profits.

M’Coy hocha la tête, tira sur sa moustache rêche.

– Eh bien, dit-il, voilà une bonne nouvelle.

Il s’en allait.

– Eh bien, content de te voir en forme, fit-il. Sûr qu’on va se recroiser.

– Oui, dit M. Bloom.

– Tant que j’y pense, dit M’Coy. Tu pourrais signer pour moi aux funérailles ? J’aimerais y aller mais pas sûr que je puisse, tu comprends. Il y a eu une noyade à Sandicove ça pourrait remonter, et alors le légiste et moi on devra y aller si le corps est retrouvé. Glisse-s-y mon nom si je n’y suis pas, si tu peux.

– Je m’en charge, dit M. Bloom, sur le départ. Ça posera pas de problème.

– Tope, dit M’Coy jovial. Merci mon vieux. J’irais si ça m’était possible. Eh bien, à plus. C.P M’Coy, ça suffira.

– Ce sera fait, confirma M. Bloom.

Pas tombé dans le panneau. Bon coup. Pigeon. On me l’a fait pas. J’y tiens particulièrement, à cette valise. Cuir. Coins renforcés. Rivets sur les côtés. Verrou double-action. Bob Cowley lui a prêté la sienne pour le concert de la régate Wicklow l’an dernier, et n’en a pas revu la couleur depuis.

M. Bloom, que sa flânerie amenait vers la rue de Brunswick, sourit. Ma bourgeoise vient de recevoir un. Soprano nasillard et grêlée. Souris sur son fromage. Pas si mal à sa manière : pour une petite ballade. Pas de tripe. Toi et moi, tu vois : dans le même bateau. Et que je te passe la brosse. Ça me rend fou à. Pas capable d’entendre la différence ? Il a un penchant à ça, je pense. Mauvais sens du poil, le mien. Pensé que Belfast ça lui en boucherait un. J’espère que la petite vérole n’empire pas là-bas. Elle ne se laisserait pas revacciner, je crois. Ta femme et ma femme.

Serait pas en train de me filer, me demande ?

M. Bloom s’arrêta au coin, les yeux errant sur les placards publicitaires multicolores. Ginger Ale Cantrell et Cochrane (Aromatique). Soldes d’été chez Clery. Non, il s’en va tout droit. Hello. Leah ce soir. Mme Bandmann Palmer. Aimerais bien la revoir là-dedans. A joué Hamlet la nuit dernière. Travesti. Peut être que c’était une femme. Explique le suicide d’Ophélie. Pauvre papa ! Comme il parlait de Kate Bateman dans ce rôle. Attendu toute une après-midi devant la porte de l’Adelphi à Londres, pour avoir une place. L’année avant ma naissance : soixante-cinq. Et la Ristori à Vienne. Qu’est-ce c’était que le titre était ? Par Mosenthal c’est. Rachel, non ? Non. La scène dont il parlait toujours, le vieil Abraham, aveugle, qui reconnaît la voix et lui touche la figure de ses doigts.

La voix de Nathan ! La voix de son fils ! J’entends la voix de Nathan qui laissa son père mourir de chagrin et de détresse entre mes bras, qui abandonna la maison de son père et abandonna le dieu de son père.

Ces mots sont si profonds, Léopold.

Pauvre papa ! Pauvre homme ! Je suis content de ne pas être entré dans la chambre pour voir son visage. Cette journée ! Oh, mon dieu ! Mon dieu ! Pffou ! Eh bien, peut-être que c’était mieux pour lui.

M. Bloom prit la rue de côté et passa devant les rosses affaissées à la station de taxi. N’y pensons plus, ça ne sert à rien. L’heure du sac d’avoine. Pourquoi j’ai croisé ce gars M’Coy ?

Il s’approcha et entendit l’avoine dorée qu’ils broyaient, les dents qui mastiquaient lentement. Leurs yeux pleins et lourds le contemplaient alors qu’il passait, dans les remontées doucereuses et maltées de la pisse de cheval. Leur Eldorado. Pauvres bougres ! Foutre s’ils savent ou se soucient de rien avec leur long nez au fond du sac d’avoine. Trop pleins pour le dire. Tout de même ils ont le gîte et le couvert assurés. Châtrés par-dessus le marché : ce moignon de caoutchouc noir et mou qui pendouille de leur arrière-train. Si ça se trouve ils sont bien contents comme ça. Pauvres brutes, z’ont l’air braves. Mais ça peut porter sur les nerfs le hennissement.

Il tira la lettre de sa poche et l’enveloppa dans le journal qu’il portait. Pourrais me trouver nez-à-nez avec elle ici. La ruelle est plus sûre.

Il dépassa la cabane des chauffeurs. Drôle de vie, les chauffeurs à gage. Partout et par tout temps, à l’heure ou à la course, sans volonté propre. Voglio e non. J’aime bien leur filer une cigarette de temps à autre. Sociables. Hurlent toujours quelque-chose à la volée à chaque passage. Il fredonna :

>*La ci darem la mano  
La la lala la la.*

Il s’engagea dans la rue Cumberland et après quelques pas s’arrêta contre le mur de la station, à l’abri. Personne. Meade, bois de construction. Poutres empilées. Ruines et taudis. Avec précaution il traversa le tracé d’une marelle où gisait un palet oublié. Pécheur : pas un. Près de l’entrepôt un enfant esseulé, accroupi sur ses billes, pointait maladroitement. Une chatte tigrée, sage sphinge aux yeux mi-clos, observait d’un rebord tiède. Dommage de les déranger. Mahomet coupa un bout de son manteau plutôt que de la réveiller. Ouvre-la. Fut un temps où moi aussi je jouais aux billes, quand j’allais au cours de cette vieille dame. Elle aimait la mignonnette. Mme Ellis. Et M. ? Il ouvrit la lettre à l’abri du journal.

Une fleur. Me semble que. Une fleur jaune, les pétales aplatis. Pas fâchée, donc ? Que dit-elle ?

Cher Henry

J’ai reçu votre dernière lettre et merci beaucoup pour cela. Je suis désolée que vous n’ayez pas aimé ma dernière lettre. Pourquoi avoir inclus des timbres ? Je suis terriblement fâchée contre vous. J’aimerais bien pouvoir vous punir pour cela. Je vous ai appelé vilain garnement parce que je n’aime pas cet autre monde. Dites-moi voulez-vous le sens de ce mot ? N’êtes-vous pas heureux chez vous, pauvre petit garnement ? J’aimerais tant pouvoir vous aider. S’il vous plaît, dites-moi ce que vous pensez de ma petite personne. Je pense souvent à ce nom si beau que vous avez. Cher Henry, quand nous reverrons nous ? Je pense à vous si souvent vous n’avez pas idée. Jamais je ne me suis sentie autant attirée par un homme. J’en ai honte. S’il vous plaît écrivez-moi une longue lettre et dites-moi en plus. Rappelez-vous que si vous ne le faites pas je vous punirais. Vous savez donc ce que je vous ferai, vilain petit garnement, si vous n’écrivez pas. Comme je me languis de vous. Henry, très cher, ne vous esquivez pas, ma patience s’épuise. Et je vous raconterai tout. Maintenant au revoir, mon garnement chéri, j’ai une telle migraine. aujourd’hui. et écrivez par retour de courrier à votre

Martha, qui vous attend.

P.S. Dites-moi sans faute quelle sorte de parfum met votre femme. Je veux savoir.

Il arracha gravement la fleur de son épingle, respira sa quasi-absence d’odeur et la plaça dans sa poche de poitrine. Le langage des fleurs. Elles aiment ça parce que personne ne peut écouter. Ou un bouquet empoisonné pour l’abattre. Puis, avançant avec lenteur il relut la lettre, murmurant un mot de-ci de-là. Fâchée les tulipes contre toi chéri homme-fleur punir ton cactus si s’il te plaît pauvre ne-m'oubliez-pas comme je me languis de violettes chères roses quand nous nous anémone reverrons toute vilaine queue de nuit femme parfum de Martha. L’ayant toute relue il l’ôta du journal et la remit dans sa poche de veston.

Une joie fragile entrouvrit ses lèvres. Quel changement depuis la première lettre. L’a-t-elle écrit elle-même, me demande. Jouant l’indignée : une fille de bonne famille comme moi, personne respectable. Pourrions-nous rencontrer un dimanche après la messe. Merci : très peu pour moi. La dispute amoureuse ordinaire. Et puis c’est sauve-qui-peut. Aussi pénible qu’une scène avec Molly. Le cigare calme. Narcotique. Avance tes pions, la prochaine fois. Vilain garnement : punir : peur des mots, bien sûr. Brutal, pourquoi pas ? Autant essayer. Un pion à chaque coup.

Ses doigts qui tripotaient encore la lettre dans sa poche en retirèrent l’épingle. Épingle ordinaire, donc ? Il la jeta sur la chaussée. Prélevée sur ses habits : à quatre épingles. Curieux la quantité d’épingles qu’elles ont toujours. Pas de roses sans épines.

Des accents de Dublin résonnaient dans sa tête, poissards. Ces deux putes cette nuit dans le Coombe, épinglées l’une à l’autre sous la pluie.

>*La Marie a perdu l’épingle de ses bas  
Que faire, que faire, elle ne sait pas  
Pour que ça tienne droit  
Pour que ça tienne droit.*

Ça ? Ses. Une telle migraine. Elle a ses roses, probable. Ou toute la journée assise à taper. La tension visuelle affaiblit l’estomac. Quel parfum met votre femme. Comment inventer un truc pareil ?

Pour que ça tienne droit.

Martha, Marie. J’ai vu ce tableau quelque part me souviens plus vieux maître ou un faux. Il est assis dans leur maison. Parle. Mystérieux. Et les deux putes de Coombe seraient là à écouter.

Pour que ça tienne droit.

Une sensation plaisante comme de fin de journée. Fini l’errance. Se détendre, simplement : paisible pénombre : laisser couler. Oublier. Raconter ces endroits où on est allé, ces coutumes étranges. L’autre, une cruche sur la tête, apportait le souper : fruits, olives, eau délicieuse et fraîche d’un puits, froide comme la pierre, ce trou dans le mur à Ashtown. Faut que j’amène un gobelet en carton la prochaine fois que je vais aux courses de trot. Elle écoute de ses grands yeux sombres et doux. Lui dire : plus, plus encore : tout. Puis un soupir : silence. Long long long repos.

Sous le viaduc du chemin de fer il sortit l’enveloppe, la déchiqueta vite fait et en éparpilla les morceaux sur la route. Les bouts voletaient, coulaient sous l’air humide : quelques battements blancs, puis tous coulèrent.

Henry Flower. Tu pourrais déchirer un chèque de cent livres aussi bien. Un bête bout de papier. Lord Iveagh a touché une fois un chèque à sept chiffres, un million, à la banque d’Irlande. Montre bien qu’on peut faire de l’argent sur la bière. Et pourtant son frangin lord Ardilaun doit changer de chemise quatre fois par jour, dit-on. Peau à poux, sac à vermine. Un million de livres, voyons voir. Deux pence la pinte, quatre pence le quart, huit pence le gallon de brune, non, un et quatre pence le gallon. Un et quatre sur vingt : environ quinze. Oui, exactement. Quinze millions de barriques de brune.

Qu’est-ce que je raconte barriques ? Gallons. Dans les un million de barriques en tout cas.

Un train entrant passa au dessus de sa tête, avec fracas, wagon après wagon. Les barriques valsaient dans sa tête : la bière brune tanguait, bouillonnante. Les bondes sautèrent et un flot immense et terne s’écoula, ses bras s’unissaient, serpentaient le long d’îlots boueux, recouvrant les terres basses, la liqueur tourbillonnait paresseusement, charriant les fleurs étales de son écume.

Il était arrivé devant la porte arrière de l’église de la Toussaint. Il pénétra le porche et retira son chapeau, prit la carte de sa poche et la remit sous le bandeau de cuir. Merde. J’aurais pu entreprendre M’Coy pour un passe pour Mullingar.

Toujours la même affiche sur la porte. Sermon du très révérend père John Conmee, S.J., sur Saint Pierre Claver, S.J., et la Mission Africaine. Z’avaient aussi prié pour la conversion de Gladstone quand il était sur la fin. Les protestants sont pareils. Convertir le Dr William J. Walsh D.D. à la vraie religion. Sauver la Chine et ses millions. Comment s’y prennent-ils pour expliquer tout ça aux païens de chinetoques, me demande. Mieux vaut une once d’opium. Célestes. Une hérésie grossière, à leurs yeux. Leur dieu Bouddha allongé de côté au musée. Se la coule douce, la joue reposant sur la main. Bâtons d’encens qui brûlent. Pas comme Ecce Homo. Couronne d’épine et croix. Bonne idée saint Patrick le trèfle. Des baguettes ? Conmee : Martin Cunningham le connaît : air distingué. Dommage que je ne l’ai pas entrepris pour faire entrer Molly dans le chœur, au lieu de ce père Farley qui avait l’air d’un idiot mais trompait son monde. On leur apprend ça. Pas du genre à aller baptiser les noirs, lunettes noires et trempé de sueur. Attirés par l’éclat des verres. Aimerais les voir assis en rond, leurs grosses lèvres ouvertes, fascinés, tout oreilles. Nature morte. Boivent ça comme du petit lait j’imagine.

L’odeur froide de la pierre sacrée l’appelait. Il foula les marches polies, poussa le battant de la porte et pénétra en douceur par l’arrière.

Se passe un truc : confrérie ou genre. Si peu de monde : dommage. Petit coin discret pour être à côté d’une fille. Qui est mon prochain ? Coincés là jusqu’à l’heure, et musique lente. Cette femme pendant la messe de minuit. Septième ciel. Des Femmes étaient agenouillées par rangée, têtes ployées dans les encolures cramoisies. Une fournée agenouillée devant la grille de l’autel. Le prêtre passait parmi elles, tout murmure, la chose en main. Il s’arrêtait devant chacune, prenait une communion, l’égouttait à petit coup (elles sont dans de l’eau?), et la plaçait avec soin dans sa bouche. Tête et chapeau qui s’abaissent. Et la prochaine. Tête et chapeau qui tombent de suite. Et la prochaine : une petite vieille. Le prêtre se pencha pour la lui mettre dans la bouche, sans arrêter son murmure. Latin. La prochaine. Ferme les yeux et ouvre la bouche. Quoi ? Corpus : corps. Cadavre. Trouvaille le latin. Les stupéfie d’un coup. Hospice pour mourants. Elles n’ont pas l’air de mâcher : juste d’avaler. Drôle d’idée : manger des bouts de cadavre. Explique que les cannibales aient un faible pour ça.

Il se tenait de côté et observait les masques aveugles qui descendaient l’allée un à un, et cherchaient leur place. Il s’approcha d’un banc et s’assit au bout, chapeau et journal au bras. Ces pots de fleur qu’on doit porter. Il faudrait des chapeaux qui épousent nos têtes. Éparses elles l’entouraient, les têtes toujours abaissées dans leurs colliers pourpres, dans l’attente que cela fondît dans leurs estomacs. Un peu comme le mazzoth : c’est ce genre de pain : le pain de présence, azyme. Regardez-moi ça. Je parie que ça les rend heureuses. Sucette. Vraiment. Oui, le pain des anges, on appelle ça. Une grande idée par-derrière, genre le Royaume de Dieu est en vous, sentez. Premiers communiants. Glace à deux sous. Et les voilà qui se sentent tous part d’une grande famille, comme au théâtre, tous dans le même bain. Vraiment. J’en suis sûr. Moins seuls. Dans notre confraternité. En sortent un peu grisés. Soupape de sécurité. Enfin, faut vraiment y croire. La cure de Lourdes, les eaux de l’oubli, et l’apparition de Knock, les statues qui saignent. Le vieux qui dort à côté du confessionnal. D’où les ronflements. Croire les yeux fermés. Dans les bras de que votre règne vienne. À l’abri. Calme toute souffrance. Réveil l’année prochaine à la même heure.

Il vit le prêtre remiser le ciboire, bien au fond, et s’agenouiller un instant devant, une large semelle grise sortant du fouillis de dentelle qu’il avait par-dessus. Imagine qu’il perde l’épingle. Saurait pas quoi faire. Rond lisse derrière. Lettres sur son dos : I.N.R.I. ? Non : I.H.S. Molly me l’a dit un jour je lui avais demandé. Ici horrible sadique : ou plutôt : Ici homme supplicié, c’est ça. Et l’autre ? Il nous rend idiots.

Se rencontrer un dimanche après la messe. Ne vous esquivez pas. La voir arriver avec voilette et sac noir. Au crépuscule, soleil dans le dos. Elle pourrait être ici avec un ruban autour du cou, et aussi bien faire le reste en douce. Bien le style. Ce type qui a dénoncé les invincibles il la recevait bien la, Carey c’était son nom, la communion tous les matins. Dans cette même église. Peter Carey, voilà. Non, c’est Peter Claver auquel je pense. Denis Carey. Imagine ça. Femme et six enfants à la maison. Et tout ce temps à combiner le meurtre. Tartuffes, c’est bien le mot, il y a toujours un truc pas net chez eux. Pas bien nets en affaire non plus. Ah, non, elle n’est pas là : la fleur : non, non. Au fait, j’ai bien déchiré l’enveloppe ? Oui, sous le pont.

Le prêtre rinçait le calice : puis il s’enfila le jus vite fait. Vin. Toute de suite plus aristocratique que s’il buvait ce que d’habitude, Guinness ou une de ces boissons sans alcool l’amer Wheatley Dublin ou la ginger ale (aromatique) de Cantrel et Cochrane. Leur en donne pas une goutte : vin de présence : juste l’autre truc. Maigre chère. Pieuse fraude mais bien raison : sinon ils auraient tous les vieux poivrots, pas un pour rattraper l’autre, à pleurer pour un peu. Bizarre toute cette atmosphère de. Bien raison. Tout à fait raison.

M. Bloom regarda du côté du chœur. Y aura pas de musique. Dommage. Qui tient l’orgue ici me demande ? Le vieux Glynn, il savait faire parler l’instrument, le vibrato : cinquante livres à l’année on dit qu’il touchait rue Gardiner. Molly était en voix ce jour-là, le Stabat Mater de Rossini. Mais d’abord le sermon du père Bernard Vaughan. Christ vs Pilate. Christ, mais n’en fais pas un roman. C’est pour la musique qu’ils étaient venus. Le bruit de pieds s’arrêta. Aurait entendu une mouche voler. Je lui avais dit de diriger la voix vers ce coin. Je pouvais sentir l’émotion dans l’air, à plein, les gens les yeux levés :

Quis est homo.

La vieille musique d’église, il y a du splendide. Mercadante : les sept derniers mots. La douzième messe de Mozart : Gloria. Ces vieux papes, ils kiffaient la musique, et l’art et les statues et les images de tout poil. Palestrina par exemple, aussi. Ils ont eu du bon temps à l’époque. Sains de corps en plus, le chant, les horaires fixes, puis on distille les liqueurs. Bénédictine. Chartreuse Verte. Bon, les eunuques dans leurs chœurs c’était pousser le bouchon un peu. Quel genre de voix ? Ça doit faire curieux après les basses fortes qu’ils ont. Connaisseurs. J’imagine qu’on ne sent plus rien. Une forme de placidité. Plus de souci. Ils entrent en chair, non ? Gloutons, grands, jambes étirées. Qui sait ? Eunuque. Une façon de s’en tirer.

Il vit le prêtre se courber et baiser l’autel, puis faire volte-face et bénir l’assemblée. Tous se signèrent et se mirent debout. M. Bloom regarda alentour et se mit debout, parcourant des yeux les faîtes des chapeaux. Debout pour l’évangile bien sûr. Puis tous se remirent à genoux et il se rassit discrètement sur son banc. Le prêtre descendit de l’autel, la chose bien en avant, et lui et l’enfant de chœur échangèrent en latin. Puis le prêtre s’agenouilla et commença à lire d’une fiche :

– Ô Dieu, notre refuge et notre force…

M Bloom allongea le cou pour saisir les mots. De l’anglais. Ça leur fait un os à ronger. Vague souvenir. Ça fait combien depuis ta dernière messe ? Vierge glorieuse et immaculée. Joseph, son époux. Pierre et Paul. Plus intéressant si tu comprenais de quoi il retourne. Merveilleuse organisation pas de doute, une vraie boîte à musique. La confession. Tout le monde en veut. Alors, je vous dirai tout. Pénitence. Punissez-moi je vous en prie. Quelle arme en leurs mains. Plus qu’un médecin ou un avocat. La femme en crève d’envie. Et je chuchuchuchuchu. Et avez-vous chachachachacha ? Et pourquoi avez-vous ? Baisse les yeux sur sa bague pour chercher une excuse. Les galeries des murmures ont des oreilles. Le mari l’apprend, à sa surprise. Petite blague du bon dieu. Et la voilà qui sort. Repentance à fleur de peau. Honte adorable. Prière à l’autel. Je vous salue Marie sainte Marie. Fleurs, encens, cierges qui fondent. Cache ses rougeurs. Armée du salut une imitation flagrante. Une prostituée repentie va maintenant s’adresser à l’assemblée. Comment j’ai trouvé le Seigneur. Z’ont la tête bien carrée ces gars à Rome : ils ont réglé tout le papier à musique. Et ils en ratissent, de l’argent ! Sans parler des legs : au P.P., à disposer en toute discrétion pour le temps présent. Messes publiques pour le repos de mon âme, portes ouvertes. Monastères et couvents. Le prêtre, dans  cette affaire du testament Fermanagh, à la barre des témoins. Pas moyen de l’intimider. Il avait réponse à tout. Préparée. Liberté et Triomphe de notre sainte mère l’Église. Les docteurs de l’église : ils en ont planifié toute la théologie.

Le prêtre priait :

– Saint Michel Archange, défendez-nous dans le combat et soyez notre protecteur contre la méchanceté et les embûches du démon (que Dieu lui commande, nous vous en supplions !) :
et vous, Prince de la Milice Céleste, par le pouvoir divin qui vous a été confié, précipitez au fond des enfers Satan et les autres esprits mauvais qui parcourent le monde pour la perte des âmes.

Le prêtre et l’enfant de chœur se relevèrent et s’en furent. Game over. Les femmes restaient en plan : l’action de grâce.

Mieux vaut filer. Frère Le Timbre. Va s’amener avec le panier si ça se trouve. L’obole de Pâques.

Il se leva. Hello. Déboutonnés tout ce temps ces deux boutons de gilet ? Les femmes apprécient. Ne te le disent jamais. Mais nous. Excusez-moi mademoiselle, il y a un (fft !), juste un (fft !) petit quelque. Ou leur jupe par-derrière, dégrafée. Aperçus de la lune. Ça les ennuie quand tu ne. Pourquoi ne me l’avez pas vous dit plus tôt. Pourtant je vous préfère débraillée. Une veine que ce ne soit pas plus au sud. Il descendit l’allée, se reboutonnant discrètement, et prenant la porte principale sortit à la lumière. Il s’arrêta un moment aveuglé à côté du froid bassin de marbre noir, pendant que devant lui deux fidèles trempaient une main furtive dans l’eau bénite à marée basse. Tram : une voiture des teintureries Prescott : une veuve dans ses habits de deuil. La remarque parce que le suis aussi. Il se couvrit. On en est où côté temps ? Et quart. Encore le temps. Ferais bien de commander cette lotion. Où est-ce ? Ah oui, la dernière fois. Chez Sweny, place Lincoln. Les pharmaciens, ça se déplace peu. Leurs bocaux verts et or sont trop lourds à bouger. Hamilton Long, fondée l’année du déluge. Cimetière huguenot pas loin. À visiter un de ces jours.

Il prit l’allée Westland vers le sud. Mais la formule est dans l’autre pantalon. Oh, et j’ai oublié le passe, en plus. Quelle barbe ces funérailles. Bah, pauvre gars, ce n’est pas sa faute. C’est quand que j’en ai fait faire la dernière fois ? Voyons. J’ai fait la monnaie sur un souverain je me rappelle. Le premier du mois ça devait être, ou le deux. Bah, il pourra le retrouver dans son registre d’ordonnance.

Le pharmacien rebroussait son cahier, page après page. Odeur flétrie et rêche, semble dégager. Crane réduit. Et vieux. Quête de la pierre philosophale. Les alchimistes. Les drogues ça vous fait vieillir, l’excitation mentale passée. Puis la léthargie. Pourquoi ? Réaction. Toute une vie en une nuit. Te change peu à peu la personnalité. Toute la journée au milieu des herbes, des baumes, des désinfectants. Tous ses vases d’albâtre biscornus. Mortier et pilon.  Aq. Dist. Fol. Laur. Te Virid. L’odeur te guérit presque, comme la sonnette du dentiste. Docteur maboul. Il devrait s’auto-doctorer un peu lui aussi. Électuaire ou émulsion. Le premier qui a cueilli une plante pour se soigner ne manquait pas d’estomac. Les simples. Demandent de la prudence. Il y en a assez ici pour vous chloroformer. Test : vire le papier de tournesol au rouge. Chloroforme. Overdose de laudanum. Pastilles de somnifère. Philtres d’amour. Élixir parégorique, mauvais pour la toux. Obstrue les pores ou le flegme. Poisons, les seuls remèdes. La solution là où on l’attend le moins. Maline, la nature.

– Il y a une quinzaine, monsieur ?

– Oui, répondit M. Bloom.

Il attendait au comptoir, respirant longuement l’exhalaison acre des médicaments, l’odeur sèche et poussiéreuse des éponges artificielles et végétales. Le temps qu’on passe à raconter ses problèmes.

– Huile d’amande douce et teinture de benjoin, fit M. Bloom, et puis de l’eau de fleur d’oranger…

Cela lui rendait certainement la peau d’une blancheur délicate comme de la cire.

– Et de la cire blanche aussi, dit-il.

Fait ressortir la noirceur de ses yeux. Me regardait, le drap remonté jusqu’aux yeux, espagnole, respirant sa propre odeur, quand j’arrangeais mes boutons de manchette. Ces recettes de grand-mère sont souvent les meilleures : des fraises pour les dents : orties et eau de pluie : flocons d’avoine trempés de petit-lait, dit-on. Aliments pour peau. Un des fils de la vieille reine, le duc d’Albanie ? N’avait qu’une peau. Léopold, oui. Nous en avons trois. Verrues, oignons et pustules pour arranger le tout. Mais il te faut un parfum aussi. Quel parfum met votre ? Peau d’Espagne. Cette eau de fleur d’oranger, c’est si frais. Sentent bon, ces savons. Surgras, 100 % végétal. Il est temps de s’offrir un bain au coin de la rue. Hammam. Turc. Massage. La crasse s’amasse dans le nombril. Plus agréable si c’était une gentille fille. Et je pense que je. Oui je. Le faire dans le bain. Curieuse cette envie que j’. L’eau retourne à l’eau. Affaire, et plaisir. Pas le temps pour un massage, bien dommage. Rafraîchi pour la journée. L’enterrement, va pas être gai.

– Oui monsieur, fit le pharmacien. C’était deux et neuf. Avez-vous apporté un flacon ?

– Non, fit M. Bloom. Préparez-le, s’il vous plaît. Je repasserai plus tard et je prendrai un de ces savons. À combien sont-ils ?

– Quatre pence monsieur.

M. Bloom amena un pain à ses narines. Cire douce et citronnée.

– Je prendrai celui-là, dit-il. Cela fait donc trois et un penny.

– Oui, monsieur, répondit le pharmacien. Vous pouvez régler le tout quand vous revenez, monsieur.

– Bien, fit M. Bloom.

Il sortit sans se presser du magasin, badine de journal sous le bras, le savon, frais sous son emballage, dans la main gauche.

À hauteur d’aisselle la main et la voix de Bantam Lyons :

– Salut Bloom. Quoi de neuf ? C’est celui du jour ? Fais nous voir une minute.

Rasé sa moustache, encore, sacrebleu ! La lèvre du haut, froide et sinueuse. Pour avoir l’air plus jeune. Qu’est-ce qu’il a l’air atteint. Plus jeune que moi.

Les doigts jaunes et les ongles noirs de Bantam Lyon déroulaient la badine. Lui faut un bain lui aussi. Décrassage. Bonjour, avez-vous essayé le savon Pears ? Pellicules sur les épaules. Cuir chevelu a soif d’huile.

– Je veux vérifier ce cheval français qui court aujourd’hui, fit Bantam Lyons. Il est où le bougre ?

Il froissait les pages plissées, remuant le menton contre son col trop haut. Rasé de trop près. Col serré il perdra ses cheveux. Mieux vaut lui laisser le journal et m’en défaire.

– Tu peux le garder, dit M. Bloom.

– Ascot. Coupe d’Or. Attends, marmonna Bantam Lyons. Une demi-mi. Max la seconde.

– J’allais le jeter, continua M. Bloom.

Bantam Lyons releva brusquement les yeux et le lorgna sourdement.

– Comment ça ? fit-il de sa voix pointue.

– Je disais que tu peux le garder, répondit M. Bloom. J’étais sur le point de le jeter.

Bantam Lyons hésita un instant, le regard en coin : puis rejeta les feuillets béants sur les bras de M. Bloom.

– Je prends le risque, dit-il. Tiens, merci.

Il fila vers chez Conway. Bon vent, petit scarabée.

M. Bloom replia soigneusement les feuilles en carré et y logea le savon, sourire aux lèvres. Drôle de museau, le gars. Pari. C’est devenu une épidémie. Les coursiers chipent pour allonger six pence. Tombola : une grosse dinde tendre. Votre dîner de Noël pour trois pence. Jack Fleming, détourne pour miser puis file en Amérique. Il tient un hôtel maintenant. Ils ne reviennent jamais. Marmites garnies d’Égypte.

Il se dirigea d’un pas allègre vers la mosquée des bains. Cela vous évoque une mosquée, les briques rouges, les minarets. Course de vélo à l’université aujourd’hui, je vois. Il jeta un œil sur le poster en fer à cheval plaqué sur le portail du parc du collège : les cyclistes pliés comme des épingles à cheveux. Quelle affiche mal foutue. Pourquoi ne l’avoir pas fait rond, comme une roue. Et les rayons : sports, sports, sports : et le gros moyeu : collège. Un truc qui attire l’œil.

Voilà Hornblower debout devant la loge. Brosser le poil : peut-être un jour y entrer à l’œil. Et comment allez-vous, M. Hornblowler ? Comment allez-vous, monsieur ?

Quel temps divin. Si la vie était toujours comme ça. Idéal pour le cricket. Rester assis à l’ombre. Ronde après ronde. Out ! Ils ne savent pas jouer ici. Six-à-zéro. Et pourtant Buller, le capitaine, a cassé une vitre du club de la rue Kildare en battant vers la défense. La foire de Donnybrook c’est plus leur genre. Et des crânes on en a cassés, quand M’Carthy est descendu dans l’allée. Vague de chaleur. Ne durera pas. Sans cesse s’écoule, le flux de la vie, le sillon que nous traçons dans le flux de la vie nous est plus cher que tout autre.

Le plaisir d’un bon bain, maintenant : un bassin d’eau claire, l’émail frais, le doux courant tiède. Ceci est mon corps.

Il voyait d’avance son corps pâle tout entier reposant, nu, dans un chaud utérus, onctueux et parfumé sous le savon fondant, mollement baigné. Il voyait son torse et ses membres sous les ridules, portés, dansant légers vers le midi, jaune citron : son nombril, bouton de chair : et les boucles sombres et emmêlées du buisson flottant, flottante dans le courant la toison ornant le membre et père de myriades, une fleur aquatique et languide.