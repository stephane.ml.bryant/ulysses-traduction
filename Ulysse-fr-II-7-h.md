Tom Rochford prit le premier disque de la pile qu’il serrait contre son veston bordeaux.

— Suivez ? dit-il. Disons que ce soit le sixième tour. Ici vous voyez. Tour en cours.

Il le fit glisser pour eux dans la fente de gauche. Le disque fusa dans la rainure, oscilla un moment, s’immobilisa et leur fit de l’œil : six.

Des magistrats d’un autre temps, hautains, en plaidoirie, virent devant eux passer du bureau d’intégration fiscale à la cour d’Assise Richie Goulding portant la sacoche des recettes de Goulding, Collins et Ward et entendirent s’avancer à force froufrou de la section maritime de la cour du roi vers la cour d’appel une femme âgée aux dents postiches souriant avec incrédulité, à la jupe de soie noire d’une ampleur peu commune.

— Suivez ? dit-il. Regardez maintenant le dernier que j’ai mis est ici : tour terminé. L’impact. Effet levier, suivez ?

Il leur signala la colonne de disques qui s’élevait sur la gauche.

— Malin, dit Flynn-la-belette en reniflant. Comme ça un type qui arrive en retard voit de suite quel tour c’est et quels tours sont passés.

— Suivez ? dit Tom Rochford.

Il inséra un disque pour lui-même, le regarda fuser, osciller, s’arrêter : quatre. Tour en cours.

— Je vais le voir aujourd’hui à l’Ormond, dit Lenehan, je le sonderai. Un bienfait ne doit pas rester impuni.

— Faites, dit Tom Rochford. Dites-lui que je suis Boylan d’impatience.

— Bonsoir, dit M’Coy brusquement. Quand vous vous y mettez vous deux…

Nosey-la-belette se pencha sur le levier, le renifla.

— Mais comment ça marche à ce niveau-là, Tommy ? demanda-t-il.

— Tchao, dit Lenehan. À plus tard.

Il suivit M’Coy et ils traversèrent le miniscule square du passage Crampton.

— C’est un héros, dit-il simplement.

— Je sais, dit M’Coy. Vous parlez de la conduite.

— Conduite ? dit Lenehan. C’était une bouche d’égout.

Ils passèrent devant le music-hall de Dan Lowry où Mary Kendall, charmante soubrette, leur décocha depuis une affiche un sourire lourdement maquillé.

En descendant la rue Sycamore, au niveau de l’Empire music-hall Lenehan montra depuis le trottoir de quoi il en retournait à M’Coy. Une de ses foutues bouches d’égout comme un tuyau à gaz, et le pauvre diable coincé au fond, à moitié asphyxié par les gaz d’égout. Tom Rochford se débrouilla pour descendre, en veston de bookmaker et tout le tintouin, une corde autour de lui. Eh sacrebleu il est arrivé à mettre la corde autour du pauvre gars et on les a hissés tous les deux à la surface.

— Un acte héroïque, dit-il.

À hauteur du Dolphin ils s’arrêtèrent pour laisser passer une ambulance filant au galop vers la rue Jervis.

— Par là, dit-il, prenant sur sa droite. Je veux faire un saut chez Lynam pour voir la cote de départ de Sceptre. Quelle heure nous donne votre montre en or du bout de sa chaîne ?

M’Coy ausculta du regard le bureau sombre de Marcus Tertius Mose, puis l’horloge de chez O’Neill.

— Trois passées, dit-il. Qui la monte ?

— O. Madden, dit-il. Et c’est une fière pouliche.

Pendant qu’il attendait à Temple bar M’Coy repoussa du trottoir au caniveau une peau de banane par petits à-coups de la pointe de sa chaussure. Un gars pourrait bougrement se casser la pipe en passant pinté par là dans le noir.

Les grilles de l’allée s’ouvrirent en grand pour laisser sortir le cortège du vice-roi.

Ils montèrent quelques marches, sous la galerie des Marchands. Une silhouette au dos sombre passait en revue des livres sur la carriole d’un bouquiniste.

— Le voilà, dit Lenehan.

— Qu’est-ce qu’il peut bien acheter, je me demande, dit M’Coy, jetant un coup d’œil en arrière.

— *Leopoldo*, ou *The Bloom is on the Rye*, dit Lenehan.

— Dès qu’il voit une occase il ne se tient plus. J’étais avec lui un jour il a acheté un livre à un vieux bonhomme sur la rue Liffey, pour deux thunes. Dedans il y avait des gravures qui en valaient bien le double, des étoiles, la lune, des comètes avec une longue queue. Un truc d’astronomie.

Lenehan se mit à rire.

— Je vais vous en raconter une bien bonne sur les queues de comètes, dit-il. Allons du côté du soleil.

Ils prirent le pont métallique et continuèrent le long du quai Wellington, près du bord.

Maitre Patrick Aloysius Dignam sortit de chez Mangan, successeur de Fehrenbach, avec une livre et demi de filets de porc.

— Il y avait un gros raout à la correctionnelle de Glencree, dit Lenehan tout excité. Le diner annuel, vous savez. Chemise empesée et tout. Le lord-maire était là, c’était Val Dillon, et sir Charles Cameron et Dan Dawson fit un discours puis un orchestre. Bartell d’Arcy a chanté et Benjamin Dollard…

— Je sais, interrompit M’Coy. Ma bourgeoise y a chanté une fois.

— Vraiment ? dit Lenehan.

Une pancarte *Appartements non-meublés* réapparut à la fenêtre du 7 de la rue Eccles.

Il interrompit son histoire un instant mais éclata d’un rire sifflant.

— Mais attendez la fin, dit-il. Delahunt, de la rue Camden, faisait le service de traiteur, et votre serviteur était le laveur-de-bouteilles en chef. Bloom et madame étaient là. Une montagne on avait amené : porto, cherry, curaçao, on n’a pas manqué d’y faire honneur. *Fast and furious* on peut dire. Après le liquide vint le solide. Viandes froides en pagaille, puddings de fruits confits…

— Je sais, dit M’Coy. L’année où ma bourgoise y était…

— Mais attendez la fin, dit-il. Après toutes ces réjouissances nous avons encore fait un déjeuner de minuit  et quand on s’est mis en route c’était le petit matin du lendemain de la veille. Le chemin du retour, une nuit d’hiver somptueuse sur le mont de l’Édredon. Bloom et Chris Callinan étaient d’un côté de la voiture et la madame et moi de l’autre. Nous avons commencé à chanter des chœurs et des duos : *vois, le premier rayon du jour*. Elle avait une bonne dose de porto Delahunt sous la ceinture. À chaque cahot de la voiture je l’avais qui valdinguait contre moi. Un régal infernal ! Elle en a une sacrée paire, Dieu m’est témoin. Comme ça.

Il tint ses mains en coupes à une coudée de son thorax, fronçant le sourcil.

— Je lui remettais le plaid sous les fesses et je lui ré-arrangeais le boa à tout bout de champ, vous voyez ce que je veux dire.

Ses mains moulèrent dans l’air d’amples rondeurs. Il plissa les yeux de délectation, son corps se contracta et ses lèvres laissèrent passer un gazouillis d’aise.

— Le petit frère était au garde-à-vous en tout cas, dit-il sur un soupir. C’est une jument qui en a sous le pied, pas de doute là-dessus. Bloom, lui, indiquait toutes les étoiles et comètes du ciel à Chris Callinan et au cocher : la Grande Ourse, Hercule et le dragon, et tout le bazar. Moi j’étais perdu, bon dieu, comme qui dirait dans la Voie Lactée. Il les connait toutes, parole. À la fin elle en a aperçue une toute riquiqui au raz de l’horizon. Et c’est laquelle cette étoile, Poldy ? qu’elle dit. Bon dieu, elle l’avait coincé le Bloom. Celle-là, n’est-ce pas ? dit Chris Callinan, c’est justement ce qu’on appelle un raseur. Bon dieu, et il n’avait pas tout à fait tort.

Lenehan s’arrêta et s’appuya au parapet, les côtes secouées d’un rire essoufflé.

— Je n’en peux plus, haleta-t-il.

Le visage blafard M’Coy s’essaya à quelques sourires puis se fit grave. Lenehan reprit sa marche. Il souleva sa casquette de marin et se gratta l’arrière du crâne avec vivacité. Il regarda du coin de l’œil M’coy, en plein soleil.

— C’est un touche-à-tout, question culture, Bloom, dit-il avec sérieux. Ce n’est pas le modèle d’usine, l’ordinaire… vous savez… Il a quelque-chose d’artiste ce brave Bloom.

***
