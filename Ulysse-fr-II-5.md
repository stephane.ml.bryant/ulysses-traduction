Rocher à l’ananas, bâtonnet de citron, caramel mou. Une fille gluante de sucre à sa pelle à glace, boules de crème pour un frère chrétien. Pas mal pour une récré. Mauvais pour le bedon. Pastilles et pâtes confites, fournisseur de Sa Majesté le Roi. Dieu. Sauve. Notre. Assis sur le trône à sucer de roux jujubes jusqu’à les laisser blancs.

Un jeune homme de l’Y.M.C.A., la mine sombre, aux aguets parmi les vapeurs tièdes et doucereuses de chez Graham Lemon, plaça un prospectus dans la main de M. Bloom.

Conversation à cœur ouvert.

Bloo... Moi ? Non : Blood : sang.

Le sang de l’Agneau.

Ses pieds trainants l’amenaient à la rivière, tout à sa lecture. Êtes-vous sauvé ? Tous sont lavés par le sang de l’agneau. Dieu veut des victimes de sang. Naissance, hymen, martyr, guerre, fondation d’édifice, sacrifice, entrailles grillées, autel de druides. Élie arrive. Dr. John alexander Dowie, restaurateur de l’église de Sion, arrive.

*Arrive ! Arrive !! Arrive !!!*  
*Tous seront bienvenus.*  

Ça rapporte. Torry et Alexander l’an dernier. Polygamie. Sa femme y mettra bon ordre. D’où était cette annonce une boite de Birmingham le crucifix lumineux. Notre sauveur. Se réveiller en pleine nuit et le voir au mur, pendu. L’idée du fantôme de Pepper. Il nous rend idiots.

Le phosphore, c’est avec ça que ça doit être fait. Si tu oublies un bout de morue par exemple. Je pouvais voir l’éclat bleuté dessus. Nuit où je suis descendu au garde-manger de la cuisine. Ces odeurs qui n’attendent qu’à sortir, aime pas ça. Qu’est-ce qu’elle voulait déjà ? Les raisins de Malaga. Pensait à l’Espagne. Avant la naissance de Rudy. La phosphorescence, ce bleuté verdâtre. Très bon pour le cerveau.

Du coin de l’édifice Butler il balaya de l’œil la promenade du Bachelier. La fille de Dedalus là encore devant la salle des ventes Dillon. Doit vouloir vendre quelque vieillerie. Reconnu ses yeux tout de suite ceux du père. Les cent pas en l’attendant. Quand la mère n’est plus, le foyer non plus. Quinze enfants il a eu. Une naissance par an ou presque. C’est dans leur théologie ou le prêtre ne donnera pas à la pauvre femme la confession, l’absolution. Croissez et multipliez. A-t-on idée ? C’est à vous mettre sur la paille. Eux n’ont pas de famille à nourrir. Ils écrèment le pays. Leurs caves et leurs celliers. J’aimerais les voir faire le jeûne noir de Yom Kippour. Brioche pascale. Un seul repas et une collation pour qu’il ne s’effondre pas sur l’autel. Une gouvernante d’un de ces types-là si tu pouvais lui délier la langue. Jamais ça ne sortira. Comme lui soutirer ses sous à lui. Sait se faire du bien. Pas d’invités. Tout pour bibi. Attentif à ses eaux. Amenez votre propre tambouille. Sa révérence : motus et bouche cousue.

Bon Dieu, la robe de cette pauvre enfant est en guenilles. N’a pas l’air de manger à sa faim non plus. Pomme-de-terre et margarine, margarine et pomme-de-terre. C’est après coup qu’ils s’en ressentent. Preuve sur pièce. Mine l’organisme.

Alors qu’il posait le pied sur le pont O’Connell, une bouffée de fumée passa le parapet. Péniche de brasserie, stout pour l’export. Angleterre. L’air de la mer la fait tourner, parait-il. Serait intéressant un jour d’avoir un passe par Hancock pour voir la brasserie. Univers à part : ses propres règles. Cuves de brune, extraordinaire. Les rats ont aussi leur passe. S’en font gonfler la panse comme des chiens noyés. Ivres morts de brune. Boivent à en gerber comme de bons chrétiens. T’imagines boire ça ! Rats qui cuvent les cuves. Ah, bien sûr, si on savait tout.

Il se pencha et aperçut, qui battaient fortement des ailes, tournoyant aux abords du morne quai, les goélands. Gros temps au large. Et si je m’y jetais ? Le fils de Reuben J a dû s’en gonfler la panse, tout droit des égouts. Un et huit pence de trop. Hhmmm. C’est cette manière cocasse qu’il a d’amener les choses. S’y entend aussi pour raconter une histoire.

Ils tournoyaient plus bas. Cherchent de quoi becqueter. Minute.

Il jeta au milieu d’eux une boule de papier froissé. Élie trente-deux pieds par seconde arr. Rien du tout. La boule se trémoussait, à la traine dans les remous, ignorée, et flotta jusqu’aux arches du pont. Pas si bêtes. Le jour aussi où j’ai jeté ce gâteau rassi du Erin King l’ont cueilli dans le sillage cinquante verges plus loin. Débrouillardise. Ils tournoyaient, les ailes battantes.

*Famélique et affamé le goéland*  
*Surveille d’aplomb le gris océan*  

Voilà comment les poètes écrivent, les sonorités similaires. Mais pourtant Shakespeare n’a pas de rimes : vers libre. Le flot du langage, voilà. Les pensées. Solennel.

*Hamlet, je suis l’esprit de ton père*  
*Condamné pour un certain temps à errer en ces lieux*  

– Un penny les deux pommes ! Un penny les deux !

Son regard survola les pommes lustrées, alignées sur l’étalage. Australiennes sans doute en cette saison. La peau brille : les fait reluire avec un chiffon ou un mouchoir.

Attends. Ces pauvres oiseaux.

Il s’arrêta à nouveau et acheta deux chaussons de Banbury à la vieille aux pommes pour un penny, en brisa la pâte friable et jeta les morceaux dans la Liffey. Voyez-moi ça. Les goélands piquèrent, deux tout d’abord, puis tous tombèrent sur la proie. Fini. N’en ont fait qu’une bouchée.

Les sachant aussi avides qu’affutés il se frotta les mains pour en faire tomber les miettes poudreuses. Ils ne s’attendaient pas à ça. Une manne. Vivent du poisson, du poisson ont la chair, tous les oiseaux marins, goélands, bernaches. Les cygnes de l’Anna Liffey descendent parfois jusqu’ici pour se lisser les plumes. Tous les goûts sont dans la nature. Me demande quel genre de viande c’est, le cygne. Robinson Crusoé a dû s’en nourrir.

Ils tournoyaient, les battements ralentis. Je ne vais pas vous en donner plus. Un penny c’est déjà bien. Et quelle gratitude. Pas même un coâ de remerciement. Ils propagent la fièvre aphteuse par-dessus le marché. Si tu gaves une dinde de châtaignes ou genre ça en prend le goût. Qui mange du cochon, cochon. Mais alors pourquoi les poissons d’eau salée ne sont pas salés ? Comment ça se fait ?

Ses yeux interrogeaient la rivière et aperçurent une barque au mouillage qui sur la mélasse ondoyante balançait paresseusement son panneau d’affichage.

*Kino*  
*11/-*  
*Pantalons*  

Fameuse idée. Me demande s’il paie une redevance à la municipalité. Comment peut-on posséder l’eau, en réalité ? Elle s’écoule sans cesse en un flux, jamais la même, le sillon que nous traçons dans le flux de la vie. Car la vie est un flux. Toutes sortes d’endroits s’y prêtent, à la pub. Ce charlatan, celui de la chaude-pisse, qui était placardé sur toutes les pissotières. On n’en voit plus. Strictement confidentiel. Dr Hy Franks. Ça ne lui coûtait pas un radis comme Maginni le professeur de dance et son auto-promotion. Trouvait des gars pour les coller ou même collait ça lui-même d’ailleurs, en catimini, quand il se déboutonnait la braguette. Oiseau de nuit. L’endroit idéal qui plus est. PAS DE BIFFETONS. 110 CACHETONS. Un mec à qui ça brûle.

Si lui… ?

Ô !

Eh !

Non… Non.

Non, non. Je n’y crois pas. Il n’en ferait rien, pour sûr ?

Non, non.

M. Bloom reprit sa marche, relevant ses yeux troublés. N’y pense plus. Une heure passée. La boule du bureau de navigation est baissée. Heure de Dunsink. Passionnant ce petit bouquin celui de sir Robert Ball. Parallaxe. Je n’ai jamais tout à fait compris. Voilà un prêtre. Pourrais lui demander. Par c’est grec : parallèle, parallaxe. Mes tempes si choses elle appelait ça jusqu’à ce que je lui explique pour la transmigration. Des prunes !

M. Bloom adressait un sourire des prunes à deux fenêtre du bureau de navigation. Elle a raison après tout. De grands mots pour des choses ordinaires parce que le son. Elle n’est pas précisément spirituelle. Peut même être grossière. Sors ce que je pense. Pourtant je ne sais pas. Elle disait toujours que Ben Dollard avait une voix de basse barilton. Il a des jambes comme des barils et c’est à croire qu’il chante la tête dans un baril. En voilà du spirituel ou non. On l’appelait Big Ben. Niveau esprit, c’est pas moitié aussi bien que de l’appeler basse barilton. L’appétit d’un albatros. Venait à bout d’un rumsteak entier. Quelle baraque il faisait quand il s’enfilait la Bass numéro un. Baril de Bass. Pigé ? tout se tient.

Une procession d’hommes-sandwich en tabliers blancs longeait lentement le caniveau en sa direction, les placards barrés d’un bandeau écarlate. Soldes. Comme ce prêtre ils sont ce matin : nous avons péché : Nous avons souffert. Il lut les lettres écarlates sur chacun de leur cinq chapeaux blancs : H. E. L. Y. S. Wisdom Hely’s. Y qui trainait à l’arrière sortit un morceau de pain de sous son placard avant, le fourra dans bouche et mastiquait en marchant. Notre aliment de base. Trois sous par jour, à longer les caniveaux, rue après rue. Juste assez pour garder la peau sur les os, pain et bouillon clair. Ce ne sont pas ceux de Boyl : non, les gars de M Glade. Attire même pas d’affaire. Je lui avais donné l’idée d’une voiture-réclame transparente avec deux jolies filles assises en train d’écrire, lettres, cahiers, enveloppes, buvards. Je parie que ça aurait marché. Des jolies filles en train d’écrire, ça attire l’œil de suite. Tout le monde crève d’envie de savoir ce qu’elle peut bien écrire. T’en as vingt autour de toi rien qu’à regarder en l’air. À vouloir donner leur avis. Les femmes aussi. Curiosité. Pilier de sel. Bien sûr n’a rien voulu savoir parce que l’idée ne venait pas de lui. Ou l’encrier que je lui avais suggéré avec une fausse tache de celluloïd noir. Ses idées pour les annonces comme les terrines du Prunier sous la nécrologie, section chambre froide. Incollables. Quoi ? Nos enveloppes. Bonjour Jones, ou allez-vous ? Pas le temps Robinson, je cours acheter le seul effaceur à encre fiable, le *Kansell*, vendu par la sté Hely's, 85 rue Dame. Quelle purge : j’ai fait du chemin. Sale boulot c’était d’aller encaisser les couvents. Le couvent Tranquilla. Il y avait une gentille nonne là-bas, un visage à croquer vraiment. La cornette ça allait bien avec sa frimousse. Sœur ? Sœur ? Je suis sûr qu’elle avait des peines de cœur, à cause de ses yeux. Pas facile de marchander avec une femme de cette sorte. Je l’ai dérangée pendant ses dévotions ce matin-là. Mais contente de communiquer avec le monde extérieur. Notre grand jour, a-t-elle dit. Fête de notre Dame du Mont Carmel. Nom à croquer aussi : caramel. Elle savait que je, je pense qu’elle savait d’après la façon dont elle. Si elle s’était mariée elle aurait changé. J’imagine qu’elles étaient vraiment à court d’argent. Mais la friture toujours avec du beurre, et du meilleur. Pas de saindoux pour ces dames. Le lard ça me donne des peines de cœur. Onctueuses au-dedans et au-dehors. Molly qui le goutait, la voilette levée. Sœur ? Pat Claffey, la sœur du prêteur-sur-gage. C’est une nonne qui a inventé le fer barbelé à ce qu’on dit.

Il traversa la rue Westmoreland après qu’apostrophe S l’eût dépassé d’un pas lourd. Bicyclettes Rover. Jour de courses aujourd’hui. Cela fait combien de temps ? L’année où est mort Phil Gilligan. Nous étions rue Lombard, côté ouest. Attends : j’étais chez Thom. Le boulot chez Wisdom Hely l’ai eu l’année de notre mariage. Six ans. Il y a dix ans : en quatre-vingt-quatorze il est mort oui c’est ça le grand incendie chez Arnott. Val Dillon était maire. Le diner Glencree. Le conseiller Robert O’Reilly qui vide le porto dans sa soupe avant qu’on abaisse les couleurs. Bobbop s’en pourlèche pour le conseiller de l’intérieur. On n’entendait pas l’orchestre. Pour tout ce que nous avons déjà reçu puisse le Seigneur nous faire. Milly était encore toute gosse. Molly avait cette robe gris-éléphant avec les brandebourgs. Cousu main avec les boutons recouverts. Elle ne l’aimait pas parce que je me suis foulé la cheville le premier jour qu’elle l’a le pique-nique de la chorale au Pain de Sucre. Comme si ça. Le haut-de-forme du vieux Goodwin bon à jeter à cause d’un truc gluant. Mouches au pique-nique elles aussi. N’a jamais eu sur le dos une robe comme celle-là. Lui allait comme un gant, épaules et hanches. Commençait juste à prendre de l’embonpoint. Tourte au lapin nous avons mangé ce jour-là. Les gens la suivaient du regard.

Heureux. Plus heureux à l’époque. La chambrette douillette qu’on avait avec le papier-peint rouge. De chez Dockrell, un et neuf pence la douzaine. La nuit du bain de Milly. Le savon américain que j’avais acheté : à la fleur de sureau. L’odeur douce de l’eau de son bain. Qu’elle était drôle sous la mousse. Et dodue. La photographie maintenant. L’atelier de daguerréotypes de pauvre papa dont il m’avait parlé. Goût héréditaire.

Il suivait le bord du trottoir.

Le flux de la vie. Comment s’appelait ce gars avec un air de curé qui biglait notre intérieur à chaque passage ? Vue basse, femme. Logeait chez Citron parade saint Kevin. Pen quelque-chose. Pendennis ? Ma mémoire commence à. Pen… ? Bien sûr, c’était il y a des lustres. Le bruit des trams probablement. Eh bien, s’il n’arrivait pas à se rappeler le nom du père des presses qu’il voit tous les jours.

Bartell d’Arcy était le ténor, il commençait juste à percer. La raccompagnait à la maison après les répétitions. Type prétentieux, avec sa moustache gominée. Lui a donné cette chanson *Ces vents qui soufflent du Sud*.

Quel vent la nuit où je suis allé la chercher il y avait cette réunion de loge à propos des billets de loterie après le concert de Goodwin dans la salle supérieure ou la salle des boiseries de l’Hôtel de Ville. Lui et moi derrière. Feuillet de sa partition qui m’échappe des mains pour aller se coller aux grilles du lycée. Une chance que. Une chose pareille lui gâche la soirée. Le professeur Goodwin accrochée à elle sur le devant. Tremblotait sur ses cannes, pauvre vieux poivrot. Ses concerts d’adieu. Positivement la dernière apparition sur une scène. Peut-être pour des mois ou peut-être pour toujours. La revois rire dans le vent, son cache-col retourné. Au coin de la route d’Harcourt me souviens cette rafale. Brrfou ! ça lui a soulevé jusqu’au dernier jupon et le vieux Goodwin quasi étouffé sous son boa. Le vent, elle en était toute rose. Me souviens quand on est rentré à la maison le feu qu’on remuait et ces pièces de selles de mouton qu’on a frites pour son souper avec la sauce Chutney qu’elle aimait. Et le grog chaud. Pouvais la voir dans la chambre depuis l’âtre qui dégrafait son corset : blanc.

Pffiou, un flop mou, son corset tombait sur le lit. Gardait toujours la chaleur de son corps. Aimait toujours s’en libérer. Assise là jusqu’à presque deux heures à ôter ses épingles à cheveux. Milly bordée dans son alcôve. Heureux. Heureux. C’est cette nuit que…

– Oh, M. Bloom, comment allez-vous ?

– Oh, comment allez-vous, Mme Breen ?

– On peut pas se plaindre. Comment va Molly ces temps-ci ? Des siècles que je l’ai pas vue.

– Tiptop, dit gaiement M. Bloom. Milly a une situation à Mullingar, vous savez.

– Non ? Voilà qui est extra !

– Oui. Chez un photographe de là-bas. Un feu d’artifice. Comment se porte la smala ?

– Font la fortune du boulanger, dit Mme Breen.

Combien en a-t-elle ? Pas d’autre en vue.

– Vous êtes de noir, je vois, vous n’avez pas…

– Non, dit M. Bloom. Je reviens juste d’un enterrement.

Ça va revenir toute la journée, je le sens. Qui est mort, quand et de quoi est-il mort ? Encore et encore.

– Oh, mon dieu, dit Mme Breen. J’espère que ce n’était pas un proche.

Autant profiter de sa compassion.

– Dignam, dit M. Bloom. Un vieil ami à moi. Il est mort d’un seul coup, le pauvre. Problème de cœur, je crois. L’enterrement était ce matin.

*C’est demain vos funérailles*  
*Cependant vous traversez la paille*  
*Drelindrelin dindin*  
*Drelindrelin*  

– Triste de perdre ses vieux amis, dirent mélancoliques et féminins les yeux de Mme Breen.

En voilà bien assez avec ça. Passons discrètement : mari.

– Et votre seigneur et maître ?

Mme Breen leva deux grands yeux au ciel. Ne les a pas perdus en chemin.

– Oh, m’en parlez pas ! dit-elle. Un vrai serpent à sornettes. Il est enfermé là maintenant à potasser la loi sur la diffamation dans le Code pénal. Il me fend le cœur. Attendez, je vais vous montrer.

Les vapeurs de la soupe d’abats, les vapeurs des bûches feuilletées à la confiture juste sorties du four se déversaient de chez Harrison. Les relents lourds du midi chatouillaient les amygdales de M. Bloom. Pour une bonne pâtisserie, du beurre, de la farine triple-A, de la cassonade, ou ils s’en rendraient compte avec le thé brûlant. Ou cela vient-il d’elle ? Un gamin nu-pied se tenait devant la grille, respirant les fumées. Trompe sa faim comme ça. Plaisir ou souffrance ? Diner à un penny. Couteau et fourchette rivés à la table.

Ouvre son sac-à-main, cuir craquelé. Épingle à chapeau : il faudrait un fourreau pour ces choses-là. Le plante dans l’œil d’un gars dans un tram. Farfouille. Ouvert. Argent. Prenez en un je vous prie. Des diablesses si elles perdent une pièce de six. Grabuge. Mari remet une couche. Où sont les dix shillings que je vous ai donnés lundi ? Vous ne feriez pas manger la famille du petit frère à mes frais ? Mouchoir sale : fiole. Pastille, ce qui est tombé. Qu’est-ce qu’elle ?…

– Il doit y avoir une nouvelle lune, dit-elle. Ça le chamboule toujours. Savez-vous ce qu’il a fait cette nuit ?

Sa main s’arrêta dans sa fouille. Ses yeux le figeaient, agrandis, alarmés et pourtant souriants.

– Quoi ? demanda M. Bloom.

Laisse la parler. Les yeux droits dans les yeux. Je vous crois. Confiance.

– M’a réveillé en pleine nuit, dit-elle. Un rêve il avait eu, un cauchemar.

Indigest.

– L’a dit que l’as de pique montait les escaliers.

– L’as de pique ! dit M. Bloom.

Elle prit dans son sac une carte postale pliée en deux.

– Lisez-moi ça, dit-elle. Il l’a reçu ce matin.

– Qu’est-ce que c’est ? demanda M. Bloom en prenant la carte. Q.I ?

– Q.I. : cuit, dit-elle. Quelqu’un se paie sa tête. C’est vraiment honteux celui qui fait ça.

– C’est bien vrai, dit M. Bloom.

Elle reprit la carte avec un soupir.

– Et maintenant il est parti au bureau de M. Menton. Il va demander dix mille livres de dommages et intérêts, il dit.

Elle replia la carte, la remit dans le désordre de son sac et fit claquer le fermoir.

Même robe de serge bleue qu’il y a deux ans, la trame perd sa couleur. Les beaux jours sont passés. Mèches sur les oreilles. Et cette toque démodée : trois vieilles grappes pour la rattraper. Misère décente. Elle qui s’habillait avec goût. Ces lignes autour de la bouche. À peine un an de plus que Molly, ou à peu près.

Faut voir le regard que lui a lancé cette femme au passage. Cruel. Le beau sexe est sans merci.

Il la regardait toujours, dissimulant l’ennui dans son regard. Ça attaque : cervelle, queue de bœuf, soupe au curry. J’ai faim aussi. Des miettes de gâteau sur le volet de sa robe : une plaque de farine sucrée collée à la joue. Tarte à la rhubarbe généreusement fourrée, intérieur riche et fruité. C’était Josie Powell. Chez Luke Doyle il y a longtemps. La Grange Dolphin, les charades. Q.I. : cuit.

Change de sujet.

– Est-ce que vous voyez parfois Mme Beaufoy ? demanda M. Bloom.

– Mina Purefoy ? dit-elle.

Philip Beaufoy, je pensais plutôt. Club des Theatropathes. Lapaire pense souvent au coup de maître. Ai-je tiré la chaîne ? Oui. Le dernier acte.

– Oui.

– Je suis passé sur le chemin pour demander si elle s’en était remis. Elle est internée à la maternité de la rue Holles. Le docteur Horne l’y a fait entrer. Trois jours qu’elle déguste.

– Oh, dit M. Bloom, js suis désolé d’apprendre ça.

– Oui, dit Mme Breen. Et la maison qui déborde de gosses. Un accouchement très difficile, l’infirmière m’a dit.

– Oh, dit M. Bloom.

Son regard lourd, apitoyé, absorbait les nouvelles. De compassion sa langue claqua par deux fois. Ttt ! ttt !

– Je suis désolé d’apprendre ça, dit-il. La pauvre ! Trois jours ! C’est terrible !

Mme Breen hocha la tête.

– Les douleurs ont commencé mardi…

M. Bloom lui toucha légèrement le petit-juif, en avertissement :

– Attention ! Laissez passer cet homme.

Une silhouette osseuse passa à grands pas le long du trottoir, dos à la rivière, lorgnon assuré par un cordon, le regard fixe et extatique dans la lumière du soleil. Un chapeau minuscule, serré comme une calotte, apprêtait son crane. À son bras un cache-poussière plié, une canne et un parapluie marquaient la cadence.

– Regardez-le, dit M. Bloom. Il marche toujours sur l’extérieur des lampadaires. Regardez !

– Qui est-ce, si ce n’est pas indiscret ? demanda Mme Breen. Il est zinzin ?

– Il s’appelle Cashel Boyle O'Connor Fitzmaurice Tisdall Farrell, dit M. Bloom en souriant. Regardez !

– Il est bien servi, dit-elle. Denis sera comme ça un de ces jours.

Elle s’interrompit soudainement.

– Le voilà, dit-elle. Il faut que je le rattrape. Au revoir. Passez-le bonjour Molly, voulez-vous ?

– Je n’y manquerai pas, dit M. Bloom.

Il l’observa qui louvoyait entre les passants cap sur les devantures. Denis Breen, redingote élimée, chaussé d’espadrilles bleues, sortait en trainant de chez Harrison, serrant contre son cœur deux gros bouquins. Rejeté par la mer. À l’ancienne. Il se laissa dépasser sans marquer de surprise et tourna sa barbe grisâtre vers elle, sa mâchoire distendue remuait alors qu’il discourait, convaincu.

Meshuggah. Manque un boulon.

M. Bloom reprit son chemin, léger, les yeux sur l’étroite calotte illuminée, la canne-à-pluie-pare-poussière qui tanguait devant lui. Sur son trente-et-un. Regarde-le ! Le revoilà qui fait un écart. Une façon de faire son chemin dans le monde. Et cet autre vieux farfelunatique dans ces frusques. Doit pas être facile tous les jours avec lui.

Q.I. : cuit. Ma tête à couper que c’est Alf Bergan ou Richie Goulding. L’ont écrit pour rigoler à la Maison écossaise je parie ce que tu veux. Vers les bureaux de Menton. Ses yeux de mollusques cloués sur la carte postale. Festin des dieux.

Il passa devant *Le temps irlandais*. Il pourrait y avoir d’autres réponses qui y attendent. Aimerais répondre à toutes. Bon système pour les criminels. Code. Heure du déjeuner. L’employé aux lunettes ne me connaît pas. Bah, laisse-les mijoter. Déjà assez barbant d’en avoir dépouillées quarante-quatre. Recherche habile dactylo, F., pour aider gentleman, travaux littéraires. Je vous ai appelé vilain chéri parce que je n’aime pas cet autre monde. S’il vous plaît dites-moi quel est le sens. S’il vous plaît dites-moi quel parfum votre femme. Dites-moi qui a créé le monde. Leur façon de vous bombarder de questions. Et l’autre Lizzie Twigg. Mes efforts littéraires ont eu la bonne fortune de gagner l’approbation de l’éminent poète A. E. (M. Geo. Russell). Pas le temps pour s’arranger les cheveux à boire du thé lavasse un livre de poésie dans les mains.

Le meilleur journal, de loin, pour une petite annonce. Z’ont les provinces maintenant. Cuisinière et gouvernante, exc. cuisine, avec femme de chambre. Recherche homme proactif pour débit de boissons. Jeune fille resp. (Rel. Cath.) souhaite situation fruiterie ou charcuterie. James Carlisle qui l’a lancé. Dividende six pour cent et demi. Fait son gros coup sur les actions Coates. Sans se presser. Vieux grippe-sous écossais. Rusé. Toutes les nouvelles de la cour. Notre gracieuse et populaire Vice-reine. Acheté *La piste irlandaise* maintenant. Lady Mountcashel s’est bien remise de ses couches et a suivi hier les chiens de la Ward Union à Rathoath, pour le lâcher. Renard immangeable. Chassent le gibier aussi. La peur fait secréter des jus ça le rend assez tendre à leur goût. À califourchon. Monte son cheval comme un homme. Trotteur. Pas question d’amazone ou de monter en croupe, pas de ça pour milady. Première au rendez-vous, devant pour l’hallali. Forte comme des juments à poulains certaines de ses femmes à cheval. Roulent des mécaniques devant les écuries. S’enfilent un verre de brandy pas le temps dire ouf. Celle de ce matin au Grovesnor. Monter avec elle dans le taxi : vœu pieux. Vous mène son cheval par-dessus le mur ou la barrière de cinq. Penser que ce beuglard l’a fait par dépit. À qui ressemblait-elle donc ? Ah oui ! Mme Miriam Dandrade qui m’a vendu ses vieux habits et ses sous-vêtements noirs à l’hôtel Shelbourne. Latino divorcée. Lui faisait ni chaud ni froid que je les ai en main. Comme si j’étais son étendoir. L’ai revue à la fête du vice-roi quand Stubbs le garde-champêtre m’a fait passer avec Whelan de l’*Express*. Faire les poubelles de ce beau monde. Souper. Mayonnaise que j’ai versée sur les prunes en pensant que c’était de la crème anglaise. Les oreilles ont dû lui en tinter pendant quelques semaines. Être son taureau. Courtisane née. Torcher les mioches, non merci.

Pauvre Mme Purefoy ! Mari méthodiste. De la méthode dans sa folie. Pour déjeuner, petit pain au safran, lait et eau de Seltz à la laiterie modèle. Y.M.C.A. Repas chronométré, trente-deux bouchées à la minute. Avec ça ses rouflaquettes ont quand même poussé. On dit qu’il a des relations. Le cousin de Théodore au château de Dublin. Toute famille a son membre en haut du panier. Lui plante sa graine tous les ans. L’ai vu devant les Trois Joyeux Pochards qui défilait tête nue et son aîné qui en portait un dans un filet à provision. Les braillards. La pauvre ! Et avoir à donner le sein à toute heure de la nuit, année après année. Égoïstes, ces abstinents. Mesquins. Un seul morceau de sucre dans mon thé, je vous prie.

Il se tenait devant le croisement la rue Fleet. La pause déjeuner. Un six-pence chez Rowe ? Faut que je consulte cette annonce à la bibliothèque nationale. Le huit-pence de Burton. Mieux. Sur le chemin.

Il repartit et dépassa la maison Bolton sur Westmoreland. Du thé. Du thé. Du thé. J’ai oublié de taper Tom Kernan.

Sss. Ttt, ttt, ttt ! Trois jours imagine-toi grognant alitée un mouchoir trempé de vinaigre sur le front, le ventre gonflé à en crever. Pffff ! Affreux, rien d’autre. Tête de l’enfant trop grosse : forceps. Recroquevillé en elle, cognant à l’aveugle pour sortir, tâtonnant pour trouver la sortie. Ça me tuerait net. Heureux que Molly s’en soit tirée facilement. Il faudrait qu’on invente quelque-chose pour empêcher ça. Condamnées aux travaux forceps. La sédation peut-être : ce qu’on a donné à la reine Victoria. Neuf elle en a eu. Une bonne pondeuse. La vieille au pied-bot, vivait dans un sabot, a eu plein de marmots. Suppose qu’il était phtisique. Serait temps que quelqu’un y réfléchisse au lieu de bavasser sur c’était quoi le sein pensif de la splendeur argentine. Ils pourraient facilement avoir de grands établissements toute la procédure presque indolore avec tous les impôts donne à chaque nouveau-né cinq thunes intérêts cumulés jusqu’à vingt-et-un ans ça fait cent shillings plus les cinq foutues livres multiplie ça par vingt système décimal encourage les gens à épargner garde cent-dix et quelque vingt-et-un ans faut faire ça au propre une bonne petite somme plus qu’on ne pense.

Pas les mort-nés bien sûr. Ne sont même pas au registre. Peine perdue.

Drôle de scène quand on en voit deux ensemble, les ventres en pointe. Molly et Mme Moisel. Sommet des mères. La phtisie se retire pour un temps, et puis revient. Qu’est-ce qu’elles ont l’air plates après ça tout d’un coup. Les yeux apaisés. Un poids de moins sur la conscience. La vieille Mme Thorton était une bien bonne âme. Tous mes bébés, qu’elle disait. La cuillère de bouillie dans sa bouche avant de les nourrir. C’est miammiam. S’est fait broyer la main par le fils du vieux Tom Wall. Sa première révérence au public. Tête comme une citrouille de foire. Le vieux bougon de Dr Murren. Les gens qui sonnent à toute heure. Pour l’amour de Dieu, docteur. Femme en souffrance. Et attendre des mois les honoraires. Pour soins promulgués sur votre femme. Les gens sont ingrats. Humains, les médecins, la plupart.

Devant l’immense portique du Parlement irlandais, une bande de pigeons prit son envol. Quart d’heure de jeu après manger. Sur qui vais-je me lâcher ? je choisis : le type en noir. Ça vient. Vient la chance. Doit être planant faire ça dans les airs. Apjohn, moi et Owen Goldberg perchés dans les arbres près de la prairie de l’Oie, à jouer les macaques. Le maquereau ils m’appelaient.

Une escouade de policiers déboucha en file indienne de la rue du Collège. Pas de l’oie. Les mines échauffées par la digestion, casques suant, tapotant leurs matraques. Après leur pitance une bonne cartouche de soupe grasse sous le ceinturon. Heureux le lot du policier, le plus souvent. Ils se divisèrent par groupes, un salut, et partirent faire leurs rondes. À leurs pâturages. Meilleur moment pour en attaquer un l’heure du pudding. Un direct dans le diner. Une seconde escouade contournait en ordre dispersé les grilles de Trinity College, revenant vers le poste. Cap sur la mangeoire. Prêts à recevoir l’assaut. Prêts à recevoir la soupe.

Il traversa sous le doigt canaille de Tommy Moore. Ils ont bien fait de le mettre au-dessus d’un urinoir : confluence des eaux. Devrait y avoir un endroit pour les femmes. Toujours à se précipiter dans la première pâtisserie. Que je me remette le chapeau d’aplomb. *Il n’y a pas une vallée en ce vaste monde*. Superbe cette chanson de Julia Morkan. A gardé sa voix jusqu’au dernier souffle. Élève de Michael Balfe, non ?

Il suivait des yeux les larges épaules de la tunique qui fermait la marche. Pas des clients faciles. Jack Power pourrait en dire long : son père un condé. Si un gars leur pose problème à la cueillette ils le passent à tabac dans le donjon. Peut pas leur en vouloir après tout avec le boulot qu’ils ont surtout les jeunes excités. Ce policier à cheval le jour où Joe Chamberlain a reçu son diplôme au Trinity il en a eu pour son argent. Ma parole ! Les sabots qui résonnaient sur nos talons dans la rue de l’Abbaye. Heureusement que j’ai eu la présence d’esprit de me jeter chez Hanning sinon j’étais cuit. Il s’est pris un sacré pain, pardieu ! A dû se fendre le crâne sur ces pavés. J’aurais pas dû me laisser entraîner avec ces carabins. Et les tronches de Trinity sous leurs mortiers. Cherchent la bagarre. Pourtant c’est comme ça que j’ai rencontré le jeune Dixon qui m’a pansé la piqure au Mater maintenant il est rue Holles où Mme Purefoy. Une chose mène à l’autre. J’entends encore le sifflet de la police. Tous mis les bouts. Pourquoi il m’a pris pour cible. M’amener à la décharge. Commencé juste là.

– Vivent les Boers !

– Hourra pour de Wet !

– Joe Chamberlain à la lanterne.

Jeunes sots : une bande de chiots qui jappent et s’époumonent. Vinnegar Hill. La fanfare des Crémiers. Quelques années et la moitié sont des magistrats et des fonctionnaires. Une guerre arrive : tous dans ce bazar d’armée : les mêmes gars qui. Qui jusqu’à l’échafaud.

N’ sait jamais à qui on parle. Corny Kelleher il a du Javert dans le regard. Comme ce Pierre ou Denis ou James Carey qui a vendu la mèche pour les invincibles. Employé de la municipalité par-dessus le marché. Chauffant des jeunes mal dégrossis pour en savoir plus et tout ce temps recevant son enveloppe du château : service secret. Puis ni vu ni connu : pestiféré. Voilà pourquoi ces policiers en civil draguent toujours les bonniches. Facile à repérer un homme qui a l’habitude de l’uniforme. La plaque contre une porte de service. La malmène un peu. Puis vient le plat principal. Mais qui est ce monsieur qui vient souvent ? Et qu’est-ce que disait le jeune maître ? L’œil sur le trou de la serrure. L’appât. Jeune étudiant le sang qui chauffe qui fait l’andouille, ses bras dodus pendant qu’elle repasse.

– Ils sont à vous, Mary ?

– Je mets pas de ces choses-là… Arrêtez ou je le dirai à Mme. Dehors la moitié de la nuit.

– De grandes choses se préparent, Mary. Vous allez voir.

– Ah, du balai avec vos grandes choses qui se préparent.

Les barmaids aussi. Et les filles des tabacs.

James Stephen son idée c’était le mieux. Il les connaissait. Des cercles de dix, comme ça un gars ne pouvait pas griller plus que son propre entourage. Sinn Fein. Renonce et c’est le couteau. La main invisible. Tu restes. Le peloton d’exécution. La fille du geôlier l’a fait sortir de Richmond, et en route pour Lusk. Au vert au Buckingham Palace hotel juste sous leur nez. Garibaldi.

Il faut avoir un certain charisme : Parnell. Arthur Griffith c’est une tête, mais les foules c’est pas son truc. Ou alors bavasser sur notre belle patrie. Balivernes. Les salons de thé de la Dublinoise de Boulangerie. Clubs de débats. Que le républicanisme est la meilleure forme de gouvernement. Que la question du langage devrait prévaloir sur la question économique. Les appâter chez vous avec vos filles. Les gaver de viande et de boisson. L’oie de la saint-Michel. Voilà pour vous, une bonne louche de cette sauce au thym de derrière les fagots. Reprenez donc de cette graisse d’oie avant que cela ne refroidisse. Enthousiastes et affamés. Un petit pain à un sou et en avant la musique. Pas de répit pour celui qui découpe. L’idée qu’un autre paie il n’y a pas meilleure sauce au monde. Qu’ils se sentent bien à l’aise. Passez-nous donc ces abricots, en montrant les pèches. Le jour viendra qui est au loin. Le soleil de l’autonomie se lèvera au nord-ouest.

Son sourire s’effaçait tandis qu’il avançait, un nuage lourd cachait peu à peu le soleil, la façade morose de Trinity College s’assombrissait encore. Les trams se croisaient, entraient, sortaient, grinçaient, carillonnaient. Vains mots. Les choses suivent leur cours, jour après jours : les détachements de police défilent dans un sens et puis dans l’autre : tram aller, tram retour. Ces deux mabouls qui traînent. Dignam expédié. Mina Purefoy ventre gonflé à grogner sur un lit qu’on lui sorte cet enfant. Un nouveau-né toutes les secondes quelque-part. Un autre qui meurt. Cinq minutes que j’ai donné à manger aux oiseaux. Trois cents ont cassé leur pipe. Trois cents autres sont nés dont on lave le sang, tous sont lavés dans le sang de l’agneau, bêlant maaaaaa.

Toute une ville y passe, une autre prend sa place et passe à son tour : et d’autres viennent et passent aussi. Maisons, rangées de maisons, rues, kilomètres de chaussée, piles de briques, pierres. Qui passent de main en main. Ce proprio-ci ce proprio-là. Le propriétaire ne meurt jamais à ce qu’on dit. Un autre se glisse dans ses souliers quand il reçoit sa notice d’éviction. Ils achètent l’endroit à prix d’or et après ils ont encore tout l’or. Il y a arnaque. Entassés dans les villes, usés par les âges. Pyramides de sable. Construites sur du pain et des oignons. Esclaves, Muraille de Chine. Babylone. Restent de gros cailloux. Tours circulaires. Subsistent décombres, banlieues tentaculaires, constructions en carton. Les maisons champignons de Kerwan faites avec des courants d’air. Un abri, pour la nuit.

Personne n’est rien.

C’est le pire moment de la journée. Vitalité. Terne, maussade : déteste cette heure-ci. Me sens comme si on m’avait mâché puis recraché.

La maison du prévôt. Le révérend Dr. Saumon : saumon en boite. Bien en boite là-dedans. Y vivrais pas même si on me payait. J’espère qu’ils auront du foie et du lard aujourd’hui. La nature a horreur du vide.

Le soleil se libérait lentement et faisait scintiller l’argenterie dans la vitrine de Walter Sexton sur le trottoir d’en face, où John Howard Parnell passait sans rien voir.

Qui vient : le frère. Son portrait. Un visage qui marque. En voilà une coïncidence. Sûr, des centaines de fois tu penses à quelqu’un et tu ne tombes pas dessus. Comme un somnambule. Personne ne le connaît. Il doit y avoir conseil municipal aujourd’hui. On dit qu’il n’a jamais mis l’uniforme de maréchal de la ville depuis qu’il a dégotté le poste. Charley Boulger lui se donnait des grands airs, cheval, tricorne, bouffi, poudré et rasé. Regarde-moi cette allure de chien battu qu’il a. Mangé un œuf passé. Œil poché sur fantôme. J’ai une douleur. Le frère du grand homme : le frère de son frère. Il aurait l’air joli sur un cheval de parade. Fait un arrêt à la D.d.B. sans doute pour un café, jouer aux échecs. Son frère usait des hommes comme de pions. Les a tous laissés choir. Peur de lui faire une remarque. Les paralyse avec ce regard qu’il a. Voilà le charisme : le nom. Ils ont tous un grain. Fanny la folle et son autre sœur Mme Dickinson qui se balade harnachée d’écarlate. Droit comme un piquet, comme ce chirurgien M’Ardle. Pourtant David Sheehy l’a battu à Meath Sud. Renonce aux Communes et c’est la sinécure de la couronne. Le banquet des patriotes. Manger des pelures d’orange dans le parc. Simon Dedalus a dit quand ils l’ont mis au parlement que Parnell sortirait de sa tombe, le prendrait par le bras pour le mettre à la porte des Communes.

– De la pieuvre à deux têtes, une de ces deux têtes est la tête où les confins du monde ont oublié de se rejoindre alors que l’autre parle avec un accent écossais. Les tentacules…

Ils passèrent dans le dos de M. Bloom, le long du trottoir. Barbe et bicyclette. Jeune femme.

Et le voilà lui aussi. C’est fort de coïncidence : la deuxième. Les évènements à venir projettent leur ombre en avant. De l’approbation de l’éminent poète, M. Geo Russell. Ça pourrait être Lizzie Twigg avec lui. A. E. : qu’est-ce que ça veut dire ? Initiales, peut-être. Albert Edward, Arthur Edmund, Alphonsus Eb Ed Esquire. Qu’est-ce qu’il disait ? Les confins du monde avec un accent écossais. Tentacules : pieuvre. Quelque-chose d’occulte : symbolisme. Se répand. Elle gobe tout. Ne dit ni mais. Pour aider gentleman, travaux littéraires.

Ses yeux suivaient la haute silhouette vêtue de bure, barbe et bicyclette, une femme à l’écoute à ses côtés. Sortent du végétarien. Rien que des laides-gommes et des fruits. Mange pas de bifteck. Si tu fais ça les yeux de la vache te poursuivront pour l’éternité. Ils disent que c’est plus sain. De l’eau et du vent cela dit. J’ai essayé. Ça vous file la courante pour la journée. Pareil que le hareng saur. Des rêves toute la nuit. Comment ils appelaient ce truc qu’ils m’ont servi steak à la noix ? Noix-ophages, fruitariens. Pour vous donner l’impression que vous mangez du rumsteck. Absurde. Et salé. Ils cuisinent au bicarbonate. Te voilà au pied du robinet pour la nuit.

Ses bas tirebouchonnent sur les chevilles : je déteste ça : quel manque de goût. Ces littéraires, éthérés tous autant qu’ils sont. Rêveurs, nébuleux, symbolistiques. Esthètes c’est le mot. Ça ne me surprendrait pas si c’était ce genre de bouffe tu vois, produit les genre ondes cérébrales le poétique. Par exemple un de ces policiers qui mouillent la chemise de ragoût irlandais tu n’en tireras pas un vers. Sait même pas ce qu’est la poésie. Faut un certain état d’esprit.

*Rêveur et nébuleux le goéland*  
*Surveille d’aplomb le gris océan*  

Il traversa au coin de la rue Nassau et s’arrêta devant la devanture de Yeates et Fils, s’informant du prix des jumelles. Ou est-ce que je fais un saut chez le vieil Harris pour bavarder un peu avec le jeune Sinclair ? De bonnes manières, ce garçon. Probablement sa pause déjeuner. Faut que je fasse réparer mes vieilles lunettes. Optiques Goerz six guinées. Les Allemands sont partout. Facilités de paiement pour conquérir le marché. Cassent les prix. Pourrais tomber sur une paire aux objets trouvés des chemins de fer. Stupéfiant ce que les gens peuvent oublier dans les trains et les toilettes. Où donc ont-ils la tête ? Les femmes aussi. Inimaginable. L’an dernier en allant à Ennis dû ramasser le sac de cette fille de fermier et le lui tendre à l’embranchement de Limerick. Et l’argent non réclamé. Il y a une petite horloge là-haut sur le toit de la banque pour tester ces lunettes.

Ses paupières s’abaissèrent sur l’iris jusqu’à frôler la prunelle. Peux pas le voir. Si tu te l’imagines tu peux presque le voir. Peux pas le voir.

Il fit volte-face et, debout entre les stores, tendit son bras droit vers le soleil, la main ouverte. Souvent voulu essayer ça. Oui : complétement. La pointe de son petit doigt oblitérait le disque du soleil. Doit être le foyer où le rayons se croisent. Si j’avais des lunettes noires. Intéressant. Il était beaucoup question de ces taches solaires quand nous étions rue Lombard. De terribles explosions. Il va y avoir une éclipse totale cette année : vers l’automne.

Maintenant que j’y pense cette boule est abaissée à l’heure de Greenwich. C’est l’horloge, est actionnée par un fil électrique depuis Dunsink. Devrais y faire un tour un premier samedi du mois. Si je pouvais me faire recommander auprès du professeur Joly ou me renseigner sur sa famille. Ça ferait le job : on est toujours flatté. Vanité où l’on s’attend le moins. Noble fier de descendre de la maîtresse d’un roi. Son aïeule. Allez-y à la truelle. Bouche aimable partout s’attable. Pas entrer et lâcher tout de go ce que tu sais que tu ne dois pas : c’est quoi la parallaxe ? Reconduisez ce monsieur à la porte.

Ah.

Sa main retomba à son côté.

Jamais ’ne saura rien là-dessus. Perte de temps. Boules de gaz qui tournent en rond, se croisent, passent. Toujours le même cirque. Gaz : puis solide : puis froid : puis coquille vide à la dérive, roche glacée, comme ce rocher à l’ananas. La lune. Il doit y avoir une nouvelle lune, a-t-elle dit. Je crois bien.

Il passa devant la maison Claire.

Attends. La pleine lune c’était la nuit on était dimanche il y a exactement quinze jours il y a une nouvelle lune. La promenade le long de la Tolka. Pas mal pour une lune de Bellevue. Elle fredonnait. La jeune lune de mai qu’elle brille, mon amour. Lui de l’autre côté. Coude, bras. Lui. Le ver luisant qu’il scintille, mon amour. Se touchent. Doigts. Qui demandent. Réponse. Oui.

Stop. Stop. Ce qui a été a été. Doit être.

M. Bloom, le souffle court, dépassa d’un pas ralenti l’impasse Adam.

Avec soulagement : reste calme, ses yeux observèrent voilà la rue ici en plein midi les épaules tombantes de Bob Doran. Parti pour sa cuite annuelle, a dit M’Coy. Ils boivent pour dire ou faire quelque-chose ou *cherchez la femme*. Un tour dans le Coombe avec les potos et des prostituées et le reste de l’année sérieux comme un pape.

Oui. Ce que je pensais. Se glisse à l’Empire. Disparu. Une eau gazeuse lui ferait du bien. Pat Kinsella avait là son théâtre de la Harpe avant que Whitbred n’ai repris le Queen. Une crème d’homme. Ce numéro de Dion Boucicault avec sa tête de pleine lune sous une capote. Les Trois Jouvencelles du Pensionnat. Comme le temps file, hein ? Les longues jambes rouges du pantalon sortant de sous ses jupons. Buveurs, buvant, crachotant dans leur rire, la boisson avalée de travers. Encore plus fort, Pat. Rouge brut : bagatelle pour poivrot : gros rire et fumée. Ôte ce blanc chapeau. Ses yeux des œufs durs. Où est-il aujourd’hui ? Un clodo quelque part. La harpe qui jadis nous harponna.

J’étais plus heureux alors. Mais était-ce moi ? Ou suis-je moi maintenant ? Vingt-huit ans j’avais. Elle vingt-trois. Quand on est parti de la rue Lombard quelque-chose a changé. N’y a plus repris goût après Rudy. On ne rembobine pas le temps. Comme tenir de l’eau entre les doigts. Y retournerais-tu ? On commençait juste. Tu le ferais ? N’es-tu pas heureux chez toi pauvre petit garnement ? Veut me recoudre les boutons. Je dois répondre. Écris à la bibliothèque.

La rue Grafton, égayée, tous stores dehors, attisa ses sens. Mousselines à motifs, dames en soie et douairières, le cliquetis des harnais, le choc sourd des sabots sur la chaussée qui cuit. Pieds épais cette femme aux bas blancs. Que la pluie les lui crotte. Bouffeuse de patates du cru. Toutes les grosses vaches y sont passées. Ça donne toujours aux femmes une démarche pataude. Molly n’a pas l’air de tenir droit.

Il dépassa en flânant la vitrine de Brown Thomas, Soieries. Cascades de rubans. Frêles soies de Chine. Une urne inclinée déversait de son embouchure un flot de popelines couleur de sang : sang chatoyant. Les Huguenots ont apporté ça ici. *La causa e santa !* Tara tara. Superbe ce chœur. Tarii tara. Doit être lavée à l’eau de pluie. Meyerbeer. Tara : boum boum boum.

Pelotes d’épingles. Ça fait un bon moment que je menace d’en acheter une. Toujours à les planter n’importe où. Des aiguilles dans les rideaux de la fenêtre.

Il dénuda légèrement son bras gauche. Éraflure : presque partie. Pas aujourd’hui en tout cas. Dois repasser pour cette lotion. Pour son anniversaire peut-être. Juinjuilletaoûtseptembre le huit. Presque trois mois encore. Et puis elle pourrait ne pas apprécier. Les femmes ne ramassent jamais les épingles. Disent que ça coupe l’am.

Soies brillantes, jupons sur de minces tringles de cuivre, raies de bas de soie lissés.

Rien ne sert de rembobiner. Devait arriver. Dis-moi tout.

Voix aigües. Soie tiède de soleil. Cliquetis des harnais. Tout ça pour une femme, foyer et maisons, toiles d’araignée, fil de soie, argent, fruits exquis, corsés de Jaffa. Agendath Netaim. La richesse du monde.

Une tiédeur humaine et toute en rondeur s’installa commodément sur son cerveau. Qui céda. Le parfum des étreintes l’assaillit tout entier. La chair affamée obscurément, sourdement, il brulait d’adorer.

Rue Duke. Nous y voilà. Dois manger. Le Burton. Après ça ira mieux.

Il tourna le coin de Combridge, pourchassé, encore. Cliquetis, sourds sabots. Corps parfumés, tièdes, pleins. Tous baisés, tous cédant : dans les prés profonds de l’été, l’herbe enchevêtrée, écrasée, dans les couloirs suintant des immeubles des pauvres, sur les sofas, les lits grinçants.

– Jack, mon amour !

– Chéri !

– Embrasse-moi, Reggy !

– Mon petit !

– Mon amour !

