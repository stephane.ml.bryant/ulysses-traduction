Corny Kelleher referma son long registre et jeta un regard tombant à un couvercle de cercueil en pin qui montait la garde dans un coin. Il se redressa, s’en approcha, le fit pivoter sur son axe, en révisa la forme et les laitons. Mâchonnant son brin de paille il posa de côté le couvercle de cercueil et avança vers le pas de la porte. Là, il inclina le rebord de son chapeau pour abriter ses yeux et s’adossa à l’armature de la porte, observant, oisif, les alentours.

Le père John Conmee monta dans le tram de Dollymount au pont Newcomen.

Corny Kelleher colla ses deux larges souliers l’un à l’autre, le regard vide, le chapeau tombant, mâchonnant son brin de paille.

L’agent 57C, qui faisait les cent pas, s’arrêta pour passer le temps.

– Voilà une belle journée, M. Kelleher.

– Ouais, dit Corny Kelleher.

– Il fait très lourd, dit l’agent.

La bouche de Corny Keller émit un jet parabolique de jus de paille au moment où un bras blanc et généreux lançait une pièce d’une fenêtre de la eur Eccles.

– Quoi de neuf ? demanda-t-il.

– J’vu hier ce parti particulier, dit l’agent dans un souffle.

***




