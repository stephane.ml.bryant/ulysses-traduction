Stephen Dedalus observait au travers de la vitrine voilée les doigts du lapidaire éprouver une chaîne ternie par le temps. La poussière voilait la vitrine et les présentoirs. La poussière noircissait les doigts au travail, leurs ongles de rapace. La poussière somnolait sur les spires ternies de bronze et d’argent, sur les losanges de cinabre, sur les rubis, gemmes lépreuses et rouge-vin.

Nées dans la nuit obscure et grouillante de la terre, froides étincelles, maléfiques, lumières qui brillent à même les ténèbres. Là les archanges déchus jetèrent les étoiles de leurs fronts. Groins fangeux, mains, qui fouissent fouissent, les agrippent les arrachent.

Elle dance dans une pénombre infâme où brûlent ensemble ail et résine. Un matelot à la barbe de rouille goulotte du rhum d’un gobelet et la regarde. Un rut, long, silencieux, de tant de mer accumulée. Elle danse, vire, ballotant son arrière-train de truie, ses hanches, et sur son ventre obscène bat un œuf rubis.

Le vieux Russell avec une chamoisine sale et déchirée astiqua sa gemme encore une fois, la retourna et la tint à la pointe de sa barbe de Moïse. Grand-père orang-outan qui couve des yeux un trésor volé.

Et vous qui arrachez de vieilles images à la terre-sépulture ! Les paroles de déraison des sophistes : Antisthène. une sapience des drogues. Blé auroral et immortel, dressé, d’éternité en éternité.

Deux vieilles femmes, fraichement revenues d’un bol d’air saumâtre, se trainaient le long de la route du pont de Londres, au travers d’Irishtown, l’une avec un parapluie fatigué et plein de sable, l’autre avec un sac de sages-femme où brinquebalaient onze coques.

Le vrombissement des courroies de cuir en roue libre, le bourdonnement des dynamos de la centrale électrique engagèrent Stephen à reprendre la marche. Êtres sans être. Arrêtez ! Cette pulsation hors vous, toujours, et la pulsation toujours en vous. Votre cœur dont vous nous rebattez. Moi entre les deux. Où ? Entre ces deux mondes rugissants où ils tournoient, moi. Brise-les, l’un, les deux. Mais m’assommer du même coup. Brise-moi, qui le peut. Ribaude et boucher, voilà les mots. Je l’affirme ! Mais pas encore, un moment. Un coup d’œil alentour.

Oui, très juste. Énorme et merveilleux, et toujours à l’heure. Vous dites vrai, monsieur. Un lundi matin, mais oui, exactement.

Stephen descendit le passage Bedford, la poignée du frêne tressautant sur son omoplate. Dans la devanture de Clohissey, une gravure passée attira son regard : Heenan contre Sayers, 1860. Autour du ring, derrière les cordes, des parieurs aux regards fixes sous leurs canotiers. Les poids-lourds en maillots serrés se présentaient aimablement leurs poings protubérants. Et ça pulse là aussi : des cœurs de héros.

Il tourna et s’arrêta devant la carriole inclinée du bouquiniste.

— Deux pence chacun, dit le camelot. Quatre pour six pence.

Pages en lambeaux. *L’apiculteur irlandais*. *Vie et Miracles du curé d’Ars*. *Le guide de poche de Killarney*.

Je pourrais bien retrouver là un de mes prix scolaires mis au clou. *Stephano Dedalo, alumno optimo, palmam ferenti*.

Le père Conmee, qui avait terminé la lecture de ses petites heures, traversa le hameau de Donnycarney, murmurant ses vêpres.

Trop bien reliés, probablement. Qu’est-ce que c’est que ça. Huitième et neuvième livres de Moïse. Secret des secrets. Le sceau du roi David. Pages écornées : lues et relues. Qui est passé ici avant moi ? Comment soulager les mains gercées. Recette du vinaigre de vin blanc. Comment gagner le cœur d’une femme. Pour moi, ça. Dire le talisman suivant par trois fois avec les mains jointes :

— *Se el yilo nebrakada femininum ! Amor me solo! Sanktus ! Amen.*

Qui est l’auteur ? Charmes et invocations du très saint abbé Peter Salanka, divulgués à l’intention de tous les vrais croyants. Pas pire qu’aucun autre abbé, pas pire que les marmottages de Joachim. À bas, crane d’œuf, ou nous te tondrons la tonsure.









