AU CŒUR DE LA MÉTROPOLE D’HIBERNIA


Devant la colonne Nelson les trams ralentissaient, changeaient d’aiguillage, changeaient de motrice, partaient pour Blackrock, Kingstown et Dalkey, Clonksjee, Rathgar et Terenure, Palmerston Park, les hauts de Rathmines, Sandymount Green, Rathmines, Ringsend et la Tour de Sandymount, la Croix d’Harold. Le pointeur enroué de la Compagnie Unie des Tramways de Dublin braillait les départs :

– Rathgar et Terenure !

– En voiture, Sandymount Green !

À droite, à gauche, parallèles, cliquetant et carillonnant une impériale et une voiture simple quittaient leur terminus, braquaient dans leurs voies, glissaient parallèles.


LE MESSAGER DE LA COURONNE


Sous les arcades du bureau central des postes les cireurs criaient et astiquaient. Rue du Prince, Nord, sur les fourgons postaux de Sa Majesté alignés en livrée vermillon rehaussée de côté des royales initiales, E. R., atterrissaient des volées bruyantes de sacs de lettres, cartes postales, recommandés, paquets, assurés et affranchis, en partance pour l’outremer, les îles britanniques, la province ou la ville.


CES MESSIEURS DE LA PRESSE


Des débardeurs aux lourdes bottes roulaient des barils dans un grondement sourd hors des entrepôts du Prince et les empilaient sur la remorque de la brasserie. Sur la remorque de la brasserie s’empilaient dans un grondement sourd les barils roulés par des débardeurs aux grosses bottes hors des entrepôts du Prince.

– Le voilà, dit Red Murray. Alexander Cleys.

– Découpez-le, voulez-vous ? dit M. Bloom, et j’irai le porter aux bureaux du *Telegraph*.

La porte du bureau de Ruttledge grinça à nouveau. Davy Stephen, perdu dans une ample cape, un menu chapeau de feutre couronnant sa tête bouclée, en sortit, un rouleau de papier sous le manteau, le messager du roi.

Les longs ciseaux de Red Murray excisèrent l’annonce en quatre mouvements nets. Copier coller.

– Je passerai par les presses, dit M. Bloom en prenant la coupure.

– Bien sûr, s’il veut un par., dit Red Murray avec empressement, nous pouvons lui en faire un.

– entendu, dit M. Bloom avec un signe de tête. Je ferai passer le message.

Nous.


WILLIAM BRAYDEN,  
GENTILHOMME, D’OAKLANDS, SANDYMOUNT


Red Murray toucha le bras de M. Bloom de ses ciseaux et souffla :

– Brayden.

M. Bloom se tourna et vit le portier en livrée soulever son képi monogrammé à l’entrée d’une silhouette imposante qui passa entre les placards de *L’homme libre et la presse nationale hebdomadaire* et *L’homme libre et la presse nationale quotidien*. Grondement sourd des barils de Guinness. Elle montait l’escalier, imposante, un parapluie en guise de gouvernail : une face solennelle encadrée d’une barbe. Le dos drapé gravissait les marches une à une : dos. Toutes ses méninges sont dans la nuque, dit Simon Dedalus. Bourrelet de chair sur sa face arrière. Plis de graisse de cou, graisse, cou, graisse, cou.

– Ne trouvez-vous pas que son visage rappelle celui de Notre Sauveur ? chuchota Red Murray.

La porte du bureau de Ruttledge chuchota : ii : crii. On construit toujours les portes en face l’une de l’autre pour que le vent. Entrée. Sortie.

Notre sauveur : visage ovale encadré d’une barbe : conversant au crépuscule. Marie, Marthe. Avec un parapluie-épée en guise de gouvernail, vers la rampe : Mario le ténor.

– Ou celui de Mario, dit M. Bloom.

– Oui, reconnut Red Murray. Mais on dit que Mario était le portrait de Notre Sauveur.

JesusMario aux joues fardées, pourpoint et fuseau. La main sur le cœur. Dans Martha.

*Vie-ens ô toi l’égarée,*  
*Vie-ens ô toi l’aimée !*  


LA CROSSE ET LA PLUME


– Son altesse a téléphoné deux fois ce matin, dit gravement Ned Murray.

Ils regardèrent genoux, mollets, bottines disparaître. Cou.

Un commis des télégraphes entra prestement, jeta une enveloppe sur le comptoir et prestement s’en fut sur un :

– *L’homme libre* !

M. Bloom dit lentement :

– Eh bien, c’est aussi un de nos sauveurs.

Un mince sourire l’accompagnait tandis qu’il soulevait le battant du comptoir, qu’il prenait la porte de côté et tout au long des escaliers et du couloir sombres et chauds, tout au long des parois qui maintenant résonnaient. Mais sauvera-t-il la circulation ? Battement. Battement.

Il poussa le tambour vitré et entra, enjamba un fatras de papier d’emballage. Entre deux rangées de rotatives pulsantes, il s’achemina vers le cabinet de lecture de Nannetti.

Hynes là aussi : compte-rendu des funérailles sans doute. Battement. Batte.

C’EST AVEC UN REGRET NON-FEINT QUE NOUS ANNONÇONS LA DISSOLUTION D’UN CITOYEN DE DUBLIN DES PLUS RESPECTÉS

Ce matin la dépouille de feu M. Patrick Dignam. Machines. Pulvérisent un homme si elles te happent. Mènent le monde maintenant. Ses engrenages à lui bossent dur eux aussi. Comme ici, partis en vrille : fermentent. Travaillent à plein, tournent à plein. Et ce vieux rat gris qui tournait à plein pour en être.

LA FABRIQUE D’UN GRAND QUOTIDIEN

M. Bloom s’arrêta derrière l’ossature sèche du chef d’atelier, admirant le brillant de sa calotte.

Bizarre qu’il n’ait jamais vu son vrai pays. Irlande mon pays. Député de College Green. Il a bien fait mousser cet angle le journalier du-jour-le-jour. C’est les annonces et les suppléments qui font vendre un hebdomadaire, pas les nouvelles défraichies de la gazette officielle. La reine Anne est morte. Publication autorisée l’an mille et. Domaine situé sur les terres communales de Rosenallis, baronnie de Tinnahinch. Avis à qui de droit ordre du jour conforme aux statuts stipulant retour de nombre de mules et ânesses exportées de Balline. Notes Nature. Caricatures. Les histoires à dormir debout de Phil Blakes, chaque semaine. La page de l’oncle Toby pour les tout-petits. Le courrier des ploucs. Cher monsieur le rédacteur, quel est le meilleur remède contre les flatulences ? Ça, ça ne me viendrait pas mal. On apprend beaucoup à enseigner aux autres. La touche personnelle. S.D.I. Surtout Des Images. Baigneurs athlétiques sur rivage doré. Le plus gros ballon du monde. Deux sœurs se marient le même jour. Deux fiancés qui se rient franchement au nez. Cuprani aussi, l’imprimeur. Plus irlandais que les Irlandais.

Les machines trépidaient sur trois temps. Batte, batte, batte. Et s’il avait une attaque maintenant et que personne ne sache comment les arrêter elles trépideraient pareil, encore et encore, imprimant à droite, à gauche, en long en large et en travers. Fable contée par un idiot. Faut la tête froide.

– Eh bien, passez-le dans l’édition du soir, conseiller, dit Hynes.

L’appellera bientôt monsieur le maire. John la perche le soutient, paraît-il.

Le chef d’atelier, sans répondre, griffonna à tirer sur un coin de la feuille et fit signe à un typographe. Sans un mot, il tendit la feuille par-dessus l’écran de verre malpropre.

– Parfait : merci, dit Hynes en s’en allant.

M. Bloom lui barrait le passage.

– Si vous voulez toucher le caissier va partir déjeuner, dit-il, avec un geste du pouce vers l’arrière.

– Et vous ? demanda Hynes.

– Mm-m, dit M. Bloom. Si vous êtes vif vous l’attraperez.

– Merci, mon vieux, dit Hynes. Je vais le taper moi aussi.

Il fila empressé vers *L’homme Libre quotidien*.

Trois balles je lui ai prêté chez Meagher. Trois semaines. Troisième rappel.

NOUS OBSERVONS LE PUBLICISTE À L’ŒUVRE

M. Bloom déposa sa coupure sur le bureau de M. Nannetti.

– Excusez-moi, conseiller, dit-il. Cette annonce, vous voyez. Cleys, vous vous en souvenez ?

M. Nanetti considéra la coupure un moment et fit oui de la tête.

– Il la veut pour juillet, dit M. Bloom.

Le chef d’atelier avança son crayon.

– Mais attendez, dit M. Bloom. Il veut un changement. Cleys, vous voyez. Il veut deux clés en haut.

Font un boucan infernal. Il ne l’entend pas. Nannan. Nerfs d’acier. Peut-être comprend-il ce que.

Le chef d’atelier se retourna pour écouter, patiemment, et levant un coude, entreprit de gratter l’alpaga de son veston sous l’aisselle, au ralenti.

– Comme ça, dit M. Bloom, croisant ses deux index sur le haut du papier.

Laissons-le capter ça d’abord.

M. Bloom, quittant des yeux ses doigts en croix, observa le teint cireux du chef d’atelier, il a un début de jaunisse je pense, et au-delà les rouleaux obéissants qui dévidaient le papier en rames énormes. Crante. Crante. Des kilomètres déroulés. Qu’est-ce que ça devient ? Oh, ça enroule la viande, les paquets : usages variés, mille et une choses.

Glissant adroitement ses mots dans les pauses des machines il fit un dessin rapide sur le bois balafré.


LA MAISON À CLÉ(Y)S


– Comme ça, voyez. Deux clés qui se croisent ici. Un cercle. Et là le nom. Alexander Cleys, négociant en thés, vins et spiritueux. Etcétéra.

Mieux vaut ne pas lui apprendre son métier.

– Vous savez bien, conseiller, exactement ce qu’il veut. Tout ça encerclé en gras : la maison à clés. Vous voyez ? Pensez-vous que ça soit une bonne idée ?

La main du chef d’atelier se déplaça sur le bas des côtes et y reprit son calme grattage.

– L’idée, dit M. Bloom, c’est la maison à clés. Vous savez, conseiller, le parlement de Manx. Allusion au Home Rule. Les touristes, vous savez, de l’Île de Man. Ça accroche l’œil, vous voyez. Est-ce possible ?

Je pourrais peut-être lui demander comment on prononce ce *voglio*. Mais s’il ne savait pas ce serait embarrassant pour lui. Mieux vaut pas.

– Ça peut se faire, dit le chef d’atelier. Avez-vous le modèle ?

– Je peux le trouver, dit M. Bloom. C’était dans un journal de Kilkenny. Il y a aussi une maison. Je cours le lui demander. Eh bien, vous pouvez lui faire ça et un petit par. pour attirer l’attention. Vous savez l’habituel. Établissement sous licence, premier ordre. Un besoin comblé. Etcétéra.

– Ça peut se faire, dit-il. Qu’il nous donne trois mois d’avance.

Un typographe lui apporta une page d’épreuve encore molle. Il se mit à la vérifier en silence. M. Bloom se tenait debout à ses côtés, écoutant la puissante pulsation des pistons, observant les typographes muets sur leurs casses.


ORTHOGRAPHIQUE


Faut être sûr de son orthographe. La fièvre des épreuves. Martin Cunningham a oublié de nous donner son casse-tête orthographique ce matin. Qu’il est amusant de voir l’embarras double erre ? sans par un erre eil d’un colporteur fourbu qui jauge ahu la symétrie avec un y-grec d’une poire pelée sous le mur d’un cimetière. Tout bête n’est-ce pas ? Cimetière pour la symétrie bien sûr.

J’aurais dû dire quand il a replanté son gibus. Merci. J’aurais dû dire quelque-chose sur un vieux chapeau ou quelque-chose. Non. J’aurais pu dire. Paraît comme neuf maintenant. Voir sa tronche.

Sllt. Le pont inférieur de la première machine projeta en avant sa planche avec sllt la première fournée de papiers pliés en cahier. Sllt. Presque humain la manière dont elle sllt pour attirer l’attention. Fait tout son possible pour parler. Cette porte qui sllt demande en grinçant qu’on la ferme. Chaque chose parle à sa façon. Sllt.


ÉMINENT HOMME D’ÉGLISE CORRESPONDANT OCCASIONNEL 

Le chef d’atelier rendit les épreuves brusquement en disant :

– Attends. Où elle la lettre de l’archevêque ? Elle doit être reproduite dans le *Télégraphe*. Où est machin-chose ?

– Moines, monsieur ? demanda une voix depuis la galée.

– Ouais. Où est Moines ?

– Moines !

M. Bloom reprit sa coupure. Temps de filer.

– Je vais donc me procurer le modèle, M. Nannetti, dit-il, et vous le mettrez en bonne place je sais.

– Moines !

– Oui, Monsieur.

Trois mois d’avance. D’abord, souffler un peu. On tentera le coup. Insister sur août : bonne idée : le mois de l’exposition chevaline. Ballsbridge. Touristes viennent pour l’exposition.

LE PÈRE DES PRESSES

Il traversa la chambre des casses, croisant un vieil homme myope recourbé sur son tablier. Le vieux Moines, le père des presses. Un singulier tas de choses qu’il a dû voir passer entre ses mains depuis le temps : notices nécrologiques, pubs de pubs, discours, procédures de divorces, noyés repêchés. Approche le bout du rouleau maintenant. Bonhomme sobre et sérieux avec un petit bout de laine je dirais. Femme bonne cuisinière, lavandière. Fille à la machine dans le salon. Jeanne-la-carrée, ça file droit.


ET C’ÉTAIT LA FÊTE DE PÂQUES


Il arrêta son élan pour observer un typographe qui disposait avec précision ses caractères. Les lit d’abord à l’envers. Et fait ça presto. Doit demander de l’entrainement, ça. mangiD kcirtaP. Pauvre papa avec son Haggada qu’il me lisait de droite à gauche en pointant l’index. Pessa’h. L’année prochaine à Jérusalem. Oh, mon dieu ! Mon dieu ! Et toute cette histoire sans queue ni tête qui nous a sorti de la terre d’Égypte pour entrer dans la maison de servitude *alléluia*. *Shema Israel adonai elohenu*. Non, c’est l’autre. Puis les douze frères, les fils de Jacob. Et puis l’agneau et le chat et le chien et le bâton et l’eau et le boucher. Et puis l’ange de la mort tue le boucher et il tue le bœuf et le chien tue le chat. Ça paraît un peu idiot tant qu’on y regarde pas de plus près. La justice, voilà ce que ça signifie mais c’est chacun mange son prochain. C’est ça la vie après tout. Comme il va vite en besogne. C’est en forgeant qu’on devient forgeron. C’est comme s’il voyait avec les doigts.

M. Bloom reprit sa marche et échappa aux trépidations par la galerie qui menait au palier. Maintenant est-ce que je me fais tout ça en tram pour le trouver peut-être. Mieux vaut téléphoner avant. Numéro ? Oui. Le même que la maison de Citron. Vingt-huit. Vingt-huit double quatre.


MAIS UNE FOIS ENCORE CE SAVON


Il prit la cage d’escalier. Qui diable a gribouillé tous ces murs avec des allumettes. On dirait un pari. Toujours une lourde odeur à cambouis dans ces ateliers. La colle tiédasse de la porte à côté quand j’étais chez Thom.

Il sortit son mouchoir et se tamponna le nez. Citronlemon ? Ah, le savon que j’ai mis là. Un coup à le perdre cette poche. Replaçant son mouchoir il sortit le savon et le garda, boutonné, dans la poche revolver de son pantalon.

Quel parfum met votre femme ? J’ai encore le temps de passer à la maison : tram : oublié quelque-chose. Un petit coup d’œil : avant : la toilette. Non. Ici. Non.

Un rire strident vola soudain des bureaux du *Télégraphe du soir*. Connais cette voix. Quoi de neuf ? Passe juste téléphoner. Ned Lambert c’est.

Il entra sans bruit.


ERIN, VERTE GEMME DE LA MER D’ARGENT


– Passe le fantôme, annonça le professeur MacHugh à la fenêtre poussiéreuse dans un murmure doux et biscuité.

M. Dedalus, fixant des yeux le visage persifleur de Ned Lambert depuis l’âtre vide, lui demanda aigrement :

– Christ-sur-sa-croix, n’en as-tu pas des hémorroïdes au gosier ?

Ned Lambert, assis sur la table, continua sa lecture :

– *Ou encore, notez les méandres de quelque ruisselet tourbillonnant qui babille en chemin, cherchant toutefois querelle aux obstacles rocheux, vers les eaux creusées du bleu domaine de Neptune, entre les rives moussues, rafraichi par les plus doux zéphyrs, jouet ici du soleil glorieux, là protégé son sein pensif des faîtes feuillus des géants des forêts*. Qu’en dis-tu Simon ? demanda-t-il par-dessus la frange de son journal. C’est de la bonne came, ou quoi ?

– Il a changé de boisson, dit M. Dedalus.

Ned Lambert, hilare, frappa ses genoux du journal, répétant :

– *Son sein pensif des fesses feuillues*. Ah mes enfants ! Mes enfants !

– Et Xénophon regardait Marathon, dit M. Dedalus, qui posa à nouveau son regard sur la cheminée, puis la fenêtre, et Marathon regardait la mer.

– ça suffira, s’écria le professeur MacHugh depuis la fenêtre. Je ne veux pas en entendre davantage.

Il dépêcha le quart-de-lune de biscuit sec qu’il venait grignotant et, mis en appétit, se disposa à attaquer celui qu’il tenait dans l’autre main.

Came ronflante. Baudruches. Ned Lambert a posé un jour je vois. Ça vous met le jour en l’air, un enterrement c.a.d. Il a de l’influence, à ce qu’on dit. Le vieux Chatterton, le vice-chancelier, c’est son grand-oncle ou son arrière-grand-oncle. Pas loin de quatre-vingt-dix ans dit-on. Colonne nécrologique écrite il y a une paye probable. S’accroche pour leur faire les pieds. Pourrait bien y passer en premier, lui-même. Johnny, laisse la place à ton oncle. Le très honorable Eyre Chatterton. Parie qu’il lui signe un chèque bancal ou deux les jours de mauvais vent. Ce sera l’avalanche quand il clamse. Alléluia.

– Juste un dernier spasme, dit Ned Lambert.

– Qu’est-ce que c’est demanda M. Bloom.

– Un fragment de Cicéron récemment découvert, répondit le professeur MacHugh avec pompe. *Notre belle patrie*.


BREF ET SANS DÉTOUR


– La patrie de qui ? dit simplement M. Bloom.

– Question des plus pertinentes, dit le professeur pausant sa mastication. Emphase sur de qui.

– La patrie de Dan Dawson, dit M. Dedalis.

– C’est son discours d’hier ?

Ned Lambert hocha du chef.

– Mais écoutez-moi ça, dit-il.

le bouton de porte, poussé de l’extérieur, vint heurter M. Bloom à la hauteur des reins.

– Excusez-moi, dit J. J. O'Molloy en entrant.

M. Bloom s’écarta prestement.

– C’est moi, dit-il.

– Bonjour, Jack.

– Passe, passe.

– Bonjour.

– Comment allez-vous, Dedalus ?

– Bien. Et vous-même ?

J. J. O'Molloy secoua la tête.


TRISTE


L’espoir du barreau c’était. Poitrinaire, pauvre gars. Ces accès de rougeurs, pour un homme, ça veut dire : the end. Sur le fil. Quel vent l’amène, je me demande. Souci d’argent.

– *Ou encore, si seulement nous gravissons des montagnes les pics crénelés.*

– Vous avez une mine superbe.

– Le chef est-il visible ? J. J. O'Molloy, l’œil sur la porte du fond.

– Pleinement, dit le professeur MacHugh. Visible et même audible. Il est dans le saint-des-saints avec Lenehan.

J. J. O'Molloy avança sans hâte vers le pupitre et se mit à feuilleter les pages roses de la collection.

Clientèle qui fond. Un futur antérieur. Perd courage. Joue. Dettes d’honneur. Récolte la tempête. Il recevait de belles avances de D. et T. Fitzgerald. Leurs perruques pour montrer leur matière grise. La cervelle à l’affiche comme la statue à Glasnevin. Me semble qu’il fait quelques travaux littéraires avec Gabriel Conroy pour l’*Express*. Type cultivé. Myles Crawford a commencé à l’*Indépendant*. Marrant la manière dont ces gens de la presse virent de bord dès qu’ils ont vent d’un nouveau poste. Girouettes. Soufflent le froid et le chaud. Lequel est vrai ? Une fable chasse l’autre. Se tirent à boulets rouges dans les journaux, puis tout ça se dissipe. Copains comme cochon l’instant d’après.

– Ah, écoutez-moi ça, pour l’amour de Dieu, supplia Ned Lambert. *Ou encore, si seulement nous gravissons des montagnes les pics crénelés…*

– Du vent ! interrompit rudement le professeur. Assez de cette outre boursouflée !

– *Pics*, continua Ned Lambert, *qui de hauteur en hauteur s’élèvent, comme pour laver nos âmes…*

– Qu’il aille donc se laver la bouche, dit M. Dedalus. Dieu tout puissant ! Allo ? On le soigne pour ça ?

– *Nos âmes dans le panorama sans pair de l’album d’Irlande, inégalé, en dépit des prototypes couverts de lauriers d’autres régions vantées et primées, précisément, pour leur beauté, de vallées boisées et de plaines ondulées et de vertes pâtures luxuriantes et printanières, plongées dans l’éclat transcendant et translucide de notre doux et mystérieux crépuscule…*


EN DORIQUE DANS LE TEXTE


– La lune, dit le professeur MacHugh. il a oublié Hamlet.

– *Qui enveloppe la vista d’horizon à horizon et attend que l’orbe embrasé de la lune vienne éclatant irradier sa splendeur argentine…*

– Oh ! s’écria M. Dedalus, laissant s’échapper un grognement de désespoir. Quelle poêlée d’étrons ! Ça ira, Ned. La vie est trop courte.

Il ôta son haut-de-forme, souffla son impatience sur sa moustache fournie, se peigna le chef de ses doigts ouverts.

Ned Lambert jeta le journal de côté, de joie gloussant. L’instant d’après un croassement de rire rauque vint secouer les lunettes et le visage mal rasé du professeur MacHugh.

– Le pétrin ! s’écria-t-il.


CE QUE DISAIT WETHERUP


Bien joli de railler ça maintenant à froid mais ça vend comme des petits pains ce truc. Et n’était-il dans la boulange ? Pour ça qu’on l’appelle le Pétrin. Mais il a bien fait son nid en tout cas. Fille fiancée à ce gars du fisc qui a un engin. Joliment ferré le poisson. Divertissements. Table ouverte. Gros gueuletons. Wetherup disait toujours ça. Faut les attraper par l’estomac.

La porte du fond s’ouvrit violemment sur une tête cramoisie et pincée qui, surmontée d’une crête de cheveux duvetés, se propulsa en avant. Les yeux bleus et sûr d’eux regardaient sans détour et la voix dure questionna :

– Qu’est-ce que c’est ?

– Mais voilà qu’arrive Messire de la Pacotille en personne ! dit le professeur MacHugh avec emphase.

– Foulez-moi la paix, bougre de vieux pédagogue ! dit le rédacteur en chef en écho.

– Viens, Ned, dit M. Dedalus en mettant son chapeau. Il me faut un verre après ça.

– Un verre ! s’écria le rédacteur-en-chef. On ne sert pas avant la messe.

– Et c’est bien ainsi, dit M. Dedalus sur le départ. Amène-toi Ned.

Ned Lambert se laissa glisser de la table. Les yeux bleus du rédacteur-en-chef vaguaient, tournés vers le visage de M. Bloom, un sourire en coin.

– Vous en êtes, Myles ? demanda Ned Lambert.


RÉMINISCENCE DE MÉMORABLES BATAILLES


– La milice de Cork Nord ! s’écria le rédacteur-en-chef, avançant au pas de charge vers la cheminée. Nous gagnions à chaque fois ! Cork Nord et officiers espagnols !

– Où était-ce, Myles ? demanda Ned Lambert qui regardait d’un air pensif la pointe de ses souliers.

– en Ohio ! gueula le rédacteur-en-chef.

– Pardi oui, approuva Ned Lambert.

En se faufilant il glissa à J. J. O'Molloy :

– Début de Parkinson. Bien triste.

– Ohio ! croassa le rédacteur-en-chef, voix haute perchée et visage écarlate tourné vers le plafond. Mon Ohio !

– Un crétique parfait ! dit le professeur. Long, court, long.


Ô, HARPE ÉOLIENNE !


Il prit une bobine de fil dentaire de sa poche de veston, en cassa un bout et le fit résonner avec adresse entre deux et deux de ses dents creuses et mal lavées.

– Bingbang, bangbang.

M. Bloom, voyant le terrain libre, poussa vers la porte du fond.

– Une petite minute, M. Crawford, dit-il. Je vais juste passer un coup de fil pour une annonce.

Et il entra.

– Qu’en est-il de l’éditorial du soir ? demanda le professeur MacHugh, qui s’approcha du rédacteur-en-chef et lui mit une main ferme sur l’épaule.

– Ça va bien le faire, dit Myles Crawford plus calmement. Pas de mauvais sang. Hello, Jack. Ça va le faire.

– Le bonjour Myles, dit J. J. O'Molloy, qui laissa les pages qu’il tenait en main retomber mollement dans leur dossier. Est-ce que cette escroquerie canadienne passe aujourd’hui ?

Le téléphone ronronnait à l’intérieur.

– Vingt-huit… Non, vingt… Double quatre… Oui.


TROUVEZ LE GAGNANT


Lenehan sortit du bureau du fond avec les papillons du *Sport*.

– Qui veut un tuyau d’acier pour la Coupe d’Or ? Sceptre monté par O. Madden.

Il balança les papillons sur la table.

Les hurlements des livreurs de journaux, pieds nus dans le hall, passèrent précipitamment la porte qui s’ouvrit brusquement.

– Chut, dit Lenehan. J’entends des pieds en venir aux mains.

Le professeur traversa la pièce à grandes enjambées et attrapa par le col un gamin pleurnichant pendant que les autres vidaient le hall et dévalaient ventre à terre les escaliers. Les papillons frémirent sous le courant d’air, flottèrent hésitants quelques gribouillages bleus qui vinrent atterrir sous la table.

– C’est pas moi, monsieur. C’est le grand qui m’a poussé, monsieur.

– Flanquez-le dehors et fermez la porte, dit le rédacteur-en-chef. Il y a un ouragan ici.

Lenehan se mit à repêcher les papillons du plancher, grognant alors qu’il se courbait, par deux fois.

– On attendait l’édition spéciale course, monsieur, dit le livreur. C’est Pat Farrell qui m’a poussé monsieur.

Il pointa du doigt deux têtes qui lorgnaient depuis le chambranle.

– Lui, monsieur.

– Fous-moi le camp, dit le professeur MacHugh, bourru.

Il poussa le garçon dehors et claqua la porte à sa suite.

J. J. O'Molloy feuilletait les dossiers qui gémissaient sous ses doigts, murmurant dans sa recherche :

– Continué sur la page six, colonne quatre.

– Oui, ici le *Télégraphe du Soir*, disait au combiné M. Bloom dans le bureau. Est-ce que le patron…? Oui, le *Télégraphe*… Parti où ? Aha ! À quelle vente ? … Aha ! Je vois… bien. Je l’y trouverai.


UNE COLLISION S’ENSUIT


Le timbre se remit à bruire quand il raccrocha. Il entra en coup de vent et vint heurter Lenehan, encore aux mains avec le deuxième papillon.

– *Sorry, sir*, dit Lenehan qui s’agrippa à lui avec une grimace.

– C’est ma faute, dit Bloom, attrapé. Je vous ai fait mal ? Je suis pressé.

– Genou, dit Lenehan.

Il fit une grimace comique et geignit, frottant son genou :

– Le poids des *anno Domini*.

– Désolé, dit M. Bloom.

Il gagna la porte, l’ouvrit à demi et s’arrêta. J. J. O'Molloy referma sèchement le lourd dossier. Un bruit, deux voix perçantes, un harmonica, résonnait dans le hall nu où les livreurs de journaux attendaient accroupis, appuyés aux portes.

>*C’est nous les gars de Wexford,*  
*On s’est battu âmes et corps*


EXIT BLOOM


– Je fais juste un saut jusqu’à la promenade du Bachelier, pour cette annonce de Cleys. Veut l’arranger. On me dit qu’il est là-bas, chez Dillon.

Il les dévisageait, indécis. Le rédacteur-en-chef qui, adossé à la cheminée, tenait appuyée sa tête sur une main, étendit un bras d’un geste ample et soudain.

– Partez ! Le monde vous appartient.

– J’en ai pour un rien de temps, dit M. Bloom, et il fila.

J. J. O'Molloy prit les papillons des mains de Lenehan et se mit à les lire, soufflant doucement dessus pour les décoller, en silence.

– Il va l’avoir cette annonce, dit le professeur qui, derrière les ronds noirs de ses lunettes observait par-dessus les volets montants. Regardez ces petits fripons lui courir après.

– Faites voir. Où ? s’écria Lenegan, accourant à la fenêtre.


UN CORTÈGE DE RUE


Tous deux souriaient par-dessus le volet à la vue des livreurs qui filaient M. Bloom, un cerf-volant moqueur ondoyant blanc dans la brise, sa traîne de rubans blancs.

– Regardez ce titi derrière lui, à cor et à cri, dit Lenehan, c’est à crever. Mes côtes me coûtent ! Pile la dégaine et la patte plate. Un petit quarante-trois. Le pas fuyant.

Il se lança dans une mazurka, caricature mordante, et glissait sur le plancher, passait la cheminée pour arriver devant J. J. O'Molloy qui déposa les papillons dans ses mains ouvertes.

– Qu’est-ce que c’est ? dit Myles Crawford dans un sursaut. Où sont parti les autres ?

– Qui ? dit le professeur en se retournant. Ils sont partis boire un coup à l’Ovale. Paddy Hooper y est avec Jack Hall. Arrivés hier au soir.

– Eh bien, allons-y, dit Myles Crawford. Où est mon chapeau ?

Il regagna d’un pas saccadé le bureau du fond, écartant les volets de son veston, faisant tinter ses clés dans sa poche arrière. Elles carillonnèrent à l’air libre et encore contre le bois quand il ferma le tiroir du secrétaire.

– Il est bien parti, dit le professeur MacHugh à voix basse.

– On dirait, murmura J. J. O'Molloy, songeur, en sortant un étui à cigarette, mais l’apparence peut être trompeuse. Qui a le plus d’allumettes ?


LE CALUMET DE LA PAIX


Il offrit une cigarette au professeur et en prit une. Lenehan craqua promptement une allumette et donna du feu à l’un puis à l’autre. J. J. O'Molloy rouvrit l’étui et le lui présenta.

– *Merci you*, dit Lenehan, qui se servit.

Le rédacteur sortit du bureau, un chapeau de paille planté de guingois sur le front. Pointant un doigt sévère sur le professeur MacHugh, il déclama d’un ton chantant :

> *C’est le rang et la gloire qui t’ont séduit*  
*C’est l’empire qui a charmé ton cœur*

Le professeur grimaça un long sourire, les lèvres pincées.

– Hein ? Vieux débris de l’empire romain ? dit Myles Crawford.

Il prit une cigarette de l’étui ouvert. Lenahan lui présenta le feu avec preste prestance, et dit :

– Faites silence pour ma devinette du jour !

– *Imperium romanum*, dit doucement J. J. O'Molloy. Cela a une touche plus noble que Britannique ou Brixton. D’une certaine manière, ça vous fait penser à de l’huile versée sur un feu.

Myles Crawford expulsa sa première taf au plafond, violemment.

– C’est ça, dit-il. Nous sommes l’huile. Vous et moi sommes l’huile sur le feu. Autant dire un flocon en enfer, question chance.


LA GRANDEUR QUE FUT ROME


– Halte-là, dit le professeur MacHugh, élevant deux serres tranquilles. Ne nous laissons pas emporter par les mots, par la sonorité des mots. Nous pensons Rome, impériale, impérieuse, impérative.

Il étendit posément deux bras rhétoriques hors de leurs manchettes sales et élimées, puis continua :

– Qu’était leur civilisation ? Vaste, je l’admets : mais vile. Collecteurs : égouts. Les Juifs, dans le désert, en haut de la montagne, dirent : *qu’il est bon d’être ici. Érigeons un autel à Jéhovah*. Le Romain, comme l’Anglais qui a marché sur sa trace, n’a apporté dans son sillage, où qu’il posât le pied (et chez nous il ne l’a pas posé) que son obsession des égouts. Il observait alentours engoncé dans sa toge, puis disait : *qu’il est bon d’être ici. Érigeons des cabinets*.

– Ce qu’ils ont fait par voie de conséquence, dit Lenehan. Nos antiques anciens ancêtres, comme on peut le lire dans le premier chapitre du livre de Guinness, avaient un faible pour la liberté de miction.

– Des gentilshommes de la nature, murmura J. J. O'Molloy. Mais nous avons aussi la loi Romaine.

– Et Ponce Pilate est son prophète, répondit le professeur MacHugh.

– Vous connaissez cette histoire à propos du grand chancelier Palles ? demanda J. J. O'Molloy. C’était au diner de l’université royale. Tout marchait comme sur des roulettes…

– D’abord ma devinette, dit Lenehan. Vous êtes prêts ?

M. O’Madden Burke, grand dans son complet trop grand en tweed gris de Donegal, fit son entrée par la porte du hall. Stephen Dedalus, à sa suite, se découvrit sur le seuil.

– *Come in, mi lads !* s’écria Lenehan.

– J’escorte un suppliant dit M. O’Madden Burke d’un ton mélodieux. La Jeunesse guidée par l’Expérience rend visite à la Notoriété.

– Comment allez-vous ? dit le rédacteur-en-chef, la main tendue. Entrez. Votre paternel vient de sortir.


 ? ? ?

Lenehan s’adressa à la ronde :

– Silence ! Quel opéra ressemble à une mante andalouse ? Réfléchissez, soupesez, cogitez, répondez.

Stephen remit les feuillets dactylographiés, indiquant du doigt titre et signature.

– Qui ça ? demanda le rédacteur-en-chef.

Bout déchiré.

– M. Garret Deasy, dit Stephen.

– Ce vieux sac-de-bile, dit le rédacteur-en-chef. Qui a déchiré ça ? Il était à court ?

>*La voile oriflamme, d’ire*  
*Du Sud orageux, sa couche*  
*il vient, pâle vampire,*  
*à ma bouche sa bouche*  

– Bonjour, Stephen, dit le professeur, qui s’approchait pour regarder par-dessus leurs épaules. Fièvre aphteuse ? Seriez-vous devenu… ?

Barde kiffe-les-bœufs.


SCANDALE DANS UN RESTAURANT À LA MODE


– Bonjour Monsieur, répondit Stephen, virant pivoine. La lettre n’est pas de moi. M. Garrett Deasy m’a demandé de…

– Oh, je le connais, dit Myles Crawford, et je connaissais sa femme aussi. Foutue vieille harpie, la pire qui ait jamais vu le jour. Christ ! La fièvre aphteuse elle l’avait, pas d’erreur ! Le soir où elle a balancé le potage à la tête du garçon au Star and Garter. Oho !

Une femme apporta le péché en ce monde. Hélène la fugitive, femme de Ménélas, les Grecs dix ans. O’Rourke, prince de Breifne.

– Est-il veuf ? demanda Stephen.

– Ouais, par coûtumace, dit Myles Crawford, dont l’œil parcourait la copie. Chevaux de l’empereur. Habsbourg. Un Irlandais lui sauva la vie sur les remparts de Vienne. Ne l’oubliez pas ! Maximilian Karl O’Donnell, graf von Tirconnell en Irlande. Envoyé son fils pour en faire le roi un maréchal de campagne autrichien aujourd’hui. Causera du grabuge un jour. Oies sauvages. Oh, oui, à tous les coups. Ne l’oubliez pas !

– Mais la question en suspens c’est lui l’a-t-il oublié, dit J. J. Molloy d’une voix basse, en retournant un presse-papier fer-à-cheval. Sauver les princes, c’est du bénévolat.

Le professeur MacHugh se retourna sur lui :

– Et sinon quoi ? dit-il.

– Laissez-moi vous dire comment ça s’est passé, commença Mules Crawford. Il y avait un jour un Hongrois…


CAUSES PERDUES


ON MENTIONNE UN NOBLE MARQUIS


– Notre loyauté fut toujours acquise aux causes perdues, dit le professeur. Le succès est pour nous la mort de l’intellect et de l’imagination. Nous n’avons jamais été loyaux envers ceux qui réussissent. Nous les servons. J’enseigne ce langage sans vergogne, latin. Je parle la langue d’une race dont la mentalité atteint son acmé dans la devise : le temps c’est de l’argent. Domination du matériel. *Dominus* ! Seigneur ! Où est le spirituel ? Seigneur Jésus ? Seigneur Salisbury ? Un divan dans un club du West End. Mais les Grecs !


KYRIE ELEISON !

Une lueur perça et fit sourire ses yeux derrière leurs ronds noirs, allongea encore ses lèvres sinueuses.

– Les Grecs ! répéta-t-il. *Kyrios* ! Mot de lumière ! Les voyelles que le Sémite et le Saxon ignorent. *Kyrie* ! La brillance de l’intellect. C’est le Grec que je devrais professer, le langage de l’esprit. *Kyrie Eleison* ! Les fabricants de cabinets et les constructeurs d’égouts ne seront jamais seigneurs et maitres de notre âme. Nous sommes des hommes-liges de la chevalerie catholique européenne, qui a sombré à Trafalgar et de l’empire de l’esprit, pas d’un *imperium*, qui a coulé par le fond avec les flottes athéniennes à Aigos Potamos. Oui, oui. Coulées par le fond. Pyrrhus, égaré par un oracle, fit une dernière tentative pour relever la fortune de la Grèce. Loyal à une cause perdue.

Se détournant d’eux, il s’élança vers la fenêtre.

– Ils avançaient en ordre de bataille, dit M. O’Madden Burke d’une voix grise, mais toujours tombaient.

– Bouhou ! pleurnicha Lenehan. Tout ça à cause d’une brique reçue pendant le *money time*. Pauvre, pauvre, pauvre Pyrrhus !





