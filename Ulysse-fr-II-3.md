Martin Cunningham, le premier, enfonça sa tête en haut-de-forme dans la voiture grinçante et, s’y étant introduit avec agilité, s’assit. M. Powell l’y suivit, courbant l’échine avec précaution.

– À ton tour, Simon.

– Après vous, ajouta M. Bloom

M. Dedalus se couvrit le chef aussitôt et monta en disant :

Oui, oui.

– On est tous là maintenant ? demanda Martin Cunningham. Amenez-vous, Bloom.

M.Bloom passa et prit la place vacante. Il tira la portière à lui et la claqua deux fois, qu’elle fût bien fermée. Il passa un bras dans une sangle et se mit à observer avec componction du carreau de la voiture les stores baissés de l’avenue. Un se souleva de côté : une vieille femme aux aguets. Nez blanc aplati contre la vitre. Remercie son étoile qu’on ait passé son tour. Extraordinaire cet intérêt qu’elles portent au cadavre. Heureuses de nous voir partir on leur donne une telle peine à l’arrivée. Besogne qui leur va bien. Chu-chuintements dans les coins. Chaussons qui chouinent de peur de le réveiller. Puis elles s’affairent. Tout bien ficelé. Molly et Mme Fleming qui font le lit. Tire plus de ton côté. Notre linceul. On ne sait jamais qui vous touchera une fois mort. Toilette et shampoing. Il me semble qu’ils coupent les ongles et les cheveux. En gardent un peu dans une enveloppe. Continue de croître après. Pas propre, ce boulot.

Tous attendaient. Sans mot dire. On charge les couronnes, probablement. Je suis assis sur un truc dur. Ah, le savon ! Dans ma poche de derrière. Vaudrait mieux l’ôter de là. Attendre l’occasion.

Tous attendaient. Puis on entendit les roues de devant, qui tournaient : puis plus près : puis les sabots des chevaux. Une saccade. La voiture se mit à bouger, grincer, tanguer. D’autres sabots et d’autres roues qui grincent, derrière. Les stores de l’avenue défilèrent et le numéro neuf avec son heurtoir crêpé, la porte qui baillait.
Au pas.

Ils attendaient immobiles, les genoux secoués, jusqu’à ce qu’ils eussent tourné pour longer les rails du tramway. Route de Tritonville. Au trot. Les roues tressautaient sur les pavés de la chaussée et les vitres démentes ébranlaient leurs cadres.

– Par où nous amène-t-il ? demanda M. Power, interrogeant les deux portières.

– Irishtown, dit Martin Cunningham. Ringsend. Rue de Brunswick.

M. Dedalus hocha la tête, jetant un regard à l’extérieur.

– C’est une belle et vieille coutume, fit-il. Je suis heureux de voir qu’on ne l’a pas abandonnée.

Pendant un moment, chacun regarda par son carreau casquettes et chapeaux que soulevaient les passants. Respect. La voiture vira, laissant les voies de tram pour la route plus égale après la traverse Watery. M. Bloom à l’affût aperçut un mince jeune homme, en habits de deuil, chapeau large.

– Voilà qu’on dépasse un de vos amis, Dedalus, annonça-t-il.

– Qui ça ?

– Votre fils et héritier.

– Où est-il ? dit M. Dedalus, s’étirant vers la portière opposée.

La voiture passa les égouts ouverts et la chaussée défoncée devant les immeubles de misère, braqua au coin d’une embardée et, virant à nouveau dans les rails du tramway, continua dans le fracas de ses roues caquetantes.
Retombant à sa place, Dedalus demanda :

– Ce voyou de Mulligan était-il avec lui ? Son *fidus achates* !

– Non, répondit Bloom. Il était seul.

– En visite chez sa tante Sarah, je suppose, continua M. Dedalus. La bande à Goulding, le petit ivrogne de comptable et Crissie, la petite bouse à son papa, sage cette enfant qui connaît son propre père.

M. Bloom souriait sans joie sur la route de Ringsend. Wallace Bros : fabrication de bouteilles : Pont Dodger.

Richie Goulding et la sacoche de l’étude. Goulding, Collis et Wards, il appelle la boîte. Ses blagues sentent un peu le rassi. Un sacré coup c’était. La valse rue Stamer, un dimanche matin, les deux chapeaux de sa logeuse empilés sur le crâne et Ignatius Gallaher dans les bras. En virée jusqu’au bout de la nuit. Ça commence à se voir maintenant : ce mal au dos qu’il a, j’ai bien peur. Sa femme qui lui applique le fer à repasser. Pense qu’il en guérira avec des pilules. De la mie de pain et rien d’autre. Profit : dans les six cents pour cent.

– Il traîne avec la chienlit, grognait M. Dedalus, Ce Mulligan est une foutue pourriture fripouille avérée par où qu’on le prenne. La lie de tout Dublin. Mais que Dieu et sa sainte mère m’assistent j’en fais mon affaire j’écrirai une lettre un de ces jours à sa mère à sa tante ou qui que ce soit qui lui ouvrira les yeux grands comme une porte cochère. Je vais lui chatouiller la catastrophe, croyez-moi bien.

Il s’écria par-dessus le vacarme des roues :

– Je ne permettrai pas que son bâtard de neveu ruine mon fils. Le fils d’un commis de comptoir. Vendait des rubans chez mon cousin, Peter Paul M’Swiney. Pas une chance.

Il se tut. L’œil de M. Bloom passa de sa moustache furieuse à la face bénigne de M. Power puis aux yeux et à la barbe de Martin Cunningham qui oscillaient gravement. Type bruyant et obstiné. Tout plein de son fils. Il a raison. Quelque-chose à transmettre. Si le petit Rudy avait vécu. Le voir grandir. Entendre sa voix dans la maison. Marcher aux côtés de Molly dans un costume d’Eton. Mon fils. Moi dans ses yeux. Quelle étrange sensation ce serait. Issu de moi. Une seule chance. Ça a dû être ce matin-là Terrasse Raymond elle était à la fenêtre à regarder les deux chiens qui s’y mettaient devant le mur de la Cessez-de-faire-le-mal. Le sergent qui levait le nez en ricanant. Elle avait cette robe de chambre crème avec cet accroc qu’elle n’a jamais raccommodé. Un peu de tendresse, Poldy. Bon dieu, j’en meurs d’envie. Et la vie commence.

Puis elle a enflé. A dû refuser le concert à Greystones. Mon fils en elle. J’aurais pu l’aider dans la vie. J’aurais pu. En faire quelqu’un d’indépendant. Apprendre l’allemand aussi.

– Est-on en retard ? demanda M. Powell.

– Dix minutes, dit Martin Cunningham en regardant sa montre.

Molly. Milly. La même chose, diluée. Ses jurons de garçon manqué. Fortiche ! Mais c’est chelou ! Mais c’est une gentille fille. Une femme bientôt. Mullingar. Papounet chéri. Jeune étudiant. Oui, oui : une femme aussi. La vie, la vie.

La voiture tanguait d’un bord sur l’autre, entraînant les quatre bustes.

– Corny aurait pu nous trouver un carrosse plus commode, dit M. Power.

Il aurait pu, dit M. Dedalus, s’il n’avait ce problème de strabisme divergeant, si tu vois ce que je veux dire ?

Il ferma son œil gauche. Martin Cunningham passa la main sous ses cuisses pour en chasser des miettes de pain.

– Qu’est-ce que c’est que ça, bon Dieu ? dit-il, des miettes ?

– Il semble qu’on ait pique-niqué ici dernièrement, dit M. Power.

Ils soulevèrent leurs cuisses en chœur et observèrent avec réprobation le cuir moisissant, aux boutons absents, des sièges. M. Dedalus, nez et sourcil froncés, abaissa le regard et dit :

– Ou je me trompe fort, ou. Qu’est-ce que tu en penses, Martin ?

– Cela m’a frappé, moi aussi, dit Martin Cunningham.

M. Bloom reposa sa cuisse. Content d’avoir pris ce bain. Sens mes pieds bien propres. Mais dommage que Mme Fleming n’ait pas mieux raccommodé ces chaussettes.

M. Dedalus soupira résigné :

– Après tout, dit-il, c’est la chose la plus naturelle du monde.

– Tom Kernan s’est pointé ? demanda Martin Cunningham en tortillant doucement sa barbe.

– Oui, répondit M. Bloom. Il est derrière avec Ned Lambert et Hynes.

– Et Corny kelleher lui-même ? demanda M. Powell.

– Au cimetière, dit Martin Cunningham.

– Je suis tombé sur M’Coy ce matin, dit M. Bloom. Il a dit qu’il essaierait de venir.

La voiture s’arrêta net.

– Quoi maintenant ?

– Nous sommes arrêtés.

– Où sommes-nous ?

M. Bloom passa la tête par la fenêtre.

– Au grand canal, dit-il.

Usine à gaz. La coqueluche, il paraît que ça guérit. Bonne affaire que Milly ne l’ait jamais attrapée. Pauvres enfants ! Pliés en deux, bleus noirs, les convulsions. Bien triste. S’en est sortie plutôt bien question maladies, en comparaison. La rougeole et c’est tout. Tisane de graines de lin. Épidémies de scarlatine, grippe. Porte-à-porte de la mort. Ne ratez pas l’occasion. Refuge pour chiens par là. Pauvre vieil Athos ! Sois bon avec Athos, Leopold, c’est mon dernier vœu. Que ta volonté soit faite. Nous leur obéissons, même dans le tombeau. Gribouillage de mourant. Il n’a pas pu s’y faire, s’est laissé dépérir. Une bête tranquille. Les chiens des vieillards le sont souvent.

Une goutte de pluie s’écrasa sur son chapeau. Il rentra la tête et vit l’averse brusque piqueter les pavés gris. Espacées. Curieux. Comme une passoire. Je m’y attendais. Mes souliers craquaient je me rappelle maintenant.

– Changement de temps, dit-il placide.

– Fâcheux que ça ne soit pas resté au beau, dit Martin Cunningham.

– La campagne en a besoin, dit M. Power. Voilà le soleil qui revient.

M. Dedalus, de derrière ses verres leva son regard sur le soleil voilé, et lança une imprécation muette au ciel.

– Aussi peu sûr qu’un derrière d’enfant, dit-il.

– On est reparti.

La voiture se remit à tourner sur ses roues raides et leurs bustes à se balancer doucement. Martin Cunningham tortillait plus vite la pointe de sa barbe.

– Tom Kernan était immense la nuit dernière, dit-il. Et Paddy Leonard qui le singeait ouvertement.

– Oh, refais-le, dit M. Power avec ardeur. Attends d’avoir écouté ça, Simon, le bout sur Ben Dollard qui chante Le Jeune Rebelle.

– Immense, dit Martin Cunningham pompeusement. Son rendu de cette simple ballade, Martin, est la plus incisive interprétation qu’il m’a été donné d’entendre au cours de ma longue expérience.

– Incisif, dit M. Powell en riant. Il n’a que ce mot à la bouche. Ça et l’arrangement rétrospectif.

– As-tu lu le discours de Dan Dawson ? demanda Martin Cunningham.

– Ma foi non, dit M. Dedalus. Où se trouve-t-il ?

– Dans le journal du matin.

M. Bloom sortit le journal de sa poche intérieur. Ce livre que je dois lui échanger.

– Non, non, dit promptement M. Dedalus. Plus tard je vous prie.

Le regard de M. Bloom parcourut la marge du journal, passant les morts en revue : Callan, Coleman, Dignam, Fawcett, Lowry, Naumann, Peake, c’est quel Peake ? Le gars qui était chez Crosbie et Alleyne ? non, Sexton, Urbright. L’encre des caractères s’efface vite sur le papier fripé cassant. Merci à la Petite Fleur. Perte cruelle. À l’indicible douleur de ses. Âgé de 88 après une longue et pénible maladie. Messe de quarantaine : Quinlan. Que le doux Jésus l’ait en sa miséricorde.

>*Il y a juste un mois que notre cher Henri  
dans les cieux d’en haut demeure a pris.  
Les pleurs de sa famille sonnent haut,  
dans l’espoir de le revoir un jour là-haut.*

J’ai déchiré l’enveloppe ? Oui. Où ai-je mis la lettre après l’avoir lu au bain ? Il palpa sa poche de veston. Bien là. Cher Henri a pris demeure. Ma patience s’épuise.

École publique. Chantiers Meade. La station. N’y en a plus que deux. Hochant la. Pleines comme une tique. Plus d’os que de cervelle. L’autre trotte avec un client. Il y a une heure que j’y suis passé. Les cochers soulevèrent leur chapeau.

Le dos d’un aiguilleur se redressa brusquement contre un pylône du tramway, non loin de la fenêtre de M. Bloom. Ils ne pourraient pas inventer un truc automatique pour que la roue toute seule plus commode ? Mais alors ce gars perdrait son boulot ? Oui mais alors un autre gars aurait le boulot d’inventer ça ?

Salles de concert d’Antient. Rien à l’affiche. Un homme en complet beige avec un brassard de crêpe. Chagrin modéré. Quart de deuil. Belle-famille peut-être.

Ils dépassèrent la chaire lugubre de Saint-Marc, passèrent sous le pont du chemin de fer, dépassèrent le théâtre de la Reine : en silence. Placards : Eugene Stratton, Mme Bandmann Palmer. Pourrais-je aller voir Leah ce soir, je me demande. J’ai dit que j’. Ou le Lys de Killarney ? Troupe d’Opéra Elster Grimes. Une nouveauté qui détonne. Affiches humides et pimpantes pour la semaine prochaine. Le Bristol s’amuse. Martin Cunningham pourrait me dégotter une place pour la Gaîté. Contre un verre ou deux. Un prêté pour un rendu.

Il vient cet après-midi. Ce qu’elle chante.

Chez Plasto. Buste fontaine à la mémoire de Sir Philip Crampton. Qui était-ce ?

– Comment allez-vous ? dit Martin Cunningham, esquissant un salut militaire.

– Il ne nous voit pas, dit M. Power. Mais si. Comment allez-vous ?

– Qui ? demanda M. Dedalus.

– Braise Boylan, dit M. Power. Il est là à aérer son brushing.

Juste quand j’y pensais.

M. Dedalus salua du buste. De la porte des Côtes Rouges répondit l’éclat blanc d’un chapeau de paille : dépassé.

M. Bloom examinait les ongles de sa main gauche, puis passa à ceux de droite. Les ongles, oui. Y a-t-il autre chose en lui qu’ils elle voit ? Fascination. Le plus vilain bonhomme de Dublin. Ça le maintient en vie. Elles perçoivent parfois ce qu’est une personne. L’instinct. Mais un type de ce genre. Mes ongles. Je suis en train de les regarder : bien taillés. Et après : seule dans ses pensées. Le corps s’avachit un peu. Je m’en rends compte : souvenir d’antan. Qu’est ce qui cause ça ? La peau ne se contracte pas assez vite quand la chair part, je suppose. Mais la ligne reste. Épaules. Hanches. Rondeurs. S’habillant le soir du bal. Le fourreau de sa robe coincée dans la raie des fesses.

Il joignit les mains entre ses genoux et, satisfait, darda un regard absent sur leurs visages.

M. Power demanda :

– Ça prend, la tournée de concert, Bloom ?

– Oh, très bien, dit M. Bloom. Je n’en entends que du bien. C’est une bonne idée, vous savez…

– Vous en serez ?

– Eh bien, non, dit M. Bloom. Le fait est que j’ai une affaire personnelle à régler au comté de Clare. L’idée, c’est de faire le tour des chefs-lieu. Ce qu’on perd dans l’un on le rattrape dans l’autre.

– Très juste, dit Martin Cunningham. C’est ce que fait Mary Anderson en ce moment.

Vous avez de bons artistes ?

– Louis Werner s’occupe d’elle, dit M. Bloom. Oh oui, on aura le haut du panier. J.C. Doyle et John MacCormack j’espère et. Ce qu’il a de mieux, en fait.

– Et *madame*, dit M. Powell tout sourire. Last but not least.

Poliment, M. Bloom ouvrit doucement les mains, puis les referma. Smith O’Brien. Quelqu’un y a déposé une gerbe de fleurs. Femme. Doit être le jour de sa mort. Joyeux anniversaire. La voiture passa en cahotant le buste de Farrell et leurs genoux à la dérive communièrent sans bruit.

Cets : un vieux bonhomme grisâtre offrait sa marchandise sur le trottoir, la bouche ouverte : cets.

– Quatre lacets pour un penny.

Me demande pourquoi il a été radié. Avait ses bureaux rue Hume. Même maison que l’homonyme de Molly, Tweedy, procureur du roi sur Waterford. Il en a gardé ce haut-de-forme. Vestiges de son ancienne décence. En deuil lui aussi. Quelle descente aux enfers, pauvre bougre ! Chassé comme un chien galeux. O’Callahan, le bout du chemin.

Et *madame*. Onze heures vingt. Levée. Mme Fleming est là qui nettoie. À sa coiffure, fredonnant. *Voglio e non vorrei*. Non. *Vorrei e non*. Regarde les pointes de ses cheveux pour voir si ça fourche. *Mi trema un poco il*. Quelle voix magnifique elle trouve sur le *tre* : le ton d’un sanglot. Un rossignol. Une grive. Il y a un mot grive qui exprime ça.

Son regard effleura le visage agréable de M. Power. Grisonnant sur les tempes. *Madame* : sourire. J’ai rendu le sourire. Un sourire en dit long. Pure politesse peut-être. Brave type. Qui sait est-ce vrai cette femme qu’il entretient ? Pas gai pour l’épouse. Mais on dit, qui me l’a dit, il n’y a rien de charnel. T’imagines qu’ils en auraient vite fait le tour. Oui, c’était Crofton l’ai rencontré un soir qui lui amenait une livre de rumsteak. C’est quoi qu’elle faisait ? Serveuse au Jury. Ou c’était au Moira ?

Ils passèrent sous l’énorme manteau du Libérateur.

Martin Cunningham fit du coude à M. Power.

– De la tribu de Ruben, dit-il.

Une longue silhouette à barbe noire, penchée sur sa canne, tournait avec peine au coin de la maison de l’éléphant, chez Elvery, en leur montrant une main recourbée sur les lombaires.

– Dans sa beauté première et virginale, dit M. Power.

M. Dedalus suivit des yeux la silhouette branlante et dit doucement :

– Le diable t’étripe !

M. Power, pris d’un fou rire, dissimula son visage de la fenêtre, alors que la voiture passait la statue de Gray.

– Nous y sommes tous passés, dit M. Martin Cunningham à la ronde.

Ses yeux rencontrèrent ceux de M. Bloom. Il caressa sa barbe et ajouta :

– Enfin, presque tous.

Soudain volubile, M. Bloom se mit à parler aux nez de ses compagnons.

– C’en est une bien bonne, qui circule sur Ruben J et fils.

– Celle du batelier ?

– Oui. C’est tordant n’est-ce pas ?

– De quoi il retourne ? demanda M. Dedalus. Je n’en ai pas entendu parler.

– Il y a une histoire de fille, commença M. Bloom, et il a pris la décision de l’envoyer sur l’Île de Man en lieu sûr mais quand ils étaient tous les deux…

– Quoi ? demanda M. Dedalus. Cette espèce d’asperge ?

– Oui, dit M. Bloom. Ils étaient en route pour le bateau et il a tenté de se noyer…

– Coule, Barabbas ! s’écria M. Dedalus. Jésus ! S’il pouvait réussir !

M. Power laissa échapper un long rire, par-dessous la main qui cachait encore son nez.

– Non, dit M. Bloom. Le fils lui-même…

Martin Cunningham lui coupa grossièrement la parole :

– Ruben et le fiston s’amenaient sur le quai côté rivière pour prendre le bateau de l’Île de Man et ce petit sournois lui a filé entre les doigts et hop par-dessus le mur dans la Liffey.

– Mon Dieu ! s’exclama M. Dedalus pris d’effroi. Est-il mort ?

– Mort ! s’écria Martin Cunningham. Jamais de la vie ! Un batelier a attrapé une gaffe et l’a repêché par le fond de la culotte et il a atterri sur le quai pile sur le père plus mort que vif. La moitié de la ville était là…

– Oui, dit M. Bloom, et le plus drôle c’est…

– Et Ruben J, dit Martin Cunningham, a donné au batelier un florin pour avoir sauvé la peau de son fils.

Un soupir réprimé échappa à la main de M. Power.

– Oh, mais oui, affirma Martin Cunningham. Comme à un héros. Un florin d’argent.

– Elle est terrible, n’est-ce pas ? dit, empressé, M. Bloom.

– Un et huit pence de trop, dit, sec, M. Dedalus.

Le rire étranglé de M. Power éclata en silence dans la voiture.

La colonne Nelson.

– Un penny les huit prunes ! Un penny les huit !

– Nous ferions mieux d’avoir l’air un peu sérieux, dit Martin Cunningham.

M. Dedalus soupira.

– Ah mais tout de même, dit-il, le pauvre petit Paddy ne nous en voudrait pas pour un rire. Il en a raconté lui-même plus d’une bonne.

– Dieu me pardonne ! dit M. Power qui essuyait de ses doigts ses yeux humides. Pauvre Paddy ! Je ne pensais guère il y a une semaine la dernière fois que je l’ai vu et qu’il était dans son état de santé habituel que je roulerais ainsi à sa suite. Il nous a quittés.

– Plus décent que ce petit bonhomme, on ne trouve pas sous un chapeau, dit M. Dedalus. Il est parti si soudainement.

– Syncope, dit Martin Cunningham. Le cœur.

Il se tapota la poitrine tristement.

Face enflammée : rouge vif. Abus de Jean Graindorge. Le remède contre le nez rouge. Boire comme un trou et ça tourne rubis. Il en a dépensé de l’argent à lui donner de la couleur.

M. Power regardait les maisons qui filaient avec une appréhension chagrine.

– Quelle mort soudaine, le pauvre gars, dit-il.

– La meilleure des morts, dit M. Bloom.

Leurs yeux agrandis se posèrent sur lui.

– Pas de souffrance, dit-il. Un instant et tout est fini. Comme mourir en dormant.

Personne ne parla.

Côté mort de la rue ici. De jour les affaires languissent, agents fonciers, hôtel de sevrage, Falconer : indicateur des chemins de fer, école de fonctionnaires, Gill &, club catholique, les aveugles industrieux. Pourquoi ? Bien une raison. Soleil ou vent. La nuit aussi. Titis et bonniches. Sous le patronage de feu le Père Mathieu. Pierre d’angle pour Parnell. Syncope. Le cœur.

Des chevaux blancs aux blanches aigrettes prirent la rotonde au galop. Un cercueil réduit passa en éclair. En trombe pour la tombe. Une voiture de deuil. Non marié. Noire pour les mariés. Pie pour les célibataires. Baie pour les abbés.

– Triste, dit Martin Cunningham. Un enfant.

Une figure de nain, mauve et ridée comme l’était le petit Rudy. Corps de nain, mou comme du mastic, dans une boite au rabais, doublée de blanc. L’amicale des enterrements paie. Un penny par semaine pour un carré de gazon. Notre. Petit. Bécassou. Bébé. Ne voulait rien dire. Erreur de la nature. Quand c’est en bonne santé ça tient de la mère. Sinon c’est l’homme. Plus de chance la prochaine fois.

– Pauvre petite chose, dit M. Dedalus. S’est bien tirée d’affaire.

La voiture ralentit sur la côte du square Rutland. Valdinguent ses os. À chaque cahot. N’est qu’un miséreux. Plus personne ne l’veut.

– Au beau milieu de l’existence, dit Martin Cunningham.

– Mais le pire de tout, dit M. Power, c’est l’homme qui se donne la mort.

Martin Cunningham tira promptement sa montre, toussa et la remit en place.

– Il n’y a pas de plus grande honte pour une famille, ajouta M. Power.

– Folie passagère, certainement, détermina Martin Cunningham. Nous devons adopter un point de vue charitable.

– On dit que l’homme qui le fait est un lâche, dit M. Dedalus.

– Ce n’est pas à nous de juger, dit Martin Cunningham.

M. Bloom, sur le point de parler, referma la bouche. Les yeux grands ouverts de Martin Cunningham. Il regarde ailleurs maintenant. C’est un homme humain, plein d’empathie. Intelligent. Comme le visage de Shakespeare. Toujours une bonne parole à la bouche. Ils n’ont aucune pitié pour ça ici ni l’infanticide. Refusent une sépulture chrétienne. Ils leur transperçaient le cœur d’un épieu de bois dans la tombe, dans le temps. Comme s’il n’était pas déjà assez percé. Ils regrettent parfois mais trop tard. Dans le lit de la rivière, cramponné aux roseaux. Il me regardait. Et cette effroyable soularde de femme qu’il avait. Jours après jours à équiper le ménage et tous les samedis ou presque le mobilier au clou. Lui faisait mener une vie d’enfer. À faire pleurer les pierres. Lundi matin. Repartir de zéro. Pousser sa pierre. Seigneur, elle devait avoir une tronche cette nuit-là Dedalus m’a dit il y était. Complétement saoule, à se trémousser avec le parapluie de Martin.

> *La perle de l’Asie déjà  
de l’Asie déjà  
La Geisha*

Il a évité de me regarder. Il sait. Valdinguent ses os.

L’après-midi de l’enquête. Le flacon à l’étiquette rouge sur la table. La chambre d’hôtel avec les scènes de chasse. Étouffant là-dedans. Le soleil qui perce les stores. Tombe sur les oreilles du légiste, longues et velues. Le garçon d’étage qui fait sa déposition. D’abord pensé qu’il dormait. Puis vu comme des lignes jaunes sur son visage. Avait glissé au pied du lit. Verdict : overdose. Mort accidentelle. La lettre. Pour mon fils Léopold.

Ne plus souffrir. Ne plus s’éveiller. Plus personne ne l’veut.

La voiture valdinguait avec entrain le long de la rue Blessington. À chaque cahot.

– On file à bonne allure, il me semble, dit Martin Cunningham.

– Dieu nous garde d’un accident de la route, dit M. Power.

– J’espère bien, dit Martin Cunningham. Il y a une grande course demain en Allemagne. La Gordon Bennett.

– Oui, parbleu, dit M. Dedalus. Cela en vaudrait le coup d’œil, ma foi.

Alors qu’ils s’engageaient dans la rue Berkeley, un orgue de Barbarie proche du Bassin les poursuivit de refrains entrainants et cahoteux de music-hall. Quelqu’un ici a-t-il vu Kelly ? Ka heu double ell igrec. Marche funèbre du *Saul*. Aussi mauvais que le vieil Antonio. Il m’a laissé sans ponio’. Pirouette. La *Mater Misericordiae*. Rue Eccles. Ma maison plus bas. Grand édifice. Soins palliatifs. Très encourageant. L’hospice Notre Dame des mourants. Une morgue en sous-sol, pratique. La vieille Mme Riordan y est morte. Elles ont un aspect terrible les femmes. Son écuelle et la bouche qu’on racle avec la cuillère. Puis le paravent autour du lit pour la laisser mourir. Aimable le petit étudiant qui s’est occupé de ma piqure d’abeille. Il est parti à la maternité on m’a dit. D’un extrême à l’autre. La voiture prit un virage au galop : et pila.

– Qu’est-ce qu’il y a maintenant ?

Un troupeau de bestiaux marqués passait sous les fenêtres, des deux côtés, meuglant, trainant leurs sabots feutrés, balayant lentement de la queue leur croupe crottée et osseuse. Entrant et sortant de la masse, des moutons à bout de force couraient en glapissant leur peur.

– Des émigrants, dit M. Power.

– Houuuh ! résonna la voix du vacher, et son aiguillon claquait sur leurs flancs.

Houuuh ! Tirez-vous de là !

Jeudi, bien sûr. Demain jour d’abattage. Bouvillons. Cuffe les a vendus à vingt-sept thunes la tête à peu près. Pour Liverpool probable. Du rosbif pour la vieille Angleterre. Ils trustent les meilleurs morceaux. Et puis on en jette la moitié : toute la matière brute, cuir, poil, cornes. Ça en fait du volume en un an. Commerce de viande morte. Sous-produits des abattoirs pour les tanneries, savon, margarine. Me demande si cette combine marche encore, décharger de la viande avariée du train à Clonsilla.

– Je n’arrive pas à piger pourquoi la municipalité ne tire pas une ligne de tram des portes du parc jusqu’aux quais, dit M. Bloom. Tous ces animaux pourraient être amenés par fourgons aux bateaux.

– Au lieu de bloquer toute la circulation, dit Martin Cunningham. Très juste. Ils devraient.

– Oui, dit M. Bloom, et une chose que j’ai souvent pensée, ce serait d’avoir des trams funéraires municipaux comme ils ont à Milan, vous savez. Faire arriver la ligne aux portes du cimetière avec des trams spéciaux, corbillard et voiture et tout, vous voyez ce que je veux dire ?

– Oh, en voilà une sacrée histoire, dit M. Dedalus. Wagon pullman et restaurant.

– Triste perspective pour Corny, ajouta M. Power.

– Pourquoi ? demanda M. Bloom en s’adressant à M. Dedalus. Ne serait-ce pas plus décent que d’y aller de front au galop ?

– Eh bien, c’est un bon point, admit M. Dedalus.

– Et, dit Martin Cunningham, nous n’aurions plus le spectacle de comme quand le corbillard a chaviré au coin de Dunphy et renversé le cercueil sur la route.

– C’était affreux, fit le visage effaré de M. Power, et le cadavre a roulé sur chaussée. Affreux !

– En tête au virage Dunphy, dit M. Dedalus, hochant du chef. Coupe Gordon Bennett.

– Dieu soit loué ! dit pieusement Martin Cunningham.

Boum ! Renversé. Un cercueil rebondit sur la chaussée. Grand ouvert. Paddy Dignam jaillit et roule dans la poussière, tout raide dans un habit brun trop grand pour lui. Face rougeaude : elle est grise maintenant. Bouche béante. Demande qu’est-ce qui se passe maintenant. On fait bien de fermer. Ouvert c’est une horreur. Et puis l’intérieur se décompose vite. Mieux vaut boucher tous les orifices. Oui, en plus. À la cire. Le sphincter se relâche. Tout bien scellé.

– Dunphy, annonça M. Power alors que la voiture prenait à droite.

Au coin de Dunphy. Les voitures de deuil alignées, noyant leur chagrin. Une pause sur la route. Tip-top pour un pub. Parie qu’on y fera un stop au retour pour boire sa santé. Passez-moi la consolation. Élixir de vie.

Mais suppose vraiment que ça arrive. Est-ce qu’il saignerait si genre il se coupait sur un clou pendant la culbute ? Oui et non je suppose. Ça dépend où. La circulation s’arrête. Mais ça pourrait s’écouler un peu d’une artère. On ferait mieux de les enterrer en rouge : un rouge sombre.

Ils roulaient en silence sur la route de Phibsborough. Un corbillard vide, qui revenait du cimetière, les croisa au trot : a l’air soulagé.

Pont de Crossgun : le canal royal.

L’eau s’engouffrait rugissante entre les vannes. Un homme se tenait debout sur sa péniche, entre des plaques de tourbe. Sur le chemin de halage près de l’écluse un cheval au bout d’une longe lâche. À bord du *Lougarou*.

Leurs yeux le suivaient. Au fil de cette eau lente et filandreuse il avait flotté, sur sa barge tirée par une corde de halage, traversant l’Irlande vers la côte, par-delà les lits de roseaux et la vase, passant les bouteilles gavées de boue et les charognes de chiens. Athlone, Mullingar, Moyvalley, je pourrais aller voir Milly en marchant le long du canal. Ou à bicyclette. Louer une vieille bécane, quelque chose de stable. Wren en avait une l’autre jour aux enchères mais pour femme. Développer les voies navigables. La marotte de M’Cann, me faire passer le bac à la rame. Transport plus économique. Par petites étapes. Maisons flottantes. Plein air. Les corbillards aussi. Au ciel par voie d’eau. Peut-être irai-je sans écrire. Arriver par surprise, Leixlip, Clonsilla. Descendre écluse par écluse jusqu’à Dublin. Avec la tourbe des marais du centre. Salut. Il souleva son chapeau de paille brune, saluant Paddy Dignam.

Ils passèrent la maison de Brian Boroimhe. Plus très loin.

– Je serais curieux de savoir ce que devient notre ami Fogarty, dit M. Power.

– Ça, il faut le demander à Tom Kernan, dit M. Dedalus.

– Comment ça ? dit Martin Cunningham. Ne lui a laissé que ses yeux pour pleurer, j’imagine ?

– Loin des yeux, dit M. Dedalus, mais près du cœur.

La voiture s’engagea à gauche sur la route de Finglas.

Le chantier du marbrier sur la droite. Dernier tour. Amassées sur la langue de terre des formes silencieuses apparurent, blanches, chagrines, les mains paisiblement tendues, agenouillées dans la douleur, pointant du doigt. Fragments de formes taillées. Dans un blanc silence : implorant. Ce qu’on fait de mieux. Thos. H. Dennany, monuments et sculptures.

Dépassé.

Sur le bord du trottoir devant la maison de Jimmy Geary, le sacristain, un vieux clodo était assis et vidait en grommelant graviers et gravats d’une énorme botte béante, marron de poussière. Après le voyage de la vie.

Puis défilèrent de sinistres jardins : un après l’autre : sinistres demeures.

M. Power pointa son doigt.

– C'est là que Childs a été assassiné, dit-il. La dernière maison.

– En effet, dit M. Dedalus. Une effroyable affaire. Seymour Bushe l’a tiré de là. Assassin de son frère. C’est ce qui se disait.

– La couronne n’avait pas de preuve, dit M. Power.

– Des présomptions, rien de plus, ajouta Martin Cunningham. C’est le principe de la loi. Mieux vaut laisser échapper quatre-vingt-dix-neuf coupables que condamner à tort un innocent.

Ils regardaient. Scène de meurtre. Elle défilait sombrement. Volets clos, inhabitée, jardin à l’abandon. Pauvre endroit, quelle descente aux enfers. Condamné à tort. Meurtre. L’image du meurtrier dans les yeux de la victime. Ils adorent ça. Tête d’homme découverte dans un jardin. Elle portait un. Comment elle a trouvé la mort. Derniers outrages. L’arme du crime. Le meurtrier court toujours. Indices. Un lacet. Le corps sera exhumé. Le meurtrier finira par se trahir.

Serrés dans cette voiture. Elle n’aimerait peut-être pas ça, que j’arrive sans prévenir. Faut faire attention avec les femmes. Prenez-les au dépourvu. Elles ne vous pardonneront jamais. Quinze ans.

Les hautes grilles de Prospect ondoyaient sous leurs regards. Sombres peupliers, quelques rares formes blanches. Des formes plus fréquentes, silhouettes blanches qui se pressent entre les arbres, des formes blanches, des moignons muets défilaient, gesticulant vainement dans les airs.

La jante ripa contre le cordon : à l’arrêt. Martin Cunningham allongea le bras, tira fortement à lui la poignée et du genou força la porte ouverte. Il descendit. M. Power et M. Dedalus suivirent.

Change le savon maintenant. La main de M. Bloom déboutonna promptement sa poche revolver et transféra le savon englué à son papier dans la poche intérieure de son veston. Il descendit de la voiture, en remettant à sa place le journal qu’il tenait encore dans son autre main.

Piètres funérailles : fourgon et trois voitures. Pour ce que ça change. Porteurs, cordons dorés, messes de requiem, salves d’artillerie. La mort en ses pompes. Derrière la voiture de queue un vendeur ambulant se tenait debout à côté d’une charrette de fruits et gâteaux. Gâteaux de Pâques, tout collés. Des gâteaux pour les morts. Biscuits pour chiens. Qui en mangeait ? Les endeuillés en sortant.

Il suivit ses compagnons. M. Kernan et Ned Lambert venaient après, Hynes derrière eux. Corny Kelleher était debout devant le corbillard ouvert et en sortit deux couronnes. Il en tendit une au garçon.

Où est donc passé le cortège de l’enfant ?

Un attelage de chevaux à la peine passa, trainant d’un pas lourd dans le silence funèbre, depuis Finglas, une carriole grinçante qu’écrasait un bloc de granit.

Le cercueil maintenant. Est arrivé avant nous, tout mort qu’il est. Le cheval qui tourne l’encolure pour le regarder, plumeau de guingois. Œil terne : collier serré au cou, opprime un vaisseau sanguin ou truc. Sont-ils conscients de ce qu’ils amènent ici tous les jours ? Sans parler du Mont Jérôme pour les protestants. Des enterrements aux quatre coins du globe, partout, à chaque minute. Charretées, pelletées, dare-dare, sous le tapis. Des milliers toutes les heures. Trop pour ce monde.

Deux figures endeuillées passèrent les portes : une femme et une enfant. Harpie aux joues creuses, âpre, le bonnet de travers. Visage de la fillette barbouillé de crasse et de larmes, pendue au bras de la femme, levant les yeux vers elle pour pouvoir pleurer. Une figure subaquatique, anémiée et livide.

Les muets de service portèrent le cercueil à l’épaule et avancèrent vers les portes. Que de poids mort. Me suis senti plus lourd moi aussi au sortir du bain. D’abord le refroidi : puis les amis du refroidi. Corny Kelleher et le garçon suivaient avec les couronnes. Qui c’est à leur côté ? Ah, le beau-frère.

Tous emboitèrent le pas.

Martin Cunningham chuchota :

– J’étais à l’agonie quand tu parlais de suicide devant Bloom.

– Quoi ? chuchota M. Power. Comment ça ?

– Son père s’est empoisonné, chuchota Martin Cunningham. Tenait l’hôtel Queen à Ennis. Vous l’avez entendu dire qu’il allait à Clare. Anniversaire.

– Oh mon Dieu ! chuchota M. Power. Première fois que j’en entends parler. Empoisonné ?

Il jeta un regard en arrière, vers le visage aux yeux sombres et pensifs qui suivait en direction du Mausolée du Cardinal. Bavarde.

– Était-il assuré ? demanda M. Bloom.

– Je crois, répondit M. Kernan. Mais la police est lourdement hypothéquée. Martin essaie de faire rentrer le plus jeune à Artane.

– Combien d’enfants laisse-t-il derrière lui ?

– Cinq. Ned Lambert dit qu’il essaiera de faire entrer une des filles chez Todd.

– Triste affaire, dit M. Bloom avec douceur. Cinq jeunes enfants.

– Un rude coup pour la pauvre femme, ajouta M. Kernan.

– Oui, vraiment, appuya M. Bloom.

Qui rit le dernier.

Il baissa les yeux sur les souliers noirs qu’il avait cirés et lustrés. Elle lui avait survécu. Perdu son mari. Plus mort pour elle que pour moi. Un survit toujours à l’autre. Disent les sages. Il y a plus de femmes que d’hommes dans le monde. Lui présenter mes condoléances. Votre terrible perte. J’espère que vous le retrouverez bientôt. Pour veuves hindoues seulement. Elle se remariera. Lui ? Non. Pourtant qui sait après. Le veuvage n’est plus en vogue depuis que la vieille reine est morte. Trainée dans un affut à canon. Victoria et Albert. Deuil au mémorial de Frogmore. Mais sur le tard elle mettait quelques violettes à son bonnet. Coquette au fond du cœur. Tout ça pour une ombre. Consort, même pas un roi. Son fils voilà la substance. Quelque chose de neuf de quoi espérer pas comme le passé qu’elle voulait ravoir, attendait. Cela ne vient jamais. On doit partir en premier : seul, sous terre : et ne plus reposer auprès d’elle sur sa couche chaude.

– Comment vas-tu, Simon ? dit Ned Lambert à voix basse, main dans la main. Il y a des lustres que je ne t’ai pas vu.

– On ne peut mieux. Comment se porte cette bonne ville de Cork ?

– J’y suis descendu pour les courses du parc le lundi de Pâques, dit Ned Lambert. Toujours les mêmes chicaneurs. Me suis arrêté chez Dick Tivy.

– Et comment va Dick le costaud ?

– Il n’y a plus rien entre lui et le ciel, répondit Ned Lambert.

– Par saint Paul ! dit M. Dedalus, contenant sa surprise. Dick Tivy le chauve ?

– Martin va mettre en place une cagnotte pour les plus jeunes, dit Ned Lambert, avec un geste vers l’avant. Quelques shillings par tête. Pour qu’ils s’en sortent jusqu’à toucher l’assurance.

– Oui, oui, dit M. Dedalus hésitant. C’est le plus âgé, en tête ?

– Oui, dit Ned Lambert, avec le frère de la femme. John Henry Menton les suit. Il a souscrit pour une thune.

