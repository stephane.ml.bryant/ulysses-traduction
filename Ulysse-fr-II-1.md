[comment]: # (reprendre le tutoiement/vouvoiement. Le vouvoiement semble plus adapté aux échanges entre Bloom et le reste)

M. Léopold Bloom se délectait des organes internes du gibier et de la volaille. Il aimait l’épaisse soupe d’abats, les gésiers crissant sous la dent, le cœur rôti avec sa farce, les tranches de foie panées à la poêle, les œufs de morue frits. Mais par-dessus tout il aimait les rognons de mouton grillés, qui flattaient son palais d’un arôme fugitif d’urine.

Il avait les rognons en tête alors qu’il s’affairait sans bruit dans la cuisine et disposait des éléments de son petit déjeuner à elle sur le plateau bosselé. La cuisine baignait dans un air et une lumière glaciale, mais passée la porte partout un doux matin d’été. Lui creusait l’appétit, un peu.

Les charbons rougeoyaient.

Une autre tranche de pain beurrée : trois, quatre : bien. Elle n’aimait pas que l’assiette déborde. Bien. Il se détourna du plateau, souleva la bouilloire et la mit sur le feu, de guingois. Assise, terne et trapue, le bec saillant. Bientôt le thé. Bien. Gorge sèche. La chatte tournait avec raideur le pied de table, la queue dressée.

– Mkgnao !

– Ah, te voilà toi, fit M. Bloom, se détournant du feu.

La chatte répondit d’un miaulement et se raidit à nouveau contre le pied de table, miaulant. Comme avec mon secrétaire. Prr. Gratte ma tête. Prr.

M. Bloom observait avec curiosité et bienveillance la forme noire et souple. Proprette : le lustre de son pelage lisse, le bouton blanc sous la pointe de sa queue, l’éclair de ses yeux verts. Il se pencha vers elle, les mains sur les genoux.

– Du lait pour la minouche, annonça-t-il.

– Mrkgnao ! s’écria la chatte.

Et on les dit stupides. Ils comprennent ce qu’on dit mieux que nous ne les comprenons. Elle comprend tout ce qu’elle veut comprendre. Et vindicative. Cruelle. C’est sa nature. Curieux que les souris ne couinent jamais. Ont l’air d’aimer ça. De quoi j’ai l’air, me demande, pour elle ? Une tour ? Non, elle peut me sauter dessus.

– Mais qui a la trouille des poulets ? plaisanta-t-il. Qui a peur des ‘tits poussins ? Je n’ai jamais vu une minouche aussi bête que la minouche.

Cruelle. Sa nature. Curieux que les souris ne couinent jamais. Doivent aimer ça.

– Mrkrgnao ! ajouta-t-elle fermement.

Elle leva des yeux mi-clos de honte et de désir qu’elle cligna longuement dans un miaulement plaintif, montrant ses dents blanches comme le lait. Il contempla les fines pupilles qui rétrécirent jusqu’à ce que ses yeux ne fussent que deux gemmes vertes. Puis il alla prendre dans le buffet la cruche que le laitier de Hanlon venait de remplir, versa le lait tiède et mousseux dans une soucoupe et la déposa lentement sur le sol.

– Gurrhr ! s’écria-t-elle, accourant.

Il regardait les moustaches luire comme des fils d’acier dans la pénombre pendant que la chatte par trois fois hochait la tête et donnait un léger coup de langue. Me demande si c’est vrai que si on les coupe ils ne peuvent plus chasser. Pourquoi ? Elles brillent dans l’obscurité, peut-être, les pointes. Ou un genre d’antennes dans l’obscurité, peut être.

Il l’écoutait laper. Jambon et œufs, non. Pas de bons œufs par cette sécheresse. Faut de l’eau fraîche. Jeudi : pas le bon jour non plus pour un rognon de mouton chez Buckley. Frit dans du beurre, une pincée de poivre. Mieux vaut du rognon de porc chez Dlugacz. Pendant que l’eau bout. Elle lapait plus lentement, puis nettoya de la langue la soucoupe. Pourquoi ont-ils la langue si rêche ? Pour mieux laper, pleine de trous de pores. Rien qu’elle puisse manger ? Il jeta un coup d’œil alentour.
Non.

Sur ses souliers qui grinçaient discrètement, il remonta dans l’entrée, s’arrêta devant la porte de la chambre. Elle aimerait peut-être quelque chose de goûtu. De fines tartines beurrées, c’est ce qu’elle aime le matin. Pourtant peut-être, une fois, par hasard.

Il annonça doucement dans la salle vide :

– Je vais au coin de la rue. De retour dans une minute.

Et quand il se fut écouté le dire ajouta.

– Tu ne veux rien pour le petit-déjeuner ?

Un grognement endormi répondit faiblement :

– Mn.

Non. Elle ne voulait rien. Il entendit un soupir lourd et chaud, plus faible encore, comme elle se retournait dans le lit et les anneaux de laiton de la tête de lit, lâches, qui tintaient. Faut que je les fasse arranger, vraiment. Dommage. Tout ce chemin depuis Gibraltar. Oublié le peu d’espagnol qu’elle connaissait. À combien son père les aura obtenus, me demande. À l’ancienne. Ah, c’est vrai ! Bien sûr. Les a achetés aux enchères du gouverneur. Un coup de marteau d’avance. Dur en affaire, le vieux Tweedy. Oui monsieur. C’était à Plevna. Je suis parti de rien, monsieur, et j’en suis fier. Bah, il a bien eu le flair de sur le coup des timbres. Et c’était voir loin.

Sa main pécha son chapeau de la patère où pendait son épais pardessus, marqué de ses initiales, et son imperméable des objets trouvés. Timbres : effigies au derrière collant. Doit dire que des tas de fonctionnaires sont aussi dans la combine. Bien sûr. La devise érodée qui ornait la couronne de son chapeau lui rappela en silence : Plasto chapeaux de lu. Il jeta un coup d’œil rapide sous la bande de cuir intérieure. Petit papier blanc. En sûreté.

Sur le pas la porte il palpa sa poche postérieure : la clé de l’entrée. Pas là. Dans mon autre pantalon. Doit la chercher. La pomme de terre je l’ai. Penderie qui grince. Inutile de la déranger. Elle était bien endormie, cette fois. Il tira à lui la porte d’entrée, très doucement, plus, jusqu’à ce que la latte du bas dépassât à peine le seuil, couvercle de guingois. A l’air fermé. Ira bien jusqu’à ce que je revienne en tout cas.

Il traversa du côté du soleil, évitant au passage la trappe instable de la cave du numéro soixante-quinze. Le soleil approchait le clocher de l’église Saint-George. Va faire chaud, j’ai l’impression. Surtout dans ces habits noirs, le sens plus. Le noir conduit, reflète, (ou réfracte ?), la chaleur. Mais je ne pouvais pas y aller avec ce complet clair. Pas un picnic. Il fermait ses paupières lentement, fréquemment, en marchant, baigné d’une chaleur béate. Le van de Boland qui livre le pain du jour, mais elle préfère le reste des miches de la veille la croûte chaude et croustillante. On se sent jeune. Quelque-part à l’Est : le petit jour : pars à l’aube. Pars en avance sur le soleil, vole-lui un jour. Continue et tu ne vieilliras plus ni d’un jour, techniquement. Suis un rivage, une terre inconnue, voilà la porte d’une ville, voici une sentinelle, vieux troupier, les grosses moustaches du vieux Tweedy, qui s’appuie sur un genre de longue lance. Erre parmi les rues penchées. Visages enturbannés qui passent. Cavernes sombres des marchands de tapis, un malabar, Turko le terrible, assis les jambes croisées, qui fume une pipe spirale. Cris des vendeurs de rue. Bois une eau parfumée au fenouil, un sharbat. Flâne toute la journée. Pourrais rencontrer un voleur, ou deux. Eh bien, va pour la rencontre. Jusqu’au coucher du soleil. L’ombre des mosquées sur les piliers : un prêtre, un parchemin roulé. Les arbres frissonnent, signal, le vent du soir. Je passe. Ciel d’or qui se dissipe. Une mère me regarde du pas de sa porte. Elle appelle ses enfants dans leur obscur langage. De hauts murs : et derrière des cordes qu’on pince. Ciel nocturne, lune, violet, la couleur des nouvelles jarretelles de Molly. Des cordes. Une jeune fille joue un de ces instruments comment tu appelles ça : des dulcimers. Je passe.

Pas comme ça du tout, probable. Genre de truc qu’on lit : sur les traces du soleil. Soleil éclatant sur la page de garde. Il sourit, content de lui. Ce qu’a dit Arthur Griffith du bandeau de *L’homme libre*, au-dessus de la une : le soleil de l’autonomie qui se lève au nord-ouest, de la ruelle derrière la banque d’Irlande. Son sourire satisfait persistait : p’tit côté youpin là-dedans, le soleil qui se lève au nord-ouest.

Il n’était plus loin de chez Larry O’Rourke. Du soupirail de la cave montaient des effluves de bière brune. De sa porte ouverte le bar éructait gingembre, poussière de thé, biscuit détrempé. Bien placé, pourtant : juste où terminent les lignes de la ville. Comparé avec M’Aulay plus bas : endroit ml. sit.. Bien sûr s’il y avait une ligne de tram sur le périphérique Nord de la halle aux bestiaux aux quais, la valeur grimperait d’un coup. 

Tête chauve, dépasse le volet. Vieux dingo. Pas la peine de le démarcher pour une annonce. Connaît son affaire comme personne, pourtant. Il est là, pour sûr, mon brave Larry, en bras de chemise, appuyé sur la caisse à sucre, l’œil sur le serveur en chasuble qui passe la serpillière. Simon Dedalus le singe au poil avec ses yeux plissés. Savez-vous ce que je vais vous dire ? Qu’est-ce que c’est M. O’Rourke ? Vous savez, les Russes, les Japonais les avaleront au petit-déjeuner.

S’arrêter et dire un mot : à propos des funérailles peut-être. Bien triste, ce pauvre Dignam, M. O’Rourke.

Alors qu’il s’engageait dans la rue Dorset, il salua par la porte d’un ton gaillard :
– Bonjour, M. O’Rourke.

– Bonjour à vous.

– Belle matinée, monsieur.

– Pour sûr.

Comment font-ils leur oseille ? On les voit arriver, les serveurs rouquins, tout juste sortis de Leitrim, ils rincent les verres et les cuves à bière. Et hop, les voilà florissants comme Adam Findlater ou Dan Tallon. Et pensez à la compétition. Soif générale. Casse-tête : traverser Dublin sans passer devant un pub. Pas moyen d’épargner. Avec les ivrognes peut-être. Poser trois retenir cinq. Mais quoi, une thune par-ci par-là, au compte-goutte. Peut-être sur les commandes de gros. Tours de passe-passe avec les voyageurs de commerce. Arrondissez pour le patron et on se partage le pot, ça vous va ?

Ça ferait un tot. de combien, sur la bière, en un mois ? Mettons dix barriques de marchandise. Mettons qu’il se fait dix pour cent. Ou plus. Quinze. Il dépassa l’école nationale Saint-Joseph. Piaillement des mouflets. Fenêtres ouvertes. L’air frais favorise la mémoire. Ou la cadence. Aaabéécéédée heuuheffgéé kahellehèmopé cuherrehesseté uvé doublevé. Des garçons ? Oui. Inishturk, Inishark. Inishboffin. En pleine george-a-frit. La mienne. Les coteaux de Bloom.

Il s’arrêta devant la vitrine de Dlugacz, fixant les écheveaux de saucisses, de boudins blancs et noirs. Quinze multiplié par. Les chiffres s’estompaient sans réponse dans son esprit : contrarié, il les laissa disparaître. Du regard, il se repaissait des chaînons luisants, enflés de farce, et aspira avec quiétude l’haleine tiède et épicée du sang de cochon cuit.

D’un rognon suintaient des gouttes de sang sur le plat en faïence : le dernier. Il se tenait devant le comptoir, après la bonne des voisins. Va-t-elle le prendre, un article de plus qu’elle épelle, bout de papier à la main ? Gercée : lessive de soude. Et une livre et demi de saucisses de Denny. Ses yeux se posèrent sur les hanches vigoureuses. Lui s’appelle Woods. Me demande ce qu’il fait. Vieillotte, sa femme. Chair fraîche. Visites non-autorisées. Belle paire de bras. À force de taper le tapis sur la corde à linge. Et qu’est-ce qu’elle tape, pardieu. La jupe de traviole qui vole à chaque coup.

Le charcutier aux yeux de fouine repliait de ses doigts maculés, rose-saucisse les saucisses qu’il avait débitées. Voilà de la viande ferme : comme une génisse nourrie à l’étable.

Il prit un feuillet sur la pile de prospectus : la ferme modèle de Kinnereth, sur les rives du lac de Tibériade. Pourrait faire un sanatorium d’hiver idéal. Moïse Montefiore. Je pensais bien qu’il en était. Corps de ferme, murs autour, bétail qui paît alentour, flou. Il tenait la page le bras tendu : intéressant : lis-le de plus près, le titre, le bétail indistinct dans le pré, le bruissement du papier. Une jeune génisse blanche. Ces matins à la halle aux bestiaux, les bêtes qui meuglent dans leurs enclos, les moutons marqués, bouse qui s’écoule et tombe, les éleveurs en bottes ferrées qui pataugent dans le fumier, claquent un arrière-train, mûr pour l’abattoir : en voilà un de première qualité, aiguillons bruts en main. Il maintenait la page de biais, patiemment, ployant ses sens et sa volonté, le regard soumis, mou, au repos. La jupe de traviole qui vole, clac, clac, clac.

Le charcutier attrapa deux feuilles sur la pile, enveloppa les saucisses premier-choix et dit dans une grimace rouge :

– Et voilà, ma mamoizelle.

Elle tendit une pièce, le sourire large, un poignet massif à la vue.

– Merci, ma mamoizelle. Et un shilling et trois pence. Pour vous, s’il vous plaît ?

M. Bloom pointa du doigt, hâtivement. La rattraper, marcher derrière elle, si elle n’est pas trop loin, derrière ses jambons qui balancent. Pour bien commencer la journée. Active, merde. C’est quand le soleil brille qu’on fait le foin. Elle se tenait devant la boutique, au soleil, d’un pas nonchalant glissant vers la droite. Il soupira du nez : elles ne comprennent jamais. Mains gercées de lessive. Ongles des pieds en charpie. Scapulaires bruns en charpie, un devant un derrière : défense. L’aiguillon de son indifférence fit rougeoyer une faible braise de plaisir dans sa poitrine. Pas pour moi : l’agent qui la serrait au passage Eccles, après son service. Elles les aiment de bonne taille. Saucisse de premier choix. Oh je vous en prie, M. le policier, je suis perdue dans le bois.

– Trois pence, s’il vous plaît.

Sa main reçut la glande humide et tendre et la glissa dans une poche de son veston. Puis elle alla chercher trois pièces dans la poche du pantalon et les déposa une à côté de l’autre sur le caoutchouc rugueux du comptoir. Elles se tenaient là, rondes, on les vérifia vite et vite on les glissa, une à une, dans le tiroir-caisse.

– Merci, monsieur. À la prochaine.

Une lueur ardente traversa les yeux de renard en remerciement. Il détourna son regard, après un instant. Non : vaut mieux pas : une prochaine fois.

– Bonne matinée, fit-il, sur le départ.

– Bonne matinée, monsieur.

Rien en vue. Perdue. Qu’importe ?

Il remontait la rue Dorset, plongé dans sa lecture. Agendath Netaim : Société de planteurs. Acheter des friches sablonneuses au gouvernement turc, planter des eucalyptus. Excellent ombrage, combustible, construction. Orangeraies et immenses champs de melons au nord de Jaffa. Vous versez quatre-vingts marks pour un dunam de terre, qu’on plante pour vous avec des oliviers, des orangers, des amandiers ou des citronniers. Les olives c’est moins cher : les oranges ont besoin d’irrigation. Vous recevez chaque année un échantillon de la récolte. Votre nom est inscrit comme propriétaire à vie sur le livre de l’union. Pouvez payer dix comptant et le solde en traites annuelles. Bleibtreustrasse 34, Berlin, W. 15.

Très peu pour moi. Il y a de l’idée, cependant.

Il regardait le bétail, gommé sous une chaleur argentée. Oliviers poudrés d’argent. Jours longs, paisibles : taille, maturité. Les olives, on met ça dans des bocaux, non ? Il m’en reste quelques-unes de chez Andrews. Molly qui les recrache. En sait le goût maintenant. Oranges sous papier de soie, entassées dans des cageots. Citrons pareil. Me demande, ce pauvre Citron vit toujours parade Saint-Kevin. Et Mastiansky avec sa vieille cithare. Plaisantes soirées d’antan. Molly dans le fauteuil en osier de Citron. Plaisant en main, fruit, frais, ciré, le tenir en main, le porter aux narines, en respirer le parfum. Comme ça, parfum lourd, sucré, sauvage. Toujours le même, année après année. Et ça se vendait cher, m’a dit Moisel. Place Arbustus, rue des Plaisants : plaisant passé. Doivent être sans défaut, disait-il. Après un tel voyage : Espagne, Gibraltar, Méditerranée, le Levant. Caisses alignées devant le quai à Jaffa, un type qui les pointe sur un livret, les débardeurs aux pieds nus, en bleu souillé, qui se les coltinent. Voilà comment s’appelle-t-il qui sort de. Comment ça v ? Voit pas. Type qu’on connaît juste assez pour dire bonjour, rasoir. Le même dos que ce capitaine norvégien. Me demande si je le verrai aujourd’hui. La voiture d’arrosage. Pour provoquer la pluie. Sur la terre comme au ciel.

Un nuage empiétait sur le soleil, lentement, pleinement. Gris. Lointain.

Non, c’est pas ça. Étendue désolée, friche stérile. Lac volcanique, la mer morte : ni algues ni poissons, enfoncée dans un coin de terre. Ni le vent ne soulève ces vagues, gris de plomb, eaux brumeuses, vapeurs empoisonnées. Il pleut du sulfure, ils ont dit : les cités de la plaine, Sodome, Gomorrhe, Edom. Tous ces noms morts. Une mer morte dans une terre morte, grise, vieille. Vieille à présent. Elle a porté la plus ancienne, la première race. Une vieille sorcière sortit de chez Cassidy, la main crochetant un flacon par le goulot, et traversa. Les plus anciens. Errant au loin, aux quatre coins du globe, de captivité en captivité, multipliant, mourant, naissant de partout. Elle gisait à présent. Les flancs épuisés. Morte : d’une vieille femme, le con gris rabougri du monde.

Désolation.

Une terreur grise lui saisit la chair. Il tourna dans la rue Eccles et pressa le pas, pliant le feuillet dans une poche. Une poix froide glissait dans ses veines, figeait son sang : l’âge le recouvrant d’une chape de sel. Bon, j’y suis à présent. Oui, j’y suis. Haleine du matin, esprit chagrin. Levé du pied gauche. Reprendre la gym sandow. Les mains à plat. Maisons de briques brunes piquetées. Le quatre-vingt toujours à louer. Pourquoi donc ? Il n’est qu’à vingt-huit. Towers, Battersby, North, MacArthur : les fenêtres du salon en sont placardées. Plaisir pour les yeux, ces avis. Humer la douce odeur du thé, la poêle qui fume, le beurre qui grésille. Plus près de sa chair généreuse, encore chaude du lit. Oui, oui.

Une raie de soleil, vive et chaude, accourait par la rue Berkeley sur de fines sandales, illuminant son chemin. Court, elle court à ma rencontre, fille aux cheveux d’or portés par le vent.

Deux lettres et une carte postale par terre dans l’entrée. Il se baissa pour les ramasser. Mme Marion Bloom. Son cœur accéléré freina brutalement. Écriture hardie. Mme Marion.

– Poldy !

Il pénétra dans la chambre, les yeux mi-clos, et traversa la pénombre jaune et tiède vers la tête ébouriffée.

– Pour qui sont les lettres ?

Il les regarda. Mullingar. Milly.

– Il y a une lettre pour moi de Milly, dit-il avec circonspection, et une carte pour toi. Et une lettre pour toi.

Il déposa carte et lettre sur le couvre-lit de laine, près du creux de ses genoux.

– Veux-tu que j’ouvre les persiennes ?

Pendant qu’il remontait les persiennes à petits coups, il la vit du coin de l’œil un œil jeter sur la lettre et la glisser sous l’oreiller.

– Ça ira ? fit-il, se retournant.

Appuyée sur le coude, elle lisait la carte.

– Elle a reçu les affaires, dit-elle.

Il attendit qu’elle laisse la carte de côté, puis se remette lentement en boule en exhalant un soupir béat.

– Dépêche, avec le thé, fit-elle. J’ai la gorge sèche.

– L’eau bout, répondit-il.

Mais il s’attarda à débarrasser la chaise : son jupon rayé, du linge sale jeté là : et prit tout dans ses bras pour le mettre au pied du lit.

Il descendait à la cuisine et elle appela :

– Poldy !

– Quoi ?

– Échaude la théière.

Bouillu, pour sûr : un plumeau de vapeur planté dans le bec. Il échauda, rinça la théière et y mit quatre cuillères de thé pleines, puis inclina la bouilloire pour y verser l’eau. Laissant infuser, il ôta la bouilloire, plaqua la poêle sur les charbons ardents et observa le morceau de beurre patiner et fondre. Il déballait le rognon et la chatte miaulait avidement à ses côtés. Trop de viande et elle ne chassera plus. On dit qu’ils ne mangent pas de porc. Casher. Tiens. Il laissa le papier sanguinolent tomber à sa portée, et le rognon dans le beurre frémissant. Poivre. Du coquetier ébréché, il en saupoudra des doigts, en rond.

Puis il fendit l’enveloppe et parcourut la lettre recto-verso. Remerciements : nouveau béret de tartan : M. Coghlan : picnic au lac Owel : jeune étudiant : les jeunes filles de la côte de Braise Boylan.

Le thé était prêt. Il remplit sa propre tasse, celle à moustache, faux Crown Derby, avec un sourire. Cadeau d’anniversaire de Milly-la-bécasse. Cinq ans à l’époque. Non. Attends. Quatre. Je lui avais offert ce collier d’ambre synthétique qu’elle a cassé. Glissant des bouts de papier bruns, pliés, dans la boîte aux lettres, à son adresse. Il souriait en versant.

>*O, Milly Bloom, ma petite poire,  
Du soir au matin tu es mon miroir,  
Sans un sou je préfère t’avoir,  
que Katey Keogh et tout son avoir.*

Pauvre vieux professeur Goodwin. Cas désespéré. Si courtois, pourtant, le vieux. La courbette old-school en raccompagnant Molly hors scène. Et le petit miroir dans son chapeau en soie. Cette nuit quand Milly l’a apporté dans le salon. Oh, regardez ce que j’ai trouvé dans le chapeau du professeur Goodwin ! Comme on a ri. Le sexe qui perce déjà. Petite chose mutine.

Il piqua le rognon d’une fourchette et le retourna d’un coup sec : puis fit une place à la théière sur le plateau. La bosse en ballotta quand il le souleva. Tout y est ? Pain et beurre, quatre, sucre, cuillère, sa crème. Oui. Il le porta à l’étage, le pouce crochetant l’anse de la théière.

Poussant du genou il entrouvrit la porte, entra avec le plateau et le posa sur la chaise à la tête du lit.

– Tu en as mis du temps ! fit-elle.

Elle se releva vivement, le coude sur l’oreiller, faisant tinter les anneaux. Il toisait calmement la forme arrondie, le creux des larges mamelons qui soulevaient la robe de chambre, comme les pis d’une chèvre. La chaleur de son corps s’élevait de la couche et se mêlait à la fragrance du thé qu’elle versait.

Un bout d’enveloppe, déchiré, pointait sous l’oreiller plissé. Sur le départ, il s’arrêta pour accommoder le couvre-lit.

– De qui était la lettre ? Demanda-t-il.

Main hardie. Marion.

– Ah, Boylan, fit-elle. Il envoie le programme.

– Qu’est-ce que tu chantes ?

– Là Ci Darem avec J. C. Doyles, répondit-elle, et Pour un peu d’Amour.

Ses lèvres pleines souriaient en buvant. Plutôt rance cette odeur que laisse l’encens le lendemain. Comme l’eau croupie des fleurs.

– Veux-tu que j’ouvre un peu la fenêtre ?

Elle plia une tranche de pain dans sa bouche, tout en demandant :

– À quelle heure sont les obsèques ?

– Onze heures, je crois, répondit-il. Je n’ai pas lu le journal.

Suivant son doigt pointé, il ramassa du lit une jambe de sa culotte sale. Non ? Cette jarretière grise enroulée sur un bas alors : semelle fripée, luisante.

– Non, ce livre.

L’autre bas. Son jupon.

– Il a dû tomber, dit-elle.

Il tâtait ça et là. Voglio e non vorrei. Me demande si elle prononce ça bien : voglio.  Pas sur le lit. Doit avoir glissé. Il se pencha et souleva la frange. Le livre chu s’ouvrait contre la bosse du pot de chambre aux frises orange.

– Passe, fit-elle. J’ai mis une marque. Il y a un mot que je voulais te demander.

Elle avala une gorgée de thé, la tasse en main, hors-la-anse, et, après avoir finement essuyé ses doigts sur la couverture, se mit à chercher dans le texte avec son épingle à cheveux jusqu’à trouver le mot.

– Mes tempes si quoi ? demanda-t-il.

– Là, fit-elle. Qu’est-ce que ça veut dire ?

Il se pencha et lut, près de l’ongle poli de son pouce.

– Métempsychose ?

– Oui. D’où sort-il celui-là ?

– Métempsychose, fit-il, fronçant du sourcil. C’est grec : des Grecs. Cela signifie transmigration des âmes.

– Des prunes ! Et avec des mots ?

Il sourit, les yeux de biais sur ses yeux moqueurs. Les mêmes yeux de jouvencelle. Cette première nuit après les devinettes. Dolphin’s Barn. Il feuilletait les pages salies par les traces de doigts. Ruby : L’orgueil de la piste. Hello. Illustration. Italien féroce, fouet en main. Doit être Ruby l’orgueil de, nue sur le sol. Le monstre Maffei renonce d’un juron et rejette sa victime loin de lui. Toute cette cruauté. Animaux drogués. Le trapèze chez Hengler. Pas pu regarder. La foule bouche-bée. Casse-toi le cou et on se tordra les côtes. Des familles entières. Désosser les jeunes pour qu’ils métempsychosent. Qu’on vive après la mort. Nos âmes. Que l’âme d’un homme après qu’il meure. L’âme de Dignam…

– Tu l’as fini ? Demanda-t-il.

– Oui, répondit-elle. Il n’y a rien de bien osé. Est-ce qu’elle reste amoureuse du premier type tout le long ?

– Jamais lu. Tu en veux un autre ?

– Oui. Trouves-en un autre de Paul de Kock. Avec ce joli nom.

Elle versa plus de thé dans sa tasse, le surveillant couler du coin de l’œil.

Faut que je renouvelle ce livre de la bibliothèque de la rue Capel, sinon ils écriront à Kearney, ma caution. Réincarnation : voilà le mot.

– Certaines personnes croient, dit-il, que nous continuons à vivre dans un autre corps après la mort, et que nous avons vécu avant. Ils appellent ça la réincarnation. Que nous avons tous vécu sur terre il y a des milliers d’années, ou sur une autre planète. Ils disent que nous l’avons oublié. Certains disent qu’ils se souviennent de leurs vies antérieures.

La crème rancie formait des volutes poisseuses dans son thé. Mieux vaut lui rappeler le mot : métempsychose. Un exemple, ce serait mieux. Un exemple ?

La nymphe au bain, au-dessus du lit. Supplément gratuit, numéro de Pâques, Photo Bits : superbe chef-d’œuvre aux couleurs artistiques. Thé avant d’y mettre du lait. Quelque chose d’elle, les cheveux dénoués : mais plus fine. Payé trois et six pour le cadre. Elle disait que ça ferait joli au-dessus du lit. Nymphes nues : la Grèce : et par exemple tous les gens de ce temps-là.

Il tournait les pages à rebours.

– La métempsychose, continua-t-il, voilà comment les Grecs de l’antiquité appelaient ça. Ils croyaient par exemple que tu pouvais être changé en animal ou en arbre. Ce qu’ils appelaient les nymphes, par exemple.

Sa cuillère, qui remuait le sucre, s’arrêta. Elle regarda droit devant elle, inhalant, les narines tendues.

– Ça sent le brûlé, fit-elle. As-tu laissé quelque-chose sur le feu ?

– Le rognon ! s’écria-t-il.

Il empocha le livre d’un geste brusque et, sans manquer de heurter son orteil contre la commode bancale, se précipita vers l’odeur, dévalant les escaliers comme un échassier effaré. Un jet de fumée âcre fusait furieusement d’un côté de la poêle. Forçant la pointe d’une fourchette sous le rognon il le détacha et le retourna. À peine cramé. Il le glissa de la poêle sur une assiette et laissa dégoutter dessus le jus chiche et noirâtre.

Maintenant, tasse de thé. Il s’assit, découpa et beurra une tranche de pain. Décapa la chair brûlée et la balança au chat. Puis il mit en bouche un bon morceau, mastiquant en connaisseur la viande souple et savoureuse. Juste à point. Une gorgée de thé. Puis il découpa des dés de pain, en trempa un dans le jus et le mit en bouche. C’était quoi cette affaire de jeune étudiant et de picnic  ? Il déplia la lettre à son côté et la lut lentement en mâchant, détrempant un autre dé de pain avant de le porter à sa bouche.

	Papounet chéri

Merci encore merci pour le mignon cadeau d’anniversaire. Il me va splendide. Tout le monde dit que je suis d’un chic. J’ai reçu la boite de chocolats de maman et lui écris. Ils sont choux. Je suis bien lancée dans la photo maintenant. M. Coghlan en pris une de moi et Mme l’enverra quand elle sera développée. Gros volume hier. Jour de marché, toutes les grosses vaches y sont passées. Nous allons au lac Owel lundi avec quelques amis pour un picnic improvisé. Dites à maman que je l’aime, et pour vous un gros bisou et un gros merci. Je les entends jouer du piano au rez-de-chaussée. Il va y avoir un concert au Greville Arms samedi. Il y a un jeune étudiant vient ici certains soirs, il s’appelle Bannon ses cousins ou je ne sais quoi sont de grosses légumes et il chante la chanson de Boylan (pour un peu j’écrivais Braise Boylan) sur les filles de la côte. Transmettez-lui les profonds respects de Milly-la-bécasse. Il faut que je termine, bien tendrement.

Votre fille qui vous aime,

MILLY.

P.S. Pardon pour le style doit y aller. tchaotchao.

M.

Quinze ans hier. Curieux, c’est aussi le quinze du mois. Son premier anniversaire loin de la maison. Séparation. Me souviens du matin d’été quand elle est née, la course et réveiller Mme Thornton rue Denzille. Une brave vieille. Elle a du en mettre au monde, des bébés. Elle a vu au premier coup d’œil que le pauvre petit Rudy ne vivrait pas. Eh bien monsieur, loué soit Dieu. Elle a su tout de suite. Il aurait onze ans s’il avait vécu.

Il fixait, piteux et absent, le post-scriptum. Pardon pour le style. Y aller. Piano en dessous. Sort de sa coquille. La scène au café XL, pour le bracelet. Ne voulait ni manger ses gâteaux, ni parler ni lever les yeux. Ce culot. Il trempait un à un les dés de pain, avalait le rognon morceau par morceau. Gagne douze et six par semaine. Pas grand-chose. Ça pourrait être pire. Phase music hall. Jeune étudiant. Il but une gorgée de thé refroidi pour faire passer son repas. Puis il relut la lettre. Par deux fois.

Bon : elle sait se tenir. Et pourtant ? Non, il n’y a rien eu. Évidemment, cela pourrait. Attendre que ce soit le cas, de toute façon. Incontrôlable. Ses jambes fines qui volent dans l’escalier. La destinée. Une fleur qui s’ouvre. Maintenant.

Coquette : très.

Il souriait avec une tendresse inquiète, tourné vers la fenêtre de la cuisine. Quand je l’ai surprise dans la rue à se pincer les joues pour les rendre plus roses. Un peu anémique. Allaitée trop longtemps. Sur l’Erin King ce jour-là, près du Kish. Vieux rafiot de malheur, ça tanguait. Même pas peur. Son écharpe bleu pâle, ses cheveux dans le vent.

>*Bouclettes, fossettes  
Ça t’fait tourner la tête*

Filles de la côte. Enveloppe déchirée. Mains plantées dans les poches du pantalon, cocher en goguette, va chantant. Ami de la famille. Ça turne, fait-il. Jetée illuminée, soir d’été, orchestre,

>*Ces filles, ces minottes,  
Ces jolies filles de la côte*

Milly aussi. Verts baisers : les premiers. C’est si loin. Mme Marion. En train de lire, recouchée, ses doigts séparent les mèches de ses cheveux, elle sourit, elle tresse.

 À peine un malaise, un regret, s’insinua, coula le long de son dos, puis enfla. Ça va arriver, oui. L’empêcher. Inutile : peux pas bouger. Lèvres douces, légères, jeunes. Cela va arriver. Il sentait le malaise se répandre, l’envahir. Trop tard, rien à faire. Lèvres baisées, baisant, baisées. Lèvres pleines et collantes de femme.

Vaut mieux qu’elle reste là-bas : loin. L’occuper. Voulait un chien pour passer le temps. Pourrais y faire un tour. En août pour les fêtes. Aller-retour deux et six. Mais c’est dans six semaines. Pourrait obtenir un passe de presse. Ou par M’Coy.

La chatte, sa toilette dûment achevée, revint au papier taché de sang, le renifla, et dédaigneuse avança vers la porte. Un regard en arrière, un miaulement. Veut sortir. Attend devant une porte, ça finira par s’ouvrir. Qu’elle attende. Tient pas en place. Électrifiée. Orage dans l’air. Elle s’est lavé l’oreille aussi, le dos au feu.

Il se sentit lourd, plein : puis les boyaux qui se dénouaient doucement. Il se leva et dénoua sa ceinture. La chatte miaula après lui.

– Miaou, répondit-il. Attends que j’aie fini.

Lourdeur : va faire chaud. Trop pour grimper les marches jusqu’au palier.

Un journal. Il aimait à lire sur le trône. Pourvu qu’un balourd ne vienne pas toquer juste quand j’y suis.

Dans le tiroir de la table il trouva un vieux numéro de Titbits. Il le plia sous l’aisselle, se dirigea vers la porte et l’ouvrit. La chatte grimpa par bonds élastiques. Ah, voulait monter se pelotonner sur le lit.

Prêtant l’oreille, il entendit sa voix :

– Viens, viens, minouche. Viens.

Il sortit dans le jardin par la porte de derrière : s’arrêta, l’oreille tendue vers le jardin d’à côté. Pas un bruit. Peut-être à étendre le linge. La bonne était au jardin, et rin et rin. Belle matinée.

Il se pencha pour examiner la maigre rangée de menthe verte qui poussait le long du mur. Faire un cabanon pour l’été. Haricots rouges. Vigne vierge. Faut recouvrir tout ça de purin, sol ingrat. Une couche de sulfate de potasse. Sans fumier tous les sols sont comme ça. Ordures ménagères. Le terreau, qu’est-ce que c’est que c’est ? Les poules du jardin d’à côté : leurs fientes font un très bon couvert. Mais rien ne vaut le bétail, surtout quand on le nourrit au tourteau. Paillage de bouse. Rien de tel pour nettoyer les gants en chevreau de ces dames. Le sale rend propre. Et les cendres. Récupérer tout ce terrain. Faire pousser des pois dans ce coin-là. Des laitues. Ne jamais manquer de légumes frais. Pas que des avantages, pourtant. Cette sorte d’abeille ou de grosse mouche le lundi de Pentecôte.

Il fit quelques pas. Où est mon chapeau, au fait ? Du le remettre sur la patère. Ou abandonné par terre. Drôle que je m’en souvienne pas. Trop de choses sur le portemanteau. Quatre parapluies, son imperméable. Ramassé les lettres. La sonnette de chez Drago. Bizarre, j’y pensais justement. Les cheveux bruns collés à la brillantine qui dépassent du col. Toilette rapide. Me demande si j’aurais le temps d’aller aux bains ce matin. Rue Tara. Le caissier a aidé James Stephen à se faire la malle, dit-on. O’Brien.

Voix de basse, ce Dlugacz. Agendath je-sais-plus-quoi ? Au tour de ma mamoizelle. Zélateur.

D’un coup de pied, il ouvrit la porte branlante des chiottes. Gaffe à pas salir ces pantalons avant l’enterrement. Il y entra, courbant la tête sous le linteau bas. Laissa la porte entrouverte et, baigné des remugles de chaux moisie et de toiles d’araignée poussiéreuses, défit ses bretelles. Avant de s’asseoir il mit l’œil à une fente et le jeta sur la fenêtre des voisins. Le roi comptait ses écus, et ru et ru. Personne.

Accroupi sur la cuvette il déplia son journal, et tournait les pages sur ses genoux nus. Quelque chose de nouveau, de facile. Rien ne presse. Resserre un peu. Notre morceau de choix : Le coup de maître de Lapaire. De la plume de M. Philip Beaufoy, club des Théâtropathes, Londres. Une guinée par colonne a été payée à l’auteur. Trois et demi. Trois livres trois. Trois livres, treize et six.

Il lisait posément, en se retenant, la première colonne puis, cédant et résistant tout à la fois, commença la seconde. A mi-colonne, ses dernières résistances cédèrent, et il laissa ses boyaux se soulager en paix pendant qu’il lisait, et continuait patiemment à lire, que cette petite constipation d’hier soit bien partie. Pourvu que ce soit pas trop large, encore des hémorroïdes. Non, pile poil. Voilà. Ah ! Constipé : prenez une tablette de cascara. Si la vie était si. Cela ne le touchait ni ne l’émouvait, mais c’était bref et bien mené. On imprime n’importe-quoi. Saison des marronniers. Il lisait toujours, trônant calmement sur son odeur à lui, qui s’élevait. Adroit, certainement. Lapaire pense souvent au coup de maître qui lui gagna la riante sorcière qui maintenant. Moral, le début et la fin. Main dans la main. Judicieux. Il parcourut ce qu’il avait déjà lu, ses fluides s’écoulaient sans accroc, il enviait ce bon M. Beaufoy qui avait écrit la chose et reçu pour cela trois livres, treize et six.

Une saynète, je pourrais y arriver. Par M. et Mme L. M. Bloom. Illustrer un proverbe. Lequel ? Quand j’essayais de gribouiller sur ma manche ce qu’elle disait en s’habillant. N’aime pas qu’on s’habille ensemble. Coupé en me rasant. Se mord la lèvre inférieure quand elle agrafe sa jupe. Chronométrée. 9:15. Est-ce que Roberts t’as payé ? 9:20 Qu’est-ce que portait Greta Conroy ? 9:23. Qu’est-ce qu’il m’a pris d’acheter ce peigne ? 9:24. Je suis toute gonflée avec ce chou. Un point de poussière sur sa botte vernie.

Dont elle frottait adroitement les coutures, une après l’autre, sur son mollet gainé. Le matin, le lendemain du bal de charité, la fois où l’orchestre de May a joué la danse des heures de Ponchielli. Expliquer ça : les heures du matin, midi, puis vient le soir, et les heures de la nuit. Se brossait les dents. C’était la première nuit. Sa tête qui dansait. Le cliquetis de son éventail. Il est fortuné, ce Boylan ? Il a de l’argent. Pourquoi ? Son haleine, quand on dansait : ça sentait bon, riche. Inutile de fredonner dans ce cas. Y faire allusion. Étrange, le style de musique qu’on donnait ce dernier soir. La glace était dans l’ombre. Elle frotta vivement son miroir de poche contre son débardeur, contre son nichon plein et qui bougeait. S’y regarda. Stries dans les yeux. Le pivot vers l’arrière ne marchait pas.

Les heures du soir, des filles voilées de gris. Puis les heures de la nuit : noires, poignards et loups. Idée poétique : rose, puis doré, puis gris, et enfin noir. Et pourtant réaliste. Le jour : et enfin la nuit.

Il déchira d’un coup sec la moitié du morceau de choix et s’essuya avec. Puis remonta son pantalon, rajusta ses bretelles et se reboutonna. Il tira à lui la porte branlante et rétive des chiottes et de la pénombre sortit au grand jour.

En pleine lumière, allégé et rafraîchi, il vérifia soigneusement ses pantalons noirs : le bas, les genoux, l’arrière de genoux et les mollets. À quelle heure est l’enterrement ? Faut que je vérifie dans le journal.

Au-dessus de lui, l’air grinça et vrombit. Les cloches de Saint-George. Elles marquaient l’heure : fonte sombre et puissante.

*Hei-ho ! Hei-ho !  
Hei-ho ! Hei-ho !  
Hei-ho ! Hei-ho !*

Moins le quart. Encore une fois : l’harmonique suivante qui résonne, une tierce.

Pauvre Dignam !