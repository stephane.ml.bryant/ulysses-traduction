Mademoiselle Dunne cacha l’exemplaire de *La femme en blanc*, bibliothèque de la rue Capel, bien au fond de son tiroir et enfila dans sa machine à écrire un papier à lettre criard.

Trop de mystère là-dedans. Est-ce qu’il est amoureux de celle-là, Marion ? Échange-le pour un autre de Mary Cecil Haye.

Le disque fusa dans la rainure, oscilla un moment, s’immobilisa et leur fit de l’œil : six.

Miss Dunne fit cliqueter le clavier :

– 16 juin 1904.

Cinq hommes-sandwiches sous de blancs hauts-de-forme, entre le coin de Monypeny et la dalle d’où ne s’élevait pas la statue Wolfe de Tone, tournèrent sinueusement les talons, pivotant le H. E. L. Y.’S, et repartirent pesamment sur leurs pas.

Puis elle fixa du regard la grande affiche de Marie Kendall, charmante soubrette, et tuant mollement le temps, griffonna sur le bloc-note des seize et des esses majuscules. Cheveux moutarde et joues à la truelle. Pas bien jolie, ou quoi ? Cette façon de relever le coin de sa jupe. Me demande si ce type sera à l’orchestre ce soir. Si je pouvais me faire faire par la couturière une jupe plissée comme celle de Susy Nagle. Ça en met plein la vue. Shannon et tous les beaux mecs du club d’aviron ne l’ont pas quittée du regard. J’espère bien qu’il ne va pas me tenir ici jusqu’à sept heures.

Le téléphone lui sonna grossièrement aux oreilles.

– Allô. Oui, monsieur. Non, monsieur. Oui, monsieur. Je les appelerai après cinq heures. Seulement ces deux-là, monsieur, pour Belfast et Liverpool. Très bien, monsieur. Alors je peux partir après six heures si vous n’êtes pas de retour. Un quart d’heure après. Vingt-sept et six. Je lui dirai. Oui : un, sept, six.

Elle griffonna trois chiffres sur une enveloppe.

– M. Boylan ! Allô ! Ce monsieur du *Sport* vous cherchait. M. Lenehan, oui. Il a dit qu’il serait à l’Ormond à quatre heures. Non, monsieur. Oui, monsieur. Je les appellerai après cinq heures.

***