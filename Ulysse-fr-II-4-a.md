
L’ÉPIGRAMME DE LENEHAN


Il chuchota pour l’oreille de Stephen :

>– *Voilà MacHugh ponte et pontifiant*  
*les yeux derrière d’ébène cadrants,*  
*Mais comme il y voit surtout en double,*  
*les porter pourquoi tant se trouble ?*  
*T’y crois, toi ? blague à deux roubles !*  

Portant deuil de Salluste, dit Mulligan. Dont la mère est morte comme une bête.

Myles Crawford fourra les feuillets dans sa poche latérale.

– Ça le fera, dit-il. Je lirai le reste plus tard. Ça le fera.

Lenehan étendit ses deux mains en signe de protestation :

– Et ma devinette ! Quel opéra ressemble à une mante andalouse ?

– Opéra ? Le visage de sphinx de M. O’Madden Burke se fit énigme.

Lenehan jubilant s’empressa :

– *L’étoile de Séville*. Vous pigez ? Toiles de Séville. Na !

Du doigt il vint mollement percer les côtes de M. O’Madden Burke à la hauteur de la rate. M. O’Madden Burke se prêtant au jeu s’affaissa sur son parapluie, le souffle faussement coupé.

– À l’aide, exhala-t-il. Je me sens bien mal.

Lenehan, monté sur la pointe des pieds, lui éventa prestement le visage à l’aide des papillons, dans un bruit de frou-frou.

Le professeur, qui revenait en passant devant les casiers, balaya de la main les cravates lâches de Stephen et de M. O’Madden Burke.

– Paris, hier et aujourd’hui, dit-il. Vous avez l’air de communards.

– De gars qui ont fait sauter la Bastille, se moqua doucement J. J. O’Molloy. Ou d’avoir descendu le lieutenant gouverneur de Finlande à vous deux ? Vous avez tout l’air d’en être les auteurs. Le général Bobrikoff.

– Nous y pensions seulement, dit Stephen.


TODUS REUNIONUS


– Tous les talents, dit Myles Crawford. Le droit, les classiques…

– Le turf, proposa Lenehan.

– La littérature, la presse.

– Si Bloom était là, dit le professeur, l’art aimable de la publicité.

– Et Madame Bloom, ajouta M. O’Madden Burke. La muse du chant. De Dublin L’idole sans égale.

Lenehan toussa bruyamment.

– Ahem ! souffla-t-il. Ah, pour une frise d’air bref ! J’ai attrapé froid dans le parc. La grille était ouverte.


VOUS LE POUVEZ


Le rédacteur-en-chef posa sa main nerveuse sur l’épaule de Stephen.

– Je veux que vous m’écriviez quelque-chose, dit-il. Quelque-chose qui ait du chien. Vous le pouvez. Je le lis sur votre visage. *Dans le parler de la jeunesse*…

Le lis sur ton visage. Le lis dans tes yeux. Petit branleur vicieux.

– La fièvre aphteuse ! s’exclama le rédacteur-en-chef débordant de mépris. Grande réunion nationaliste à Borris-en-Ossory. Conneries ! Pour des bœufs, on prend le public ! Donnez-leur quelque-chose qui ait du chien. Collez-nous y tous dedans, foutre-dieu. Le Père, le fils et le Saint-Esprit et Jakes M’Carthy.

– Nous avons tous de la matière grise à fournir, dit M. O’Madden Burke.

Stephen leva les yeux et rencontra un regard insensible, sûr de soi.

– Il vous presse, dit J. J. O’Molloy.


LE GRAND GALLAHER


– Vous pouvez le faire, répéta Myles Crawford, le poing serré avec emphase. Un instant. Nous paralyserons l’Europe comme disait Ignatius Gallaher quand il tenait le billard au Clarence, dans la zone. Gallaher, voilà un homme de presse pour vous. Voilà une plume. Vous savez comment il a percé ? Laissez-moi vous dire. Voilà le plus beau scoop qu’on ait jamais vu. C’était en quatre-vingt-un, le six mai, l’époque des Invincibles, le meurtre au parc du Phénix, vous n’étiez pas né je pense. Laissez-moi vous montrer.

Il s’empressa vers les casiers.

– Regardez-moi ça dit-il en se retournant. Le télégramme du *New York World* pour une exclusive. Vous vous rappelez ?

Le professeur MacHugh hocha la tête.

– Le *New York World*, dit le rédacteur-en-chef, qui repoussa excité son chapeau de paille sur son crane. Là où ça c’est passé. Tim Kelly, ou Kavanagh je veux dire. Joe Brady et les autres. Là où Écorche-Bouc a conduit la voiture. Tout le trajet, vous voyez ?

– Écorche-Bouc, dit M. O’Madden Burke. Fitzharris. Il tient cette station de taxis, parait-il, là-bas, au pont Butt. Holohan m’a dit ça. Vous connaissez Holohan ?

– Pose une patte et en retient deux, c’est ça ? dit Miles Crawford.

– Et ce pauvre Gumley y est aussi, c’est ce qu’il m’a dit, à garder des cailloux pour la municipalité. Veilleur de nuit.

Stephen se tourna, surpris.

– Gumley ? Vous êtes bien sûr ? Un ami de mon père, n’est-ce pas ?

– La paix avec Gumley, s’écria Myles Crawford irrité. Laissez Gumley à ses cailloux, qu’ils ne se fassent pas la malle. Regardez-moi ça. Qu’a fait Ignatius Gallaher ? Laissez-moi vous dire. Un coup de génie. Un télégramme sur le champ. Vous avez l’hebdo de *L’Homme Libre* du 17 mars ? Oui. Vous l’avez ?

Il faisait voler les feuillets de la collection à rebours, puis planta son doigt sur un passage.

– Prenez la page quatre, la publicité pour le café Bransome, disons. Vous l’avez ? Oui.

Le téléphone se mit à bourdonner.


UNE VOIX DANS LA DISTANCE


– Je prends, dit le professeur, qui partit.

– B, la grille du parc. Bien.

Son doigt bondissait d’un point à un autre en vibrant.

– T, la résidence du vice-roi. C l’endroit du meurtre. K l’entrée Knockmaroon.

La chair flasque de son cou battait comme un barbillon de coq. Un faux-col mal empesé fit une saillie soudaine et d’un geste brusque il le força à rentrer dans son gilet.

– Allo ? Le *Télégraphe du soir* à l’appareil… Allo ? Qui est là ? Oui… Oui… Oui.

– De F à P la route qu’a pris Écorche-Bouc avec la voiture pour l’alibi, Inchicore, Roundtown, Windy Arbour, Palmerston Park, Ranelagh. F.A.B.P. Pigé ? X c’est le pub de Davy en haut de la rue Leeson.

Le professeur reparut à la porte.

– C’est Bloom au téléphone, dit-il.

– Dites-lui d’aller se faire mettre, répliqua vivement le rédacteur-en-chef. X le pub de Burke, pigé ?


FUTÉ, TRÈS FUTÉ


– Futé, dit Lenehan. Très futé.

– Leur a donné comme sur un plateau, dit Myles Crawford, toute cette foutue histoire.

Cauchemar dont tu ne t’éveilleras jamais.

– J’ai tout vu, dit fièrement le rédacteur-en-chef. J’étais là. Dick Adams, un cœur d’or comme ce foutu Cork n’en a pas connu depuis, seigneur, et moi-même.

Lenehan fit une courbette imaginaire, annonçant :

– Madame ânonna Adam. Et la marine va venir à Malte.

– L’histoire ! s’écria Myles Crawford. La vieille dame de la rue du Prince arriva la première sur la scène. Ça a fait grincer quelques dents, sortir quelques mouchoirs. Par le biais d’une annonce. Gregor Grey en a fait le dessin. Ça lui mit le pied à l’étrier. Puis Paddy Hooper a travaillé Té-Pé qui l’a pris avec lui au *Star*. Maintenant il est passé chez Blumenfeld. Voilà la presse. Voilà le talent. Pyatt ! Leur papa à tous !

– Le père de la presse à sensation, confirma Lenehan, et le beau-frère de Chris Callinan.

– Allo ?… Vous êtes encore là ?… Oui, il est toujours là. Venez vous-même.

– Où trouver un homme de presse de ce moule-là aujourd’hui, hein ? s’écria le rédacteur-en-chef.

Il referma le classeur avec fracas.

– Frais tort, dit Lenehan à M. O’Madden Burke.

– Brillantissime, dit M. O’Madden Burke.

Le professeur MacHugh revint du bureau du fond.

– À propos d’invincibles, dit-il, saviez-vous que des vendeurs à la sauvette ont été amenés devant le juge…

– Oh oui, dit J. J. O’Molloy avec animation. Lady Dudley rentrait chez elle par le parc pour voir tous les arbres qui ont été arrachés par ce cyclone l’an dernier et pensait s’offrir une vue de Dublin. Et c’était plutôt une carte postale à la mémoire de Joe Brady ou Numéro Un ou Écorche-Bouc. Pile devant la résidence du vice-roi, imaginez ça !

– Ils ne sont plus bons qu’aux chiens écrasés, dit Myles Crawford. Pfff ! La presse et le barreau ! Où trouver aujourd’hui à la barre un homme comme ces types-là, comme Whiteside, comme Isaac Butt, comme O’Haggan qui parlait d’or. Hein ? Ah, foutaise. Pfff. De la barre au rabais.

Sa bouche continuait à tressauter, dessinant muette et nerveuse des moues de mépris.

Qui donc voudrait de cette bouche pour son baiser ? Qu’en sais-tu ? Pourquoi l’as-tu écrit alors ?


RIMES ET RAISONS

Bouche, couche. Est-ce que la bouche est une couche, en sorte ? Ou la couche une bouche ? Dans une mesure. Couche, souche, fourche, louche, touche. Des rimes : Deux hommes vêtus de même, de même allure, deux par deux.

>. . . . . . . . . . . . . . . . . . . . . . . . . . .*la tua pace*  
. . . . . . . . . . . . . . . . . . . . *che parlar ti piace*  
. . *mentrem che il vento, come fa, si tace*.

Il les voyait approcher, trois par trois, les jeunes filles, de vert, de rose, de roux, entrelacées, *per l’aer perso*, de mauve, de pourpre, *quelle pacifica oriafiamma*, d’or et de feu, *di rimirar de piu ardenti*. Mais moi, tous les vieillards, les pénitents, aux semelles de plomb, soussombre la nuit : bouche couche. Tombe, sombre.

– Défendez-vous, dit M. O’Madden Burke.


À CHAQUE JOUR SUFFIT…


J. J. O’Molloy, avec un pâle sourire, releva le gant.

– Mon chez Myles, dit-il, tirant sa cigarette de côté, vous forcez le sens de mes mots. Je ne prendrai pas la défense, comme on me le conseille présentement, de la tierce profession *qua* profession, mais votre ardeur corcagienne écorche les faits. Pourquoi ne pas citer à la barre Henry Grattan, et Flood, et Démosthène, et Edmund Burke ? Ignatius Gallaher, nous le connaissons tous, lui et son boss de Chapelizod, Harmsworth, messire de la presse à deux sous, et son cousin américain, du caniveau de Bowery, sans même mentionner les *Bonnes affaires de Paddy Kelly*, les *Bons mots de Pue* ou notre ami, *L’aigle de Skibbereen*, qui veille toujours. Pourquoi citer un maître de l’éloquence judiciaire tel que Whiteside ? À chaque jour suffit son quotidien.


AUX JOURS PASSÉS DU TEMPS JADIS


– Grattan et Floyd écrivaient pour le journal ici-présent, lui cria à la figure le rédacteur-en-chef. Volontaires Irlandais. Où êtes-vous aujourd’hui ? Fondé en 1763. Dr Lucas. Qui avez-vous aujourd’hui comme John Philpot Curran ? Pfff !

– Eh bien, dit J. J. O’Molloy, Bushe, K.C., par exemple.

– Bushe ? dit le rédacteur-en-chef. Eh bien, oui : Bushe, oui. Il lui reste un peu de ça dans le sang. Kendal Bushe ou Seymour Bushe je veux dire.

– Il aurait été nommé juge il y a bien longtemps, dit le professeur, sans cette… mais passons.

J. J. O’Molloy se tourna vers Stephen, et dit d’une voix basse et posée :

– Une des plus élégantes périodes qu’il m’a été donné d’entendre, je pense, est sortie des lèvres de Seymour Bushe. C’était dans cette affaire de fratricide, l’affaire Childs. Bushe le défendait.

*Et me versa dans le creux de l’oreille*

D’ailleurs, comment a-t-il découvert ça ? Il est mort dans son sommeil. Et l’autre histoire, la bête à deux dos ?

– Qu’était-ce ? demanda le professeur.


ITALIA, MAGISTRA ARTIUM


– Il parlait du droit des preuves, dit J. J. O’Molloy, de la loi romaine en opposition au code mosaïque antérieur, la *lex talionis*. Et il en vint au Moïse de Michel-Ange au Vatican.

– Ah.

– Quelques mots biens sentis, préfaça Lenehan. Silence !

Pause. J. J. O’Molloy sortit son étui à cigarette.

Faux suspense. Que de l’ordinaire.

Messager sortit pensivement sa boite d’allumettes et alluma son cigare.

J’ai souvent pensé depuis, en me remémorant cette époque singulière, que ce fut ce petit acte, en soi trivial, ce frottement d’allumette, qui décida du reste de nos deux vies.


UNE ÉLÉGANTE PÉRIODE


J. J. O’Molloy reprit, en modulant ses mots :

– Il en a dit : *Cette effigie de pierre, de musique figée, cornue et terrible, de forme humaine divine, ce symbole éternel de sagesse et de prophétie qui, si oncques ce que l’imagination ou la main du sculpteur ont arraché du marbre, transfiguré d’âme et d’âme transfigurant, mérite de vivre, mérite de vivre*.

Sa main gracile ondoyait en écho et accompagna la chute.

– Soit ! dit Myles Crawford sans attendre.

– Le souffle sacré, dit M. O’Madden Burke.

– Vous avez aimé ? demanda J. J. O’Molloy à Stephen.

Stephen, le sang flatté par la grâce du langage et du geste, rougit. Il prit une cigarette de l’étui. J. J. O’Molloy présenta l’étui à Myles Crawford. Lenehan alluma leurs cigarettes cette fois encore, et préleva son tribut en disant :

– Millibus mercibus.


UN HOMME D’UNE HAUTE MORALITÉ


– Le professeur Maggenis m’a parlé de vous, J. J. O’Molloy dit à Stephen. Que pensez-vous vraiment de ce ramassis d’hermétiques, les poètes de l’opale secret : A. E. le maistre-mystique ? C’est la Blavatsky qui a initié ça. Un joli sac à malices, cette vieille. A. E. a raconté à un journaliste yankee que vous étiez allé le trouver au point du jour pour le questionner sur les plans de conscience. Magennis pense que vous l’avez mené en bateau, A. E. C’est un homme de haute moralité, Magennis.

Parlé de moi. Qu’a-t-il dit ? Qu’a-t-il dit ? Qu’a-t-il dit de moi ? Ne demande pas.

– Non, merci, dit le professeur MacHugh, écartant d’un geste l’étui à cigarette. Attendez. Laissez-moi vous dire une chose. La plus belle démonstration d’éloquence que j’ai jamais entendue, c’est le discours que prononça John F. Taylor devant la société historique de l’université. Le juge Fitzgibbon, président actuel de la cour d’appel, s’était exprimé et l’objet du débat était un essai (une nouveauté à l’époque) qui prônait la renaissance de la langue irlandaise.

il se tourna vers Myles Crawford et dit :

– Vous connaissez Gerald Fitzgibbon. Vous pouvez donc imaginer le style de son discours.

– Il siège aux côtés de Tim Healy, dit J. J. O’Molloy, le bruit court, au conseil d’administration de Trinity College.

– Il siège aux côtés d’un enfant de chœur, dit Myles Crawford, en costume de premier-communiant. Continuez. Eh bien ?

– C’était le discours, notez bien, dit le professeur, d’un orateur accompli, plein d’une courtoisie hautaine et déversant dans une diction châtiée, je ne dirais pas l’ampoule de son courroux, mais plutôt le mépris de l’homme fier sur le nouveau mouvement. C’était alors un nouveau mouvement. Nous étions faibles, par conséquent nous ne valions rien.

Il referma ses lèvres longues et sinueuses un instant mais, pressé de poursuivre, porta une main étirée à ses lunettes et, le pouce et l’index touchant délicatement le cadre, tremblants, les ajusta à une nouvelle focale.


IMPROMPTU


D’un ton badin il s’adressa à J. J. O’Molloy :

– Taylor, comme vous devez le savoir, se relevait d’une maladie. Qu’il ait préparé son discours, je ne le crois pas, car il n’y avait pas un sténographe dans la salle. Son visage mat, mince, était envahi par un poil revêche. Il portait une cravate de soie blanche, lâche, et avait tout l’air (bien qu’il ne le fût pas) d’un mourant.

Son regard se tourna d’un coup, mais avec lenteur, du visage de J. J. O’Molloy vers celui de Stephen, puis se pencha d’un coup vers le sol, en recherche. Son col de lin, sans éclat, apparut derrière sa tête penchée, encrassé par sa chevelure fanée. Toujours en quête, il dit :

– Quand Fitzgibbon eut achevé son discours John F. Taylor se leva pour répondre. Voici, brièvement, autant que je puisse me les remettre en tête, ses paroles.

Il releva la tête avec décision. Ses yeux se perdirent une fois encore. De sots mollusques nageaient dans les verres épais, en avant en arrière, à la recherche d’une issue.

Il commença :

– *M. le Président, Mesdames et Messieurs : C’est avec une profonde admiration que j’ai écouté à l’instant les remarques que mon savant ami a adressées à la jeunesse d’Irlande. Il m’a paru être transporté dans une contrée distante, loin de ce pays, dans un âge distant, loin de cette époque, et que je me trouvais en Égypte ancienne et écoutais le discours de quelque grand-prêtre de l’endroit à l’adresse du jeune Moïse.*

Ils écoutaient, cigarettes en suspens, leurs fumées s’élevant en des tiges fragiles qui fleurissaient avec son discours. *Et que nos fumées ondoyantes*. Nobles paroles à l’horizon. Attention. Mais t’y essaierais-tu ?

– *Et il me parut entendre la voix de ce grand-prêtre égyptien qui s’élevait, aussi hautaine qu’elle était fière. J’entendais ses paroles et leur sens me fut révélé.*


D’APRÈS LES PÈRES


Il me fut révélé que sont bonnes ces choses qui cependant sont corrompues qui ni si elles étaient suprêmement bonnes ni à moins qu’elles ne soient bonnes ne sauraient être corrompues. Ah, malédiction ! Du saint Augustin.

– *Pourquoi vous autres Juifs n’acceptez-vous pas notre culture, notre religion et notre langue ? Vous êtes une tribu de pasteurs nomades : nous sommes un peuple puissant. Vous n’avez ni cités ni richesses : nos villes sont des ruches d’humanité et nos galères, trirèmes et quadrirèmes, lourdes de marchandises de toute sorte sillonnent les eaux du globe connu. Vous émergez à peine de conditions primitives : nous avons une littérature, un sacerdoce, une histoire millénaire et un gouvernement.*

Le Nil.

Enfant, homme, effigie.

Sur les rives du Nil, les Maries-nourrices s’agenouillent, berceau de jonc : un homme souple, au combat. De pierre ses cornes, de pierre sa barbe, cœur de pierre.

– *Vous vénérez une idole obscure et casanière : nos temples, sièges de majesté et de mystère sont les demeures d’Isis et Osiris, d’Horus et Amon Ra. À vous la servitude, la crainte et l’humilité : à nous le tonnerre et les mers. Israël est faible, et peu sont ses enfants. L’Égypte est légion, et terribles sont ses armes. Vagabonds et manouvriers, c’est ainsi qu’on vous appelle : le monde tremble à notre nom.*

Un rot muet poussé par la faim vint interrompre son discours. Il l’écrasa de sa voix, hardiment :

– *Mais, Mesdames et messieurs, le jeune Moïse eût-il écouté et accepté cette façon de voir, eût-il courbé la tête et courbé la volonté et courbé l’esprit devant cette arrogante admonestation, il n’aurait jamais délivré le peuple élu de sa maison de servitude, ni suivi en plein jour la colonne de nuées. Il n’aurait jamais, parmi les éclairs et au sommet du Sinaï conversé avec l’Éternel ni redescendu transcendé de lumière et d’inspiration, portant dans ses bras les tables de la Loi, gravée dans la langue du hors-la-loi.*

Il s’arrêta et les regardait, jouissant de leur silence.


DE MAUVAIS AUGURE – POUR LUI !


J. J. O’Molloy dit, non sans regret :

– Et cependant il mourut avant d’entrer en terre promise.

– Une disparition-soudaine-bien-que-d’une-maladie-chronique-souvent-précédemment-expectorée-à-l’instant-où, ajouta Lenehan. Et avec un grand avenir derrière lui.

On entendit la légion de pieds nus se précipiter dans le couloir, crépiter en grimpant l’escalier.

– Ça, c’est de l’art oratoire, dit le professeur, sans contradicteurs.

Autant en emporte le vent. Les osts à Mullaghmast et le Tara des rois. Les oreilles des porches sur des kilomètres. Les paroles que le tribun hurle et disperse aux quatre vents. Un peuple trouve refuge dans sa voix. Bruit d’enfer. Les annales akashiques de tout ce qui a jamais été en quelque lieu et temps que ce soit. Aimez-le et louez-le : non plus moi.

J’ai de l’argent.

– Messieurs, dit Stephen. Puis-je suggérer que la prochaine motion mise à l’ordre du jour soit l’ajournement immédiat de la chambre ?

– Vous me coupez le souffle. N’est-ce pas là d’aventure une gauloiserie ? demanda M. O’Madden Burke. Voilà l’heure où, m’est idée, Dame-jeanne, métaphoriquement j’entends, est des plus obligeantes à l’antique hostellerie.

– Ainsi soit-il, et par la présente résolument résolue. Que tous ceux qui sont en faveur fassent oui, annonça Lenehan. L’opposition non. Je déclare la motion adoptée. À quelle cabane à spiritueux ? Je donne mon vote à : Mooney’s !

Il ouvrit la marche, sermonnant au passage :

– Nous refuserons fermement le partage des eaux, de vie, ou non ? Oui, c’est non. Pas moyen ni d’aucune manière.

M. O’Madden Burke, qui suivait de près, se fendit d’une attaque amicale de son parapluie :

– En garde, Cyrano !

– Bon sang ne saurait mentir ! s’écria le rédacteur-en-chef, avec une claque à l’épaule de Stephen. Allons-y. Où sont ces foutues clés ?

Il farfouilla dans sa poche et en sortit les feuillets chiffonnés.

– Fièvre aphteuse. Je sais. Ça ira bien. Ça le fera. Où sont-elles donc ? Ça ira.

Il rempocha les feuillets et rentra dans le bureau du fond.


ESPÉRONS


J. J. O’Molloy, qui prenait sa suite, dit à voix basse à Stephen :

– J’espère que vous verrez cela publié de votre vivant. Myles, un instant.

Il entra dans le bureau et ferma la porte derrière lui.

– Venez donc, Stephen, dit le professeur. C’est pas mal, n’est-ce pas ? La vision prophétique. *Fuit Ilium !* Le sac de Troie la venteuse. Royaumes d’ici-bas. Les maîtres de la Méditerranée sont les Fellahins aujourd’hui.

Un premier crieur de journaux dévalait les escaliers sur leurs talons et se précipita dans la rue en hurlant :

– Spéciale des Courses !

Dublin. J’ai tant, tant à apprendre.

Ils tournèrent à gauche dans la rue Abbey.

– J’ai moi aussi une vision, dit Stephen.

– Oui ? dit le professeur, qui allongea son enjambée pour lui emboîter pas.

Un autre crieur les dépassa en trombe, criant sans marquer de pause :

– Spéciale des Courses !


DUBLIN, SALE ET CHÉRI


Gens de Dublin.

– Deux vestales de Dublin, dit Stephen, pieuses et vieillies, ont vécu cinquante et cinquante-trois ans dans l’allée Fumbally.

– Où est-ce ? demanda le professeur.

– Après Blackpitts, dit Stephen.

Nuit humide aux relents de pétrin maigre. Contre le mur. Visage luisant de suif sous un châle de futaine. Cœur en chamade. Annales Akashiques. Plus vite, chiri !

Va-s-y. Ose. Que la vie soit.

– Elles veulent voir le tout Dublin du haut de la colonne Nelson. Elles ont épargné trois et dix pence dans une tirelire boite-au-lettre de fer-blanc rouge. Elles en font tomber les piécettes de trois et six pence en secouant, et en extirpent les pennies avec la lame d’un couteau. Deux et trois en argent et un et sept en cuivre. Elles mettent leurs bonnets et leurs plus beaux habits et prennent aussi leur parapluie de peur qu’il ne pleuve.

– Sages vierges, dit le professeur MacHugh.


LA VIE À VIF

– Elles achètent pour un et quatre pence de fromage de tête et quatre tranches de pain de mie aux cantines du nord de la ville, rue Malborough, auprès de Mlle Kate Collins, propriétaire… Elles achètent vingt-quatre prunes bien mûres à une fillette au pied de la colonne Nelson, parce que le fromage de tête donne soif. Elles donnent deux piécettes de trois pennies au monsieur du tourniquet et entreprennent l’ascension, se dandinant dans l’escalier en colimaçon, grognant, s’encourageant mutuellement, tremblantes dans la pénombre, essoufflées, l’une demande à l’autre avez-vous le fromage, que Dieu soit loué et la Sainte Vierge, menaçant de redescendre, collant l’œil aux meurtrières. Dieu tout puissant. Elles n’auraient jamais cru que c’était si haut.

Elles s’appellent Anne Kearns et Florence MacCabe. Anne Kearns a un lumbago qu’elle frictionne à l’eau de Lourdes, qu’elle tient d’une dame qui en a eu une pleine bouteille d’un père de la Passion. Florence MacCabe s’offre un pied de porc et une bière double tous les samedis au souper.

– Antithèse, dit le professeur en hochant deux fois du chef. Vierges vestales. Je peux me les figurer. Qu’est-ce qui retient notre ami ?

Il se retourna.

Une volée de crieurs de journaux déboula des escaliers et s’éparpilla aux quatre vents, piaillant, les feuillets blancs battant l’air. Sur leurs talons apparut Myles Crawford en haut des marches, le visage cramoisi auréolé de son chapeau, en discussion avec J. J. Molloy.

– Venez donc, cria le professeur, agitant un bras.

Il reprit sa marche aux côtés de Stephen.


LE RETOUR DE BLOOM


– Oui, dit-il, je me les figure.

M. Bloom, hors d’haleine, empêtré dans un tourbillon de crieurs déchaînés aux portes du *Journal de Dublin et de l’Irlandais Catholique à un sou*, appela :

– M. Crawford ! Un instant !

– *Le Télégraphe* ! Spéciale des courses !

– Qu’y a-t-il ? dit Myles Crawford, ralentissant le pas.

Un crieur hurla à la figure de M. Bloom :

– Terrible tragédie à Rathmines ! Un enfant mordu par un soufflet !


ENTRETIEN AVEC LE RÉDACTEUR-EN-CHEF


– Juste cette annonce, dit M. Bloom qui avançait avec peine vers les marches, en soufflant, et tira la coupure de sa poche. Je viens à l’instant de parler avec M. Cleys. Il la renouvellera pour deux mois, il dit. Après il verra. Mais il veut un doublon pour attirer l’attention dans *Le Télégraphe* aussi, la feuille rose du samedi. Et il veut qu’on le copie si ce n’est pas trop tard je l’ai dit au conseiller Nanetti sur le *Gens de Kilkenny*. Je peux y avoir accès à la Librairie Nationale. Maison à Clés, vous voyez ? Il s’appelle Cleys. C’est un jeu de mots sur le nom. Mais il a pratiquement promis de faire le renouvellement. Mais il veut juste un petit coup de pouce. Qu’est-ce que je lui dirai, M. Crawford ?


Q.I.M.S.L.B.

– Pourrez-vous lui dire qu’il me suce la bite ? dit Myles Crawford, s’accompagnant d’un geste du bras pour plus d’effet. Sans plus de fioriture, dites-lui ça.

Un peu à cran. Avis de tempête. Tous partis boire. Bras dessus bras dessous. La casquette de marin de Lenehan à la ronde. Toujours le même baratin. Me demande si ce jeune Dedalus est l’instigateur. A une bonne paire de bottes aujourd’hui. Dernière fois que je l’ai vu on lui voyait les talons. A pataugé dans la boue quelque part. Que faisait-il donc à Irishtown ?

– Eh bien, dit M. Bloom, dont les yeux resurgirent, si je peux mettre la main sur le dessin je suppose que ça vaut bien un petit doublon. Il prendrait l’annonce, je pense. Je lui dirai…


Q.I.S.M.G.B.I.


– Qu’il suce ma grosse bite irlandaise, gueula Myles Crawford par-dessus l’épaule. Quand ça lui plaira, dites-le-lui.

Pendant que M. Bloom absorbait immobile l’information, esquissant enfin un sourire, il s’éloigna d’un pas foutraque.


TROUVER L’OSEILLE

– *Nulla bona*, Jack, dit-il, en se prenant le menton. Je suis sous l’eau. J’ai dû y passer moi aussi. Pas plus tard que la semaine dernière je cherchais un gars pour m’endosser une traite. Désolé, Jack. Le cœur y est, sachez-le. Et ce serait des deux mains si je pouvais trouver l’oseille de quelque façon.

J. J. O’Molloy avançait en silence, la mine longue. Ils rattrapèrent les autres et tous marchèrent de front.

– Après avoir mangé le fromage de tête et le pain et essuyé leurs vingts doigts sur le papier qui enveloppait le pain elles se rapprochent du garde-corps.

– Quelque-chose pour vous, expliqua le professeur à Myles Crawford. Deux vieilles Dublinoises en haut de la colonne Nelson.


ÇA C’EST DE LA COLONNE – VOILÀ CE QUE DINDE NUMÉRO UN EN DIT.


– Voilà du neuf, dit Myles Crawford. Voilà du tirage. En goguette. Deux vieilles filoutes, quoi ?

– Mais elles ont peur que la colonne ne tombe, continua Stephen. Elles voient les toits et se disputent les emplacements des différentes églises : le dôme bleu de Rathmines, celui d’Adam et Eve, de saint Laurence O’Toole. Mais ça leur donne le tournis de regarder, aussi retroussent-elles leurs jupes…


CES FEMELLES QUELQUE PEU TURBULENTES


– Calmos, dit Myles Crawford. Point de licence poétique. Nous sommes ici dans l’archidiocèse.


– Et s’accommodent-elles sur leurs jupons rayés, regardant d’en dessous la statue du manchot adultère.

– Manchot adultère, s’exclama le professeur. Ça me plait. Je vois l’idée. Je vois où vous voulez en venir.


DAMES FONT UN DON AUX CIT. DE DUBLIN  
OGIVES AÉROLITHES CÉLÉRIFÈRES, CROIT-ON


– Elles en attrapent un torticolis, dit Stephen, et sont trop fatiguées pour regarder, haut ou bas, ou parler. Elles mettent le sac de prunes entre elles, en tirent et mangent les prunes, une après l’autre, essuyant de leurs mouchoirs le jus qui dégouline de leurs bouches, et crachent les noyaux lentement, entre les barreaux.

Il termina d’un éclat de rire soudain, sonore et juvénile. Lenehan et M. O’Madden se retournèrent à l’entendre, firent un signe et traversèrent les premiers en direction de Mooney’s.

– Fini ? dit Myles Crawford. Tant qu’elles s’en tiennent là.


SOPHISTE COGNE HAUTAINE HÉLÈNE PLEIN DANS LA  
TROMPE. SPARTIATES GRINCENT DES MOLAIRES. ITHAQUIENS  
DÉCLARENT PEN’ CHAMPIONNE.


– Vous me rappelez Antisthène, dit le professeur, un disciple de Gorgias, le sophiste. On dit de lui que personne ne pouvait savoir s’il était plus amer envers les autres ou envers soi. C’était le fils d’un noble et d’une dépendante. Et il a écrit un libre où il enlève la palme de beauté à Hélène d’Argos pour la remettre à la pauvre Pénélope.

Pauvre Pénélope. Pénélope Rich.

Ils s’apprêtaient à traverser la rue O’Connell.


BIEN LE BONJOUR, CENTRAL !


Échelonnées irrégulièrement le long des huit lignes, des voitures de tramway attelées à des trolleys immobiles attendaient sur les voies, en direction ou en provenance de Rathmines, Rathfarnham, Blackrock, Kingstown et Dalkey, Sandymount Green, Ringsend et la tour
de Sandymount, Donnybrook, Palmerston Park et les hauts de Rathmines, inertes, échouées là par un court-circuit.


COMMENT ? – ET MÊME – OÙ ?


– Mais comment appelez vous ça ? demanda Myles Crawford. Où ont-elles trouvées les prunes ?


VIRGILIEN, DIT PÉDAGOGUE.  
SOPHOMORE PENCHE POUR LE VIEUX MOÏSE.


– Appelez ça, voyons, dit le professeur, dont les longues lèvres s’entrouvrirent alors qu’il réfléchissait. Appelez ça, voyons voir. Appelez ça : *Deus nobis haec otia fecit*.

– Non, dit Stephen. Je l’appelle *Vue sur la Palestine depuis le mont Pisgah, ou la parabole des prunes*.

– Je vois, dit le professeur.

Il rit abondamment.

– Je vois, reprit-il, avec un plaisir renouvelé. Moïse et la terre promise. Nous lui avons donné l’idée, ajouta-t-il pour J. J. Molloy.


HORATIO MARQUE LE SEPTENTRION EN CE BEAU JOUR DE JUIN


J. J. Molloy jeta un regard oblique, las, à la statue, et n’en dit pas plus.

– Je vois, dit le professeur.

Il se planta sur l’ilot pavé de sir John Gray et toisa d’en bas, entre les mailles de son sourire ironique, Nelson.


DOIGTS EN MOINS S’AVÈRENT TROP ÉMOUSTILLANTS  
POUR ROMBIÈRES EN GOGUETTE. ANNE SE TROUSSE, FLO  
SE TORTILLE – MAIS QUI PEUT LEUR JETER LA PIERRE ?


– Adultère manchot, dit-il avec un sourire sombre. Ça me titille, je dois dire.

– Ça a bien titillé les vieilles, dit Myles Crawford, pour dire les choses telles qu’elles sont, Dieu tout-puissant.