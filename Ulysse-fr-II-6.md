Policé, plein d’obligeance à leur égard, le bibliothécaire quaker ronronnait :

– Et nous avons, n’est-ce pas, ces pages inestimables du *Wilhelm Meister*. D’un grand poète, à propos d’un grand frère en poésie. Une âme indécise qui se lève en armes contre un océan d’adversités, déchirée par des doutes antagonistes, comme on le voit dans la vie réelle.

Il fit un pas en avant, un pas de cinq sur le cuir craquant de vachette, un pas en arrière, un pas de cinq sur le parquet cérémonieux.

Un assistant silencieux entrouvrit la porte mais à peine et lui fit un appel silencieux de la tête.

– De suite, dit-il, craquant sur le départ, s’attardant cependant. Le rêveur, magnifique et vain, qui désespère de la dure réalité. On a toujours le sentiment que le jugement de Goethe est si juste. Juste quand on prend du champ.

Du champ il pricraqua d’un pas de coranto. Tête chauve dans la porte, zélé zélote il prêta toute entière sa large oreille aux paroles de l’assistant : les entendit : et s’en fut.

Plus que deux.

– Monsieur de la Palice, ricana Stephen, était vivant quinze minutes avant sa mort.

– Avez-vous trouvé les six braves carabins, demanda John Eglinton avec la bile d’un ainé, pour écrire *Le paradis perdu* à votre dictée ? *Les tristesses de Satan* il appelle ça.

Souris. Souris du sourire de Cranly.

>*Il la chatouilla primum*  
*La tapota secondum*  
*puis lui mit son spéculum*  
*Car c’était un carabin*  
*Un joyeux cara...*  

– Je crois qu’il vous en faudrait un de plus pour *Hamlet*. Le sept est cher à l’esprit mystique. Les sept scintillants W.B. les appelle.

Scintillants des yeux le crane rougeâtre auprès de la lampe de travail coiffée de vert chercha le visage barbu dans la pénombre plus sombreverte, un griot, les yeux saints. Il rit tout bas : rire de boursier du Trinity : sans écho.

>*Satan l’orchestral, pleure des croix*  
*Des larmes comme des anges choient*  
*Ed egli avea del cul fatto trombetta.*  

Il tient mes folies en otage.

Cranly et ses onze preux de Wicklow pour libérer l’Irlande séminale. Kathleen l’édentée, ses quatre belles prairies d’émeraude, l’étranger dans sa maison. Et un de plus pour lui rendre hommage : *Ave, Rabbi* : les douze de Tinahely. Dans l’ombre du vallon il roucoule à leur intention. La jeunesse de mon âme je la lui ai donnée, nuit après nuit. Bon vent. Bonne chasse.

Mulligan a mon télégramme.

Folie. Persiste.

– Nos jeunes bardes irlandais, morigéna John Eglinton, ont encore à créer une figure que le monde mette à la hauteur du Hamlet de Shakespeare, saxon, quoique que je l’admire, comme le vieux Ben à l’époque, presque à l’idolâtrer.

– Toutes ces questions sont purement académiques, vaticina l’ombre de Russell. Je veux dire, qu’Hamlet soit Shakespeare ou James I ou Essex. Discussions de curés sur l’historicité de Jésus. L’art doit nous révéler des idées, des essences spirituelles dégagées de la forme. La question suprême pour une œuvre d’art c’est la profondeur de vie dont elle jaillit. La peinture de Gustave Moreau est une peinture d’idées. La poésie la plus profonde de Shelley, les paroles de Hamlet mettent nos esprits au contact de la sagesse éternelle, du monde des idées de Platon. Tout le reste ce sont des spéculations d’écoliers à l’usage des écoliers.

A. E. a raconté à un journaliste yankee. Que le grand crisse me crosse !

– Les maîtres furent d’abord des écoliers, dit Stephen ultra-poliment. Aristote a été l’écolier de Platon.

– Et l’est resté, il faut espérer, fit John Eglinton sobrement. On se l’imagine, un écolier modèle avec son diplôme sous le bras.

Il rit à nouveau pour le visage barbu qui maintenant souriait.

Spirituel informe. Père, Verbe et Souffle Saint. Tout-père, l’homme des cieux. Hiesos Kristos, magicien du beau, le Logos qui souffre en nous à tout instant. Ceci en vérité est cela. Je suis le feu sur l’autel. Je suis la graisse du sacrifice.

Dunlop, Judge, le plus noble parmi les Romains, A.E., Arval, l’Ineffable Nom, haut dans les cieux : K.H. leur maître dont l’identité n’est pas un secret pour les adeptes. Les frères de la grande loge blanche toujours à l’affut pour voir s’ils peuvent donner un coup de pouce. Le christ avec la sœur de la mariée, rosée de lumière, né d’une vierge in-spirée, sophia repentante, partie pour le plan du buddhi. La vie de l’ésotérique n’est pas pour monsieur Tout-le-Monde. M. T.l.M doit d’abord travailler sur son mauvais karma. Mme Cooper Oakley a une fois entrevu le fondamental de notre très illustre sœur H.P.B.

Fi de la vie ! fi ! Pfuiteufel ! ça s’fait pas d’regarder, mâme, alors y faut pas quand une dame elle montre son fondamental.

M. Best entra, grand, jeune, avenant, léger. Il portait dans sa main, avec grâce, un cahier, neuf, grand, propre, brillant.

– Cet écolier modèle, dit Stephen, trouverait les rêveries de Hamlet sur la vie post-mortem de son âme princière, l’improbable, insignifiant et morne monologue, aussi creuses que celles de Platon.

John Eglinton, le sourcil froncé, la moutarde au nez, dit :

– Ma parole, cela me fait bouillir d’entendre quiconque comparer Aristote et Platon.

– Lequel des deux, demanda Stephen, m’aurait banni de sa république ?

Dégainez vos définitions. La chevalité est la quiddité du tout-cheval. Faisceaux de tendances et éons, voilà ce qu’ils adorent. Dieu : ce bruit dans la rue : très péripatétique. Espace : ce qu’il vous faut bien foutre voir. Au travers d’espaces plus petits que les globules rouges du sang de l’homme ils rampent à plat ventre tels des milles-pattes derrière le fondement de Blake et jusque dans l’éternité dont ce monde végétatif n’est qu’une ombre. Accroche-toi au maintenant, à l’ici, au travers desquels tout futur plonge dans le passé.

M. Best s’avança, affable, vers ses collègues.

– Haines est parti, dit-il.

– Vraiment ?

– J’étais en train de lui montrer le livre de Jubainville. Il est très fan, vous savez, des *chants d’amour de Connacht* de Hyde. Je n’ai pas pu l’amener jusqu’ici pour suivre la discussion. Il est parti l’acheter chez Gill.

>*Vole, mon folio, file,*  
*Ignore du public la bile.*  
*Mal écrit, bien mal armé,*  
*dans cet Anglais décharné.*  

– La vapeur des tourbières lui monte à la tête, opina John Eglinton.

Nous avons le sentiment, en Angleterre. Bon larron. Parti. J’ai fumé son bacta. Gemme verte, étincelante. Une émeraude sertie dans l’anneau de la mer.

– Les gens n’ont pas conscience du péril que posent les chansons d’amour, avertit l’œuf aurique et sibyllin de Russell. Les lames de fond qui travaillent les révolutions de ce monde sont filles des rêves et visions d’un cœur de paysan, là sur la colline. Pour eux la terre n’est pas sol exploitable mais mère nourricière. L’air anémié de l’académie et du cirque enfante le roman à deux sous, la chanson de music-hall. La France enfante la plus exquise fleur de corruption en Mallarmé, mais la vie désirable n’est révélée qu’au pauvre de cœur, la vie des Phéaciens d’Homère.

À ces paroles M. Best vers Stephen tourna un visage sans aspérité.

– Mallarmé, vous savez, dit-il, a écrit ces merveilleux poèmes en prose que Stephen MacKenna avait la coutume de me lire à Paris. Celui sur *Hamlet*. Il dit : *il se promène, lisant au livre de lui-même*, vous savez, *lisant au livre de lui-même*. Il décrit Hamlet, joué dans une ville française, vous savez, une ville de province. Ils avaient fait une affiche.

Sa main libre traça, avec grâce, de petits signes dans l’air.

>*Hamlet*  
*Ou*  
*Le distrait*  
*Pièce de Shakespeare*  

Il répéta pour le sourcil nouvellement froncé de John Eglinton :

– *Pièce de Shakespeare*, vous savez. C’est si français. Le point de vue français. *Hamlet ou…*

– le mendiant dans la lune, termina Stephen.

John Eglinton rit.

– Oui, je l’imagine bien, dit-il. Un peuple des meilleurs, aucun doute, mais d’une superficialité désespérante sur certains aspects.

Somptueuse et stagnante exagération du meurtre.

– Un bourreau de l’âme, comme l’a appelé Robert Greene, dit Stephen. Ce n’est pas pour rien qu’il était le fils d’un boucher, maniant la lourde masse et crachant dans ses paumes. Neuf vies sont biffées pour une seule, celle de son père. Notre Père qui est au purgatoire. Les hamlets de kaki n’hésitent pas à tirer. La pagaille ruisselante de sang de l’acte cinq annonce le camp de concentration chanté par M. Swinburne.

Cranly, et moi son aide-de-camp muet, suivant de loin les batailles.

>*Portées et femelles d’ennemis sanguinaires que nul*  
*autre que nous n’aurait épargnées…*  

Entre le sourire du Saxon et les jacasseries yankees. Peste et choléra.

– Il prétend faire de *Hamlet* une histoire de fantômes, dit M. Eglinton à l’attention de M. Best. Comme le petit gros dans Pickwick il veut nous donner des frissons.

>*Oyez ! Oyez ! De grâce, Oyez !*

Ma chair l’entend : se hérisse, entend.

>*Si oncques tu as…*

– Qu’est-ce qu’un fantôme ? dit Stephen, parcouru d’un frisson d’énergie. Celui qu’estompe la mort, l’absence, le changement des mœurs. Le Londres élisabéthain est aussi loin de Stratford que Paris la corrompue de la vierge Dublin. Qui est ce fantôme qui, du *limbo patrum*, revient à ce monde qui l’a oublié ? Qui est le roi Hamlet ?

John Eglinton déplaça son armature sèche, se calant en arrière pour mieux juger.

Ça a pris.

– C’est à cette heure, un jour, la mi-Juin, dit Stéphane, sollicitant leur attention d’un regard rapide. Le pavillon est hissé sur le théâtre, près des quais. L’ours Sackerson grogne dans sa fosse, pas loin, jardin de Paris. Des gabiers qui ont navigué avec Drake mâchent leurs saucisses parmi les gens du sol.

Couleur locale. fourre-s-y tout ce que tu sais. Rends les complices.

– Shakespeare a quitté la maison huguenote de la rue Silver et marche le long du fleuve, devant les volières à cygnes. Mais il ne s’attarde pas à nourrir la femelle qui presse sa couvée de cygnets vers les roseaux. Le cygne d’Avon a d’autres pensées.

Unité de lieu. À moi, Ignace de Loyola ! hâte-toi !

– La pièce commence. Un acteur s’avance dans la pénombre, affublé d’une cotte de mailles, le rebus de quelque galant de la cour, un homme bien bâti avec une voix de basse. C’est le fantôme, le roi, un roi mais non le roi, et l’acteur est Shakespeare qui pendant toutes les années de sa vie qui ne furent pas vanité a étudié *Hamlet* afin de jouer le rôle du spectre. Il dit les mots à Burbage, le jeune acteur qui se tient devant lui de l’autre côté du rideau de suaire, l’appelant par son nom :

>*Hamlet, je suis l’esprit de ton père*,  

commandant son écoute. C’est à un fils qu’il parle, le fils de son âme, le prince, le jeune Hamlet, et au fils de sa chair, Hamnet Shakespeare, qui est mort à Stratford pour que celui qui porte son nom vive à jamais.

Est-il possible que l’acteur Shakespeare, un fantôme par l’absence, sous l’habit du Danemark enterré, un fantôme par la mort, en disant ses propres mots au prénom de son propre fils (Hamnet Shakespeare eût-il vécu, il aurait été le jumeau du prince Hamlet), est-il possible, je veux savoir, ou probable qu’il n’est pas tiré ou prévu la conclusion logique de ces prémisses : tu es le fils dépossédé : je suis le père assassiné : ta mère est la reine coupable, Ann Shakespeare, née Hathaway ?

– Mais cette intromission dans la vie de famille d’un grand homme, s’impatienta Russell.

Fieu Louis, es-tu là ?

– N’intéressent que le greffier de paroisse. Je veux dire, nous avons les pièces. Je veux dire quand nous lisons la poésie du *Roi Lear* que nous importe comment le poète a vécu ? Pour ce qui est de vivre nos domestiques peuvent faire ça pour nous, disait Villiers de l’Isle. Regarder par le trou de serrure, fouiner dans les potins de coulisse, les problèmes de boisson du poète, les dettes du poète. Nous avons *King Lear* : et cela est immortel.

Pris à témoin, le visage de M. Best approuva.

>*Roule sur eux de tes ondes et de tes eaux, Mananaan,*  
*Mananaan Maclir*  

Et maintenant, monsignor, cette livre qu’il vous a prêtée quand vous aviez faim ?

Diantre, j’en avais besoin.

Acceptes-tu ce noble.

Allez au ! Vous en avez dépensé le plus gros dans le lit de Georgina Johnson, la fille du pasteur. Contrition de l’esprit.

Avez-vous l’intention de rendre bourse ?

Oh, oui.

Quand ? Maintenant ?

Eh bien… Non.

Quand, alors ?

J’ai payé mon dû. J’ai payé mon dû.

Tiens bon. Il vient d’outre-Boyne. Le coin nord-est. Tu es redevable.

Minute. Cinq mois. Les molécules ont toutes changé. Je suis un autre je maintenant. Un autre a empoché la livre.

Bzzz. Bzzz.

Mais moi, entéléchie, forme de formes, suis moi par mémoire parce que sous les formes sans-cesse changeantes.

Moi qui ai péché et prié et jeûné.

Enfant que Conmee sauva de la férule.

Moi, je et Moi je.

A.E.Moi.Je.Vous.Dois.

– Avez-vous l’intention de jeter aux orties trois siècles de tradition ? demanda la voix corrosive de John Eglinton. Son fantôme à elle au moins a été enterré une fois pour toutes. Elle est morte, en ce qui concerne la littérature en tout cas, avant d’être née.

– Elle est morte, Stephen riposta, soixante-sept ans après sa naissance. Elle l’a vu venir au monde et en sortir. Elle a reçu ses premières étreintes. Elle a porté ses enfants et a posé des pennies sur ses yeux pour en tenir les paupières fermées quand il gisait sur son lit de mort.

Le lit de mort de mère. Bougie. Le miroir voilé d’un drap. Qui m’a mis au monde git là, paupières de bronze, sous quelques fleurs bon marché. *Liliata rutilantum*

J’ai pleuré seul.

John Eglinton observait le ver luisant retors de sa lampe.

– L’opinion générale est que Shakespeare a commis une erreur, dit-il, et s’en est sorti le plus vite, et le mieux qu’il a pu.

– Foutaise ! dit Stephen grossièrement. Un homme de génie ne commet pas d’erreurs. Ses erreurs sont filles de sa volonté et ouvrent les portes de la découverte.

Les portes de la découverte s’ouvrirent pour laisser passer le bibliothécaire quaker aux pieds craquant-glissant, sa tête chauve ses oreilles son zèle.

– On imagine mal qu’une mégère, énonça malignement John Eglinton, s’ouvre sur la découverte. Quelle découverte utile a pu tirer Socrate de Xanthippe ?

– La dialectique, répondit Stephen, et de sa mère l’art de mettre les idées au monde. Ce qu’il a appris de son autre femme Myrto (*Absit nomen !*), l’Epipsychidion de Socrate, aucun homme, ni femme, ne le saura jamais. Mais ni les secrets de sage-femmes ni les causeries au coin du feu ne l’ont sauvé des archontes du Sinn Fein et de leur fiole de cigüe.

– Mais Ann Hathaway ? s’oublia la voix calme de M. Best. Oui, il semble que nous l’oublions comme Shakespeare lui-même l’a oubliée.

Son regard passa de la barbe du pensif au crâne du corrosif, pour leur rappeler, les reprendre non sans bienveillance, puis continua vers la pomme chauve et rose du trembleur, sans faute et pourtant diffamé.

– Il avait bien pour deux sous d’esprit, et une mémoire qui n’avait pas fait l’école buissonnière. Il emportait un souvenir dans sa besace alors qu’il peinait sur la route de Romeville, en sifflant *La fille que j’ai laissée derrière*. Si le tremblement de terre n’avait pas marqué le coup nous saurions où placer pauvre Lièvre, assis à même sa forme, les abois de la meute, la bride cloutée et ses fenêtres azur à elle. Ce souvenir, *Venus et Adonis*, des amours inconstantes de Londres hante toutes les alcôves. Catherine la mégère est-elle disgracieuse ? Hortensio la dit jeune et belle. Pensez-vous que l’auteur d’*Antoine et Cléopâtre*, pèlerin passionné, avait ses yeux dans sa poche au point de choisir la greluche la plus moche de tout le comté de Warwick pour partager sa couche ? Bien : il l’a quittée et a conquis le monde des hommes. Mais ses héroïnes, jouées par de jeunes garçons, sont les héroïnes d’un jeune garçon. Leur vie, pensée, discours, leur sont prêtés par des mâles. Il a mal choisi ? Il a été choisi, de mon point de vue. À William les welléités, Ann, Hathaway, a son souhait. Morbleu, à elle le blâme. Elle lui a mis le grappin dessus, lolita à vingt-six ans. La déesse aux yeux gris qui recouvre le jeune adonis, ployant pour mieux conquérir, préfaçant l’acte en majesté, est une gueuse impudente de Stratford qui culbute un amoureux plus jeune qu’elle dans un champ de blé.

À quand mon tour ? Quand ?

Viens !

– Champ de seigle, dit M. Best rayonnant, heureux, levant son cahier neuf, heureux, rayonnant.

Il murmura pour eux, partageant sa blonde félicité :

>*Entre les sillons de seigle,*  
*Ces jolis campagnards se couchèrent.*  

Paris : le combleur comblé.

Une silhouette vêtue de bure barbue se dressa dans la pénombre et dévoila sa montre coopérative.

– J’ai bien peur qu’on ne m’attende au *Foyer*.

Le voilà qui se fane ? Terrain à exploiter.

– Vous partez ? demandèrent les sourcils agités de John Eglinton. Vous verrons-nous chez Moore ce soir ? Piper y sera.

– Piper ! pépia M. Best. Piper est de retour ?

Peter Piper piqua sa pipe pile à pic du pack de pickles.

– Je ne sais pas si je pourrai. Jeudi. Nous avons notre réunion. Si je peux m’échapper à temps.

La boite-à-Yogi-groggy des suites Dawson. *Isis dévoilée*. Leur bouquin en Pâli qu’on a essayé de mettre au clou. En lotus sous l’ambre d’un parasol il trône, Logos aztèque, officiant aux sphères astrales, leur âme suprême, mahamahatma. Les hermétistes fidèles attendent la lumière, mûrs pour l’ashram, anneau circulaire autour de lui. Louis H. Victory. T. Caulfield Irwin. Les dames du Lotus attentives au moindre signe de leur part, les glandes pinéales incandescentes. Empli de son dieu, il trône, Bouddha sous bananier. Gouffre d’âmes, engouffrant. Âmes mâles, âmes femelles, âmes amassées. Englouties gémissantes et pleurnichardes, tournoyantes en tourbillons, elles geignent.

>*Quintessence de la trivialité,*  
*Dans cet étui de chair une âme femelle des années s’est logée.*  

– Une surprise littéraire nous attend, à ce qu’on dit, dit, enthousiaste, amical, le bibliothécaire quaker. M. Russell, le bruit court, prépare un bouquet de vers de nos jeunes poètes. Notre impatience à tous confine à l’anxiété.

Confinant à l’anxiété il regarda le cône que projetait la lampe, où trois figures, illuminées, luisaient.

Vois donc. Souviens-toi.

Stephen baissa le regard sur un ample galurin, orphelin de tête, planté sur le manche de son bâton, surplombant son genou. Mon casque et mon épée. Les toucher légèrement des deux index. L’expérience d’Aristote. Un ou deux ? La nécessité est ce en vertu de quoi il est impossible qu’il en soit autrement. Ergo, un chapeau est un chapeau.

Écoute.

Colum le jeune et Starkey. George Roberts sur la partie commerciale. Longworth fera le battage à l’*Express*. Oh, et comment. J’ai apprécié le *vacher* de Colum. Oui, je pense qu’il possède cette singulière chose le génie. Pensez-vous vraiment qu’il a du génie ? Yeats admire son vers : *comme en terre agreste un vase attique*. Vraiment ? J’espère que vous pourrez venir ce soir. Malachi Mulligan vient lui aussi. Moore lui a demandé d’amener Haines. Connaissez-vous la plaisanterie de Miss Mitchell sur Moore et Martyn ? Que Moore est l’avatar de Martyn ? Terrible, n’est-ce pas ? Ils font penser à Don Quichotte et Sancho Panza. Notre épopée nationale reste à écrire, dit le Dr Sigerson. Moore est l’homme de la situation. Un Chevalier à la Triste Figure ici, à Dublin. En kilt safran ? Oh, oui, et il doit parler l’antique et sublime langue. Et sa Dulcinée ? James Stephen travaille quelques esquisses intéressantes. Nous prenons de l’importance, semble-t-il.

Cordelia. *Cordoglio*. La plus esseulée des filles de Lir.

Tarabiscoté. Et maintenant ton meilleur vernis français.

– Je vous serais très obligé, M. Russell, dit Stephen en se levant. Si vous vouliez avoir l’amabilité de remettre cette lettre à M. Norman…

– Oh, oui. S’il considère que c’est important cela passera. Nous avons une telle correspondance.

– Je comprends, dit Stephen. Merci.

Dieu vous le rende. Le journal des cochons. Kiffe-les-bœufs.

Synge m’a aussi promis un article pour le *Dana*. Nous lira-t-on ? Je le pressens. La Ligue Gaélique veut quelque chose en irlandais. J’espère que vous passerez cette nuit. Amenez Starkey.

Stephen s’assit.

Le bibliothécaire quaker laissa ceux qui prenaient congé. Son masque rougissant fit :

– M. Dedalus, vos vues sont des plus éclairantes.

Il craquait, en avant en arrière, se haussant vers les cieux d’une hauteur de talon, sur la pointe des pieds et, couvert par le bruit du départ, dit tout bas :

– Est-ce votre opinion, donc, qu’elle n’était pas fidèle au poète ?

Visage alarmé qui m’interroge. Pourquoi est-il venu ? Politesse, ou une illumination ?

– Où il y a réconciliation, dit Stephen, dût avoir lieu une rupture.

– Oui.

Fox-messie en chausses de cuir, qui se cache, un fugitif entre des fourches d’arbres affligés, fuyant les cors et les cris. Sans connaître femelle, marcheur solitaire dans cette quête. Des femmes il s’est acquises, gente gens, une putain de Babylone, des épouses de juges, des femmes de cabaretiers brutaux. Renard et oies. Et à New Place un étui de chair distendue et déshonorée qui jadis fut avenante, jadis aussi douce, aussi fraiche que la cannelle, et maintenant ses feuilles tombent, toutes, dépouillée, dans l’effroi de la tombe étroite, sans avoir reçu de pardon.

– Oui. Et vous pensez donc…

La porte se referma sur le sortant.

La quiétude reprit dans l’instant possession de la cellule, voutée, discrète, une quiétude tiède de couveuse.

Une lampe de vestale.

Ici, il médite de choses qui ne furent pas : ce que César eût accompli s’il avait écouté l’augure : ce qui aurait pu être : possibilités du possible en tant que possible : de choses non connues : le nom que portait Achille quand il vivait parmi les femmes.

Pensées mises en bière autour de moi, momifiées dans des casiers, embaumés aux aromates de mots. Thot, dieu des bibliothèques, un dieu-oiseau, couronné de lune. Et j’entendis la voix de ce grand-prêtre égyptien. *En des chambres peintes gorgées de livres d’argile*.

Elles sont immobiles. Jadis véloces dans les cervelles des hommes. Immobiles : mais un prurit létal est en elles, de me verser dans le creux de l’oreille une fable larmoyante, de m’exhorter à perpétrer leur vouloir.

– Certainement, songeait John Eglinton, de tous les grands hommes c’est le plus énigmatique. Nous ne savons rien, sinon qu’il a vécu et souffert. Pas même cela. D’aucuns ont satisfait notre demande. Un ombre s’étend sur tout le reste.

– Mais *Hamlet* est si personnel, ne trouvez-vous pas ? plaida M. Best. Je veux dire, un genre d’archive personnelle, vous savez, de sa vie privée. Je veux dire, je me soucie comme d’une guigne, qui est tué ou qui est coupable…

Il posa un cahier innocent sur le bord du bureau, bravache et souriant. Ses archives personnelles en version originale. *Ta an bad ar an tir. Taim in mo shagart*. Tartine-z-y de l’angliche, petit-jean.

Et ainsi parla petit-jean Eglinton :

– Je m’étais préparé à des paradoxes, après ce que nous avait dit Malachi Mulligan, mais autant vous prévenir que si vous voulez ébranler ma conviction que Shakespeare est Hamlet, la tâche sera âpre.

Un peu de patience.

Stephen soutint le feu des yeux défiants qui étincelaient, âpres, sous les sourcils froncés. Un basilic. *E quando vede l’uomo l’attosca*. Messer Brunetto, merci pour ces paroles.

– Tout comme nous, ou mère Dana, tissons et détissons nos corps, dit Stephen, jour après jour, leurs molécules tels des navettes, l’artiste tisse et détisse son image. Et tout comme la marque sur mon sein droit est là où elle était quand je suis né, bien que tout mon corps ait été maintes fois tissé de neuf, au travers du fantôme du père sans repos l’image du fils sans vie perce. Dans l’incandescence de l’imagination, quand l’esprit, dit Shelley, n’est qu’une braise sur le point de s’éteindre, ce que j’étais est ce que je suis et ce qu’en puissance je pourrais devenir. Ainsi dans l’avenir, frère du passé, je peux me voir tel que je suis assis ici maintenant, mais par réflexion de ce qu’alors je serai.

Drummond de Hawthorden t’a aidé à manœuvrer ce tourniquet.

– Oui, dit M. Best-le-jeune. Je sens Hamlet très jeune. L’amertume pourrait bien venir du père mais les passages avec Ophélie viennent sûrement du fils.

Il s’emmêle les pinceaux. Il est dans mon père. Je suis dans son fils.

– Cette marque est la dernière à partir, dit Stephen avec un rire.

John Eglinton fit une moue de dégoût.

– Si cela était la marque de naissance du génie, le génie s’achèterait en pharmacie. Les pièces des dernières années de Shakespeare que Renan admirait tant sont animées d’un autre esprit.

– L’esprit de réconciliation, s’anima le bibliothécaire quaker.

– Il ne peut y avoir de réconciliation, dit Stephen, s’il n’y a pas eu rupture.

Déjà dit.

– Si vous voulez connaître les évènements qui couvrent de leur ombre les temps obscurs du *Roi Lear*, de *Hamlet*, de *Troilus et Cressida*, observez où et comment l’ombre se dissipe. Ce qui attendrit le cœur d’un homme, naufragé des tempêtes les pires, Mis à l’épreuve, tel un second Ulysse, Périclès, prince de Tyr ?

Tête, sous un bonnet rouge et conique, souffletée, aveuglée d’embrun.

– Un enfant, une fille, qu’on place dans ses bras, Marina.

– Le penchant des sophistes pour les chemins de traverse des apocryphes est une quantité constante, détecta John Eglinton. Les grand-routes sont mornes mais elles mènent à la ville.

Bon Bacon : a tourné. Shakespeare l’avatar de Bacon. Jongleurs de secrets courant les grand-routes. Sur la trace du grand mystère. Quelle ville, mes bons maîtres ? noms masqués, mots couverts : A. E., eon : Magee, John Eglinton. À l’est du soleil et à l’ouest de la lune : *tit na n-og*. Le duo botté, canne à la main.

>*Combien de lieues jusqu’à Dublin ?*  
*Monsieur, dix et trois-vingts.*  
*Y serons-nous pour les chandelles ?*

– M. Brandes la tient, dit Stephen, pour la première pièce de la période finale.

– Vraiment ? Et qu’en dit M. Sidney Lee, ou M. Simon Lazarus, comme d’aucuns assurent qu’il se nomme ?

– Marina, dit Stephen, enfant de la tempête, Miranda, l’admirable, Perdita, celle qui fut perdue. Ce qui était perdu lui est rendu : l’enfant de sa fille. *Ma femme chérie*, dit Périclès, *ressemblait à cette jouvencelle*. Quel homme aimera la fille s’il n’a pas aimé la mère ?

– Grand-père, interjeta d’un murmure M. Best, *L’art d’être grand*…

– Ne verra-t-il pas renaître en elle, surchargée des souvenirs de sa propre jeunesse, une autre image ?

Sais-tu de quoi tu parles ? L’amour, oui. Un mot connu de tout homme. *Amor vero aliquid alicui bonum vult unde et ea quae concupiscimus…*

– L’image de soi, pour un homme doté de cette chose singulière le génie, est la mesure de toute expérience, matérielle ou morale. Un tel recours l’atteindra. Les images d’autres mâles de son lignage le rebuteront. Il verra en eux les tentatives grotesques de la nature de l’annoncer ou de le répéter.

Le front benoit du bibliothécaire quaker s’alluma d’une rose espérance.

– J’espère, pour l’édification du public, que M. Dedalus mènera sa théorie à bon port. Et nous devrions mentionner un autre commentateur irlandais, M. George Bernard Shaw. Sans oublier non plus M. Frank Harris. Ses articles sur Shakespeare dans *La revue du Samedi* étaient certainement brillants. Curieusement lui aussi dresse pour nous le tableau d’une liaison malheureuse, avec la dame en noir des sonnets. L’heureux rival est William Herbert, compte de Pembroke. J’avoue que si le poète doit être rejeté, un tel rejet semblerait plus conforme avec – comment dirai-je ? – notre idée de ce qui n’aurait pas dû être.

Content de lui il se tut et tint sa tête, humblement, au milieu d’eux, un œuf de pingouin, le prix du tournoi.

Sous les tu et les toi de graves mots d’époux. Aimes-tu Myryam ? Aimes-tu ton homme ?

– Ce peut être également vrai, dit Stephen. Il y a une phrase de Goethe que M. Magee aime à citer. Prenez garde à ce que vous souhaitez lors de votre jeunesse, car vous l’obtiendrez à l’âge mûr. Pourquoi envoie-t-il à une *buonaroba*, une baie où tous les hommes relâchent, une demoiselle de compagnie, de jeunesse scandaleuse, un petit seigneur pour la courtiser à sa place. Lui qui était un seigneur du verbe, qui avait fait de lui-même un gentilhomme-valet, qui avait écrit *Roméo et Juliette*. Pourquoi ? Sa confiance en soi a été tuée dans l’œuf. Il a été dompté dans un champ de blé (de seigle, devrai-je dire) et ne sera jamais vainqueur à ses propres yeux : valet, il se couchera toujours devant une dame de pique. Poser en Don Juan ne le sauvera pas. À défaire sur le tard il ne défera pas sa première défaite. La défense du sanglier l’a blessé là où l’amour saigne encore. Si la mégère est matée, il lui reste cependant l’arme invisible de la femme. Il y a, je le sens en ses mots, comme un aiguillon de chair qui le presse vers une autre passion, une ombre de la première, plus sombre, qui obscurcit sa compréhension de lui-même. Un sort similaire l’y attend et les deux rages s’entremêlent en un seul tourbillon.

Ils écoutent. Et dans les porches de leurs oreilles je verse.

– L’âme a été auparavant frappée mortellement, un poison versé dans le porche d’une oreille endormie. Mais ceux qui sont livrés à la mort dans leur sommeil ne peuvent connaître la manière de leur trépas, à moins que leur Créateur ne gratifie leurs âmes de ce savoir dans l’autre vie. L’empoisonnement et la bête à deux dos qui l’a précipité, le fantôme du roi Hamlet n’en pouvait savoir mais s’il n’avait pas été gratifié de cette connaissance par son créateur. C’est pourquoi son discours (cet Anglais décharné, désarmé) est tourné vers un ailleurs, en arrière. Ravisseur et ravi, ce qu’il voulait mais ne voulait pas, cela l’accompagne, des globes d’ivoire cerclés d’azur de Lucrèce à la poitrine d’Imogène, nue, avec son signe aux cinq pointes. Il s’en retourne, las de la création qu’il a bâtie pour se dérober à soi-même, un vieux chien léchant une vielle plaie. Mais, parce que la défaite est sa victoire, il passe à la postérité la personnalité intacte, sans que l’entame la sagesse qu’il a couchée sur le papier ni les lois qu’il a révélées. Sa visière est relevée. Il est un fantôme, une ombre maintenant, le vent entre les rochers d’Elseneur ou ce que vous voudrez, la voix de la mer, une voix qui n’est entendue que dans le cœur de celui qui est la substance de son ombre, le fils consubstantiel au père.

– Amen ! répondit le pas de la porte.

M’as-tu retrouvé, Ô mon ennemi ?

*Entr’acte*.

Un visage ribaud, d’une morosité de vicaire, Buck Mulligan, s’avança, puis aussitôt d’une insouciance bouffonne, vers l’accolade de leurs sourires. Mon télégramme.

– Vous parliez du gazeux vertébré, si je ne me trompe ? demanda-t-il à Stephen.

Tout de primevère il salua gaiement de son panama, comme d’un grelot.

Ils le reçoivent bien. *Was du verlachst wirst du noch dienen*.

Portée de railleurs : Photius, pseudomalachi, Johann Most.

Lui qui lui-même s’engendra par l’entremise de l’Esprit Saint et de lui-même s’envoya lui-même, entre lui-même et les autres, Qui, abusé par Ses démons, dépouillé, flagellé, fut cloué comme une chauve-souris sur une porte de grange, affamé sur l’arbre-croix, Qui Le laissa ensevelir, se releva, dévasta l’enfer, partit pour les cieux et là depuis ces dix-neuf cents ans siège à la droite de Soi-Même mais reviendra au dernier jour pour damner les vifs et les morts quand tous les vifs seront d’ores-et-déjà morts.

>*Glo–o–ri–a in ex–cel–sis De–o.*

Il lève ses mains. Les voiles tombent. Oh, des fleurs ! Cloches sur cloches sur cloches en chœur.

– Oui, en vérité, dit le bibliothécaire quaker. Une discussion des plus instructives. M. Mulligan, j’en jurerais, a lui aussi sa théorie sur la pièce et sur Shakespeare. Il est bon que tous les angles soient également présentés.

Il leur présenta un sourire isocèle.

Buck Mulligan réfléchissait, perplexe :

– Shakespeare, dit-il. Le nom me dit quelque-chose.

Un sourire fugace ensoleilla ses traits distendus.

– Mais bien sûr, dit-il, triomphant. Le type qui écrit à la manière de Synge.

M. Best se tourna vers lui.

– Haines vous a manqué, dit-il. L’avez-vous croisé ? Il vous verra plus tard à la D.d.B. Il est parti chez Gill acheter les *chants d’amour de Connacht*.

– Je suis passé par le musée, dit Buck Mulligan. Était-il ici ?

– Les compatriotes du barde, répondit John Eglinton, sont peut-être un peu fatigués de nos fulgurances théoriques. J’ai entendu dire qu’une actrice a joué Hamlet pour la quatrecenthuitième fois hier à Dublin. Vining soutenait que le prince était une femme. Ne s’est-il trouvé personne pour en faire un Irlandais ? Le juge Barton est, je crois, en quête d’indices. Il jure (son altesse, pas son honneur) par saint Patrick.

– La plus fulgurante de toutes est cette histoire de Wilde, dit M.Best, levant son fulgurant cahier. Ce *portrait de M. W. H.* où il démontre que les sonnets furent écrits par un certain Willie Hughes, *a man all hues*.

– Pour Willie Hugues, n’est-ce pas ? demanda le bibliothécaire quaker.

Ou Hughie Wills ? M. William lui-même, William Himself. W. H. : qui suis-je ?

– Pour Willie Hugues, je veux dire, amenda sans effort M. Best. Bien sûr, c’est tout en paradoxes, vous savez bien, Hughes et *hews* et *hues*, il taille, il colore, mais c’est si typique la manière dont il amène ça. C’est l’essence même de Wilde, vous savez. La légèreté de touche.

Son regard leur effleura légèrement le visage d’un sourire, un éphèbe blond. Essence de Wilde à usage domestique.

T’as fichtrement d’l’esprit. Trois lampées de spiritueux tu as bues sur les ducats de Dan Deasy.

Combien ai-je dépensé ? Oh, quelques shillings.

Pour une ronde de plumitifs. Humour acide et caustique.

L’esprit. Tu perdrais tous tes esprits pour ce fier plastron de jeunesse dans lequel il bouffonne. Linéaments de désir satisfait.

Y en aura plein d’aut’. Prends-la pour moi. Temps de faire paire. Jupiter, sers leur un bath temps de rut. Va-s-y, roucoule-moi-la.

Eve. Péché nu au ventre de blé. Un serpent l’enroule de ses anneaux, baiser-avec-crochet.

– Pensez-vous que ce ne soit qu’un paradoxe ? demandait le bibliothécaire quaker. Le railleur n’est jamais pris au sérieux quand il l’est le plus.

Ils parlèrent sérieusement du sérieux du railleur.
