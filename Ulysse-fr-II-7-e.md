– *Ma !* dit Almidano Artifoni.

Il contemplait, par-delà l’épaule de Stephen, la caboche bosselée de Goldsmith.

Deux charretées de touristes passèrent avec lenteur, leurs femmes à l’avant, cramponnées aux accoudoirs. Visages-pâles. Les bras des hommes entourant sans détour leur formes chétives. Leurs yeux passaient du Trinity au portique à colonnade aveugle de la banque d’Irlande où des pigeons roucoucoulaient.

– *Anch'io ho avuto di queste idee,* dit Almidano Artifoni, *quand’ ero giovine come Lei. Eppoi mi sono convinto che il mondo è una bestia. È peccato. Perchè la sua voce… sarebbe un cespite di rendita, via. Invece, Lei si sacrifica*.

– *Sacrifizio* incruento, dit Stephen, souriant, en faisant osciller son bâton depuis son milieu dans un lent mouvement de balançoire, en apesanteur.

– *Speriamo,* fit, amène, le visage rond, moustachu. *Ma, dia retta a me. Ci rifletta.*

À la hauteur de la main de pierre de Grattan qui, sévère, commandait une halte, un tram d’Inchicore déversa en ordre dispersé un orchestre militaire des Highlands.

– *Ci rifletteró*, dit Stephen, dont le regard descendit le long de la jambe massive sous le pantalon.

– *Ma, sul serio, Eh ?* dit Almidano Artifoni.

Sa lourde main se saisit fermement de celle de Stephen. Yeux humains. Qui s’arrêtèrent avec curiosité un instant puis se tournèrent rapidement vers le tram de Dalkey.

– *Eccolo,* dit Almidano Artifoni, pressé et amical. *Venga a trovarmi e ci pensi. Addio, caro.*

– *Arrivederla, maestro,* dit Stephen, soulevant son chapeau quand sa main lui fut rendue. *E grazie*.

– Di che ?, dit Almidano Artifoni. *Scusi, eh ? Tante belle cose !*

Almidano Artifoni, levant un bâton de partitions enroulées en signal, trotta sur ses robustes pantalons à la poursuite du tram de Dalkey. En vain trotta-t-il, en vain fit-il signe en traversant la cohue de chasseurs en kilt qui se bousculaient avec leurs instruments entre les grilles du Trinity.

***
