Le visage retombé, Buck Mulligan posa ses yeux sur Stephen pendant quelques instants. Puis, remuant du chef, il s’approcha, tira de sa poche un télégramme plié. Ses lèvres mobiles lurent, souriantes d’une délectation renouvelée :

– Télégramme ! dit-il. Merveilleuse inspiration ! Télégramme ! Une bulle papale !

– *Le sentimental est qui voudrait la jouissance sans assumer la dette, immense, pour la chose faite*. Signé : Dedalus. D’où avez-vous lancé ça ? La baraque ? Non. College Green. Avez-vous bu les quatre thunes ? La tante va en appeler à votre père inconsubstantiel. Télégramme ! Malachi Mulligan, Le Ship, rue basse de l’Abbaye. Ô, Arlequin sans égal ! Ô, Kinch-la-calote !

Il enfourna gaiement message et enveloppe dans une poche tout en se lançant dans une jérémiade patoisante :

– C’est ce que que j’vous dis, monsieur chéri, Tout drôle et chose que nous on étiait, moi-même et Haines, le moment qu’lui-même l’y a apporté. C’te murmure qu’on vous a levé pour une potion qui tue, ça vous en réveillerait un moine, j’crois bien, tout boiteux, l’bâton levé. Et nous et une heure et deux heures et trois heures chez Connery assis bien polis en attendant chacun sa pinte.

Il gémit :

– Et nous voilà là, peuchère, et vous à notre insu de nous qui nous envoyez vos conglomérances et nous les langues qui pendent une lieue à la ronde comme les frocards asséchés quand y crèvent d’s’en engorger une.

Stephen rit.

Buck Mulligan se pencha soudain, comme alarmé :

– Synge le vagabond vous cherche, dit-il, pour vous assassiner. Il a entendu dire que vous aviez compissé sa porte d’entrée à Glasthule. Il a chaussé ses brogues et est sorti pour vous tuer.

– Moi ! s’exclama Stephen. C’était votre contribution à la littérature.

Buck Mulligan partit en arrière, jubilant, et lança un rire vers le plafond sombre et indiscret.

– Vous assassiner ! riait-il.

Visage abrupt de gargouille surplombant l’amas d’abats hachés, rue Saint-André-des-Arts, guerroyant, contre moi. Mots de mots pour mots, palabras. Oisin et Patrick. Le faune qu’il avait rencontré dans les bois de Clamart, brandissant une bouteille de vin. *C’est vendredi saint !* Irlandais haschischin. Son image, dans l’errance, il a rencontrée. Et moi la mienne. J’ai rencontré un bouffon dans la forêt.

– M. Lyster, dit un assistant par la porte entrouverte.

– … où tout le monde trouve ce qui lui sied. Ainsi le juge Madden a-t-il relevé dans son *Journal de Maître Silence* les termes de vénerie… Oui ? Qu’est-ce que c’est ?

– Il y a ici un monsieur, monsieur, dit l’assistant, qui s’avançait en présentant une carte. De l’*Homme libre*. Il veut voir la collection des *Gens de Kilkenny* de l’an dernier.

– Certainement, certainement, certainement. Ce monsieur est-il ?…

Il prit la carte empressée, y jeta un œil, sans voir, la reposa non regardée, regarda, craqua, demanda :

– Est-il ?… Oh, là !

Alerte, sur un pas de gaillarde il s’en fut : dehors. Dans le corridor illuminé par le jour il parlait, dans les affres volubiles du zèle, enchaîné au devoir, si équanime, si aimable, si honnête et si tremblant.

– Ce monsieur ? *Le journal de l’homme libre* ? *Gens de Kilkenny* Mais bien sûr. Bonjour, monsieur. *Kilkenny*… Nous avons certainement…

Une silhouette patiente attendait, écoutait.

– …tous les principaux provinciaux… *Le libéral du Nord*, *L’enquêteur de Cork*, *La vigie d’Enniscorthy*, 1903… Voulez-vous s’il vous plaît ? Evans, conduisez ce monsieur… Si vous voulez bien suivre l’assi… Ou, permettez-moi je vous en prie… Par ici… S’il vous plaît monsieur…

Volubile, dévoué, il montra le chemin vers la section des provinciaux, tous les principaux, avec sur ses talons empressés une forme sombre et courbée.

Le porte se referma.

– Le youpin ! s’écria Buck Mulligan.

Il bondit et s’empressa sur la carte.

– Comment s’appelle-t-il ? Shlomo Moïse ? Bloom.

Il enchaîna :

– Jéhovah, le collecteur de prépuce, n’est plus. Je suis tombé dessus au musée où j’étais allé saluer Aphrodite née de l’écume. Lèvres grecques que jamais ne défigura la prière. Chaque jour, nous lui devons hommage. *Vie de la vie, tes lèvres embrasent*.

Il se tourna brusquement vers Stephen :

– Il vous connaît. Il connaît votre daron. Oh, j’en ai peur, il est plus Grec que les Grecs. Ses pâles yeux galiléens étaient posés sur son sillon mésial. Vénus Callipyge. Oh, quel tonnerre en ces reins ! *Le dieu pourchassant la vierge cachée*.

– Nous voulons en entendre davantage, trancha John Eglinton avec l’approbation de M. Best. Nous commençons à nous intéresser à Mme S. Jusqu’à présent nous pensions à elle, si est-ce que nous y pensions, comme d’une patiente Griselda, une Pénélope au coin du feu.

– Antisthène, le pupille de Gorgias, dit Stephen, enleva la palme de beauté à la poulinière du Kyrios Ménélas, Hélène l’Argienne, la jument de Troie dont le bois accueillit des héros à la douzaine, pour la donner à la pauvre Pénélope. Vingt ans vécut-il à Londres et, durant une partie de ce temps, il reçut un traitement égal à celui du Grand chancelier d’Irlande. Sa vie était opulente. Son art, plus que l’art du féodalisme, ainsi que l’appela Walt Whitman, est l’art de la profusion. Tourtes de hareng fumantes, gobelets verts de Xérès, sauces au miel, sucres de rose, massepains, pigeons aux groseilles, bugnes. Sir Walter Raleigh, quand on l’arrêta, avait sur le dos un demi-million de francs, dont un somptueux corset. Liz Tudor l’usurière avait assez de linge de corps pour rivaliser avec celle de Saba. Pendant vingt ans y vécut-il, oscillant entre l’amour conjugal et ses chastes délices et l’amour scortatoire et ses plaisirs infâmes. Vous connaissez l’histoire de Mannigham, la femme de bourgeois qui conviait Dick Burbage à sa couche après l’avoir vu dans *Richard III* et comment Shakespeare, qui l’avait entendue, sans faire de bruit pour rien, prit la vache par les cornes et, quand Burbage vint toquer à la porte, répondit depuis les couvertures du chapon : *William le conquérant est venu avant Richard III*. Et la galante petite chose, mistress Fitton, en selle et à hue-dia, et ses bichettes graciles, dame Pénélope Rich, une femme propre et honnête sied à un acteur, et les mômes des quais à deux sous la séance.

Cours la Reine. *Encore vingt sous. Nous ferons de petites cochonneries, minette ? Tu veux ?*

– La fine fleur de la haute société. Et sir William Davenant, de mère oxfordienne, son coq-au-vin offert au premier coq venu.

Buck Mulligan, les yeux pieusement tournés vers les cieux, éleva une prière :

– Sainte Marguerite Marie Aimelescoqs !

– Et la fille d’Henri-aux-six-femmes. Et autres bonnes amies des paroisses voisines, comme le chante messire Tennyson, gentlemen-poète. Mais au long de ces vingts années que supposez-vous que la pauvre Pénélope de Stratford ait fait, derrière les losanges des fenêtres ?

Fais et fais. Chose faite. Dans la roseraie de l’allée Fetter, celle de Gérard, herboriste, il marche, grisonnant et châtain. Une campanule a l’azur de ses veines. Paupières de Junon, violettes. Il marche. Une vie et c’est tout. Un corps. Fais. Fais donc. Au loin, parmi les relents de désir et de misère, des mains se posent sur une blancheur de chair.

Buck Mulligan martela sèchement des doigts le bureau de John Eglinton.

– Qui soupçonnez-vous ? lança-t-il.

– Mettons qu’il soit l’amoureux éconduit des sonnets. Éconduit une fois éconduit deux fois. Mais la courtisane sans vergogne l’a éconduit au profit d’un gentilhomme, son propre chéridamour.

Amour qui n’ose dire son nom.

– En bon Anglais, voulez-vous dire, recadra John Eglinton le carré, il aimait un gentilhomme.

Vieux murs que raye l’éclair des lézards. À Charenton je les observais.

– C’est ce qu’il semble, dit Stephen, quand il veut remplir pour lui, et pour toutes et chacune des matrices en jachère, le saint office que le palefrenier remplit pour l’étalon. Peut-être, à l’instar de Socrate, a-t-il eu pour mère une sage-femme, tout comme il eut une mégère pour femme. Mais elle, la courtisane lascive, ne rompait pas de vœux conjugaux. Deux forfaits hantent l’esprit de ce fantôme : des vœux rompus et le rustre sans cervelle vers lequel elle a tourné ses faveurs, frère de l’époux défunt. Anne la douce, c’est mon avis, avait le sang chaud. Qui courtise sans retenue courtise sans cesse.

Stephen se retourna, défiant, sur sa chaise :

– La charge de la preuve vous incombe, et non à moi, dit-il, le sourcil froncé. Si vous niez que dans la cinquième scène de *Hamlet* il la marque du fer de l’infamie dites-moi pourquoi il n’y a aucune mention d’elle pendant les trente-quatre ans qui séparent le jour où elle l’épousa du jour où elle l’enterra. Toutes ces femmes tombaient et entombaient leurs hommes : Mary, son brave John, Ann, son pauvre Willy, quand il vint à elle pour trépasser, rageant d’être le premier à partir, Joan, ses quatre frères, Judith, son époux et tous ses fils, Susan, son époux aussi, tandis que la fille de Susan, Elizabeth, pour parler comme grand-papa, mariait son deuxième, ayant au préalable tué son premier.

Oh, mais si, il y a mention. Pendant ses années d’opulence dans le Londres royal elle dût pour payer une dette emprunter quarante shillings au berger de son père. Expliquez vous, donc. Expliquez le chant du cygne aussi dans lequel il la recommande à la postérité.

Il faisait front à leur silence.

Et ainsi parla Eglinton :

           Le testament, voulez-vous dire.  
    Cela a été expliqué, je crois, par les juristes.  
    Elle avait droit à son douaire de veuve  
    Selon les usages. Sa connaissance des lois était grande  
    Nous disent nos magistrats.  

            Et Satan dans un rictus,  
    Le railleur :  
        Et par voie de conséquence il omit son nom  
    Du premier jet mais n’oublia pas  
    Les présents pour ses petites-filles, pour ses filles,  
    Pour sa sœur, pour ses vieux compères de Stratford  
    Et de Londres. Et par voie de conséquence quand on le pressa,  
    Comme je le crois, de la nommer  
    Il lui légua son  
    Second 
    Lit.

            *Punkt*

    Luiléguasson  
    Secondbon  
    Luiléguasson  
    Presquessibon  
    Sequébon
    Légunlit

Ouah !

– Les beaux campagnards avaient alors peu de mobilier, observa John Eglinton, et en ont toujours peu si nos œuvres pastorales respectent leur modèle.

– Lui était un opulent gentilhomme campagnard, dit Stephen, avec armoirie, domaine rural à Stratford et maison à Ireland Yard, un actionnaire et capitaliste, le promoteur d’une loi, un fermier à dîme. Pourquoi ne pas lui laisser son premier lit s’il lui souhaitait de ronfler paisiblement jusqu’à sa dernière nuit ?

– Il est clair qu’il y avait deux lits, un premier et un second, seconda M. Best avec finesse.

– *Separatio a mensa et a thalamo* renchérit Buck Mulligan, s’attirant les sourires.

– L’Antiquité mentionne des couches célèbres, grimaça Eglinton le second, accouchant d’un sourire. Laissez-moi réfléchir.

– L’Antiquité mentionne ce garnement apprenti du Stagyrite et sage chauve et païen, dit Stephen, qui quand il meurt en exil libère et dote ses esclaves, rend hommage à ses pères, demande à être enterré auprès des ossements de sa défunte épouse et prie ses amis de prendre soin d’une vieille maîtresse (n’oubliez pas Neil Gwynn Herpyllis) et de la laisser vivre dans sa villa.

– Voulez-vous dire qu’il est mort de cette manière ? s’inquiéta quelque peu M. Best. Je veux dire…

– Il est mort ivre mort, compléta Buck Mulligan. Un demi de bière est un mets digne d’un roi. Oh, je dois vous raconter ce que Downden a dit !

– Quoi ? demanda Besteglinton.

William Shakespeare et compagnie, SARL. Le William pour tous. Pour tout renseignement s’adresser à : E. Dowden, maison Highfield…

– Exquis ! soupira amoureusement Buck Mulligan. Je lui ai demandé ce qu’il pensait de l’accusation de pédérastie portée contre le barde. Il a levé les bras au ciel et dit : *Tout ce que nous pouvons dire c’est qu’on brûlait la chandelle par les deux bouts à l’époque.* Exquis.

Mignon.

– Le sentiment de beauté nous égare, dit beaudanssatristresse Best au laidissant Eglinton.

John le ferme répondit, sévère :

– C’est au médecin à nous expliquer ces termes. On ne peut pas avoir le beurre et l’argent du beurre.

C’est toi qui le dit ? Nous arracheront-ils, m’arracheront-ils, la palme de beauté ?

– Et le sentiment de propriété, dit Stephen. Il a tiré Shylock de sa propre, et profonde, poche. Le fils d’un agioteur en malt et usurier, il était lui-même agioteur en grain et usurier, avec ses dix mesures de grain accaparées pendant les émeutes de la faim. Ses emprunteurs sont sans nul doute ces divers dignes de foi que mentionnait Chettle Falstaff et qui témoignèrent de sa probité. Il fit un procès à un acteur, un confrère, pour le prix de quelques sacs de malt et prélevait un lourd tribut pour tout argent prêté. Comment le palefrenier d’Ostley et apprenti-machiniste aurait-il pu s’enrichir vite autrement ? Tout évènement lui apportait du blé à moudre. Shylock fait écho à la persécution des juifs qui suivit la pendaison et l’écartèlement du médicastre de la reine, Lopez, son cœur de juif arraché alors que le youpin vivait encore ; *Hamlet* et *Macbeth* à l’accession au trône d’un philosophastre écossais avec une faiblesse pour le rôti de sorcière. L’armada perdue espagnole devient sa tête de Turc dans les *Peines d’Amour perdues*. Ses fresques historiques, boursouflées, toutes voiles dehors, sont portées par une marée d’enthousiasme à la Mafeking. Les jésuites de Warwickshire sont jugés et nous voilà avec un portier qui théorise sur la restriction mentale. Le *Sea Venture* rentre des Bermudes et la pièce qu’admirait Renan se trouve écrite, avec son nigaud de Caliban, notre cousin américain. Les sonnets guimauves suivent ceux de Sidney. Quant à la fée Elizabeth, a.k.a. Bess poil-de-carotte, la vierge mal dégrossie qui a inspiré *Les Joyeuses Commères de Windsor*, laissons quelque meinherr d’Allémanie farfouiller sa vie entière à la recherche d’un sens caché dans les tréfonds du panier à linge.

Je trouve que tu t’en sors très bien. Mixe juste une mixture de théologicophilosophique. *Mingo, minxi, mictum, mingere*

– Prouvez que c’était un Juif, le mit au défi John Eglinton, l’attendant au détour. Votre doyen le tient pour catholique romain.

– Fabriqué en Allemagne, répondit Stephen, le cirage français champion pour scandales italiens.

– Homme aux mille esprits, rappela M. Best avec son seul esprit, Coleridge l’appela l’homme aux mille esprits.

*Amplius. In societate humana hoc est maxime necessarium ut sit amicitia inter multos.*

– Saint Thomas, commença Stephen…

– *Ora pro nobis* grogna Buck Mulligan en s’affalant sur une chaise,

d’où il éleva, mugissant, une complainte :

– *Pogue mahone ! Acushla Machree !* C’est détruit c’qu’on est depuis ce jour ! Détruit c’qu’on est pour sûr !

Chacun y alla de son sourire.

– Saint Thomas, dit, souriant, Stephen, dont j’aime à lire dans le texte les œuvres ventripotentes, traitant de l’inceste d’un point de vue différent de celui de la nouvelle école Viennoise dont M. Magee a parlé, l’assimile, à sa manière sage et toujours curieuse, à une avarice des émotions. Il veut dire que l’amour ainsi versé à un sang si proche est jalousement gardé de quelque étranger qui, peut-être, en avait soif. Les Juifs, que les chrétiens taxent d’avarice, sont parmi toutes les races les plus enclins au mariage consanguin. Les accusations sont filles de la colère. Les lois chrétiennes qui faisaient gonfler le bas-de-laine des juifs (qui, comme les Lollards, trouvaient abri dans la tempête) liaient aussi leurs affections comme dans des cerceaux d’acier. Que ce soit là vice ou vertu le vieux papa-nihil nous le dira au tribunal du jugement dernier. Mais un homme qui s’accroche aussi fermement à ce qu’il appelle ses droits sur ce qu’il appelle ses dettes s’accrochera tout aussi fermement à ce qu’il appelle ses droits sur celle qu’il appelle sa femme. Nul sire souriez voisin n’ira convoiter ni son bœuf ni sa femme ni son valet ni sa soubrette ni son bourricot.

– Ni sa bourrique, reprit comme une antienne Buck Mulligan.

– Quelle volonté de maltraiter le doux Will, glissa en douceur le doux M. Best.

– Quelle volonté ? reprit comme une antienne Buck Mulligan. On s’embrouille.

– La volonté de vivre, philosopha John Eglinton est pour la pauvre Ann, veuve de Will de et par sa volonté, la volonté de mourir.

– *Requiescat !* pria Stephen.

>*Et cette belle volonté d’agir ?*  
*s’est envolée avec l’ire du bel âge…*

– Elle git là dans une raide rigueur, étale sur ce second lit, la reine mal-meublée, quand bien même vous prouveriez qu’un lit était alors aussi rare qu’une automobile l’est aujourd’hui et que ses bas-reliefs faisaient la merveille de sept paroisses. Dans ses vieux jours elle s’entiche de prédicants (il y en avait à demeure à New Place, qui buvait son quart de vin d’Espagne au frais de la ville mais quant au lit où il dormait est de bon ton de ne pas demander), et apprit qu’elle avait une âme. Elle lut ou se fit lire ses bondieuseries, les trouvant plus à son goût que les *Joyeuses Commères* et, quand elle se soulageait nuitamment sur le jules, méditait *Œillets et accroches pour culottes de vrais croyants* et *Le tabac à priser du Saint-Esprit fait éternuer les âmes les plus dévotes*. Vénus avait les lèvres déformées par la prière. Contrition de l’esprit : remords de conscience. C’est un âge où la putasserie exténuée tâtonne vers son dieu.

– L’histoire démontre cela, *inquit Eglintonus Chronolologos*. Les âges se succèdent. Nous tenons de bonne source que les pires ennemis d’un homme seront toujours de sa propre famille et sous son propre toit. Je pressens que Russell est dans le vrai. Que nous importent sa femme et son père ? Je dirais volontiers que seuls les poètes du foyer ont une vie familiale. Falstaff n’avait rien du père de famille. À mon sens, le gras chevalier est sa création suprême.

Maigre chevalier, il repartit en arrière. Timide, renie tes gens, gens bien trop bien. Timide, il buvote avec les sans-dieu, et fauche la coupe. Un sire d’Antrim l’ultonienne le lui a ordonné. Passe le voir les jours d’embauche. M. Magee, monsieur, un monsieur est là pour vous. Moi ? Il dit qu’il est votre père, monsieur. Donnez-moi mon Wordsworth. Entre Magee Mor Matthew, un rude et rugueux piquier, crâne dur, braies et braguette boutonnées, ses bas de chausses chargeant la fange de dix forêts, une verge de bois vert à la main.

Le tien ? Il connaît votre daron. Le veuf.

Accourant du gai Paris à son nid de mort sordide, sur les quais j’ai touché sa main. La voix, une chaleur nouvelle, parlait. Dr. Bob Kenny s’occupe d’elle. Les yeux qui me veulent du bien. Mais ne me connaissent pas.

– Un père, dit Stephen, bataillant contre le désespoir, est un mal nécessaire. Il écrivit la pièce dans les mois qui ont suivi la mort de son père. Si vous soutenez que lui, un homme grisonnant avec deux filles bonnes-à-marier, avec trente-cinq ans d’âge, *nel mezzo del cammin di nostra vita*, et cinquante d’expérience, est le collégien imberbe de Wittenberg, vous devez admettre que sa mère de soixante-dix ans est la reine lascive. Non. Le cadavre de John Shakespeare n’arpente pas la nuit. Heures après heures il pourrit et se décompose. Il repose, déchargé de sa paternité, ayant déposé cet état mystique en son fils. Le Calandrino de Boccace fut le premier et le dernier homme à se sentir enceint. La paternité, en tant qu’engendrement conscient, est inconnue de l’homme. C’est un état mystique, une succession apostolique, d’engendreur seul à seul engendré. Sur ce mystère et non sur la madone que la rouerie de l’intellect italien a jeté en pâture aux populaces d’Europe, l’église est fondée, et fondée inébranlablement car fondée, comme le monde, macro et microcosme, sur le vide. Sur l’incertain, sur l’improbable. L’*Amor matris*, génitif subjectif et objectif, est, peut-être, la seule chose véritable dans la vie. La Paternité est, peut-être, une fiction légale. Qui oncques est père d’un quelconque fils qu’un fils quelconque devrait aimer ou lui le fils ?

Où diable veux-tu en venir ?

Je sais. Ferme-la. Va au diable. J’ai mes raisons.

*Amplius. Adhuc. Iterum. Postea.*

Es-tu condamné à faire cela ?

– Ils sont séparés par une honte charnelle si tenace que les annales criminelles du monde, maculées de toutes les autres nuances d’incestes et de bestialités, n’ont que peu de traces de sa transgression. Fils avec leurs mères, pères avec leurs filles, sœurs lesbiennes, amours qui n’osent dire leur nom, neveux et grand-mères, gibiers de prison et trous de serrures, reines et taureaux de foire. Le fils à naitre abîme la silhouette : né, il amène la douleur, divise les affections, accroît les soucis. C’est un mâle : sa croissance est le déclin de son père, sa jeunesse l’envie de son père, son ami l’ennemi de son père.

Rue Monsieur-le-Prince ça m’est venu.

– En quoi sont-ils liés par la nature ? Par un instant de rut, aveugle.

Suis-je un père ? Si je l’étais ?

Main recroquevillée, incertaine.

– Sabelius, l’Africain, le plus subtil des hérésiarques dans cette arène aux fauves, soutenait que le Père était Lui-même son Propre Fils. Le bouledogue d’Aquin, pour qui nul mot ne saurait être anathème, le réfute. Eh bien : si le père qui n’a pas de fils n’est pas un père le fils qui n’a pas de père peut-il être un fils ? Quand Rutlandbaconsouthamptonshakespeare ou quelque autre poète du même nom dans la comédie des erreurs a écrit *Hamlet* il n’était pas le père de son propre fils seulement mais, n’étant plus un fils, il était et se sentait le père de toute sa race, le père de son propre grand-père, le père de son petit-fils à naitre qui, soit dit en passant, oncques ne naquit, car la nature, telle que la comprend M. Magee, abhorre la perfection.

Eglintonœil, vif au plaisir, frétilla timide. Dardant un regard allègre, le joyeux puritain, au travers de l’églantine retorse.

Flatte. Rarement. Mais flatte.

– Lui-même son propre père, Filsmulligan soliloqua. Minute. Je me sens engrossé. J’ai un fœtus dans la cervelle. Pallas Athéna ! Une pièce ! La pièce v’là la chose. Place, que je mette bas !

Il étreignit son front gravide de ses deux mains-forceps.

– Quant à sa famille, dit Stephen, le nom de sa mère survit dans la forêt d’Arden. Sa mort le fit accoucher de la scène de Volumnia dans *Coriolan*. La mort de son jeune fils est celle du jeune Arthur dans le *Roi Jean*. Hamlet, le prince noir, est Hamnet Shakespeare. Qui sont les jeunes filles dans *La tempête*, dans *Périclès*, dans le *Conte d’Hiver*, nous le savons. Qui sont Cléopâtre, la marmite garnie de l’Égypte, et Cressida et Vénus nous pouvons le deviner. Mais un autre membre de la famille y figure.

– L’affaire se complique, dit John Eglinton.

Le bibliothécaire quaker, coi, sur la pointe des pieds entra, couinant, le masque hâtif, coincé, couard, couac.

Porte close. Cellule. Jour.

Tout ouïe. Trois. Eux.

Je vous il eux.

Viens, désordre.



Il avait trois frères, Gilbert, Edmund, Richard. Gilbert sur ses vieux jours racontait à quelques cavaliers qu’il avoit gagnu une entrée pour de r’in à la messe une fois du Maistre Collecteur, et avoit vu son frère Maistre Wull l’écriveur à pièces là-bas à Londonne dans une pièce de batailles avec un homme accroché à son dos. Les saucisses de l’entracte comblèrent l’âme de Gilbert. Il n’apparaît nulle part : mais un Edmund et un Richard figurent dans les œuvres du doux William.

        MAGEEGLINJOHN

Des noms ! Qu’y a-t-il dans un nom ?

        BEST

C’est mon nom, Richard, vous savez. J’espère que vous allez dire du bien de Richard, vous savez, pour le mien propre.

*(Rires)*

        BUCK MULLIGAN

*(Piano, diminuendo)*

>*Ainsi parla carabin Dick*  
*à son compère carabin Davy…*  

        STEPHEN

Dans sa trinité de Wills noirs, les vils branle-sacs, Iago, Richard le contrefait, Edmund dans le *Roi Lear*, deux portent les noms des méchants oncles. Pire, cette pièce était écrite ou en train d’être écrite alors même que son frère Edmund agonisait à Southwark.

        BEST

J’espère qu’Edmund va tout prendre. Je veux pas que Richard, mon nom…

*(Rires)*

        QUAKERLYSTER

*(A tempo)* Mais celui qui me fauche mon bon renom…

        STEPHEN

*(Stringendo)* Il a dissimulé son propre nom, un beau nom, William, dans les pièces, un figurant ici, un rustre là, comme un peintre de la vieille Italie place son visage dans un coin sombre de la toile. Il l’a révélé dans les sonnets où il y a du Will en surabondance. À l’instar de Jean de Gand son nom lui est cher, comme lui sont chères les armoiries qu’il a obtenues à force de flagorneries, sur bande de sable une lance d’or à pointe d’argent, honorificabilitudinitatibus, plus cher que sa gloire de plus grand branle-scène du pays. Qu’y a-t-il dans un nom ? C’est ce que nous nous demandons enfant à écrire le nom qu’on nous dit être le nôtre. Une étoile, une étoile diurne, un astre enflammé, est apparu à sa naissance. Il brillait en plein jour seul sur le firmament, plus que Vénus ne brille de nuit, et la nuit brillait sur le delta de Cassiopée, la constellation qui, étale, signe son initiale parmi les étoiles. Ses yeux la suivaient, alitée sur l’horizon, à l’est de l’Ours, pendant qu’il traversait les champs endormis de l’été, à minuit, revenant de Shottery et de ses bras à elle.

Tous deux satisfaits. Moi aussi.

Ne leur dis pas qu’il avait neuf ans quand l’astre s’est éteint.

Et de ses bras à elle.

Attends qu’on te courtise, qu’on te conquiert. Ah, nigaud. Qui viendra te séduire ?

Déchiffre les cieux. *Autontimorumenos. Bous Stephanoumenos*. Où se trouve ta configuration ? Stephen, Stephen, donne-toi la peine. S. D. : sua donna. *Già: di lui. gelindo risolve di non amare S. D.*

– Qu’en était-il, M. Dedalus, demanda le bibliothécaire quaker. Était-ce un phénomène céleste ?

– Une étoile la nuit, dit Stephen. Une colonne de nuées le jour.

Que dire de plus ?

Stephen regarda son chapeau, son bâton, ses bottes.

*Stephanos*, ma couronne. Mon épée. Ses bottes gâtent le tour de mes pieds. Acheter une paire. Mes chaussettes trouées. Mouchoir aussi.

– Vous tirez un bon parti du nom, admit John Eglinton. Votre propre nom est d’ailleurs étrange. Je suppose que cela explique votre humour fantasque.

Moi, Magee et Mulligan.

Fabuleux démiurge. L’homme-faucon. Tu volas. Vers où ? Newhaven-Dieppe, passager troisième classe. À Paris et de retour. Vanneau. Icare. *Pater, ait*. Barbotant sur l’onde, tombé, ruisselant. Vanneau tu es. Vanneau sois.

M. Best discretavidemment leva son livret pour dire :

– C’est très intéressant parce que le motif du frère, vous savez, nous le trouvons aussi dans les vieux mythes irlandais. Tout juste ce que vous dites. Les trois frères Shakespeare. Dans Grimm aussi, vous savez, les contes de fée. Le troisième frère qui à tous les coups épouse la beauté endormie et gagne le gros lot.

Le meilleur des frères Best. Bon, mieux, le *best*.

Le bibliothécaire quaker se rapprocha d’un cloche-pied.

– J’aimerais bien savoir, dit-il, lequel des frères vous… À ce que je comprends, vous suggérez qu’il eut maille à partir avec un des frères… Mais peut-être que j’anticipe ?

Il se surprit lui-même sur le fait : les regarda tous : se restreint.

Un assistant appela depuis la porte :

– M. Lyster ! Le Père Dineen veut…  

– Oh, le Père Dineen ! De suite.

Suivit de suite un craquement, vite, tout de suite, pfuit.

John Eglinton engagea le fer.

– Allons, dit-il. écoutons donc ce que vous avez à dire de Richard et Edmund. Vous les gardiez pour la fin, n’est-ce pas ?

– Vous demander de vous souvenir de ces deux nobles parents, tonton Richie et tonton Edmund, répondit Stephen, c’est j’en ai peur, trop demander peut-être. On oublie un frère aussi facilement qu’un parapluie.

Vanneau.

Où est ton frère ? La chambre des Apothicaires. Ma pierre à aiguiser. Lui, puis Cranly, Mulligan : maintenant ceux-là. Paroles, paroles. Acte. Acte de parole. Ils te raillent pour te mettre à l’épreuve. Sujet de l’action. Objet de l’action.

Vanneau.

Je suis fatigué de ma propre voix, la voix d’Ésaü. Mon royaume pour une boisson.

En avant.

– Vous direz que ces noms figuraient déjà dans les chroniques dont il a tiré la matière de ses pièces. Pourquoi a-t-il pris ceux-là plutôt que d’autres ? Richard, contrefait, fils bâtard d’une catin, file l’amour avec une veuve, Ann (qu’y a-t-il dans un nom ?), la courtise et la conquiert, la veuve joyeuse du fils de la catin. Richard le conquérant, le troisième frère, vint après William le conquis. Les quatre actes suivant pendent piteusement du premier. De tous ses rois Richard est le seul que Shakespeare ne couve pas de sa révérence, l’ange du monde. Pourquoi l’intrigue secondaire du *Roi Lear* où figure Edmond est-elle prélevée de l’*Arcadia* de Sidney et plaquée à même une légende celtique plus ancienne que l’histoire ?

– C’était la manière de Will, contra John Eglinton. Nous ne combinerions pas aujourd’hui une saga nordique avec un extrait de roman de George Meredith. *Que voulez-vous ?* dirait Moore. Il situe la Bohème au bord de la mer et son Ulysse cite Aristote.

– Pourquoi ? répondit de lui-même Stephen. Parce que le thème du faux-frère, du frère usurpateur, du frère adultère ou tout cela à la fois est pour Shakespeare, ce que le pauvre n’est point, toujours avec lui. La note du bannissement, bannissement du cœur, bannissement du foyer, résonne sans interruption depuis *Les Deux Gentilshommes de Vérone* jusqu’à ce que Prospero ne brise sa baguette, l’enfouisse sous une profondeur certaine de terre et noie son livre. Elle redouble au milieu de sa vie, se reflète dans une autre, se répète, protase, épistase, catastase, catastrophe. Elle se répète à nouveau quand il approche du tombeau, quand sa fille mariée Susan, c’est dans le sang, est accusée d’adultère. Mais ce fut le péché original qui obscurcit sa compréhension, affaiblit sa volonté et lui laissa un forte propension au mal. Tels sont les mots de messeigneurs les évêques de Maynooth : un péché original et, en tant que péché original, commis par un autre par le péché duquel il pèche aussi. C’est entre les lignes des derniers mots qu’il a écrits, cela se retrouve pétrifié sur sa pierre tombale où ses quatre os à elle ne doivent reposer. L’âge ne l’a pas amoindri. La beauté et la paix n’en ont pas eu raison. Cela parsème dans une variation infinie le monde qu’il a créé, dans *Beaucoup de bruit pour rien*, deux fois dans *Comme il vous plaira*, dans *La Tempête*, dans *Hamlet*, dans *Mesure pour Mesure*, et dans toutes les autres pièces que je n’ai pas lues.

Il rit pour délivrer son esprit de la tyrannie de son esprit.

Le juge Eglinton résuma.

– La vérité est à mi-chemin, affirma-t-il. Il est le fantôme et le prince. Il est tout en tout.

– Il l’est dit Stephen. Le garçon de l’acte un est l’homme mûr de l’acte cinq. Tout en tout. Dans *Cymbeline*, dans *Othello*, il est maquerelle et cocu. Il est sujet et objet de l’action. Amoureux d’un idéal ou d’une perversion, comme José il tue la Carmen réelle. Son intellect inexorable est le Iago cornu et fou-furieux qui sans cesse veut la souffrance du Maure qui est en lui.

– Coucou ! Coucou ! Caqueta Cuck Mulligan lubrique. Ô mot terrifiant !





