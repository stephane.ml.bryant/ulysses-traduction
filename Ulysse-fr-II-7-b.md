Un marin unijambiste manœuvrait le coin MacConnell sur ses béquilles, frôla la voiture des glaces Rabaiotti, et s’engagea d’une saccade dans la rue Eccles. Arrivé à la hauteur de Larry O’Rourke, en bras de chemises sur son pas-de-porte, il grogna sans aménité :

– Pour Albion…

D’un coup de balancier violent il dépassa Katey et Boody Dedalus, s’arrêta et grogna :

– patrie et beauté.

Au visage hâve, pâle de soucis de J. J. Molloy il fut répondu que M. Lambert était à l’entrepôt avec un visiteur.

Une forte dame s’arrêta, pécha une piécette dans son sac-à-main et la laissa tomber dans la casquette qu’on lui tendait. Le marin grommela un merci, jeta un œil aigre aux fenêtres sourdes, renfonça la tête et avança de quatre enjambées.

Il s’arrêta et grogna avec colère :

– Pour Albion…

Deux gamins aux pieds nus, qui suçaient de longs lacets de réglisse, s’arrêtèrent non loin, bouches-bées devant son moignon, barbouillés de jaune.

Il se projeta en avant par vigoureuses saccades, s’arrêta, leva la tête vers une fenêtre et aboya sourdement :

– patrie et beauté.

Le gai gazouillis continua à l’intérieur, aimable, sur une ou deux mesure, s’interrompit. On écarta le store de la fenêtre. Une pancarte *Appartements non-meublés* glissa du dormant et tomba. Un bras rond, nu, généreux brilla, se laissa voir, planté sur un corset blanc aux lacets tendus. Une main de femme lança une pièce par-dessus les grilles du parterre. Elle tomba sur le trottoir.

L’un des deux gosses accourut, la ramassa et la fit tomber dans la casquette de ménestrel en disant

– Voilà, monsieur.

***