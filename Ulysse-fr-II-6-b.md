Dôme obscur reçut, réverbéra.

– Et quel caractère que Iago ! s’exclama l’intrépide John Eglinton. En définitive Dumas *fils* (ou est-ce Dumas *père*) a raison. Après Dieu, c’est Shakespeare qui a le plus créé.

– De l’homme il ne se délecte pas, ni de la femme, dit Stephen. Il revient après une vie d’absence à cet endroit sur terre où il est né, où il a toujours été, homme et enfant, un témoin silencieux, et là, son voyage terminé, il plante son murier en pleine terre. Puis meurt. Le mouvement s’arrête. Les fossoyeurs enterrent Hamlet *père* et Hamlet *fils*. Enfin : roi et prince, dans la mort, avec musique de circonstance. Et, quoiqu’assassiné et trahi, pleuré par tout cœur tendre fragile car, Danoises ou Dublinoises, le regret du mort est le seul époux qu’elles se refusent à divorcer. Si vous aimez l’épilogue, observez-le avec attention : Prospero le prospère, le brave home récompensé, Lizzie, la pomme d’amour de grand-papa, et tonton Richie, le méchant homme qu’une justice poétique envoie là où vont les mauvais nègres. Rideau magistral. Il trouva en acte dans le monde extérieur ce qui était virtualité dans son monde intérieur. Maeterlinck dit : *si Socrate ouvre sa porte aujourd’hui, il trouvera Socrate assis sur le seuil. Si Judas sort ce soir, il ira vers Judas*. Une vie, c’est beaucoup de jours, jour après jour. Nous marchons à travers nous-mêmes, rencontrant voleurs, fantômes, géants, vieillards, jeunes hommes, épouses, veuves, beaux-faux-frères, mais toujours nous rencontrant nous-même. Le dramaturge qui a écrit l’in-folio de ce monde, et mal écrit (il nous donna d’abord la lumière et deux jours plus tard le soleil), le seigneur des choses telles qu’elles sont et que les plus romains des catholiques appellent *dio boia*, le dieu-bourreau, est sans aucun doute tout en en nous tous, palefrenier et boucher, et serait maquerelle et cocu aussi, si ce n’est que dans l’économie du ciel, prédite par Hamlet, il n’y a plus de mariage, l’homme en sa gloire, ange androgyne, étant à lui-même une épouse.

– *Eureka !* s’écria Buck Mulligan. *Eureka !*

Pris d’une joie soudaine il sauta sur ses pieds et atteint d’une enjambée le bureau de John Eglinton.

– Puis-je ? dit-il. Le seigneur a parlé à Malachi.

Il se mit à griffonner sur une fiche.

Prendre quelques fiches du comptoir en m’en allant.

– Ceux qui sont mariés déjà, dit M. Best hérault de douceur, vivront. Les autres resteront comme ils sont.

Il rit, encore garçon, à l’intention de John Eglinton, garçon confirmé.

Sans moitié, sans attraits, cauteleux, chaque nuit chacun pétrit son édition variorum de *La mégère apprivoisée*.

– Vous êtes une illusion, dit dans détour John Eglinton à Stephen. Vous nous avez fait faire tout ce chemin pour nous montrer un triangle amoureux. Croyez-vous en votre propre théorie ?

– Non, répondit sans attendre Stephen.

– Allez-vous l’écrire ? demanda M. Best. Vous devriez en faire un dialogue, vous savez, comme les dialogues platoniques qu’a écrits Wilde.

John Eclecticon eut un double sourire.

– Eh bien, dans ce cas, dit-il je ne vois pas pourquoi vous vous attendez à être payé puisque vous n’y croyez pas vous-même. Dowden croit qu’il y a quelque mystère dans *Hamlet* mais n’en dira pas plus. Herr Bleibtreu, l’homme que Piper a rencontré à Berlin, qui se fait le promoteur de l’hypothèse Rutland, croit que le secret est caché dans le monument de Stratford. Il va rendre visite au présent Duc, dit Piper, pour lui prouver que son ancêtre a écrit les pièces. Sa grâce en sera surprise. Mais il croit en sa théorie.

Je crois, Ô Seigneur, aide mon incroyance. C’est-à-dire, aide-moi à croire ou aide-moi à ne pas croire ? Qui aide à croire ? *Egomen*. En qui ne pas croire ? L’autre type.

– Vous êtes le seul contributeur de *Dana* qui demande son denier. Et puis je ne sais pas pour le prochain numéro. Fred Ryan veut de la place pour un article d’économie.

Fraidrinne. Deux deniers il m’a prêté. Passer le cap. Économie.

– Pour une guinée, dit Stephen, vous pouvez publier cet entretien.

Buck Mulligan planta là son gribouillage hilare, hilare : dressé puis soudain grave, il dit, versant une maligne mélasse :

– J’ai rendu visite au barde Kinch dans sa résidence d’été, sur les hauts de la rue Mecklenburgh et l’ai trouvé plongé dans l’étude de *Summa contra Gentiles* en compagnie de deux dames de gonorrhée, Nelly la fraicheur, et Rosalie, catin du quai du charbon.

Il rompit les amarres.

– Venez, Kinch. Venez, Ængus errant aux oiseaux.

Venez, Kinch. Vous avez mangé tout ce qu’on a laissé. Ouais. Je vous servirai rebuts et abats.

Stephen se leva.

Une vie, beaucoup de jours. Ceci aura une fin.

– Nous vous verrons ce soir, dit John Eglinton, *Notre ami* Moore dit que Malachi Mulligan se doit d’être présent.

Buck Mulligan parada panama et gribouillage.

– Monsieur Moore, dit-il, conférencier es lettres françaises à l’usage de la jeunesse irlandaise. Venez, Kinch, les bardes doivent boire. Pouvez-vous marcher droit ?

Hilare, il…

Écluser jusqu’à onze heures. Les folles nuits irlandaises.

Balourd…

Et Stephen suivit un balourd…

Un jour à la bibliothèque nationale nous eûmes une discussion. Tremblements. Son dos lourd me précédait. J’ulcère la corne de ses talons.

Stephen, saluant, puis tout entier atone, suivit un bouffon balourd, une tête bien soignée, fraiche du barbier, hors de la cellule voûtée, à la lumière du jour, éclatante et sans pensée.

Qu’ai-je appris ? D’eux ? De moi ?

Marche comme Haines maintenant.

Sur le livre des visiteurs Cashel Boyle O’Connor Fitzmaurices Tisdall Farell parade ses polysyllabes. Article : Hamlet est-il devenu fou ? La caboche du quaker papote dévotement de littérature avec le cureton.

– Oh, je vous en prie monsieur… J’en serai comblé…

Buck Mulligan amusé murmura plaisamment à sa muse, s’approuvant du museau :

– Un postérieur comblé.

Le tourniquet.

Est-ce… ? Chapeau à ruban bleu… Écrivant à son aise… ? Quoi… ? A levé le nez… ?

L’ovale de la balustrade : Mincius glissant sans à-coup.

Puck Mulligan, au casque panama, descendait marche par marche, donnant de la jambe, donnant dans le ïambe.

>*John Eglinton, mon jo, John,*  
*Dis, que ne prends-tu femme ?*  

Il bredouilla à la cantonade :

– Ô Eglinnetok le chinetoque ! Chin Chong Eg Lin Ton San Men Ton. Nous sommes allés faire un tour à leur studio, moi et Haines, au syndicat des plombiers. Nos acteurs sont en train d’accoucher d’un nouvel art pour l’Europe, comme les Grecs ou M. Maeterlinck. Le théâtre de l’Abbaye ! Je peux sentir la sueur pubienne des moines.

Il cracha à blanc.

Oublié : pas plus qu’il n’oublia la raclée que Lucy la pouilleuse lui infligea. Et quitta la *femme de trente ans*. Et pourquoi n’y eut-il pas d’autres enfants ? Et son ainé une fille ?

Esprit à retardement. Retourne.

L’austère reclus toujours là (il y a son beurre) et le suave jouvenceau, mignon d’agrément, Phédon à la blonde crinière, plaisir des doigts.

Euh… Je… Euh… voulais juste… J’ai oublié… Il…

– Longworth et M’Curdy Atkinson y étaient…

Puck Mulligan allait d’un pas léger, chantonnant :

>*Quand j’entends un cri dans l’impasse,*  
*Ou un bidasse qui jacasse et qui passe,*  
*Ma pensée, elle, ne fait qu’un bond*  
*La voilà sur F. M’Curdy Atkinson*  
*Dont un bois pend du croupion,*  
*et sur ce flibustier en jupon,*  
*qui jamais ne remplit son bidon,*  
*Magee, la bouche sans menton.*  
*Terrifiés de convoler ici-bas,*  
*ils se masturbèrent jusqu’au trépas.*

Blague toujours. Connais-toi toi-même.

Arrêté, une marche plus bas, me cuisine du regard. Je m’arrête.

– L’Arlequin endeuillé, gémit Buck Mulligan. Synge a laissé tomber le noir pour se vêtir selon la nature. Il n’y a que les corbeaux, les prêtres et le charbon anglais qui soient noirs.

Un rire déborda ses lèvres.

– Longworth en est malade, dit-il, de ce que vous avez écrit sur cette vieille morue de Gregrory. Ô grand jésuif inquisivrogne ! Elle vous dégote un taf au papelard et vous y allez et plombez ses délires. Vous n’auriez pas pu faire dans le Yeats ?

Il reprit sa descente, balayant le sol des pieds, balayant l’air de ses bras, avec grâce, scandant :

– Le plus beau livre qu’ait produit notre pays cette génération. On pense à Homère.

Il s’arrêta au pied de l’escalier.

– J’ai conçu une pièce pour arlequins, dit-il solennellement.

La salle des colonnes mauresques, l’entrelac des ombres. Disparus les neuf Maures arlequins aux chapeaux de carrés.

Variant les voix, mielleux, Buck Mulligan lut sur sa tablette :

>__*Tout homme est sa meilleure moitié*__  
*ou*  
__*Une lune de miel à pleine main*__  
*(une immoralité nationale en trois orgasmes)*  
*par*  
__*Burnes Mulligan*__  

Il dirigea une grimace d’idiot béat à Stephen, et dit :

– Le déguisement est mince, j’en ai peur. Écoutez plutôt.

Il lut, *marcato* :

– Personnages

**T**OBY PADTUNSKI (un Polonais sur la paille)  
**M**ORBACK (garde-chatte)  
**C**ARABIN **D**ICK  
et (d’une pierre deux coups)  
**C**ARABIN **D**AVY  
**M**ÈRE **G**ROGAN (porteuse d’eau)  
**N**ELLY LA **F**RAICHEUR,  
et
**R**OSALIE (catin du quai du charbon).

Il rit, la tête oscillant en tout sens, avançant, Stephen à sa suite : et confia hilare aux ombres, les âmes des hommes :

– Ah, cette nuit au Camden Hall où les filles d’Erin durent relever leurs jupes pour vous enjamber alors que vous gisiez couleur de mûres dans votre vomi multicolore et multifacétique !

– Le plus innocent fils d’Erin, dit Stephen, pour lequel elles les aient jamais levées.

Arrivé sur le seuil, sentant quelqu’un derrière lui, il se mit de côté.

Pars. C’est le moment. Où, donc ? Si Socrate ouvre sa porte aujourd’hui, si Judas sort ce soir. Pourquoi ? Cela m’attend tapi dans l’espace qu’en temps voulu je dois occuper, inéluctablement.

Ma volonté : sa volonté qui fait face. Entre, des océans.

Un homme passa entre eux, s’inclinant en guise de salut.

– Bonjour à nouveau, dit Mulligan.

Le portique.

Ici j’ai observé les oiseaux dans l’espoir d’un augure. Ængus aux oiseaux. Ils vont, ils viennent. La nuit dernière j’ai volé. Volé avec aisance. Les hommes s’étonnaient. Rue des gueuses après. Melon onctueux fruité il m’a tendu. Tu verras.

– Le juif errant, murmura Buck Mulligan dans un effarement de pitre. Avez-vous vu ses yeux ? Il les a posés sur vous avec concupiscence. Je te crains, vieux marin. Ô Kinch, tu es en péril. Munis-toi d’une culotte renforcée.

Bœuf d’Oxford.

Jour. La brouette du soleil sur l’arche du pont.

Un dos sombre les précédait, une allure de pard, descend, sort du portail, sous la herse barbelée.

Ils suivaient.

Offense-moi encore. Parle.

Un air amène dessinait les encoignures des maisons dans la rue Kildare. Pas d’oiseaux. Frêles, des toits s’élevaient deux panaches de fumée, en volutes, qui dans un doux tremblement étaient emportés.

Ne t’efforce plus. Paix des druides de Cymbeline, hiérophantique : de la vaste terre un autel.

>*Louons les dieux*  
*Et laissons les spires de nos fumets monter vers leurs narines*  
*De nos autels consacrés*  


























