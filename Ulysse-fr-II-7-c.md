Katey et Boody Dedalus poussèrent la porte de la cuisine embuée de vapeur.

– As-tu placé les livres ? demanda Boody

Devant les fourneaux Magggy renfonça par deux fois de sa cuiller une masse grise sous les bouillons savonneux et s’essuya le front.

– Ils n’ont rien voulu en donner, dit-elle.

Le père Conmee traversait les champs de Clongowes, ses chevilles chatouillées par le chaume sous leurs fines chaussettes.

– Où as-tu essayé ? demanda Boody

– Chez M’Guinness.

Boody frappa du pied et lança son cartable sur la table.

– La peste l’emporte, la grosse vache ! s’écria-t-elle.

Katey s’approcha et loucha vers les fourneaux.

– Qu’est-ce qu’il y a dans la marmite ? demanda-t-elle.

– Des chemises, dit Maggy.

Boody cria de rage :

– Flute, on a rien à manger ?

Katey souleva le couvercle de la bouilloire avec un coin de sa jupe sale, demandant :

– Et là, qu’est-ce qu’il y a ?

Un épais jet de vapeur lui répondit.

– Soupe de pois, dit Maggy.

– Où as-tu trouvé ça ? demanda Katey.

– Sœur Mary Patrick, dit Maggy.

Le commis agita sa clochette.

– Diling !

Boody s’attabla et dit affamée :

– Donne-nous ça.

Maggy versa la soupe jaune, épaisse de la bouilloire dans un bol. Katey, assise en face de Boody, dit sans élever la voix, pendant que ses doigts portaient à sa bouche des miettes oubliées :

– C’est déjà bien que nous ayons ça. Où est Dilly ?

– Parti au-devant du père, dit Maggy.

Boody, tout en cassant de gros morceaux de pain dans la soupe jaune, ajouta :

– Notre père qui n’êtes pas aux cieux.

Maggy, qui versait la soupe dans le bol de Katey, s’écria :

– Boody ! Tu n’as pas honte !

Un esquif, un rebut froissé, Élie arrive, descendait la Liffey, passait sous le pont de la ligne circulaire, léger, fusant au travers des rapides, de l’eau qui battait les piles du pont, voguant ver l’orient, laissant derrière lui carènes et chaînes d’ancrage, entre le vieux dock du bâtiment des douanes et le quai George.

***