Le commis à la porte de la salle des ventes Dillon agita à nouveau sa clochette par deux fois et se contempla dans le miroir couvert de craie du cabinet d’ébénisterie.

Dilly Dedalus, qui tuait le temps sur le trottoir, écouta les coups de clochettes, les cris du commissaire-priseur à l’intérieur. Quatre et Neuf. De si jolis rideaux. Cinq shillings. Rideaux commodes. À la vente, neufs, pour deux guinées. Cinq shillings, qui dit mieux ? Adjugé à cinq shillings.

Le commis leva sa clochette et l’agita :

— Diling !

Le ding de la cloche du dernier tour éperonna l’ardeur des cyclistes qui couraient le demi-mille. J. A. Jackson, W. E. Wylie, A. Munro and H. T. Gahan, les cous tendus et battant la mesure, négocièrent la dernière courbe devant la bibliothèque du Collège.

M. Dedalus, tiraillant une longue moustache, arriva du court William. Il s’arrêta auprès de sa fille.

— Il est grand temps, dit-elle.

— Tenez-vous droite pour l’amour de Dieu, dit M. Dedalus. Est-ce que vous voulez imiter votre oncle John, le cornettiste, tête dans les épaules ? Dieu ait pitié !

Dilly haussa des épaules. M. Dedalus posa ses mains dessus et les tira vers l’arrière.

— Tenez-vous droite, jeune fille, dit-il. Vous finirez par avoir une scoliose. Savez-vous à quoi vous ressemblez ?

Il laissa soudainement sa tête plonger vers avant, sa mâchoire tomber, et courba les épaules.

— Finissez, père, dit Dilly. Tout le monde vous regarde.

M. Dedalus se redressa et tira à nouveau sur sa moustache.

— Avez-vous trouvé de l’argent ? demanda Dilly.

— Où en trouverai-je ? dit M. Dedalus. Il n’y a personne à Dublin ne me prêterait un sou.

— Vous en avez trouvé, dit Dilly, le regardant dans les yeux.

— Comment le savez-vous ? demanda M. Dedalus, avec une grimace pince-sans-rire.

M. Kernan, enchanté de la commande qu’il venait de placer, marchait le port fier le long de la rue James.

— Je le sais, répondit Dilly. N’étiez-vous pas au Scotch House à l’instant ?

— Non, je n’y étais point, dit M. Dedalus en souriant. Sont-ce les petites Sœurs qui vous ont enseigné ce culot ? Tenez.

Il lui tendit un shilling.

— Voyez ce que vous pouvez faire avec ça, dit-il.

— Je parie que vous en avez eu cinq, dit Dilly. Donnez-moi plus que ça.

— Attendez un peu, dit M. Dedalus, menaçant. Vous êtes comme les autres, alors ? Une meute de petites garces insolentes maintenant que votre pauvre mère est morte. Mais attendez un peu. Je vais vous expédier vite fait et bien le bonjour. Racaille ! Je serai vite débarrassé de vous. Ni froid ni chaud si j’étais raide mort. Il est mort. L’homme du dessus est mort.

Il la planta là et continua. Dilly lui courut après et tira sur son manteau.

— Eh bien, qu’est-ce qu’il y a ? dit-il, s’arrêtant.

Le commis agita sa clochette dans leur dos.

— Diling !

— Maudit sois-tu sacré braillard, s’écria M. Dedalus, retournant sa colère contre lui.

Le commis, à entendre le commentaire, secoua le battant de sa clochette : mais faiblement :

— Dang !

M. Dedalus braqua ses yeux sur lui.

— Regardez-le, dit-il. C’est instructif. Je me demande
s’il va nous permettre de parler.

— Vous en avez eu plus que ça, père, dit Dilly.

— Je vais vous apprendre un petit tour à ma façon, dit M. Dedalus. Je vais vous planter là comme Jésus a planté les juifs. Regardez, c’est tout ce que j’ai. J’ai eu deux shillings de Jack Power, et j’ai dépensé deux pennys pour arriver rasé à l’enterrement.

Il montra nerveusement une poignée de pièces de cuivre.

— Ne pouvez-vous pas chercher de l’argent quelque-part ? dit Dilly.

M. Dedalus réfléchit et hocha la tête.

— C’est-ce que je vais faire, dit-il gravement. J’ai cherché tout le long du caniveau de la rue O’Connell. Je vais essayer celui-là maintenant.

— Vous êtes très drôle, dit Dilly, souriant jaune.

— Tenez, dit M. Dedalus en lui tendant deux pennys. Prenez-vous un verre de lait et un petit pain ou un quelque-chose. Je serai à la maison dans pas longtemps.

Il mit le reste des pièces dans sa poche et reprit sa marche.

Le cortège du vice-roi, salué par d’obséquieux policiers, sortit de Parkgate.

— Je suis sûre que vous avez un autre shilling, dit Dilly.

Le commis cogna bruyamment.

M. Dedalus s’en fut sous le fracas, murmurant la bouche en cœur :

— Les petites sœurs ! Chères petites choses ! Oh, elles ne feraient pas de mal à une mouche ! Oh, Certainement pas ! N’est-ce pas sœur Monica !

***





