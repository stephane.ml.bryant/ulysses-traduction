Deux visages roses apparurent dans la flamme de la petite torche.

– Qui-va-là ? demanda Ned Lambert. Est-ce Crotty ?

– Ringabella et Crosshaven, répondit une voix dont le pied cherchait à tâtons un appui.

– Bonjour, Jack, est-ce vous ? dit Ned Lambert, levant en guise de salut sa latte flexible sous les arceaux dansants. Venez donc. Faites attention à vos pas par ici.

Dans la main levée de l’ecclésiastique, une allumette phosphorée acheva sa combustion en une longue flamme douce puis tomba, abandonnée. À leurs pieds, un dernier éclat rouge, puis le noir : et un air aux relents de moisi se referma su eux.

– Comme c’est intéressant !, dit dans l’obscurité une voix à l’accent raffiné.

– Oui, monsieur, dit Ned Lambert avec conviction. Nous nous tenons dans la chambre du conseil historique de l’Abbaye sainte Marie où Thomas le soyeux proclama sa rébellion en 1534. C’est l’endroit le plus historique de Dublin. O’Madden Burke va écrire quelque chose là-dessus un de ces jours. L’ancienne banque d’Irlande était par là jusqu’au temps de l’union et le temple original des juifs était aussi ici avant qu’ils ne construisent leur synagogue sur la route d’Adelaïde. Vous n’étiez encore jamais venu ici Jack, n’est-ce pas ?

– Non, Ned.

– Il vint en chevauchant par le chemin de la Dame, dit la voix raffinée, si ma mémoire est bonne. La demeure des Kildares se trouvait dans Thomas Court.

– C’est exact, dit Ned Lammbert. C’est tout à fait exact, monsieur.

– Si vous aviez l’obligeance, dit l’ecclésiastique, la prochaine fois, de m’autoriser peut-être…

– Mais certainement, dit Ned Lambert. Amenez l’appareil quand vous voulez. Je ferai enlever ces sacs qui bloquent les fenêtre. Vous pouvez prendre d’ici ou d’ici.

Dans la lumière encore chétive il allait de-ci de-là, frappant les sacs de graines de sa latte et désignant sur le sol les endroits propices.

D’un long visage surplombant l’échiquier descendaient une barbe et un regard fixe.

– Je vous suis profondément reconnaissant, M. Lambert. Je ne veux pas abuser plus de votre temps précieux…

– Je vous en prie, monsieur, dit Ned Lambert. Revenez quand il vous plaira. La semaine prochaine, disons. Pouvez-vous voir ?

– Oui, oui. Bonne après-midi M. Lambert. Enchanté d’avoir fait votre connaissance.

– Tout le plaisir est pour moi, monsieur, répondit Ned Lambert.

Il suivit son hôte jusqu’à l’ouverture, puis envoya valser sa latte entre les piliers. Accompagné de J. J. Molloy il sortit lentement sur l’Abbaye sainte Marie où des débardeurs chargeaient des barges de sacs de caroube et de semoule de palme, O’Connor, Wexford.

Il s’arrêta pour lire la carte qu’il tenait en main.

— Le révérend Hugh C. Love, Rathcoffey. Présente adresse : saint Michael, Sallins. Charmant garçon. Il écrit un livre sur les Fitzgeralds, m’a-t-il dit. Il en connait un bout en histoire, ma parole.

Avec une lente précision la jeune femme enleva de sa jupe légère une brindille qui s’accrochait.

— J’ai pensé que c’était une nouvelle conspiration des poudres, dit J. J. Molloy.

Ned Lambert claqua des doigts.

— Bon dieu ! s’écria-t-il. J’ai oublié de lui raconter l’histoire du comte de Kildare, après qu’il eut mis le feu à la cathédrale de Cashel. Vous la connaissez ? *Je suis bougrement désolé de ce que j’ai fait*, dit-il, *mais je prends Dieu à témoin que je croyais l’archevêque dedans*. Mais peut-être qu’il n’apprécierait pas, hein ? Bon dieu, je la lui dirai quand même. C’était le grand comte, Fitsgerald le Mor. Ils avaient le sang chaud, les Geraldines, pas un pour rattraper l’autre.

Les chevaux à son passage secouèrent nerveusement leurs harnais distendus. Il donna une claque à la croupe pie qui frémissait à portée de sa main et s’écria :

— Wouauh, fiston !

Il se tourna vers J. J. Molloy et demanda :

— Eh bien, Jack. Qu’y a-t-il ? Qu’est-ce qui ne va pas ? Attendez. Accrochez-vous.

Il s’immobilisa, bouche béante, tête arquée vers l’arrière puis, après un instant éternua bruyamment.

— Atchoum ! fit-il. La poisse !

— La poussière des sacs, dit poliment J. J. Molloy.

— Non, haleta Ned Lambert, j’ai attrapé… froid la nuit d’avant… quelle poisse… la nuit d’avant-hier… il y avait un sacré courant d’air.

Il tenait son mouchoir prêt pour le prochain…

— J’étais… Glasnevin ce matin… pauvre petit… comment l’appelez-vous… Atchoum !… Purée !

***