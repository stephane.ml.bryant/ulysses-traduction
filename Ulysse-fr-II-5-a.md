Le cœur en chamade il poussa la porte du restaurant Burton. La puanteur le saisit tremblant à la gorge : l’âcre jus des viandes, légumes passés. Vois : les animaux se repaissent.

Des hommes, des hommes, des hommes.

Juchés sur de hauts tabourets du bar, chapeau en arrière, attablés, redemandant du pain : offert par la maison, éclusant leurs verres, dévorant à pleine gueule leur pâtée, les yeux exorbités, torchant des moustaches souillées. Un jeune homme blême, visage de suif, faisait reluire gobelet couteau fourchette et cuillère avec sa serviette de table. Nouveau contingent de microbes. Un homme avec un bavoir d’enfant maculé de sauce accroché au menton s’envoyait en gargouillant des pelletées de soupe dans le gosier. Un homme recrachait dans son assiette : cartilage à moitié mastiqué : gencives : sans dent pour le ma-ma-macher. Côtelette grillée. S’empiffrant pour en finir plus vite. Yeux tristes d’alcoolo. Les yeux plus gros que le ventre. Est-ce que je suis comme ça ? Se voir avec les yeux des autres. Qui a faim se plaint. Turbinent des dents et des mâchoires. Attention ! Oh ! Un os ! Ce dernier roi païen d’Irlande Cormac celui du poème à l’école s’est étranglé à Sletty au sud de la Boyne. Je me demande ce qu’il mangeait. Un truc délectable. Saint Patrick l’a converti au Christianisme. N’a pas pu tout avaler cela dit.

– Rosbif et chou.

– Un ragoût.

Odeurs d’hommes. Son cœur se souleva. Sciure et crachats, fumée tiédasse et doucereuse des cigarettes, relents de chique, bière renversée, la pisse des hommes qui aussi fleure la bière, le remugle des ferments.

Pourrais pas avaler une bouchée ici. Gus qui aiguise son couteau avec sa fourchette prêt à tout avaler devant lui, vieux qui se récure les chicots. Petit spasme, repus, régurgite et remâche. Avant et après. Les grâces après le repas. Regardez ici, ce tableau d’abord, puis celui-ci. À pomper la sauce du ragoût à coups de croutons de pain détrempés. Lèche donc l’assiette, mon bonhomme ! Sortons de là.

Il jeta un regard circulaire aux mangeurs, ceux des tables et ceux des tabourets, les narines pincées.

– Deux brunes par ici.

– Un corned beef et du chou.

Ce type s’engouffre un tas de chou à l’aide de son couteau comme si sa vie en dépendait. Bien joué. Ça me fait frissonner rien que de regarder. C’est plus sûr de manger avec trois mains. Le démembre. Une seconde nature. Né avec un couteau d’argent dans la bouche. Ça c’est spirituel, il me semble. Ou non. Argent ça veut dire né riche. Né avec un couteau. Mais on perd la référence dans ce cas.

Un garçon mal sanglé ramassait avec fracas les assiettes collantes. Rock, le chef bailli, debout au bar soufflait sur la couronne de mousse de sa chope. À raz bord : elle vint gicler, jaune, près de sa bottine. Un dîneur, couteau et fourchette dressés, coudes posés sur la table, prêt pour le second service, regardait fixement le monte-plat par-dessus son carré de journal plein de tâches. L’autre gars lui raconte quelque-chose, la bouche pleine. Écoute empathique. Propos de table. Ch’l’ai croiché lunchdi dans l’Unchster Buck. Ah ? Vrai de vrai ?

M. Bloom porta deux doigts hésitants à ses lèvres. Ses yeux disaient :

– Pas là. Le vois pas.

Oust. Je déteste les cochons.

Il battit en retraite vers la porte. Prendrai un petit snack chez Davy Byrne. Un entre-deux. Ça me fera tenir. Pris un bon petit-déj.

– Rosbif et purée par ici.

– Une pinte de brune.

Chacun pour soi, et bec et ongles. Bois. Broie. Bois. Gueuleton.

Il sortit à l’air libre, plus libre, et revint sur ses pas en direction de la rue Grafton. Manger ou être mangé. Tue ! Tue !

Suppose que cette cuisine communale va encore tarder des années peut-être. Tout le monde trottant avec écuelles et gamelles à remplir. Dévorent le contenu dans la rue. John Howard Parnell donne l’exemple le prévôt du Trinity femmes et enfants taxis prêtres pasteurs maréchaux archevêques. De la route d’Ailesbury, de la route de Clyde, des quartiers ouvriers, de l’hospice du Dublin-Nord, le maire dans son carrosse en pain d’épice, la vieille reine sur une chaise de bain. Mon assiette est vide. Après vous avec notre tasse floquée aux armes de la mairie. Comme à la fontaine de Sir Philip Crampton. Essuie les microbes avec ton mouchoir. Le suivant en remet une couche avec le sien. Le père O’Flynn les ferait déguerpir comme des lapins. Y aurait du grabuge tout de même. Tout pour bibi. Enfants qui se battent pour les fonds de marmites. Faudrait une marmite grande comme le parc du Phénix. Harponneraient les côtes et gigots. Haïr tous ses voisins. La *Table d’hôte* de l’hôtel des Armes de la Cité, elle appelait ça. Soupe, bidoche et dessert. Tu ne sais jamais de qui sont les pensées que tu remâches. Et après qui laverait toutes les assiettes et les couverts ? Se nourrira peut-être tous de comprimés ce moment-là. Les dents de mal en pis.

Après tout il y a beaucoup de vrai dans ce bon goût végétarien de ce qui vient de la terre l’ail bien sûr ça pue les joueurs d’orgues de barbarie italiens oignons champignons truffes grillés. La souffrance animale aussi. Plumer et vider la volaille. Ces pauvres brutes au marché à bestiaux qui attendent là que le merlin vienne leur ouvrir le crane. Mouh. Pauvres veaux tout tremblants. Meuh. Tiennent à peine sur leurs pattes. Rata. Les reflets tremblotants dans les seaux des bouchers. Donnez-nous ce bout de poitrine sur le crochet. Plop. Bonjour Barbe-bleue. Moutons écorchés pendus par l’arrière-train les yeux vitreux, papillotes sanguinolentes de museaux de moutons, un filet de morve qui s’écoule jusque dans la sciure. Attention, sortie de têtes et queues. Massacre pas les morceaux, jeune homme.

On prescrit du sang encore chaud pour la phtisie. Faut toujours du sang. Insidieux. Lèche-le tant qu’il fume, épais et sucré. Fantômes affamés.

Ah, j’ai faim.

Il entra chez Davy Byrne. Pub vertueux. Il ne bavarde pas. Accepte un coup de temps à autre. Les années bissextiles, tous les quatre ans. A encaissé un chèque pour moi une fois.

Qu’est-ce que je vais bien prendre ? Il sortit sa montre. Voyons voir. Panaché ?

– Hello Bloom, dit Flynn-la-belette de son recoin.

– Hello Flynn.

– Comment va ?

– Au poil… Voyons. Je vais prendre un verre de bourgogne et… voyons.

Les sardines sur les rayons. On savoure rien qu’à regarder. Un sandwich ? M. Jean Bon et sa moutarde ont pris de la brioche. Terrines. Mais qu’est un foyer sans La Terrine du Prunier ? Inachevé. Quelle pub stupide ! Et sous la nécrologie ils l’ont collée. Tous pendus d’un prunier. Terrine de Dignam. Bon pour les cannibales avec riz et zeste de citron. Missionnaire blanc trop salé. Comme le porc mariné. On s’attend à ce que le chef consomme les parties honorifiques. Seront sûrement coriaces à force d’exercice. Ses femmes à la file pour constater les effets. *Il y avait un vieux roi nègre. Qui mangea ou bidule les bidules du père Pègre.* Alors qu’avec, quel pied. Dieu sait quelle mixture. Placentas tripes moisies trachées enroulées hachées menues. Casse-tête trouve la viande. Kasher. Jamais la viande et le lait ensemble. De l’hygiène c’était maintenant on appelle ça. Le jeûne de Yom Kippour le nettoyage de printemps des intérieurs. La paix dépend de la digestion d’untel. Les religions. Dindes et oies pour Noël. Le massacre des innocents. Mange, bois et réjouis-toi. Et voilà les urgences remplies. Têtes bandées. Le fromage digère tout sauf lui-même. Sacré asticot le fromage.

– Avez-vous un sandwich au fromage ?

– Oui, monsieur.

Prendrais volontiers quelques olives s’ils en avaient. Les Italiennes je préfère. Un bon verre de bourgogne ; enlève ce. Lubrifie. Une bonne salade, fraiche comme une pâquerette, Tom Kernan sait faire la vinaigrette. Y met du sien. Pure huile d’olive. Milly m’avait servi cette côtelette avec un brin de persil. Prenez un oignon espagnol. Dieu a fait la nourriture, le diable les cuisiniers. Crabe à la diable.

– Femme va bien ?

– Fort bien, merci… Un sandwich au fromage, donc. Vous avez du Gorgonzola ?

– Oui, monsieur.

Flynn-la-belette prit une gorgée de grog.

– Pousse la chansonnette ces temps-ci ?

Regarde-moi cette bouche. Arriverait à siffler dans sa propre oreille. Et les oreilles décollées pour assortir le tout. Musique. Il s’y connaît autant qu’un chauffeur de taxi. Mais autant lui dire. Fait pas de mal. Publicité gratuite.

– Elle a signé pour une grande tournée à la fin du mois. Vous en avez peut-être entendu parler.

– Non. Oh, c’est la classe. Qui monte ça ?

Le serveur servit.

– Combien est-ce ?

– Sept d., monsieur… Merci, monsieur.

M. Bloom découpa son sandwich en bandes étroites. *Père Pègre*. Plus facile que ces trucs rêveurs à la crème. *Ses cinq cents épouses. N’étaient pas jalouses.*

– Moutarde, monsieur ?

– Merci.

Il piqua de jaune la face inférieure des bandes, qu’il soulevait une à une. *Jalouses*. J’y suis. *Il était toujours plus intègre*.

– Qui monte ça ? dit-il. Eh bien c’est comme une société dans l’idée, vous voyez. Partage des actions et partage des profits.

– Ouais, ça me revient maintenant, dit Flynn-la-belette, qui passa une main dans sa poche pour se gratter l’aisselle. Qui c’est qui m’en a parlé ? Braise Boylan est dans le coup, non ?

Un choc d’air chaud la chaleur de la moutarde referma sa mâchoire sur le cœur de M. Bloom. Il leva les yeux et rencontra le regard d’une horloge bilieuse. Deux. L’horloge des pubs a toujours cinq minutes d’avance. Temps passe. Aiguilles bougent. Deux. Pas encore.

Son diaphragme se souleva d’aspiration, s’affaissa, aspira plus longuement, languissant.

Vin.

Il humagoûta une gorgée du cordial suc et, ayant contraint son gosier à l’expédier d’un trait, reposa délicatement son verre à vin.

– Oui, dit-il. De fait c’est lui l’organisateur.

Rien à craindre : rien dans la cervelle.

Flynn-la-belette reniflait et grattait. Il y a une puce qui se tape un bon gueuleton.

– Il a fait un sacré coup, Jack Mooney me disait, sur ce match de box que Myler Keogh a gagné contre ce soldat à la caserne de Portobello. Pardieu, il avait mis ce foutriquet au vert dans le comté de Carlow, qu’il me disait…

Pourvu que cette goutte de rosée ne tombe pas dans son verre. Non, reniflée à temps.

– Pour presque un mois, mec, avant que ça ait lieu. À gober des œufs de canard pardieu jusqu’à nouvel ordre. Privé de bibine, pigé ? Ah, pardieu, Braise est un sacré lascar.

Davy Byrne sortit du closet en tricot aux manches relevées, s’essuyant les lèvres d’un aller-retour de sa serviette. Rougeur pivoine. Dont le sourire sur chacun des traits joue avec tel ou tel replet. Trop de lard sur les navets.

– Voila notre homme, lieu soit doué, dit Flynn-la-belette. Pouvez-vous nous en donner un bon pour la Coupe d’Or ?

– Je ne touche pas à ça, M. Flynn, répondit Davy Byrne. Je ne mise jamais rien sur un cheval.

– Vous avez bien raison, dit Flynn-la-belette.

M. Bloom mangeait ses tranches de sandwich, pain frais et propret, prenant plaisir au dégoût, de la moutarde âpre, du fromage vert qui sent les pieds. Le vin bu à petites doses apaisait sont palais. Pas du campêche, ça. Plus de bouquet par ce temps quand le froid est parti.

Bar agréable, tranquille. Joli morceau de bois ce comptoir. Bien lissé. Cette courbe, là : j’aime.

– Je ne ferais jamais rien de ce genre, dit Davy Byrne. Ça vous a ruiné bien des hommes, ces mêmes chevaux.

Le jackpot du marchand de vin. Débit autorisé de bière, vin et spiritueux à consommer sur place. Pile je gagne face tu perds.

– Ça tient pour vous, dit Nosey Flynn. A moins d’être dans la combine. Il n’y a plus de sport honnête de nos jours. Lenehan a de bons tuyaux. Il annonce Sceptre aujourd’hui. Zinfandel est le favori, de lord Howard de Walden, il a gagné à Epsom. Monté par Morny Cannon. J’aurais pu miser sept contre un contre Saint Amant, il y a quinze jours.

– Ah vraiment ? dit Davy Byrne…

Il se dirigea vers la fenêtre et, attrapant le livre de caisse, en compulsa les feuillets.

– J’aurais pu, parole, dit Flynn-la-belette en reniflant. Un canasson comme on en voit pas souvent. Fille de Saint Frusquin. Elle a gagné pendant un orage, la pouliche Rothschild, les oreilles remplies de bourre. Casaque bleue et casquette jaune. La peste du gros Ben Dollard et son John O’Gaunt. C’est lui qui m’a retenu. Ouais.

Il but de son haut verre, résigné, passant ses doigts le long du tube.

– Ouais, dit-il avec un soupir.

M. Bloom, debout, mâchant à pleines dents, observait le soupir. Belette deux doigts de front. Est-ce que je lui dis le cheval que Lenehan ? Il sait déjà. Mieux vaut qu’il oublie. Va et perds encore. Un idiot et son oseille. La rosée perle à nouveau. Le nez froid s’il embrassait une femme. Peut-être qu’elles aimeraient ça pourtant. La barbe qui pique elles aiment. La truffe froide des chiens. La vieille Mme Riordan avec son Skye terrier l’estomac qui grondait à l’hôtel des Armes de la Cité. Molly qui lui faisait des mamours sur ses genoux. Oh, le gros toutou-ouah-ouah !

Le vin imbibait et adoucissait boule de mie spongieuse moutarde un instant fromage écœurant. Se laisse boire. Le savoure d’autant mieux que j’ai pas soif. Le bain bien sûr. Rien qu’une bouchée ou deux. Puis vers six heures je peux. Six. Six. Le temps aura passé. Elle…

Le vin à feu doux lui rallumait les veines. J’en avais salement besoin. Me sentais complètement patraque. Ses yeux passaient sans faim sur les étagères de conserves : sardines, pinces de homard criardes. Toutes les drôles de choses que les gens trouvent pour manger. Des coquillages, les bigorneaux avec une épingle, des arbres, les escargots que les Français ramassent sur le sol, des mers avec hameçon et appât. Poissons idiots n’ont rien appris en mille ans. Si tu ne sais pas risqué de mettre n’importe-quoi dans la bouche. Baies vénéneuses. Aubépine. Ce qui est rond paraît bon. Couleur criarde on se méfie. Un gars le dit à un autre et ainsi de suite. Essaie d’abord sur le chien. Guidé par l’odeur ou l’apparence. Fruit tentant. Cornets à glace. Crème. Instinct. Orangeraies par exemple. Nécessite l’irrigation artificielle. Bleibtreustrasse. Oui mais pour les huitres. Moches comme des glaires. Coquilles immondes. Et l’enfer pour les ouvrir. Qui a trouvé ça ? S’alimentent des ordures et de l’eau des égouts. Mousseux et huitres du Côte Rouge. Effet sur la libido. Aphrodis. Il était au Côte Rouge ce matin. Était-il huitres vieux poisson à table. Peut-être qu’il chair fraiche au lit. Non. Juin n’a ni erre ni huitres. Mais il y a des gens aiment le gibier avancé. Civet de lièvre. Attrape ton lièvre d’abord. Les Chinois qui mangent des œufs de cinquante ans, bleus puis verts à nouveau. Diner de trente plats. Chacun des plats inoffensif le mélange à l’intérieur pourrait. Idée pour un roman policier. C’était l’archiduc Léopold. Non. Si, ou bien Otto un des Habsbourgs ? Ou qui c’était celui qui mangeait les croûtes de sa propre tête ? Le menu le moins cher de la ville. Bien sûr les aristocrates, puis les autres suivent pour être à la mode. Milly aussi pétrole et farine. Moi-même j’apprécie la pâtisserie pas cuite. La moitié des huitres pêchées ils les rejettent à la mer pour maintenir les prix. Bon marché personne n’achèteraient. Caviar. Se la jouer. Vin du Rhin et verre émeraude. Chouette teuf. Madame de. Perles et gorges poudrées. La *jetset. Le very best*. Ils veulent des mets spéciaux pour prétendre qu’ils en sont. Un ermite avec un plat de lentilles réfrène les aiguillons de la chair. Connais-moi viens manger avec moi. Esturgeon du roi. Le haut-shérif, Coffey, le boucher, droit de gibier sur les forêts de son ex. Lui renvoyer la moitié de la vache. Le festin que j’ai vu dans les cuisines du président de la cour d’appel. *Chef* à toque blanche comme un rabbin. Canard flambé. Chou frisé *à la Duchesse de Parme*. Heureusement que c’est écrit sur la carte pour savoir ce qu’on a mangé. Trop de potions gâchent le bouillon. J’en sais quelque chose. Dosé au bouillon-cube Edwards. Les oies gavées à en crever pour eux. Homards bouillis à vif. Perdrez-donc de ce perdreau. Ça ne me déplairait pas d’être serveur dans un palace. Pourboires, robes de soirée, dames à moitié nues. Vous laisserez-vous tenter par ce filet de sole citronnée, mademoiselle Devouprie. Oui, je vous prie. Et elle en a pris. Nom huguenot ce me semble. Il y avait une demoiselle Devouprie qui vivait à Kilkenny je me souviens. *De*, *du* c’est français. Pourtant si ça se trouve c’est le même poisson que le vieux Micky Hanlon rue Moore éviscérait faisait une fortune les doigts plongés jusqu’aux jointures dans les branchies pas foutu d’écrire son nom sur un chèque on aurait dit qu’il repeignait le paysage avec sa bouche tordue. Mooou-i-ceee-ki Hach-a ignorant comme une carpe, cinquante mille livres en banque.

Collées au carreau deux mouches vrombissaient, coincées.

Bu, le vin s’attardait sur son palais, rougeoyant. Fouler le raisin dans les pressoirs de Bourgogne. La chaleur du soleil. Comme une caresse secrète qui me dit souvenir. Caressés humectés ses sens se rappelaient. Cachés sous les fougères sauvages à Howth en dessous de nous la baie endormie : ciel. Pas un bruit. Le ciel. La baie pourpre à la pointe du Lion. Verte devant Drumleck. Jaune-verte vers Sutton. Prairies sous-marines, les lignes brunes à peine visibles parmi les herbes, cités enterrées. Mon veston pour oreiller ses cheveux des perces-oreilles dans les bruyères ma main sous sa nuque, vous allez toute m’ébouriffer. Oh merveille ! sa main fraiche douce d’onguents m’a touché m’a caressé : ses yeux me regardaient et ne se détournaient pas. En extase au-dessus d’elle je m’allonge, les lèvres pleines les lèvres ouvertes j’ai embrassé ses lèvres. Miam. Doucement elle m’a fait passer dans la bouche son gâteau tiède et mâché. La pulpe écœurante que sa bouche avait mâchouillée aigre-douce de sa salive. Joie : je l’ai mangé : joie. Vie jeune, ses lèvres qui me faisaient la moue. Lèvres tendres chaudes collantes gélatine. Ses yeux des fleurs, prends-moi, ses yeux qui veulent. Des galets s’éboulèrent. Elle s’immobilisa. Une chèvre. Personne. Perchée sur les rhododendrons de Ben Howth une chèvre marchait d’un pas sûr, semant ses raisins secs. Abritée derrière les fougères elle a ri, chaude sous l’étreinte. Comme fou je pesais sur elle je l’embrassais : yeux, ses lèvres, son cou tendu les artères battantes, ses seins de femme bombant sous sa blouse de mantille, ses mamelons gras et dressés. Brûlant, je lui ai mis la langue. Elle m’a embrassé. J’ai été embrassé. S’abandonnant elle a ébouriffé mes cheveux. Embrassée, elle m’a embrassé.

Moi. Et moi maintenant.

Coincées, les mouches vrombissaient.

Ses yeux abaissés parcoururent les veines silencieuses du plateau de chêne. Beauté : courbé : les courbes sont beauté. Déesses aux formes pleines, Venus, Junon : courbes que le monde admire. Peut les voir musée de la bibliothèque dressées dans le hall circulaire, déesses nues. Facilite la digestion. Elles n’ont que fiche qu’homme regarde. Tout à la vue de tous. Ne parlent jamais. Je veux dire à des gars comme Flynn. Imagine qu’elle fasse Pygmalion et Galatée quel serait son premier mot ? Mortel ! Te remettrait à ta place. Lampant du nectar au mess avec les dieux, plats en or, tout divinement exquis. Pas comme ces menus à deux sous qu’on se tape, mouton bouilli, carottes et navets, une bouteille de bière Allsop. Nectar imagine comme boire de l’électricité : la nourriture des dieux. Adorables les formes des femmes sculptureuses junoniennes. Immortelles adorables. Et nous qui poussons la nourriture d’un trou à l’autre : bouffe, chyle, sang, bouse, terre, bouffe : faut alimenter ça c’est comme une locomotive. Elles n’ont pas de. Jamais regardé. Je regarderai aujourd’hui. Gardien n’y verra que du feu. Tu te penches laisses tomber quelque-chose regardes si elle.

Goutte à goutte un message lui venait de la vessie aller faire ne pas faire là faire. En homme préparé il vida son verre jusqu’au dépôt et se leva, aux hommes aussi elles se sont données, virilement conscientes, couchées avec leurs amants hommes, un éphèbe a joui d’elle, tout de long.

Quand le bruit de ses semelles eut cessé Davy Byrne dit de derrière son livre :

– Qu’est-ce que c’est qu’il est ? Dans les assurances ?

– Il a arrêté ça il y a longtemps, dit Flynn-la-belette. Il fait du démarchage pour l’*Homme Libre*.

– Je le connais de vue, dit Davy Byrne. Il a des soucis ?

– Des soucis ? dit Flynn-la-belette. Pas que je sache. Pourquoi ?

– J’ai remarqué qu’il était de deuil.

– En deuil ? dit Flynn-la-belette. C’est vrai, ma parole. Je lui ai demandé comment ça allait chez lui. Vous avez raison, bon dieu. Il était en deuil.

– Je n’aborde jamais le sujet, dit David Byrne humainement, quand je vois une personne qui a ce genre de souci. Cela ne fait que remuer le fer.

– Ce n’est pas la femme en tout cas, dit Flynn-la-belette. Je l’ai croisé avant-hier et il sortait de cette laiterie irlandaise que la femme de John Wyse Nolan a sur la rue Henry, il avait un pot de crème dans les mains pour sa chère moitié. Elle est bien nourrie je peux vous dire. Petits-fours et canapés.

– Et il fait ça pour l'*Homme libre* ?

Flynn-la-belette fit la moue.

– C’est pas avec les annonces qu’il récolte qu’il paie la crème. Ça suffit juste pour le lard.

– Comment ça ? demanda Davy Byrne, levant les yeux du livre.

Nosey-la-belette pianota dans l’air quelques signes cabalistiques rapides. Il cligna de l’œil.

– Il en est, dit-il.

– Qu’est-ce que vous m’affirmez là ? dit Davy Byrne.

– Absolument, dit Nosey Flynn. Ordre vénérable, franc et accepté. Un très bon frère. Lumière, vie et amour, pardieu. Ils lui donnent un coup de pouce. J’ai appris ça d’un– Eh bien, je ne le dirai pas qui.

– Est-ce chose sûre ?

– Bah, c’est une bonne société. Ils ne vous laissent pas tomber quand vous êtes dans la panade. Je connais un gars qui essayait d’y entrer. Mais fichtre ce qu’ils serrent les rangs. Bon dieu ils ont bien fait de ne pas laisser les femmes s’en mêler.

Davy Byrne souritbaillahocha tout ensemble :

– Ouiaaahaâââh !

– Il y a eu une femme, dit Flynn-la-belette, elle s’était cachée dans une horloge pour voir ce qu’ils fabriquaient enfin. Mais fichtre ils vous l’ont éventée et assermentée maître-maçon sur le champ. C’était une des Saint-Legers de Doneraile.

Davy Byrne, saturé après son bâillement, dit avec des yeux larmoyant :

– Mais est-ce chose sûre ? C’est un homme tranquille, correct. Je l’ai souvent vu ici et pas une fois je ne l’ai vu– vous savez, dépasser les bornes.

– Le tout-puissant n’arriverait pas à le saouler, opina fermement Flynn-la-belette. File quand ça commence à chauffer. Vous ne l’avez pas vu regarder sa montre ? Si vous lui offrez à boire la première chose qu’il fait il sort sa montre pour savoir ce qu’il doit prendre. Parole d’honneur il le fait.

– Il y en a des comme ça, dit Davy Byrne. C’est un homme fiable, je dirais.

– Ce n’est pas un mauvais bougre, dit Flynn-la-belette, en reniflant. On l’a vu mettre la main à la poche pour aider un confrère. Il faut lui reconnaître ça. Oh, Bloom a ses bon côtés. Mais il y a une chose qu’il ne fera jamais.

Sa main gribouilla une signature au stylo au côté de son grog.

– Je sais, dit Davy Byrne.

– Rien de noir sur blanc, dit Flynn-la-belette.

Paddy Leonard et Bantam Lyons firent leur entrée. Tom Rochford leur emboitait le pas, le sourcil froncé, lissant d’une main son veston bordeaux.

– B’jour, M. Byrne.

– B’jour, messieurs.

Ils s’arrêtèrent au comptoir.

– Qui avance ? demanda Paddy Leonard.

– Je fais du sur-place en tout cas, répondit Flynn-la-belette.

– Eh bien, qu’est-ce ce sera ? demanda Paddy Leonard.

– Je prendrai un soda-gingembre, dit Bantam Lyons.

– Comment ça ? s’écria Paddy Leonard. Depuis quand, sacré dieu ? Et vous Tom ?

– Comment va la tuyauterie ? demanda Flynn-la-belette, qui sirotait.

En guise de réponse Tom Rochford appuya sa main sur son sternum et hoqueta.

– Pourrais-je vous demander un verre d’eau, M. Byrne.

– Certainement, monsieur.

Paddy Leonard regardait ses partenaires de boisson.

– Nom d’une pipe, dit-il. Regardez-moi à qui je paie la tournée ! Eau claire et soda ! Deux gaillards qui vous auraient léché le whisky d’une jambe de lépreux. Il a un foutu canasson dans la manche pour la Coupe d’Or. Une affaire sûre.

– Zinfandel, non ? demanda Flynn-la-belette.

Tom Rochford versa d’un papier plié un peu de poudre dans l’eau qui lui était servie.

– Foutue dyspepsie, dit-il avant d’avaler.

– Le bicarbonate c’est très bon, dit Davy Byrne.

Tom Rochford hocha du chef et but.

– Est-ce que c’est Zinfandel ?

– Motus ! fit Bantam Lyons en clignant de l’œil. Je vais y aller de mes cinq thunes.

– Crache le morceau si tu as ton honneur, et au diable, dit Paddy Leonard. Qui te l’a donné ?

M. Bloom qui sortait leva trois doigts en salut.

– À la revoyure ! dit Flynn-la-belette.

Les autres se tournèrent.

– Voilà justement l’homme qui me l’a donné, murmura Bantam Lyons.

– Pfff ! fit Paddy Leonard avec mépris. M. Byrne, monsieur, nous prendrons deux de vos shots de Jameson après ça et un…

– Soda-gingembre, termina courtoisement Davy Byrne.

– Ouais, dit Paddy Leonard. Un biberon pour le bébé.

M. Bloom marchait vers la rue Dawson, sa langue passait ses dents à la brosse. Un truc vert il faudrait que ce soit : des épinards, disons. Alors avec un projecteur à rayons Röntgen tu pourrais.

Devant l’allée Duke un fox terrier affamé dégueula une bouillie de cartilages putrides sur les pavés et la lapa aussi sec avec entrain. Gloutonnerie. Retour avec remerciements après avoir pleinement assimilé le contenu. Dessert, puis fromage. M. Bloom fit un prudent détour. Ruminants. Son deuxième service. Ils bougent la mâchoire du dessus. Me demande si Tom Rochford fera quelque-chose de son invention ? Perdait son temps à l’expliquer à la truffe de Flynn. Gens maigres bouches allongées. Devrait y avoir un hall ou un endroit où les inventeurs puissent aller et inventer gratis. Sûr tu aurais aussi tous les casse-pieds qui viendraient houspiller.

Il fredonnait, prolongeant d’un écho solennel la fin de chaque mesure :

>*Don Giovanni, a cenar teco*  
*m’invitasti*  

Me sens mieux. Bourgogne. Bon remontant. Qui a distillé le premier ? Un gars qui avait le blues. Cœur au ventre. Ce *Gens de Kilkenny* à la bibliothèque nationale maintenant, il faut.

Des chaises percées, propres, nues, qui patientaient dans la vitrine de William Miller, plombier, le firent rebrousser pensée. Ils pourraient : et le regarder descendre jusqu’en bas, avale une épingle parfois elle sort des côtes des années après, visite du corps passe le conduit biliaire rate foie qui gicle suc gastrique bobines d’intestins comme des tuyaux. Mais le pauvre larbin devrait rester là tout le temps avec les entrailles en devanture. La science.

>*A cenar teco*

Qu’est-ce que ça veut dire *teco* ? Ce soir peut-être.

>*Don Giovanni, tu m’as invité*  
>*à venir diner ce soir*  
>*Da da di da doum*  

Non, ça ne le fait pas.

Cleys : deux mois si j’arrive que Nannetti. Ça fera deux livres dix dans les deux livres huit. Trois que Hynes me doit. Deux onze. Le fourgon des teintureries Prescott là-bas. Si j’obtiens l’annonce de Prescott : deux quinze. Cinq guinées environ. À l’aise, Blaise.

Pourrais acheter ces jupons de soie pour Molly, même couleur que ses nouvelles jarretelles.

Aujourd’hui. Aujourd’hui. Pas penser.

Visiter le sud alors. Pourquoi pas les villes d’eau anglaises ? Brighton, Margate. Jetées au clair de lune. Sa voix qui plane. Ces jolies filles de la côte. Adossé contre la devanture de John Long, un glandeur hébété paressait, plongé dans ses pensées, rongeant de son poing les articulations crouteuses. Homme à tout faire cherche travail. Petit salaire. Mangera n’importe-quoi.

M. Bloom tourna devant la vitrine de la pâtisserie Gray et ses tartes invendues, et dépassa la librairie du révérend Thomas Connellan. *Pourquoi j’ai quitté l’église de Rome*. Les femmes du *Nid d’oiseau* l’ont chassé. On dit qu’ils donnaient de la soupe aux enfants des pauvres s’ils se faisaient protestants du temps de la famine de la patate. La société de l’autre côté là où papa allait pour la conversion des juifs pauvres. Même carotte. Pourquoi nous avons quitté l’église de Rome.

Un jeune aveugle se tenait au bord du trottoir, qu’il tapotait de sa cane grêle. Pas de tram en vue. Veut traverser.

– Voulez-vous traverser ? demanda M. Bloom.

L’adolescent ne répondait pas. Son visage muré se contracta faiblement. Il remua la tête, hésitant.

– Vous êtes rue Dawson, dit M. Bloom. La rue Molesworth est en face. Voulez-vous traverser ? Il n’y a rien sur le passage.

La cane se tourna, tremblante, vers la gauche. M. Bloom suivit du regard la ligne qu’elle traçait et retomba sur le fourgon de teinturerie garé devant Drago. Là que j’ai vu sa tête gominée juste quand je. Cheval avachi. Cocher au John Long. Étanchant sa soif.

– Il y a un fourgon de ce côté, dit M. Bloom, mais il ne bouge pas. Je vous accompagne. Voulez-vous aller rue Molesworth ?

– Oui, répondit le jeune homme. Rue Frederick, Sud.

– Venez, dit M. Bloom.

Il toucha doucement le coude étroit : puis prit la main molle et voyante pour la guider.

Dis lui quelque-chose. Ne fais pas le condescendant. Ils se méfient de ce qu’on leur dit. Sors une banalité.

– Il n’a pas plu, finalement.

Pas de réponse.

Taches sur sa veste. S’en met partout en mangeant je suppose. Pas le même goût du tout pour lui. Doit être nourri à la cuillère pour commencer. Comme une main d’enfant, sa main. Comme était la main de Milly. Sensitive. Prend ma mesure d’après ma main, j’ai l’impression. Me demande s’il a un nom. Fourgon. Tiens sa canne hors des pattes du cheval : pauvre bête fourbue, pique un roupillon. C’est bien. À l’écart. Derrière pour un taureau : devant pour un cheval.

– Merci, monsieur.

Sait que je suis un homme. La voix.

– Ça va maintenant ? Première à gauche.

Le jeune aveugle tapota le bord du trottoir et poursuivit son chemin, relevant sa cane, tâtant à nouveau.

M. Bloom marchait derrière ces pieds dépourvus d’yeux, ce costume coupe droite en tweed à chevrons. Pauvre jeune homme ! Comment diantre a-t-il su que le fourgon était là ? A dû le sentir. Voient les choses dans leur front si ça se trouve : sorte de sens du volume. Le poids ou la taille, quelque-chose plus noir que la pénombre. Me demande s’il sentirait si on enlevait quelque-chose. Sentir un vide. Drôle d’idée de Dublin il doit avoir, à tapoter les pavés de-ci de-là. Est-ce qu’il pourrait marcher en ligne droite s’il n’avait pas sa cane. Visage exsangue pieux comme d’un gars qui va se faire prêtre.

Penrose ! C’était ça le nom du gars !

Regarde tout ce qu’ils peuvent apprendre à faire. Lire avec les doigts. Accorder les pianos. Ou alors on est surpris qu’ils aient de la cervelle. Pourquoi pense-t-on qu’une personne difforme ou un bossu est intelligent s’il dit quelque-chose qu’on pourrait dire. Bien sûr leurs autres sens sont plus. Brodent. Tressent de paniers. Les gens devraient aider. Un panier à ouvrage je pourrais l’acheter pour l’anniversaire de Molly. Déteste la couture. Pourrait y trouver à redire. Les hommes de l’ombre comme on les appelle.

L’odorat aussi doit être plus développé. Des odeurs de toutes parts, par grappes. Chaque rue une odeur différente. Chaque personne aussi. Puis le printemps, l’été : des odeurs. Goûts ? On dit qu’on ne peut pas goûter les vins les yeux fermés ou avec un rhume de cerveau. Fumer dans le noir on n’en tire pas non plus du plaisir, dit-on.

Et avec une femme, par exemple. Moins de honte à ne rien voir. Cette fille devant l’institut Stewart, tête en l’air. Regardez-moi. J’ai tout ce qu’il faut. Doit faire bizarre de ne pas la voir. Genre comme une forme dans son œil intérieur. Les voix, les températures : quand il la touche avec les doigts doit presque voir les lignes, les courbes. Sa main sur sa chevelure, par exemple. Mettons qu’elle soit brune, par exemple. Bon. On dit brun. Puis passant les doigts sur la peau blanche. Une sensation différente peut-être. Sensation de blanc.

Bureau de poste. Dois répondre. La galère aujourd’hui. Lui envoyer un mandat postal deux shillings, une demi-couronne. Acceptez ce petit présent. Papeterie juste à côté en plus. Attends. Réfléchis-y.

Du doigt, doucement et avec une grande lenteur il palpa ses cheveux peignés vers l’arrière au-dessus de l’oreille. Encore. Brins de paille fine fine. Puis son doigt toucha mollement la peau de sa joue droite. Du duvet là aussi. Pas assez lisse. Le ventre c’est le plus doux. Personne en vue. Le voilà qui prend la rue Frederick. Peut-être pour le piano de l’académie de danse Levinson. Pourrais être en train de rajuster mes bretelles.

Alors qu’il passait le pub de Doran il glissa sa main entre son veston et son pantalon, écarta doucement sa chemise et palpa un repli, mou, de son ventre. Mais je sais que c’est blanc cassé. Faut essayer dans le noir pour voir.

Il retira sa main et remit ses vêtements en.

Pauvre garçon ! Presque un enfant. Terrible. Vraiment terrible. Quels rêves pourrait-il avoir, sans rien voir ? La vie un rêve pour lui. Quelle justice y a-t-il à naître comme ça ? Toutes ces femmes et ces enfants excursion pique-nique de fin d’année brûlés et noyés à New York. Holocauste. Karma ils appellent ça cette transmigration pour les péchés qu’on a commis dans une vie antérieure la réincarnation mes tempes si quoi. Mon dieu, mon dieu, mon dieu. Horrible bien sûr : mais il y a toujours un truc avec eux qui fait que tu ne peux pas les biter.

Sir Frederick Falkiner qui entre dans le temple des Francs-maçons. Sérieux comme un pape. Après son déjeuner comme il se doit aux terrasses d’Earlsfort. Vieux compères en magistrature qui font péter un magnum. Chroniques du barreau et assises, annales de l’école des bleus vestons. Je l’ai condamné à dix ans. Je suppose qu’il ferait la fine bouche devant ce machin que j’ai bu. Que du millésimé pour ces messieurs, l’année marquée sur la bouteille poussiéreuse. A son idée à lui de la justice quand il est au tribunal. Vieillard plein de bonnes intentions. Les registres de police bourrés de cas font leur pourcentage en fabriquant le crime. Les envoie promener. La terreur des usuriers. A passé un sacré savon à Reuben J. Alors lui c’est vraiment ce qu’ils appellent un sale juif. Le pouvoir qu’ont ces juges. Vieux croûtons assoiffés sous la perruque. Ours mal lunés. Et que le Seigneur ait pitié de votre âme.

Bonjour Madame l’affiche. Vente de charité Mirus. Son excellence le lieutenant-général. Seize. Aujourd’hui donc. Au bénéfice de l’hôpital Mercer. On a donné *Le messie* pour ça. Oui. Haendel. Pourquoi ne pas y aller : Ballsbridge. Passe voir Cleys. Pas besoin de m’accrocher à lui comme une sangsue. Faut pas abuser de l’hospitalité. Sûr que je connaitrai quelqu’un à l’entrée.

M. Bloom arrivait sur la rue Kildare. D’abord je dois. La librairie.

Chapeau de paille au soleil. Souliers fauves. Pantalons retroussés. C’est. C’est.

Son cœur accéléra faiblement. À droite. Musée. Déesses. Il vira sur sa droite.

Est-ce que ? Presque sûr. Veux pas regarder. Vin monte à la tête. Pourquoi j’ai ? Trop capiteux. Oui, c’est. La démarche. Pas voir. Continuer.

À grandes enjambées forcées il avança sur la grille du musée et leva les yeux. Superbe bâtiment. Sir Thomas Dean a fait les plans. Me suit pas ?

M’a pas vu peut-être. Soleil dans l’œil.

Sa respiration se mua en une suite de courts soupirs. Vite. Froides statues : au calme là. Une minute et sauvé.

Non. M’a pas vu. Deux heures passées. Juste à l’entrée.

Mon cœur !

Les yeux battant il regarda fermement les courbes crèmes de la pierre. Sir Thomas Dean c’était l’architecture grecque.

Regarder quelque-chose que j’.

Sa main empressée plongea dans une poche, y prit, lut déplié Agendath Netaim. Ou est-ce que ?

Occupé à chercher.

Il renfourna rapide Agendath.

L’après-midi elle a dit.

Je cherche ça. Oui, ça. Essaie toutes les poches. Mouch. *Homme Libre*. Où est-ce que ? Ah, oui. Pantalons. Pomme-de-terre. Porte-monnaie. Où ?

Pressons. Marche calmement. Encore un instant. Mon cœur.

Sa main qui cherchait le où c’est que je l’ai mis trouva dans sa poche arrière savon lotion faut que j’y passe papier tiède collé. Ah savon là je oui. L’entrée.

Sauvé !
