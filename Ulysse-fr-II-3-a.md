– Je n’en doute pas une seconde, dit M. Dedalus. J’ai souvent dit à ce pauvre Paddy qu’il devait ménager ce poste. John Henry, ce n’est pas ce qu’il y a de pire sous le soleil.

– Comment l’a-t-il perdu ? demanda Ned Lambert. La boisson, quoi ?

– Le point faible de bien des honnêtes gens, dit M. Dedalus dans un soupir.

Ils s’arrêtèrent devant la chapelle mortuaire. M. Bloom se tenait derrière le garçon à la couronne et contemplait la nuque aux cheveux bien peignés qui, frêle et plissée, sortait du col tout neuf. Pauvre garçon ! Était-il là quand le père ? Tous les deux inconscients. Émerge à l'instant final et reconnaît une dernière fois. Tout ce qu’il aurait pu faire. Je dois trois shillings à O'Grady. Comprendrait-il ? Les muets portèrent le cercueil dans la chapelle. De quel côté est sa tête ?

Après quelques instants il suivit les autres à l’intérieur, clignant des yeux dans le damier de lumière. Le cercueil reposait sur son brancard à l’entrée du chœur, quatre grands cierges jaunes aux coins. Toujours sur le devant. Corny Kelleher, qui déposait ses couronnes aux coins de tête, fit signe au garçon de s’agenouiller. Les membres du cortège s’agenouillèrent ici et là aux prie-dieu. M. Bloom était resté à l’arrière près du bénitier. Quand tous furent à genoux, il fit tomber avec soin de sa poche son journal déplié et y posa son genou droit. Il accommoda délicatement son chapeau noir sur son genou gauche, le tenant du bord, et s’inclina pieusement.

Un enfant de chœur, qui portait un seau en laiton avec quelque chose dedans, surgit d’une porte. Le prêtre en blouse blanche vint après lui, ajustant l’étole d’une main, de l’autre balançant un petit livre sur sa panse de crapaud. Qui lit le credo ? dit le corbeau.

Ils pilèrent au brancard, et le prêtre commença à lire de son livre dans un corbeau fluide.

Père Serkey. Je savais que son nom c’était comme cercueil. *Domine-namine*. Bonne gueule de bulldog. Tient la baraque. Chrétien à muscle. Malheur à quiconque le regarde de travers : prêtre. Tu es Pierre. Finira par éclater ses côtes comme une oie qu’on gave, dit Dedalus. Avec cette panse de chiot empoisonné. A le chic des mots qui font mouche, cet homme. Hum : Éclater ses côtes.

– *Non intres in judicium cum servo tuo, domine*

Ce latin qu’on leur sert, s’en sentent agrandis. Messe de requiem. Pleureurs de crêpe noir. Papier à lettre bordé de noir. Votre nom sur le registre d’autel. Frisquet l’endroit. Faut bien se nourrir pour rester assis ici toute la matinée dans la pénombre, à taper du pied en attendant, le suivant s’il vous plaît. Œil de crapaud, aussi. Qu’est-ce qui le fera gonfler ? Molly, c’est le chou. L’air de l’endroit peut-être. Rempli de gaz nocifs, on dirait. Doit y avoir un tas de gaz d’enfer là-dedans. Les bouchers par exemple : ils finissent par ressembler à des biftecks crus. Qui me racontait ? Mervyn Browne. Dans les caveaux sous le beau vieil orgue cent cinquante ans de saint Werburgh ils doivent percer des trous dans les cercueils parfois pour que les gaz nocifs s’échappent et les brûler. Ça fuse : bleu. Une bouffée de ça et tu es cuit.

Ma rotule me fait mal. Aïe. Ça va mieux.

Le prêtre tira une baguette au bout enflé du seau du garçon et la secoua sur le cercueil. Puis alla à l’autre bout et la secoua à nouveau. Puis revint à son point de départ et la remit dans le seau. Tel qu’avant le repos. Tout est écrit : il se doit de le faire.

– *Et ne nos inducas in tentationem*

L’enfant de chœur récitait les réponses d’une voix flutée de soprano. J’ai souvent pensé que ce serait mieux d’avoir des garçonnets comme domestiques. Jusqu’à quinze ans environ. Après ça, bien sûr…

C’était de l’eau bénite, je suppose. Comme le marchand de sable. Il doit en avoir raz-le-bol de ce boulot, à remuer son engin sur tous les macchabées qu’on lui amène : au trot. Quel mal s’il pouvait voir ce sur quoi il secoue. Tous les jours que dieu fait une nouvelle fournée : hommes d’âge mûr, vieilles bonnes femmes, enfants, mortes en couche, hommes barbus, hommes d’affaires chauves, gamines phtisiques aux poitrines de moineau. Et toute l’année à ânonner le même truc par-devant et à égoutter son machin sur eux : dormez. Au tour de Dignam maintenant.

– *In paradisum.*

A dit qu’il allait au paradis ou est au paradis. Dit ça pour tout le monde. Quel boulot fastidieux. Mais il doit bien dire quelque-chose.

Le prêtre referma son livre et s’en fut, suivi de l’enfant de chœur. Corny Kelleher ouvrit les portes latérales. Les fossoyeurs firent leur entrée, hissèrent à nouveau le cercueil à l’épaule, le portèrent jusqu’à leur charriot et l’y poussèrent. Corny Kelleher donna une couronne au garçon et une autre au beau-frère. Tous les suivirent alors qu’ils sortaient par les portes latérales, dans l’air gris et doucereux. M. Bloom sortit en dernier, repliant son journal et le glissant dans sa poche. Il garda gravement les yeux à terre jusqu’à ce que le charriot mortuaire s’ébranlât vers la gauche. Les roues de métal crissèrent sur le gravier écrasé et le troupeau de souliers se mit sourdement en marche derrière la lente carriole le long d’une allée de sépulcres.

Le ri le ra le ri le ra le rou. Seigneur, abstiens-toi de fredonner ici.

– Le rond-point d’O’Connell, dit M. Dedalus à la ronde.

Les yeux placides de M. Power remontèrent jusqu’à la pointe du haut obélisque.

– Il repose, dit-il, parmi ses gens, le vieux Dan O’.Mais son cœur est enterré à Rome. Que de cœurs brisés sont enterrés ici, Simon !

– Sa tombe est par là, Jack, dit M. Dedalus. Je serai bientôt étendu auprès d’elle. Qu’Il me prenne quand Il le souhaite.

Cédant à l’émotion, il se mit à pleurer en silence, la démarche un peu incertaine. M. Power prit son bras.

– Elle est mieux là où elle est, dit-il affectueusement.

– Je suppose, dit M. Dedalus dans un hoquet discret. Je suppose qu’elle est au ciel, s’il y en a un.

Corny Kelleher sortit du rang et laissa le cortège le dépasser pesamment.

– Tristes circonstances, commença M. Kernan poliment.

M. Bloom ferma les yeux et inclina deux fois la tête avec un air de tristesse.

– Les autres se recoiffent, dit M. Kernan. je suppose que nous pouvons aussi. Nous sommes les derniers. Ce cimetière est un endroit traitre.

– Le révérend homme a un peu dépêché son service, ne trouvez-vous pas ? critiqua M. Kernan.

M. Bloom hocha gravement du chef, sans quitter du regard les yeux vifs et injectés de sang. Yeux voilés, yeux qui dévoilent. Maçon, je crois : pas sûr. Encore à ses côtés. Nous sommes les derniers. Dans le même bateau. J’espère qu’il va dire autre chose.

– Le service de l’église irlandaise de Mont Jérôme est plus simple, plus impressionnant je dois dire.

M. Bloom approuva prudemment. Quant à la langue bien sûr c’est autre chose.

M. Kernan dit avec solennité :

– *Je suis la résurrection et la vie*. Cela vous touche au plus profond du cœur.

– En effet, dit M. Bloom.

Ton cœur peut-être mais qu’y gagne le gars là entre quatre planches à regarder les pissenlits pousser par la racine ? Plus rien qui le touche. Siège des sentiments. Cœur brisé. Une pompe en fin de comptes, qui pompe des milliers de litres de sang tous les jours. Un beau jour elle se bouche : et vous y voilà. Il y en a des tas par ici : poumons, cœurs, foies. Vieilles pompes rouillées : au diable le reste. La résurrection et la vie. Quand tu es mort tu es mort. Cette histoire de jugement dernier. Toc toc on sort tous du tombeau. Lève-toi, Lazare ! Et il arriva en cinquième et ne fut pas embauché. Debout ! Dernier jour ! Et tous les gars qui farfouillent alentour pour récupérer leur foie leurs sens et le reste des boulons. Bougre qu’on s’y retrouve ce foutu matin. Une once de poudre dans un crâne. Douze grammes dans une once. Mesure de Troyes.

Corny Kelleher rejoignit les rangs à leur côté.

– Tout s’est déroulé de première, dit-il. Ou quoi ?

Il les regardait de ses yeux trainants. Épaules de policier. Avec son mirliton ton ton.

– Comme il se doit, dit M. Kernan.

– Quoi ? Hein ? dit Corny Kelleher.

M. Kernan réitéra.

– Qui est le type à l’arrière avec Tom Kernan ? demanda John Henry Menton. Je connais cette tête.

Ned Lambert jeta un coup d’œil en arrière.

– Bloom, dit-il, Madame Marion Tweedy c’était, c’est, je veux dire, la soprano. C’est sa femme.

– Ah, mais bien sûr, dit John Henry Menton. Voilà un certain temps que je ne l’ai pas vue. C’était une bien jolie femme. J’avais dansé avec elle, voyons, il y a quinze ou dix-sept belles années, chez Mat Dillon à Roundtown. Et on en avait sur les bras.

Il regarda en arrière, par-delà les rangs.

– il est quoi ? demanda-t-il. Qu’est-ce qu’il fait ? N’était-il pas dans la papeterie ? Je me suis embrouillé avec lui un soir, je me souviens, au boulingrin.

Ned Lambert sourit.

– Oui, c’était le cas, chez Wisdom Hely. Représentant en buvard.

– Par Dieu, dit John Henry Menton, qu’est-ce qui lui a pris d’épouser un zèbre pareil ? Elle avait de beaux atouts dans son jeu à l’époque.

– Toujours le cas, dit Ned Lambert. Il place des annonces.

Les larges yeux de John Henry Menton se rivèrent vers l’avant.

Le charriot prit une allée de côté. Un homme corpulent, à l’affut dans l’herbe haute, leva son chapeau en hommage. Les fossoyeurs touchèrent leur casquette.

– John O’Connell, dit M. Power avec aise. Il n’oublie jamais un ami.

M. O’Connell serra toutes les mains en silence. M. Dedalus dit :

– Je suis venu vous rendre une nouvelle visite.

– Mon cher Simon, répondit le gardien d’une voix basse, je ne veux en aucun cas de votre clientèle.

Il se mit à marcher aux côtés de Martin Cunningham, saluant au passage Ned Lambert et John Henry Menton, et tripotait deux longues clés dans son dos.

– Vous connaissez cette histoire, lui demanda-t-il, sur Mulcahy de la Coombe ?

– Pas celle-là, dit Martin Cunningham.

Ils inclinèrent de concert leurs hauts-de-forme et Hynes tendit l’oreille. Le gardien suspendit ses pouces aux anneaux d’or de sa chaîne de montre, et s’adressa d’une voix contenue à leurs sourires disposés.

– On raconte, dit-il, que deux pochtrons sont venus ici un soir de brouillard chercher la tombe d’un de leurs amis. Ils demandèrent Mulcahy de la Coombe et on leur dit où il était enterré. Après avoir piétiné dans le brouillard ils finirent par trouver la tombe. Un des pochards épelait le nom : Terence Mulcahy. L’autre guignait sur une statue du Rédempteur que la veuve avait fait poser.

Le gardien releva la tête et lorgna une des sépultures qu’ils laissaient de côté. Il reprit :

– Et, après avoir bien reluqué la figure sacrée, *Pas un pet de ressemblance*, dit-il. *Ce n’est pas Mulcahy*, dit-il, *foutu sculpteur*.

Récompensé par des sourires il se laissa dépasser et parlait avec Corny Kelleher, prenait les fiches qu’il lui tendait, les retournait et les vérifiait en marchant.

– Tout cela est fait à dessein, expliqua Martin Cunningham à Hynes.

– Je sais dit Hynes. Je le sais.

– Pour remonter le moral du quidam, dit Martin Cunningham. Pure bonté d’âme : au diable le reste.

M. Bloom admira le port imposant et prospère du gardien. Tous veulent être en bons termes avec lui. Un gars décent, John O’Connell, un vrai brave type. Clés : comme l’annonce de Cleys : pas de risque d’évasion. Pas de contremarque. *Habeas corpus*. Il faut que je m’occupe de cette annonce après l’enterrement. Est-ce que j’ai bien écrit Ballsbridge sur l’enveloppe que j’ai prise pour couverture quand elle m’a dérangé en train d’écrire à Martha ? Pourvu qu’elle ne se soit pas retrouvée dans les courriers non distribués. Gagnerait à se raser. La barbe qui pousse grise. C’est le premier signe quand le poil sort gris. Et l’humeur de cochon. Des fils d’argent parmi les gris. Imagine pour sa femme. Sacré culot de demander une fille en mariage. Viens donc vivre au cimetière. Fais-lui miroiter ça. Ça l’a peut-être excitée au début. Flirter avec la mort… Ténèbres nocturnes qui planent, tous les macchabées allongés dans le coin. Les ombres des tombeaux quand bâille le cimetière et Daniel O’Connell doit être un descendant je suppose qui c’est disait souvent homme excentrique prolifique grand catholique en tout cas comme un gros géant dans le noir. Feux follets. Gaz de caveau. Faut éviter d’y penser pour arriver à concevoir. Les femmes en particulier sont si impressionnables. Une petite histoire de fantômes au lit pour la faire dormir. As-tu déjà vu un fantôme ? Eh bien moi oui. C’était une nuit sans lune. Sur le coup de minuit. Pourraient tout de même se bécoter comme il faut avec la bonne dose d’excitation. Les putes dans les cimetières turcs. Pris au berceau on apprend tout. Pourrais te lever une jeune veuve ici. Les hommes aiment ça. L’amour entre les pierres tombales. Pimente le plaisir. Au milieu de la mort nous sommes en vie. Les extrêmes se touchent. Quel supplice pour les pauvres morts. Odeur de grillade pour les affamés. À ronger leurs organes vitaux. Le plaisir de la provoc. Molly qui voulait faire ça devant la fenêtre. Il a huit enfants en tout cas.

Il en a vu passer un bon nombre de l’autre côté sous sa garde, allongés tout autour de lui les un après les autres sous un peu de terre. Terre sainte. Ça dégagerait de la place si on les enterrait debout. Assis ou à genou tu ne pourrais pas. Debout ? Sa tête pourrait sortir de terre un jour lors d’un glissement de terrain, le doigt accusateur. Le sol doit être comme une ruche : cellules oblongues. Et bien propret il garde ça aussi : gazon ras et bordures. Son jardin le major Gamble appelle le Mont Jérôme. Eh bien, c’en est un. Il y faudrait des fleurs du sommeil. Les cimetières chinois avec leurs pavots géants fournissent le meilleur des opiums m’a dit Mastiansky. Les Jardins Botaniques sont juste à côté. C’est le sang qui décante dans la terre donne la vie en retour. Même idée que ces juifs qu’ils disent ont tué un enfant chrétien. Tout homme a son prix. Cadavre en bon état, gras, gentilhomme, épicurien, idéal pour un verger. Une affaire. Pour la carcasse de William Wilkinson, expert-comptable, décédé il y a peu, trois livres treize et six. Avec nos remerciements.

J’avancerais même que le sol serait bien engraissé avec de la fumure de cadavre, os, chair, ongles. Charniers. Effroyable. Virant au vert au rose en décomposition. Pourrissent vite dans la terre humide. Les vieux tout secs plus durs à ronger. Puis comme un genre de suif un genre de fromage. Puis commencent à noircir, une mélasse noire qui s’écoule. Après ils sèchent. Phalènes tête-de-mort. Bien sûr les cellules ou je ne sais quoi elles continuent à vivre. Mutent. Vivent pour toujours pratiquement. Rien à manger se mangent elles-mêmes.

Mais ils doivent engraisser une quantité infernale d’asticots. Le sol doit en être tout retourné. Ça t’fait r’tourner la tête. Fossettes, minettes. Ça n’a pas l’air de lui peser plus que ça. Lui donne un sentiment de puissance voir tous les autres qui y passent en premier. Me demande comment il voit la vie. Aime à raconter ses blagues aussi : ça lui réchauffe le cœur et ses bivalves. Celle sur le bulletin. Spurgeon parti pour les cieux ce matin 4 h. 11 h. du soir. (Heure de fermeture). Pas encore arrivé. Pierre. Les morts eux-mêmes les hommes en tout cas apprécieraient une blague de temps à autre ou les femmes savoir ce qui est à la mode. Une poire pour la soif ou un ponch pour dame, chaud, doux et corsé. Souverain contre l’humidité. Il faut bien rire parfois donc autant que ce soit comme ça. Les fossoyeurs de *Hamlet*. Montre une profonde connaissance du cœur humain. N’osent plaisanter du mort avant deux ans au moins. *De mortuis nil nisi prius*. Passe ton deuil d’abord. Difficile d’imaginer ses funérailles. Semble une drôle de blague. Lire ta propre notice nécrologique on dit qu’on vit plus longtemps. Te donne un second souffle. Un nouveau bail.

– Vous en avez combien pour demain ? demanda le gardien.

– Deux, dit Corny Kelleher. Dix-trente et onze heures.

Le gardien mit les papiers dans sa poche. Le charriot arrivait en bout de course. Le cortège se répartit des deux côtés de la fosse, chacun prenant soin de contourner les tombes avoisinantes. Les fossoyeurs soulevèrent le cercueil et posèrent son museau au bord du trou, retenu par des sangles.

L’enterrer. Nous venons pour enterrer César. Ses Ides de mars ou juin.  
Il ne sait pas qui est là ni n’en a cure.  
Mais c’est qui ce grand zigue efflanqué là-bas dans un mackintosh ?  
Mais c’est qui j’aimerais le savoir. Mais je donnerais des clopinettes pour savoir qui c’est. Toujours un qui s’amène on y aurait jamais pensé. Un type pourrait vivre dans son coin toute sa vie. Oui, il pourrait. Il lui faudrait tout de même trouver quelqu’un pour la dernière pelletée même s’il pourrait creuser sa propre tombe. Comme nous tous. Seul l’homme enterre. Non, les fourmis aussi. Premier truc qui vient à l’esprit. Enterrer les morts. Parait que Robinson Crusoé c’était fidèle à la réalité. Eh bien dans ce cas Vendredi l’a enterré. Tous les vendredis enterrent un jeudi quand on y réfléchit.

> *Oh Pauvre Robinson Crusoé !  
Tu n’avais même pas une bouée*

Pauvre Dignam ! Dernière relâche sur terre, dans sa boîte. Quand on pense à tous ceux qui sont là ça parait vraiment un gaspillage de bois. Tous rongés. Ils pourraient inventer un beau brancard avec une sorte de panneau coulissant, le laisser filer comme ça. Oui mais ils pourraient refuser d’être enterrés à la suite d’un autre type. Ils sont si sourcilleux. Laissez-moi reposer sur ma terre natale. Un peu de glaise de la terre sainte. Uniquement mère et enfant morts en couche enterrés dans un même cercueil. Je comprends le sens. Je comprends. Le protéger le plus longtemps possible même dans la terre. La maison de l’Irlandais c’est son cercueil. Embaumement dans les catacombes, les momies même idée.

M. Bloom se tenait à l’arrière, à l’écart, chapeau à la main, et comptait les têtes nues. Douze. Je suis le treize. Non. Le gars au mackintosh est le treize. Le chiffre de la Mort. D’où diable a-t-il surgi ? Il n’était pas dans la chapelle, ça j’en jurerai. Sotte superstition cette histoire de treize.

Belle étoffe de tweed le complet de Ned Lambert, soyeuse. Une pointe de pourpre. J’en avais un comme ça quand nous vivions rue des Lombards, ouest. C’était un dandy dans le temps. Changeait de costume trois fois par jour. Faut que je me fasse retourner mon complet gris par Mesias. Hello. Il est teint. Sa femme j’ai oublié il n’est pas marié ou sa logeuse aurait dû lui ôter ces fils.

Les hommes, à califourchon sur les tréteaux mortuaires, laissèrent filer le cercueil qui plongea et disparut. D’un effort ils se relevèrent et sortirent : et tous se découvrirent. Vingt.

Pause.

Si nous étions tous soudain quelqu’un d’autre.

Au loin le braiment d’un âne. Pluie. Loin d’être un âne. N’en voit jamais un mort, dit-on. Honte de la mort. Ils se cachent. Pauvre papa s’en est allé lui aussi.

Une brise douce enveloppait les têtes nues dans un murmure. Murmure. Le garçon, près de la tête de fosse, tenait des deux mains sa couronne et contemplait sans bruit l’ouverture noire et béante. M. Bloom se positionna derrière le brave imposant gardien. Redingote bien coupée. Les soupèse peut-être pour voir qui suivra. Eh bien, c’est un long repos. Ne plus ressentir. C’est sur le moment qu’on ressent. Doit être foutrement désagréable. Peut pas y croire au début. Doit y avoir erreur : mauvaise personne. Essayez la porte d’en face. Attendez, il faut que je. Je n’ai pas encore. Puis la pénombre de la chambre d’agonie. La lumière ils veulent. On chuchote tout autour. Souhaitez-vous voir un prêtre ? Puis on radote et on divague. Delirium tout ce que tu as caché toute ta vie. Lutte à mort avec la mort. Son sommeil n’est pas naturel. Presse sa paupière inférieure. Là à regarder est-ce que son nez se pince est-ce que sa joue se creuse est-ce que la plante de ses pieds est jaune. Ôtez-lui l’oreiller et finissez-le sur le sol vu qu’il est condamné. Le diable dans cette image de la mort du pécheur qui lui montre une femme. Mourant d’envie de la prendre dans ses bras dans son linge. Dernier acte de *Lucia*. *Ne te comtemplerais-je donc plus jamais ?* Bam ! Il expire. Pas trop tôt. Les gens parlent de vous un peu : vous oublient. N’oubliez pas de prier pour lui. Mentionnez-le dans vos prières. Même Parnell. Le jour du lierre se meurt. Puis ils suivent : au trou, l’un après l’autre.

Nous prions maintenant pour le repos de son âme. Espère que vous allez bien et pas en enfer. Plaisant changement d’air. De la poêle-à-frire de la vie au feu du purgatoire.

Pense-t-il jamais au trou qui l’attend ? Il paraît qu’on le fait quand on frissonne au soleil. Quelqu’un a mis son pied dessus. La cloche du messager. Près de vous. Le mien est là-bas vers Finglas, le bout que j’ai acheté. Mamma, pauvre mamma, et le petit Rudy.

Les fossoyeurs empoignèrent leurs pelles et envoyaient de lourdes mottes de glaise battre contre le cercueil. M. Bloom détourna le visage. Et s’il était en vie tout ce temps ? Fiou ! Sacredieu, ce serait terrible ! Non, non : il est mort, bien sûr. Bien sûr qu’il est mort. Lundi il est mort. Ils devraient avoir une loi qu’on perce le cœur et qu’on vérifie ou une horloge électrique ou un téléphone dans le cercueil et un genre d’aération comme un treillis. Signal de détresse. Trois jours. Plutôt long pour les conserver en été. Serait aussi bien qu’on s’en débarrasse dès que tu es sûr que non.

La glaise faisait moins de bruit. Commence à être oublié. Loin des yeux, loin du cœur.

Le gardien s’écarta de quelques pas et remit son chapeau. En a eu assez. L’assistance se ressaisit, ils se recouvrirent un après l’autre sans faire d’histoire. M. Bloom coiffa son chapeau et observa la silhouette corpulente tracer adroitement sa route dans le dédale des tombes. Sans bruit, sûr de ses pas, il traversait le morne domaine.

Hynes qui écrit quelque-chose sur son carnet. Ah, les noms. Mais il les connaît tous. Non : il vient à moi.

– Je prends juste les noms, dit Hynes dans un souffle. Quel est votre nom de baptême ? Je ne suis pas sûr.

– L, dit M. Bloom. Et vous pourriez y mettre le nom de M’Coy aussi. Il me l’a demandé.

– Charley, dit Hynes en écrivant. Je sais. Il était à *L’homme libre* dans le temps.

Il y était avant de dégotter le poste à la morgue sous Louis Byrne. Bonne idée l’autopsie pour les docteurs. Découvrent ce qu’ils imaginent savoir. Il est mort d’un mardi. Foutu dehors. Filé avec la recette de quelques annonces. Charley, mon chou. Voilà pourquoi il m’a demandé de. Eh bien, il n’y a pas de mal. J’ai fait le nécessaire, M’Coy. Merci, mon vieux : vous en dois une. Le laisser redevable : ne coûte rien.

– Et dites-nous, dit Hynes, vous connaissez cet homme avec le, l’homme était là-bas dans le…

Il regarda alentour.

– Mackintosh. Oui, je l’ai vu, dit M. Bloom. Où est-il maintenant ?

– M’Intosh, dit Hyne en griffonnant. Je ne sais pas qui c’est. C’est bien son nom ?

Il s’éloigna, regardant à droite à gauche.

– Non, commença M. Bloom, faisant volte-face puis s’arrêtant. Je dis, Hynes !

Entend pas. Quoi ? Où a-t-il disparu ? Pas trace. Eh bien ça par. Quelqu’un ici a-t-il vu ? Ka heu double ell. L’homme invisible. Bon dieu, qu’est-ce qu’il a pu devenir.

Un septième fossoyeur s’approcha de M. Bloom pour attraper une pelle inutilisée.

– Oh, excusez-moi !

Il s’écarta vivement.

La glaise, brune, humide, commençait à affleurer du trou. Ça levait. Presque fini. Une butte de mottes humides s’élevait encore, s’élevait, et les fossoyeurs déposèrent leurs pelles. Ils se découvrirent tous pour un bref instant. Le garçon cala sa couronne contre un angle : le beau-frère la sienne sur une bosse. Les fossoyeurs remirent leurs casquettes et emportèrent leurs pelles terreuses vers le chariot. Arrivés là ils en secouèrent légèrement le fer sur le gazon : propre. L’un d’eux se pencha et détacha du manche une longue touffe d’herbes. Un autre, laissant là ses compagnons, s’en fut arme à l’épaule, d’un pas lent, la lame lançant des éclairs bleus. Un troisième, resté devant la tombe, enroulait en silence la sangle du cercueil. Le beau-frère, se détournant, lui mit quelque-chose dans la main libre. Remerciements muets. Regrets, monsieur : peine. Hochement de tête. Je sais ce que. Pour vous autres.

L’assistance se dispersait sans hâte, sans but, au hasard des chemins, pausant ici et là pour lire un nom sur une tombe.

– Faisons le détour par la tombe du chef, dit Hynes. Nous avons le temps.

– Allons-y, dit M. Power.

Ils tournèrent à droite, perdus dans leurs lentes pensées. La voix blanche de M. Power s’éleva, avec vénération :

– Certains disent qu’il n’est même pas dans ce tombeau. Qu’on a rempli le cercueil de pierres. Qu’il reviendra un jour.

Hynes secoua la tête.

– Parnell ne reviendra jamais, dit-il. Il est là, tout ce qui en lui était mortel. Paix à ses cendres.

M. Bloom marchait de côté, ignoré, passant anges attristés, croix, colonnes brisées, caveaux de familles, espoirs de pierre en prière, les yeux levés au ciel, les cœurs et les bras de la vieille Irlande. Plus raisonnable de dépenser l’argent sur une charité pour les vivants. Priez pour repos de l’âme de. Y en a-t-il qui vraiment ? On le plante là et on est quitte. Comme avec le déversoir à charbon. Et faites en un tas pour gagner du temps. La Toussaint. Le vingt-sept je serai devant sa tombe. Dix shillings pour le jardinier. Il la garde des mauvaises herbes. Vieil homme lui aussi. Plié en deux sur son sécateur, clac. Aux portes de la mort. Qui s’en est allé. Qui a quitté cette vie. Comme s’ils l’avaient fait de leur plein gré. Poussés vers la sortie, tous autant qu’ils sont. Qui a cassé sa pipe. Plus intéressant si on vous disait ce qu’ils étaient. Un tel, charron. Je voyageais pour les linos. J’en payais cinq shillings la livre. Ou une femme avec sa casserole. Je faisais un bon ragoût irlandais. Éloge dans un cimetière de campagne ça devrait être ce poème de qui déjà Wordsworth ou Thomas Campbell. Entré dans le repos comme disent les protestants. Du vieux Dr Murren. Le grand médecin l’a appelé à lui. Eh bien, c’est l’arpent qui leur est consacré. Plaisante maison secondaire. Crépie et repeinte à neuf. Endroit idéal pour en fumer une au vert en lisant *La Croix*. Les annonces de mariage ils n’essaient jamais de les enjoliver. Couronnes rouillées pendues aux pignons, fines guirlandes de bronze. Format plus économique. Pourtant, les fleurs sont plus poétiques. Les autres finissent par agacer, à ne jamais se faner. N’exprime rien. Immortelles.

Un oiseau peu farouche perchait sur une branche de peuplier. Comme empaillé. Comme le cadeau de mariage que nous avait fait le conseiller Hooper. Bou ! Bouge pas d’une plume. Il sait qu’il n’y a pas de lance-pierre qui le vise. Les animaux morts encore plus triste. Milly-la-bécasse qui enterrait le petit oiseau mort dans une boite d’allumettes de cuisine, une guirlande de pâquerettes et les ’tits bouts de queues défaits sur la tombe.

Le Sacré Cœur ça c’est : il le montre. Le cœur sur la main. Devrait être de côté et rouge devrait être peint comme un vrai cœur. L’Irlande lui était consacrée ou un truc du. A l’air tout sauf content. Pourquoi m’infliger cela ? Est-ce que les oiseaux viendraient le becqueter comme le garçon au panier de fruits mais il disait non car ils auraient du avoir peur du garçon. Apollon c’était.

Il y en a tant ! Tous ceux qui sont là ont un jour arpenté Dublin. Fidèles défunts. Comme vous nous étions.

Et puis comment pourrais-tu te souvenir de tous ? Les yeux, la démarche, la voix. La voix, soit : gramophone. Avoir un gramophone dans chaque tombe ou le garder à la maison. Après le diner du dimanche. Mets donc le pauvre arrière-grand-père. Craaharc ! Bonjourbonjourbonjour suissiheureux craarc siheureuxrevoir bonjourbonjour suissss krpsssst. Vous rappelle la voix comme la photographie le visage. Autrement on ne pourrait pas se souvenir d’un visage après, mettons, quinze ans. Qui, par exemple ? Par exemple un type qui est mort quand j’étais chez Wisdom Hely.

Rtststr ! les galets claquent. Attention. Arrête-toi !

Il plongea un regard soutenu dans une crypte de pierre. Un animal. Attends. Le voilà.

Un rat obèse et gris flânait le long de la crypte, remuant les galets. Un vieux soutier : arrière-grand-père : il connaît toutes les ficelles. Gris et vivant il s’aplatit sous la plinthe, se tortillant pour s’y introduire. Bonne cachette pour un trésor.

Qui vit ici ? Repose la dépouille de Robert Emery. Robert Emmet n’a-t-il pas été enterré ici, à la lueur des torches ? Faisant sa ronde.

Voilà la queue passée.

Un de ces gaillards vous bouloterait un gars en moins de deux. Laissent les os bien nets quel que soit le proprio. Viande qual. commune pour eux. Un cadavre c’est de la viande avariée. Bien et le fromage c’est quoi ? Le cadavre du lait. J’ai lu dans *Voyages en Chine* que les Chinois disent que l’homme blanc sent le cadavre. La crémation c’est mieux. Les prêtres à mort contre. C’est l’assaisonnement de l’autre enseigne. Brûleurs de gros et vendeurs de fours hollandais. Du temps de la peste. Fosse à chaux vive pour les consumer. Chambres à gaz. De la cendre à la cendre. Ou jeter à la mer. Où se trouve cette tour du silence parsi ? Mangé par les oiseaux. Terre, feu, eau. La noyade c’est dit-on le plus plaisant. Voir sa vie entière dans un flash. Mais qu’on vous ramène à la vie non. Pas moyen d’enterrer dans les airs par contre. Laisser tomber d’un engin volant. Me demande si la nouvelle fait le tour quand il y en a un tout frais qui descend. Communication souterraine. On a appris ça d’eux. Serait pas étonnant. Pitance journalière ordinaire pour eux. Les mouches arrivent avant qu’il soit bien mort. Ont eu vent pour Dignam. De l’odeur, elles n’en ont cure. Gruau grumeleux blanc de sel de cadavre : odeur, goût comme de navet blanc cru.

Les grilles luisaient en face : encore ouvert. Retour à la surface. Assez de cet endroit. Vous en rapproche un peu plus à chaque fois. Dernière fois que j’étais là c’était les funérailles de Mme Sinico. Pauvre papa aussi. L’amour qui tue. Et même qui racle la terre la nuit à la lanterne comme cette histoire que j’ai lue pour s’en prendre à des femelles fraîchement enterrées ou même putréfiées fissurées suintantes. Vous file la chair de poule à force. Je vous apparaîtrai après la mort. Vous verrez mon fantôme après ma mort. Mon fantôme viendra vous hanter après la mort. Il y a un autre monde après la mort et ça s’appelle l’enfer. Je n’aime pas cet autre monde, qu’elle a écrit. Et moi donc. Tant à voir à entendre à sentir encore. Sentir des êtres vivant et chaud près de soi. Laissons-les dormir dans leurs lits d’asticots. Ils ne m’auront pas ce coup-ci. Lits chauds : vie chaude et palpitante.

Martin Cunningham émergea d’un chemin de traverse, parlant avec gravité.

Avoué, je crois. Je connais sa tête. Menton, John Henry, avoué, commissaire aux serments et affidavits. Dignam a été dans son étude. Celle de Mat Dillon il y a bien longtemps. Le jovial Mat. Soirées festives. Volaille froide, cigares, les coupes de Tantale. Un cœur d’or, vraiment. Oui, Menton. Une rogne ce soir-là au boulingrin parce que j’en avais glissé une devant lui. Pur bol : la boule biaisée. Pourquoi il m’a tellement pris en grippe. Haine au premier regard. Molly et Floey Dillon enlacées sous les lilas, qui riaient. Un type c’est toujours comme ça, mortifié dès qu’il y a des femmes autour.

Chapeau cabossé sur le côté. La voiture probablement.

– Excusez-moi, monsieur, dit M. Bloom depuis le côté.

Ils s’arrêtèrent.

– Votre chapeau est un peu bosselé, dit M. Bloom en pointant du doigt.

John Henry Menton le fixa du regard un instant sans faire un geste.

– Là, montra Martin Cunningham, prévenant. John Henry Menton enleva son chapeau, repoussa le creux et en lissa le poil avec soin de la manche de sa veste. Il planta de nouveau le chapeau sur sa tête.

– C’est parfait maintenant, dit Martin Cunningham.

John Henry Menton s’acquitta d’un hochement de tête et d’un bref :

– Merci.

Ils s’acheminaient vers le portail. M. Bloom, échaudé, se laissa dépasser de quelques pas afin de ne pas être indiscret. Martin qui explique la loi. Martin pourrait retourner une cruche pareille avec son petit doigt sans même qu’il s’en doute.

Yeux d’huitre. Qu’importe. Sera peut-être confus s’il s’en rend compte un jour. Avoir l’ascendant sur lui comme ça.

Merci. Nous sommes bien magnanimes ce matin !











